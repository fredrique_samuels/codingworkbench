import {RequestContext, Request, Response, Session, View, IViewFactory} from "@codingworkbench/server/common";
import IUploadedFiles from "@codingworkbench/common/IUploadedFiles";
import IFileData from "@codingworkbench/common/IFileData";
import {EntrySet} from "@codingworkbench/common/IMapLike";
import {HtmlBuilder} from "@codingworkbench/server/HtmlBuilder";


export class MockUploadFiles implements IUploadedFiles {
    get(alias: string): IFileData[] {
        return [];
    }
}

export class MockSession implements Session {
    id: string = "";

    destroy(errFunc?: (err: any) => void): void {
    }

    get(name: string): any {
    }

    set(name: string, value: any): void {
    }
}

export class MockResponse implements Response {
    terminated: boolean = false;
    data: any;

    redirect(url: string): void {
        this.data = url;
    }

    sendJson(json: string): void {
        this.data = json;
    }

    sendJsonObj(obj: any, pretty?: boolean): void {
        this.data = obj;
    }

    sendStatus(statusCode: number, text: string): void {
        this.data = [statusCode, text];
    }

    sendText(text: string): void {
        this.data = text;
    }

    view(view: View): void {
        this.data = view;
    }

}

export class MockViewFactory implements IViewFactory {

    createHtmlBuilder(): HtmlBuilder {
        return new HtmlBuilder();
    }

    modelAndView(name: string, model?: EntrySet<any>): View {
        const html = JSON.stringify({
            name: name,
            model: model
        });
        return {
            html: html
        }
    }

}

export function mock_request(req: Partial<Request>): Request {
    return {
        url: "",
        method: "get",
        body: {},
        files: new MockUploadFiles(),
        headers: {},
        params: {},
        sessionID: "",
        session: new MockSession(),
        ...req
    };
}

export function mock_request_context<C>(c: C, req: Partial<Request>): RequestContext<C> {
    return {
        context: c,
        nextFunction: reqContext => {},
        request: mock_request(req),
        response: new MockResponse(),
    }
}
