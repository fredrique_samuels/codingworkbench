const fs = require("fs");
const path = require("path");

const webpack = require('webpack');

/* Used to copy files after building webpack targets */
const CopyPlugin = require('copy-webpack-plugin');

/* used to resolve tsconfig.paths when building*/
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');


function create_config(output) {
    return {
        entry: {},
        target: 'node',
        output: { path: output, filename: '[name].js', },

        // DO NO ENABLE devtool source-map. This will require you to install @babel/core
        // It is not an issue just another dependency we can do without.
        // devtool: 'source-map',

        resolve: {
            extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
            modules: ['./node_modules/' ]
        },

        optimization:{ minimize: false },

        module: {
            rules : [
                // {
                //     // this is so that we can compile any React,
                //     // ES6 and above into normal ES5 syntax
                //     test: /\.(js|jsx)$/,
                //     // we do not want anything from node_modules to be compiled
                //     exclude: /node_modules/,
                //     use: [{
                //         loader: 'babel-loader',
                //         options: {
                //             // presets: ['es2015', 'react'],
                //             // plugins: ['react-html-attrs', 'transform-decorators-legacy2', 'transform-class-properties', 'transform-object-rest-spread']
                //         },
                //     }]
                // },
                // { enforce: "pre", test: /\.(js|jsx)$/, loader: "source-map-loader" },
            ],
        }
    };
}

function init_config_entry(config) {
    if(!config.entry) {
        config.entry = {}
    }
}

function init_config_externals(config) {
    if(!config.externals) {
        config.externals = {}
    }
}

function init_config_plugins(config) {
    if(!config.plugins) {
        config.plugins = []
    }
}

function watch_source(config) {
    config.watch= true;
}

function minimize(config) {
    if(!config.optimization) {
        config.optimization = {}
    }
    config.optimization.minimize = true;
}

function add_loader(config, regex, loader, options) {
    config.module.rules.push(
        {
            test: regex,
            loader: loader,
            options: options
        }
    );
}

function ts_loader(config) {
    add_loader(config, /\.(ts|tsx)$/, 'ts-loader', {allowTsInNodeModules: true });
}


function raw_loader(config, regex) {
    add_loader(config, regex, 'raw-loader');
}

function tsconfig_paths(config, tsconfig) {
    if(!config.resolve.plugins) {
        config.resolve.plugins = []
    }
    config.resolve.plugins.push(new TsconfigPathsPlugin({ configFile: tsconfig }));
}

/**
 *  Create a dictionary of {name: paths} for an entire directory.
 *
 *  Example:
 *      root/
 *          entry0.tsx
 *
 *  calling push_entries(config, "root", "tsx") will result in {entry0: "root/entry0.tsx"}.
 */
function push_entries_in_path(config, rootDir, ext, nameFilter=[]) {
    init_config_entry(config);

    let entries = {};

    function transformToFullPath(f) { return { name: f, fullpath: path.resolve(rootDir, f) } }
    function isTsxFile(fd) { return fs.lstatSync(fd.fullpath).isFile() && fd.name.endsWith(ext) }
    function isNamed(fd) { return nameFilter.length === 0 || nameFilter.includes(fd.name) }
    function addToEntries(fd) { entries[fd.name.replace(ext, "")] = fd.fullpath }

    fs.readdirSync(path.resolve(rootDir))
        .map(transformToFullPath)
        .filter(isTsxFile)
        .filter(isNamed)
        .forEach(addToEntries);

    config.entry = Object.assign({}, config.entry, entries);
}

/**
 *  Create a dictionary of {name: paths} for an entire directory.
 *
 *  Example:
 *      root/
 *          entry0.tsx
 *
 *  calling push_entries(config, "root", "tsx") will result in {entry0: "root/entry0.tsx"}.
 */
function push_entries(config, rootDirs, ext) {
    let roots = Array.isArray(rootDirs) ? rootDirs : [rootDirs];
    roots.forEach(
        function(r) {
            push_entries_in_path(config, r, ext);
        }
    )
}

function push_entry(config, fullpath, ext) {
    init_config_entry(config);

    let entries = {};

    function isTsxFile(fd) { return fs.lstatSync(fd.fullpath).isFile() && fd.name.endsWith(ext); }
    function addToEntries(fd) { entries[fd.name.replace(ext, "")] = fd.fullpath; }

    [path.resolve(fullpath)]
        .filter(isTsxFile)
        .forEach(addToEntries);

    config.entry = Object.assign({}, config.entry, entries);
}


/**
 * Exclude reaction from webpack builds.
 *
 * React should be added to the final html as JS resources.
 */
function exclude_react(config) {
    init_config_externals(config);

    config.externals = Object.assign(
        {},
        config.externals,
        {
            "react": "React",
            "react-dom": "ReactDOM",
        }
    );
}

function copy_react_js(config, dest) {
    init_config_plugins(config);
    config.plugins.push(
        new CopyPlugin([
            { from: 'node_modules/react/umd/react.development.js', to: path.resolve(dest,'react.development.js') },
            { from: 'node_modules/react/umd/react.production.min.js', to: path.resolve(dest, 'react.production.min.js') },
            { from: 'node_modules/react-dom/umd/react-dom.development.js', to: path.resolve(dest, 'react-dom.development.js') },
            { from: 'node_modules/react-dom/umd/react-dom.production.min.js', to: path.resolve(dest, 'react-dom.production.min.js') },
        ])
    )
}

/**
 * Get rid of ` Module not found: Error: Can't resolve 'pg-native' ` error
 */
function ignore_pg_native(config) {
    init_config_plugins(config);
    config.plugins.push(new webpack.IgnorePlugin(/^pg-native$/));
}

module.exports = {
    create_config: create_config,
    push_entries: push_entries,
    exclude_react: exclude_react,
    copy_react_js: copy_react_js,
    ignore_pg_native: ignore_pg_native,
    watch_source: watch_source,
    minimize: minimize,
    ts_loader: ts_loader,
    tsconfig_paths: tsconfig_paths,
    raw_loader: raw_loader,
    init_config_plugins:init_config_plugins
};
