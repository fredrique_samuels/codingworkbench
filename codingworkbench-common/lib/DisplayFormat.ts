import * as moment from "moment";

namespace DisplayFormat {

    export const KILO_BYTE: number = 1024;
    export const MEGA_BYTE: number = 1024 * KILO_BYTE;
    export const GIGA_BYTE: number = 1024 * MEGA_BYTE;
    export const TERA_BYTE: number = 1024 * GIGA_BYTE;

    /**
     * Create a string from a byte count. The string will be in format `0.0 Bytes`.
     *
     * @param bytes the number of bytes.
     * @param decimals The number of decimal places to display. Defaults to 2.
     */
    export function sizeToBytes(bytes: number, decimals: number = 2): string {

        if (bytes === 0) return '0 Bytes';

        const dm = decimals < 0 ? 0 : decimals;
        const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];

        const i = Math.floor(Math.log(bytes) / Math.log(KILO_BYTE));
        return parseFloat((bytes / Math.pow(KILO_BYTE, i)).toFixed(dm)) + ' ' + sizes[i];
    }

    /**
     * Format a Date object to a 'month day year' string. where month will be displayed as the name.
     *
     * @param _d the date to display.
     * @param lang The language to display in. Defaults to English(en).
     */
    export function dateToMDY(_d: Date, lang: string = 'en'): string {
        let date = new Date(_d);
        moment.locale(lang);

        let day = date.getDate();
        let month = date.getMonth();
        let year = date.getFullYear();
        return `${moment.months()[month]} ${day}, ${year}`
    }

    export function dateToMDYT(_d: Date, lang: string = 'en'): string {
        let date = new Date(_d);
        moment.locale(lang);

        let day = date.getDate();
        let month = date.getMonth();
        let year = date.getFullYear();
        let hours = date.getHours();
        let minutes = date.getMinutes();
        return `${moment.months()[month]} ${day}, ${year} ${hours}:${minutes}`
    }

}

export default DisplayFormat;
