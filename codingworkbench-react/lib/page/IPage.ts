/**
 * Expose abstract notification and OpenClose functions to page components.
 */
import {DefaultPage} from "./DefaultPage";
import {IHasState} from "../state/IHasState";
import {PageAction} from "./PageAction";

export interface IPage<S={}> extends IHasState<{}, S> {

    open<K extends keyof S>(target: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, K>): void;
    close<K extends keyof S>(target: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, K>): void;
    openHandler<K extends keyof S>(target: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, K>): () => void;
    closeHandler<K extends keyof S>(target: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, K>): () => void;

    pushErrorNotification(message: string): void;
    pushWarningNotification(message: string): void;
    pushInfoNotification(message: string): void;
    pushSuccessNotification(message: string): void;

    startLoading(title: string): void;
    stopLoading(): void;
    webAction(url: string): PageAction;
    logError(e: Error): void;
}


export function safe_page<ContextType>(page?: IPage<ContextType>) {
    if(page) {
        return page;
    }
    return new DefaultPage();
}

export default IPage;
