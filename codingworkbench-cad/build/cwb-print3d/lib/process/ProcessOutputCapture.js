"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProcessOutputCapture = (function () {
    function ProcessOutputCapture() {
    }
    ProcessOutputCapture.prototype.capture = function (error, stdout, stderr) {
        this._error = error;
        this._stdout = stdout;
        this._stderr = stderr;
    };
    Object.defineProperty(ProcessOutputCapture.prototype, "errorMessage", {
        get: function () { return this._error ? this._error.message : ""; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProcessOutputCapture.prototype, "error", {
        get: function () { return this._error; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProcessOutputCapture.prototype, "stdout", {
        get: function () { return this._stdout; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProcessOutputCapture.prototype, "stderr", {
        get: function () { return this._stderr; },
        enumerable: true,
        configurable: true
    });
    return ProcessOutputCapture;
}());
exports.default = ProcessOutputCapture;
