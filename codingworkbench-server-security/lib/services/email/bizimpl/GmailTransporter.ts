import * as nodemailer from "nodemailer";

import IEmailTransporter from "../api/IEmailTransporter";
import {ICredentials} from "@codingworkbench/common/ICredentials";
import CwbLog from "@codingworkbench/log";


export default class GmailTransporter implements IEmailTransporter {

    public transport(credentials: ICredentials, mail: any): Promise<void> {

        // https://myaccount.google.com/lesssecureapps?pli=1 and turn on access for less secure apps
        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: credentials.user,
                pass: credentials.secret
            }
        });

        return new Promise<void>(
            (resolve, reject) => {
                transporter.sendMail(mail, function(error, info) {
                    if (error) {
                        CwbLog.error(error);
                    }
                    resolve();
                });
            }
        );
    }

}
