import CwbI8N from "@codingworkbench/i8n";


export interface IMessagesContext {
    messageSource: CwbI8N.IMessageSource
}
