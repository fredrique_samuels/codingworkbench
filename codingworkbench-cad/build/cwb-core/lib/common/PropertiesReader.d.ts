interface PropertiesReader {
    getString(property: string, fallback?: string): string;
    getNumber(property: string, fallback?: number): number;
}
declare namespace PropertiesReader {
    function readFile(path: string): PropertiesReader;
}
export default PropertiesReader;
