import IFileData from "./IFileData";

export interface IUploadedFiles {
    get(alias: string): IFileData[];
}

export default IUploadedFiles;
