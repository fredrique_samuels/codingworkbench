
export type SortOrder = 'DESC' | 'ASC';
export type Operation = 'eq' | 'lt' | 'lte' | 'gt' | 'gte' | 'in' | 'like' | 'ne' | 'nin' ;
export type SearchFilterDict = { [s: string]: SearchFilter };
export const INF_PAGE_SIZE: number = Number.MAX_SAFE_INTEGER;

/**
 * Structure indicating filter parameters.
 */
export type SearchFilter = {
    value: any;
    operation?: Operation;
}

/**
 * Structure containing data needed to
 * do paginated searches.
 */
export type SearchParams = {
    pagination: IPagination,
    filters: SearchFilterDict
}

/**
 * Strcuture containing paging information
 */
export type IPagination = {
    pageSize: number;
    offset: number;
    totalRecords: number;
    sortField: string | null;
    sortOrder: SortOrder;
}

/**
 * Structure containing search results data.
 */
export interface SearchResults<T> {
    results: T[];
    next?: SearchParams;
    refresh?: SearchParams;
    prev?: SearchParams;
    pages?: number;
    currentPage?: number;
}

export function mapResults<T, R>(searchResults: SearchResults<T>, transformer: (input: T) => R): SearchResults<R> {
    return {
        ...searchResults,
        results: searchResults.results.map(transformer)
    }
}

export function search_results_from_array<T>(array: T[]): SearchResults<T> {
    return {
        results: [...array],
        pages: 1,
        currentPage: 1,
    };
}

export interface ISearchFiltersBuilder {
    addFilter(name: string, value: any, operation?: Operation): this;
    addAll(filters: SearchFilterDict): this;
}

export interface ISearchPaginationBuilder {
    setPageSize(o: number): this;
    setOffset(o: number): this;
    setSortField(o: string): this;
    setSortOrder(o: SortOrder): this;
}

// TODO build from an existing SearchParams
export class SearchBuilder {
    private readonly _filters: SearchFiltersBuilder;
    private readonly _pagination: PaginationParamsBuilder;

    constructor(searchParams?: SearchParams) {
        this._filters = new SearchFiltersBuilder(searchParams ? searchParams.filters : {});
        this._pagination = new PaginationParamsBuilder(searchParams ? searchParams.pagination : undefined);
    }

    get filters(): ISearchFiltersBuilder {
        return this._filters;
    }

    get pagination(): ISearchPaginationBuilder {
        return this._pagination;
    }

    public build(): SearchParams {
        return {
            filters: this._filters.build(),
            pagination: this._pagination.build(),
        }
    }
}

class SearchFiltersBuilder {

    private filters: SearchFilterDict;

    public constructor(f?: SearchFilterDict) {
        this.filters = f ? this.copy(f) : {};
    }

    public addFilter(name: string, value: any, operation?: Operation): this {
        this.filters[name] = {
            value,
            operation: operation || 'eq',
        };
        return this;
    }

    public addAll(filters: SearchFilterDict): this {
        this.filters = {
            ...this.filters,
            ...filters
        };
        return this;
    }

    public build(): SearchFilterDict {
        return this.copy(this.filters);
    }

    private copy(d: SearchFilterDict): SearchFilterDict {
        const r: SearchFilterDict = {};
        Object.keys(d).forEach(k => r[k] = {...d[k]});
        return r;
    }
}

class PaginationParamsBuilder {
    private readonly params: IPagination;

    constructor(p?: IPagination) {
        this.params = p ? {...p} : PaginationParamsBuilder.init();
    }

    public setPageSize(o: number): this {
        this.params.pageSize = o;
        return this;
    }

    public setOffset(o: number): this {
        this.params.offset = o;
        return this;
    }

    public setSortField(o: string): this {
        this.params.sortField = o;
        return this;
    }

    public setSortOrder(o: SortOrder): this {
        this.params.sortOrder = o;
        return this;
    }

    public build(): IPagination {
        return {...this.params};
    }

    private static init(): IPagination {
        return {
            pageSize: 25,
            offset: 0,
            totalRecords: 0,
            sortField: null,
            sortOrder: 'ASC',
        };
    }
}

export class SearchResultBuilder<T> {
    _results: SearchResults<T>;

    constructor(results: T[]) {
        this._results = {
            results: [...results],
            currentPage: 1,
            pages: 1
        };
    }

    public build(): SearchResults<T> {
        return {...this._results};
    }
}

export type PageDataSupplier<T> = (search: SearchParams) => Promise<T[]>
export type PageTotalSupplier = (search: SearchParams) => Promise<number>

export interface PaginatedDataFunctions<T> {
    readonly dataSupplier: PageDataSupplier<T>;
    readonly totalSupplier: PageTotalSupplier;
}

function fix_search_object(userSearch: SearchParams): SearchParams {
    // make sure there are not undefined fields
    const pageParams = new PaginationParamsBuilder(userSearch.pagination).build();
    const filters = new SearchFiltersBuilder(userSearch.filters).build();
    const pageSize = pageParams.pageSize <= 0 ? 25 : pageParams.pageSize;
    const invalidOffset = pageParams.offset > 0 && pageParams.offset % pageSize !== 0;

    // make sure offset is modulus 0 off page size
    return {
        ...userSearch,
        pagination: {
            ...pageParams,
            offset: invalidOffset ? 0 : pageParams.offset,
            pageSize: pageSize,
        },
        filters: {
            ...filters
        }
    };
}

function create_single_page_results<T>(results: T[], search: SearchParams): SearchResults<T> {
    return {
        results: [...results],
        refresh: {
            ...search,
            pagination: {
                ...(search.pagination as IPagination),
                totalRecords: results.length,
            }
        },
        currentPage: 1,
        pages: 1
    };
}

function is_single_page_results<T>(results: T[], pageParams: IPagination) {
    return results.length < pageParams.pageSize && pageParams.offset === 0;
}

async function create_paged_results<T>(dataFunctions: PaginatedDataFunctions<T>, search: SearchParams, results: T[]) {
    const pagination = search.pagination as IPagination;

    const inf_page_size = (pagination).pageSize === INF_PAGE_SIZE;

    const total: number = await dataFunctions.totalSupplier(search);
    const res = inf_page_size ? 0 : total % (pagination).pageSize;
    const totalPages = inf_page_size ? 1 : ( (total - res) / (pagination).pageSize ) + (res > 0 ? 1 : 0);
    const currentPage = inf_page_size ? 1 : (pagination).offset == 0 ? 1 : ((pagination).offset / (pagination).pageSize) + 1;

    const create_prev_search = (): SearchParams | undefined => {
        return currentPage > 1
            ? {
                ...search,
                pagination: {
                    ...pagination,
                    offset: pagination.offset - pagination.pageSize,
                    totalRecords: total,
                }
            }
            : undefined;
    };

    const create_next_search = (): SearchParams | undefined => {
        return currentPage < totalPages
            ? {
                ...search,
                pagination: {
                    ...pagination,
                    offset: pagination.offset + pagination.pageSize,
                    totalRecords: total,
                }
            }
            : undefined;
    };

    return {
        results: [...results],
        prev: create_prev_search(),
        refresh: {
            ...search,
            pagination: {
                ...pagination,
                totalRecords: total,
            }
        },
        next: create_next_search(),
        currentPage: currentPage,
        pages: totalPages
    };
}

export async function do_paged_search<T>(userSearch: SearchParams, dataFunctions: PaginatedDataFunctions<T>): Promise<SearchResults<T>> {
    const search: SearchParams = fix_search_object(userSearch);
    const results: T[] = await dataFunctions.dataSupplier(search);

    if (is_single_page_results(results, search.pagination as IPagination)) {
        return create_single_page_results(results, search);
    }
    return await create_paged_results(dataFunctions, search, results);
}

