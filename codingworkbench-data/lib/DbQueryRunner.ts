

/**
 * Interface describing a executable query.
 */
export interface DbQueryRunner {

    /**
     * Run the query and return the resulting rows.
     *
     * @returns The row values.
     */
    query(): Promise<any[]>
}

export default DbQueryRunner;
