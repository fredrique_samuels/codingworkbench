import * as express from "express";
import * as http from "http";
import * as fs from "fs";
import {AddressInfo} from "net";

import CwbLog from "@codingworkbench/log";

import {
    bind_handlers_to_app,
    bind_user_defined_middleware,
    configure_redis,
    ExpressApplicationMiddleware,
    ExpressApplicationOptions, IExpressApplication,
    setup_express
} from "./ExpressApplicationUtils";
import IRequestHandler from "./IRequestHandler";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
import PropertiesFileReader from "@codingworkbench/common/PropertiesFileReader";


export interface ExpressContextFactory<C> {
    createContext(options: ExpressApplicationOptions, connectionProperties: IPropertiesReader | null): Promise<C>;
    configureApplication(context: C, app: IExpressApplication<C>): Promise<void>;
}

export class ExpressApplication<ContextType={}> implements IExpressApplication<ContextType> {

    private handlers: IRequestHandler<ContextType>[];
    private middlewares: ExpressApplicationMiddleware[];
    private readonly _app: express.Express;

    constructor() {
        this._app = express();
        this.handlers = [];
        this.middlewares = []
    }

    public getApp(): express.Express {
        return this._app;
    }

    /**
     * Add a middleware function to the application.
     *
     * @param mw
     */
    public addMiddleware(mw: ExpressApplicationMiddleware): this {
        this.middlewares = [...this.middlewares, mw];
        return this;
    }


    /**
     * Add request handler to the app.
     *
     * @param handler The request handlers
     * @see RequestPath
     */
    public addHandlers(handlers: IRequestHandler<ContextType>[]): this {
        this.handlers = [...this.handlers, ...handlers];
        return this;
    }

    /**
     * A convenience method that invokes
     *      app.use(<path>, express.static( path.resolve( <resolve> ) ) );
     *
     * @param path The url path to access static files.
     * @param resolve The disk location where the files are located.
     */
    public serveStatic(path: string, resolve: string): this {
        this._app.use(path, express.static( resolve ) );
        return this;
    }

    /**
     * Start the express server
     */
    async start(options: ExpressApplicationOptions, contextFactory: ExpressContextFactory<ContextType>): Promise<void> {
        const app = this.getApp();

        const connectionProperties = this.loadConnectionProperties(options);

        setup_express(options, app);
        configure_redis(connectionProperties, options, app);
        bind_user_defined_middleware(this.middlewares, this._app);

        const applicationContext = await contextFactory.createContext(options, connectionProperties);
        bind_handlers_to_app<ContextType>(this._app, this.handlers, applicationContext);

        await contextFactory.configureApplication(applicationContext, this);

        const server: http.Server = app.listen(options.port, async () => {
            const address = server.address() as AddressInfo;
            CwbLog.info(`Server is running on port ${address ? address.port : options.port}`);
            if (options.onStartFunc) {
                await options.onStartFunc(applicationContext, this);
            }
        });
    }

    private loadConnectionProperties(options: ExpressApplicationOptions): IPropertiesReader | null {
        const connectionPropsFile = options.connectionPropertiesFile || "./conf/connection.properties";
        const confFileExists = fs.existsSync(connectionPropsFile);
        if (!confFileExists) {
            CwbLog.info(`Connection properties file "${connectionPropsFile} does not exists!"`);
        }
        return confFileExists ? new PropertiesFileReader(connectionPropsFile) : null;
    }
}

export default ExpressApplication;
