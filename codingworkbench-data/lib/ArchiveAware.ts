import {DbColumn} from "./DbColumn";
import {IArchiveAware} from "./IArchiveAware";

export class ArchiveAware {

    private o: IArchiveAware;

    constructor(o?: IArchiveAware) {
        this.o = {
            ...(o || {archived: false})
        }
    }

    public flatten(): IArchiveAware {
        return { ...this.o };
    }

    public update(o: IArchiveAware): this {
        this.o = { archived: o.archived };
        return this;
    }

    public archive(): this {
        this.o = { archived: true };
        return this;
    }

    public unArchive(): this {
        this.o = { archived: false };
        return this;
    }
}

export function archive_columns(): DbColumn[] {
    return [
        new DbColumn("archived"),
    ];
}

export default ArchiveAware;

