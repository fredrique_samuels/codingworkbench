import IMapLike, {EntrySet} from "@codingworkbench/common/IMapLike";
import Dict from "@codingworkbench/common/Dict";


export const HTML_DATA_MODEL_ID = "__CwbPageDataModel";

export type HtmlModel = EntrySet<any>;

export type ModelSupplierFunc = () => HtmlModel;

export function read_html_model() {
    const properties: HtmlModel = eval(`${HTML_DATA_MODEL_ID}`) as HtmlModel;
    return properties || {};
}

export interface ModelSupplier {
    getModel(): HtmlModel;
}

export class ModelBuilder {

    private _internal: Dict<any>;

    constructor(other?: IMapLike<any>) {
        this._internal = new Dict<any>(other);
    }

    public put(key: string, value: any): this {
        this._internal.put(key, value);
        return this;
    }

    public build(): HtmlModel {
        return this._internal.entries;
    }
}



