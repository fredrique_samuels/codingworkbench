import ArchiveAware from "./ArchiveAware";

export class DbColumn {
    private readonly _field: string;
    private readonly _column: string;
    private _primary: boolean = false;

    /**
     * Create a new mapping.
     *
     * @param field The JSObject field name.
     * @param column optional The name of the table column. If not provided then it
     *                          is assumed to be the same as the field name.
     */
    constructor(field: string, column?: string) {
        this._field = field;
        this._column = column ? column : field;
    }

    /**
     * @readonly
     * @returns the JS Object field name.
     */
    get field(): string {
        return this._field;
    }

    /**
     * @readonly
     * @returns The column name.
     */
    get column(): string {
        return this._column;
    }

    /**
     * True if this column is a primary key.
     *
     * @see setPrimary
     * @readonly
     */
    get primary(): boolean {
        return this._primary;
    }

    /**
     * Indicate that the column is a primary key.
     * @returns this.
     */
    setPrimary(): this {
        this._primary = true;
        return this;
    }
}

export default DbColumn;
