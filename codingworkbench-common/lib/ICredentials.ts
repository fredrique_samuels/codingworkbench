
export interface ICredentials {
    user: string;
    secret: string;
}
