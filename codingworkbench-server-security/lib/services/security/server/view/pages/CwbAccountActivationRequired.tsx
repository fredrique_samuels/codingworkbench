import * as React from "react"
import * as ReactDOM from "react-dom";

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {read_html_model} from "@codingworkbench/server/HtmlModel";
import Copyright from "../components/Copyright";
import SecurityViewController from "../SecurityViewController";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

interface IAccountNotActivatedState {
    linkResent: boolean;
    loading: boolean;
    email: string;
}

export default function CwbAccountActivationRequired() {
    const classes = useStyles();

    const [state, setState] = React.useState<IAccountNotActivatedState>({
        linkResent: false,
        loading: false,
        email: read_html_model().email
    });

    if (!state.email) {
        SecurityViewController.goto_signin();
    }

    const handleResendLink = () => {
        setState({...state,
            loading: true
        });

        SecurityViewController.do_resend_activation_link(state.email)
            .then(() => {
                setState({
                    ...state,
                    linkResent: true,
                    loading: false
                });
            })
            .catch(console.error)
    };

    let buttonLabel: string = "Resend Link";
    if (state.loading) {
        buttonLabel = "Resending Link";
    }
    if (state.linkResent) {
        buttonLabel = "Link Sent";
    }

    return (
        <Container component="main" maxWidth="sm">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h4">
                    We require email validation to activate your account.
                </Typography>
                <br />
                <Typography component="h1" variant="h5">
                    Please follow the link sent to your emails to activate your account.
                </Typography>
                <form className={classes.form} noValidate>
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleResendLink}
                        disabled={state.linkResent || state.loading}
                    >
                        {buttonLabel}
                    </Button>
                </form>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
}

ReactDOM.render(<CwbAccountActivationRequired />, document.getElementById("root"));
