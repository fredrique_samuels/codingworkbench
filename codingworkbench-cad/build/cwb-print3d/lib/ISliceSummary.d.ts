export default interface ISliceSummary {
    readonly gcode: string;
    readonly minx?: number;
    readonly miny?: number;
    readonly minz?: number;
    readonly maxx?: number;
    readonly maxy?: number;
    readonly maxz?: number;
    readonly dimx?: number;
    readonly dimy?: number;
    readonly dimz?: number;
    readonly centerx?: number;
    readonly centery?: number;
    readonly centerz?: number;
    readonly timeInSeconds?: number;
    readonly timeFormatted?: number;
    readonly filamentUsedInMeters?: number;
}
