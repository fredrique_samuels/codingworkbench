import * as React from "react";
import CheckCircleIcon from '@material-ui/icons/CheckCircle';
import clsx from 'clsx';
import ErrorIcon from '@material-ui/icons/Error';
import InfoIcon from '@material-ui/icons/Info';
import WarningIcon from '@material-ui/icons/Warning';

import styles from "./styles"

const typeIcon = {
    success: CheckCircleIcon,
    warning: WarningIcon,
    error: ErrorIcon,
    info: InfoIcon,
};

/**
 * A message with a colored background based on the message type.
 *
 * @constructor
 */
function Message(props: Message.Props) {
    const classes = styles();
    const { message, type } = props;
    const Icon = typeIcon[type];

    return message
        ?   (
            <span id="client-snackbar" className={classes.message}>
                    <Icon className={clsx(classes.icon, classes.iconVariant)} />
                {message}
                </span>
        )
        : null;
}

/**
 * Information used for message displays.
 */
namespace Message {
    /**
     * Message types.
     */
    export type Type = 'success'  | 'warning' | 'error' | 'info';

    /**
     * useful function to get default variant value 'info' if no variant is specified.
     * @param variant
     */
    export function fromOptional(variant?: Type): Type {
        return variant
            ? variant
            : 'info';
    }

    export interface Props {

        /**
         * The notification message.
         */
        message?: string;

        /**
         * The notification type.
         */
        type: Type;
    }
}

export default Message;