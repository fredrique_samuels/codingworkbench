

export default interface IOption {
    key: string;
    label: string;
    value: any;
    type?: string;
    help?: any;
    category?: string;
}
