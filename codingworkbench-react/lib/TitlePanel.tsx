import * as React from "react"
import {createStyles, makeStyles, Theme} from "@material-ui/core";
import Typography from "@material-ui/core/Typography";

const styles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            marginLeft: theme.spacing(3),
        }
    }),
);

function TitlePanel(state: TitlePanelProps) {
    const classes = styles();
    const {title, subtitle} = state;

    if(!title) {
        return null
    }

    let stC: React.ReactNode = null;
    if(subtitle) {
        stC = (
            <Typography key={"subtitle"} variant="body2" color="textSecondary">
                {subtitle}
            </Typography>
        )
    }

    return (
        <div className={classes.root} >
            <Typography key={"title"} variant="h4">
                {title}
            </Typography>
            { stC }
        </div>
    )
}

export interface TitlePanelProps {
    title: string;
    subtitle: string;
}

export default TitlePanel;
