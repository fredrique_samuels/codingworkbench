/**
 * Interface for any object that needs audit data.
 */
import {DbQuery} from "./DbQuery";
import {DbQueryFactory} from "./DbQueryFactory";
import {DbQueryRunner} from "./DbQueryRunner";
import {DbUpdateRunner} from "./DbUpdateRunner";

const DB_CONNECTION_SETTINGS = {debug: false};

export function db_connection_debug_mode(d: boolean) {
    DB_CONNECTION_SETTINGS.debug = d;
}

export function is_db_connection_debug_mode(): boolean {
    return DB_CONNECTION_SETTINGS.debug;
}



/**
 * A public interface to describe interaction with a database.
 */
export interface DbConnection {

    /**
     * Check if the connection is live.
     */
    readonly connected: boolean;

    /**
     * Query facroty for this type of connection.
     */
    getQueryFactory(): DbQueryFactory;

    /**
     * Attempt to connect to the database.
     */
    connect(): Promise<void>;

    /**
     * Close any connection to the database.
     */
    close(): Promise<void>;

    /**
     * Create an executable query usign the parameters provided.
     * 
     * @param q The query parameters.
     */
    createQuery<T>(q: DbQuery): Promise<DbQueryRunner>;

    /**
     * Create an executable query usign the parameters provided.
     * 
     * @param q The query parameters.
     */
    createUpdate(q: DbQuery): Promise<DbUpdateRunner>;

    /**
     * Execute the QueryData and return the results.
     *
     * @param q The query parameters.
     */
    execute(q: DbQuery): Promise<any[]>;

}

export default DbConnection;
