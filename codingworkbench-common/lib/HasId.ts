/**
 * Gives the object id context.
 *
 * @template IdType The id data type as runtime.
 */
export default interface HasId<IdType> {

    /**
     * The id of this object.
     */
    readonly id: IdType;
}