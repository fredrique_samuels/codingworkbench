import {deepStrictEqual, ok} from 'assert'
import Unique, {unique_columns} from "../../lib/Unique";


describe('IUniqueObject.ts', function() {

    it("#create()", () => {
        deepStrictEqual( {id: null},  new Unique().flatten());
    });


    it("#columns()", () => {
        /* given */
        let columns = unique_columns();

        /* then */
        deepStrictEqual(columns.length, 1);

        const [id] = columns;
        deepStrictEqual(id.field, "id");
        deepStrictEqual(id.column, "id");
        ok(id.primary);
    });

});
