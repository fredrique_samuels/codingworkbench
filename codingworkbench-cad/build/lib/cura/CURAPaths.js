"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PropertiesFileReader_1 = require("@codingworkbench/common/PropertiesFileReader");
var os = require("os");
var CURAPaths = (function () {
    function CURAPaths() {
    }
    CURAPaths.fromProperties = function (properties) {
        var platform = os.platform();
        return {
            home: properties.getString("slicer." + platform + ".cura.home"),
            definitions: properties.getString("slicer." + platform + ".cura.definitions"),
            engine: properties.getString("slicer." + platform + ".cura.engine")
        };
    };
    CURAPaths.fromPropertiesFile = function (path) {
        return CURAPaths.fromProperties(new PropertiesFileReader_1.default(path));
    };
    return CURAPaths;
}());
exports.CURAPaths = CURAPaths;
