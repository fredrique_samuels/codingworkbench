import CwbSearch from "@codingworkbench/search";

import {DbQuery} from "./DbQuery";
import {DbTable} from "./DbTable";
import {ID, IUnique} from "./IUnique";


export interface DataAccessor {
    /**
     * Execute the QueryData and return the results.
     *
     * @param q The query parameters.
     */
    execute(q: DbQuery): Promise<any[]>;

    /**
     * Save or update the given resource.
     *
     * @param table The target table.
     * @param obj The target object.
     * @param userId optional user id of the current session.
     */
    saveObject<T extends IUnique>(table: DbTable, obj: T, userId?: ID): Promise<ID>;

    /**
     * Get an SavedObject by its id.
     * 
     * @param table The target table.
     * @param id The object id.
     * @throws SystemError if the object could not be found.
     */
    getObject<T extends IUnique>(table: DbTable, id: ID): Promise<T>;

    /**
     *  Delete or archive a given record.
     * 
     * @param table The target table.
     * @param id The object id.
     */
    deleteObject(table: DbTable, id: ID): Promise<void>;

    /**
     * Search the table for all records matching the given search params.
     * 
     * @param table The target table.
     * @param searchParams Search parameters.
     * @param enhanceSearch | optional Callback used to update the search parameters if needed.
     */
    searchObjects<T extends IUnique>(table: DbTable,search: CwbSearch.Search)
        : Promise<T[]>


    searchObjectsPaginated <T extends IUnique> (table: DbTable, search: CwbSearch.Search)
        : Promise<CwbSearch.IResults<T>>;
}

export default DataAccessor;

