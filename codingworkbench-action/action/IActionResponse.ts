
/**
 * Basic action response object.
 */
export interface IActionResponse<T = any> {

    /**
     * The action type.
     */
    data?: T;

    /**
     * Flag fo indicate if there was an error.
     */
    error?: boolean;

    /**
     * An optional message.
     */
    message?:  string;

    /**
     * Optional redirect url
     */
    redirect?: string;
}

