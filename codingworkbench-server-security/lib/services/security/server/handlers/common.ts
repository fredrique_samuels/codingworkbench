import {ISecurityLinks} from "../../api/links";

export function create_activation_required_link(securityLinks: ISecurityLinks, email: string) {
    return securityLinks.accountActivationRequiredPage.replace(":email", encodeURIComponent(email));
}

