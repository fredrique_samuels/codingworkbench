"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function MetaDataParserFunc_Number(value) {
    return parseFloat(value.trim());
}
function MetaDataParserFunc_String(value) {
    return value.trim();
}
function MetaDataParserFunc_Meter(value) {
    return parseFloat(/(\d+?\.\d+)m/.exec(value)[1]);
}
var MetaDataParser = (function () {
    function MetaDataParser(prefix, parserFunc) {
        this._prefix = prefix;
        this._parserFunc = parserFunc;
    }
    Object.defineProperty(MetaDataParser.prototype, "prefix", {
        get: function () { return this._prefix; },
        enumerable: true,
        configurable: true
    });
    MetaDataParser.prototype.parse = function (line) {
        return this._parserFunc(line.replace(this._prefix, ""));
    };
    return MetaDataParser;
}());
exports.MetaDataParser = MetaDataParser;
exports.CURA_TIME_PARSER = new MetaDataParser(";TIME:", MetaDataParserFunc_Number);
exports.CURA_TIME_FORMATTED_PARSER = new MetaDataParser("Print time (hr|min|s):", MetaDataParserFunc_String);
exports.CURA_FILAMENT_USED_PARSER = new MetaDataParser(";Filament used:", MetaDataParserFunc_Meter);
exports.CURA_MIN_X_PARSER = new MetaDataParser(";MINX:", MetaDataParserFunc_Number);
exports.CURA_MIN_Y_PARSER = new MetaDataParser(";MINY:", MetaDataParserFunc_Number);
exports.CURA_MIN_Z_PARSER = new MetaDataParser(";MINZ:", MetaDataParserFunc_Number);
exports.CURA_MAX_X_PARSER = new MetaDataParser(";MAXX:", MetaDataParserFunc_Number);
exports.CURA_MAX_Y_PARSER = new MetaDataParser(";MAXY:", MetaDataParserFunc_Number);
exports.CURA_MAX_Z_PARSER = new MetaDataParser(";MAXZ:", MetaDataParserFunc_Number);
