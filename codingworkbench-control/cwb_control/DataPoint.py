
class DataPoint:

    def __init__(self, key, data_store, value):
        self.id = key
        self.dataStore = data_store
        self.dataStore.set(key, value)

    def set(self, value):
        self.dataStore.set(self.id, value)
        return self

    def get(self):
        return self.dataStore.get(self.id)

    def enable_handler(self, handler_name):
        self.dataStore.enable_handler(self.id, handler_name)
        return self

    def disable_handler(self, handler_name):
        self.dataStore.disable_handler(self.id, handler_name)
        return self

    def add_transformer_func(self, func):
        self.dataStore.add_transformer_func(self.id, func)
        return self
