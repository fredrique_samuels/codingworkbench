import CwbNode from "../node"

/**
 * An employee with a name only.
 */
interface Employee {
    name: string;
}

// node data accessors
const get_employee_name= (data: Employee) => data ? data.name : null;

// function to print the name attribute
const print_name = (nameAttribute: CwbNode.IAttribute) => console.log(`Attribute name="${nameAttribute.name}" value=${nameAttribute.value}`);

// Create a test employee
const employeeObj: Employee = { name: "Jack" };

// Describe the attribute and how to pull the value
const nameAttributeDef = new CwbNode.AttributeBuilder(get_employee_name).build();

// Build the root node from the employee object
const nodeTree = new CwbNode.NodeTreeBuilder("employee-tag", employeeObj)
    .addChild("name", nameAttributeDef)
    .build();

// get the name attribute
nodeTree.get("name").asAttribute<string>(print_name);


