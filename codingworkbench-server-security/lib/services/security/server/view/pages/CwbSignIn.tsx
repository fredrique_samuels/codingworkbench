import * as React from 'react';
import * as ReactDOM from "react-dom";

import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import {IFormValidation} from "@codingworkbench/react/form/FormInputs";
import Copyright from '../components/Copyright';
import SecurityViewController from "../SecurityViewController";
import {SecurityPasswordInput, SecurityEmailInput} from "../components";
import {ISecurityForm, SecurityForm} from "../components/SecurityForm";


const useStyles = makeStyles((theme) => ({
    root: {
        height: '100vh',
    },
    image: {
        backgroundImage: 'url(https://source.unsplash.com/random)',
        backgroundRepeat: 'no-repeat',
        backgroundColor:
            theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
        backgroundSize: 'cover',
        backgroundPosition: 'center',
    },
    paper: {
        margin: theme.spacing(8, 4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function CwbSignIn() {

    const classes = useStyles();

    const [state, setState] = React.useState<ISecurityForm>(SecurityForm.createForm());
    const stateControl = {state, setState};

    const handleSubmit = () => {
        setState(SecurityForm.startLoading(state));
        SecurityViewController.do_signin(state.inputs)
            .then((validation: IFormValidation) => setState(SecurityForm.processValidationResponse(state, validation)))
            .catch(console.error)
    };

    return (
        <Grid container component="main" className={classes.root}>
            <CssBaseline />
            <Grid item xs={false} sm={4} md={7} className={classes.image} />
            <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    {
                        !state.validation.passed && !state.loading
                            ? (
                                <Typography component="h1" variant="h6" color={"error"}>
                                    {SecurityForm.getMessage(state.validation)}
                                </Typography>
                            )
                            : null
                    }
                    <form className={classes.form} noValidate>
                        <Grid container spacing={2}>
                            <Grid item xs={12}>
                                <SecurityEmailInput {...stateControl} />
                            </Grid>
                            <Grid item xs={12}>
                                <SecurityPasswordInput {...stateControl} />
                            </Grid>
                        </Grid>
                        <FormControlLabel
                            control={<Checkbox value="remember" color="primary" />}
                            label="Remember me"
                        />
                        <Button
                            type="button"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                            onClick={handleSubmit}
                            disabled={state.loading}
                        >
                            Sign In
                        </Button>
                        <Grid container>
                            <Grid item xs>
                                <Link href="#" variant="body2" onClick={SecurityViewController.goto_forgot_password}>
                                    Forgot password?
                                </Link>
                            </Grid>
                            <Grid item>
                                <Link href="#" variant="body2" onClick={SecurityViewController.goto_signup} >
                                    {"Don't have an account? Sign Up"}
                                </Link>
                            </Grid>
                        </Grid>
                    </form>
                    <Box mt={5}>
                        <Copyright />
                    </Box>
                </div>
            </Grid>
        </Grid>
    );
}

ReactDOM.render(<CwbSignIn />, document.getElementById("root"));
