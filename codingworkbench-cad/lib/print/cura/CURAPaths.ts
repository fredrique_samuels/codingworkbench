import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
import PropertiesFileReader from "@codingworkbench/common/PropertiesFileReader";
import * as os  from "os"

export class CURAPaths  {
    home: string;
    engine: string;
    definitions: string;

    public static fromProperties(properties: IPropertiesReader): CURAPaths {
        const platform = os.platform();
        return {
            home: properties.getString(`slicer.${platform}.cura.home`),
            definitions: properties.getString(`slicer.${platform}.cura.definitions`),
            engine: properties.getString(`slicer.${platform}.cura.engine`)
        }
    }

    public static fromPropertiesFile(path: string): CURAPaths {
        return CURAPaths.fromProperties(new PropertiesFileReader(path));
    }
}
