CURA_HOME="/Applications/Cura.app/Contents/MacOS"
CURA_ENGINE="$CURA_HOME/CuraEngine"
ENDER3_DEF="$CURA_HOME/resources/definitions/creality_ender3.def.json"
STL_FILE="./resources/Marvin.stl"
GCODE_FILE="./resources/SH_Marvin.gcode"

echo "***********************************"
echo "Slicer Engine: $CURA_ENGINE"
echo "Definition: $ENDER3_DEF"
echo "***********************************"

$CURA_ENGINE slice -v -j $ENDER3_DEF -s material_bed_temperature_layer_0="60" -s material_print_temperature_layer_0="200" -s layer_height="0.2" -s machine_depth=".2" -s layer_height_0="0.2" -s material_diameter="1.75" -e0 -l $STL_FILE -o $GCODE_FILE