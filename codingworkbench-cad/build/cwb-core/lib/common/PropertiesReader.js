"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PropertiesReader_Dep = require("properties-reader");
var PropertiesFileReader = (function () {
    function PropertiesFileReader(path) {
        this._reader = PropertiesReader_Dep(path);
    }
    PropertiesFileReader.prototype.getString = function (property, fallback) {
        var p = this._reader.getRaw(property);
        if (p === null) {
            if (fallback) {
                return fallback;
            }
            throw new Error("Property '" + property + "' not found with not fallback provided");
        }
        return p;
    };
    PropertiesFileReader.prototype.getNumber = function (property, fallback) {
        try {
            var p = this._reader.getRaw(property);
            if (p === null) {
                if (fallback) {
                    return fallback;
                }
                throw new Error("Property '" + property + "' not found with not fallback provided");
            }
            return parseFloat(p);
        }
        catch (_a) {
            throw new Error("Property '" + property + "' expected to be a number");
        }
    };
    return PropertiesFileReader;
}());
var PropertiesReader;
(function (PropertiesReader) {
    function readFile(path) { return new PropertiesFileReader(path); }
    PropertiesReader.readFile = readFile;
})(PropertiesReader || (PropertiesReader = {}));
exports.default = PropertiesReader;
