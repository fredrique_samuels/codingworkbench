import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
export declare class CURAPaths {
    home: string;
    engine: string;
    definitions: string;
    static fromProperties(properties: IPropertiesReader): CURAPaths;
    static fromPropertiesFile(path: string): CURAPaths;
}
