
import time
from pyfirmata import Arduino, util
from hardware.Devices import get_serial_ports, is_serial_port_open, close_serial_port




ports = get_serial_ports()
print(ports)
for p in ports:
    print("{} {}", p, is_serial_port_open(p))

arduino_port = '/dev/tty.usbmodem14101'
if not is_serial_port_open(arduino_port):
    close_serial_port(arduino_port)


print(is_serial_port_open(arduino_port))
board = Arduino(arduino_port)

dir(board)
# it = util.Iterator(board)
# it.start()
# analog_0 = board.get_pin('a:0:i')
#
# while 1:
#     board.analog[0].enable_reporting()
#     time.sleep(0.01)
#     read = board.analog[0].read()
#     print (read)
