import {ID} from "@codingworkbench/data/IUnique";
import CwbSearch from "@codingworkbench/search";

export default interface IDataGenerator<T> {
    search(): Promise<CwbSearch.IResults<ID[]>>;
    get(ids: ID[]): Promise<T[]>;
    count(): Promise<number>;
}
