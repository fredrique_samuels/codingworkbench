

export interface MessageKeys {
    [key: string]: string;
}

export interface IMessageSource {
    get(code: string, ...args: any): Promise<string>;
}

export default IMessageSource;
