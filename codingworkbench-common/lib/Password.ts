

namespace Password {

    export function isEmailString(email: string) {
        const emailRegexp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
        return emailRegexp.test(email);
    }

    export type PasswordStrength = "weak" | "medium" | "strong";

    export function getPasswordStrength(password: string): PasswordStrength {
        const strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        if (strongRegex.test(password)) {
            return "strong";
        }

        const mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
        if (mediumRegex.test(password)) {
            return "medium";
        }

        return "weak";
    }

    export function isPasswordStrengthMediumToHigh(password: string): boolean {
        return getPasswordStrength(password) !== "weak";
    }

}

export default Password;
