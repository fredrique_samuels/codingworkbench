import PropertiesReader from "cwb-core/common/PropertiesReader";
export declare class CURAPaths {
    home: string;
    engine: string;
    definitions: string;
    static fromProperties(properties: PropertiesReader): CURAPaths;
    static fromPropertiesFile(path: string): CURAPaths;
}
