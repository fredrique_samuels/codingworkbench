"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var STLVertexDataScaleVisitor_1 = require("./STLVertexDataScaleVisitor");
var STLUtils = (function () {
    function STLUtils() {
    }
    STLUtils.scaleModel = function (stlFileContent, scaleFactor) {
        if (scaleFactor === void 0) { scaleFactor = 1.0; }
        var visitor = new STLVertexDataScaleVisitor_1.default(scaleFactor);
        stlFileContent.split("\n").forEach(visitor.visit.bind(visitor));
        return visitor.content;
    };
    return STLUtils;
}());
exports.default = STLUtils;
