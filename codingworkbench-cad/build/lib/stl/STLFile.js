"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var STLFile = (function () {
    function STLFile(filename) {
        this._filename = filename;
    }
    STLFile.prototype.isBinary = function () {
        return false;
    };
    return STLFile;
}());
exports.default = STLFile;
