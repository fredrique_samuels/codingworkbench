import {ServerInjector} from "../ServerInjector";
import Encrypt from "@codingworkbench/common/Encrypt";
import {MockViewFactory} from "./mock_requests";
import {IViewFactory} from "@codingworkbench/server/common";
import {server_properties_create} from "../IServerPropertiesAware";


export default class ServerTestInjector extends ServerInjector {

    constructor() {
        super(server_properties_create({}));
        this.bind("encryptionPolicy", async () => await Encrypt.noEncryption())
            .bind("viewFactory", async (): Promise<IViewFactory> => await new MockViewFactory())
    }
}
