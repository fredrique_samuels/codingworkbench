var canvas = document.getElementById("myCanvas");

var logo_text_img = new Image();
logo_text_img.src = CWB_LOGO_TEXT_IMG;

var logo_img = new Image();
logo_img.src = CWB_LOGO_IMG;

var background_frames = []
for (let index = 0; index < 200; index++) {
    let img = new Image();
    img.src = "/cdn/Liquid-Frames/ezgif-frame-"+(index + 1).toString().padStart(3, "0")+".jpg"
    background_frames.push(img)
}

var logo_dim = perc_width(0.15);
var logo_y = perc_width(0.10)
var logo_text_width = perc_width(0.7);
var logo_text_y = perc_width(0.15);

function perc_width(p) { return canvas.width * p; }
function perc_height(p) { return canvas.height * p; }
function clear_canvas() { canvas.getContext("2d").clearRect(0, 0, canvas.width, canvas.height) };
function draw_image(img, x, y, w, h) { canvas.getContext("2d").drawImage(img , x, y, w, h) };
function center_x(w) { return (perc_width(1) - w) * 0.5; } 
function set_opacity(o) { canvas.getContext("2d").globalAlpha = o }
function animate_variable(start, end, progress) { return start + (end - start) * progress}
function draw_logo_text_img() { draw_image(logo_text_img, center_x(logo_text_width), logo_text_y, logo_text_width, logo_text_width * .5); }
function draw_logo_img() { draw_image(logo_img, center_x(logo_dim), logo_y, logo_dim, logo_dim); }

var animation_opacity = 0 // current animation opacity
var animation_delay = 300 // delay in milliseconds
var animation_duration = 4700 // how long the animation must run
var animation_start_time = new Date().getTime()

function draw_frame(run_time) {
    var should_delay = run_time <= animation_delay
    var progress = should_delay ? 0 : Math.min((run_time - animation_delay) / animation_duration, 1.0);
    
    logo_dim = animate_variable(perc_width(0.13), perc_width(0.15), progress)
    logo_text_width = animate_variable(perc_width(0.65), perc_width(0.7), progress)

    // clear_canvas();
    set_opacity(1);
    canvas.getContext("2d").fillStyle = 'white'
    canvas.getContext("2d").fillRect(0,0,canvas.width,canvas.height); // videos cant handle transprency

    // console.log(run_time / 50, Math.floor(run_time / 50), Math.ceil(run_time / 50))
    canvas.getContext("2d").drawImage(background_frames[Math.min(199, Math.floor(run_time / 50))] ,0, 0)

    set_opacity(animate_variable(0, 1, progress));
    draw_logo_img();
    draw_logo_text_img();
}

// run animation in realtime
function update_realtime() {
    draw_frame(new Date().getTime() - animation_start_time)
}

function run_realtime() {
    setInterval(update_realtime, 66);
}

// create discrete video
function request_frame(runtime) {
    draw_frame(runtime);
    return canvas.toDataURL('image/webp', 0)
}

var total_frames = 0
var video_fps = 15
var duration = 5; // seconds
var video = new Whammy.Video(15);
function create_video_frame() {
    let frames = video_fps * duration
    let create_video = total_frames ===  frames - 1

    if (total_frames < frames) {
        draw_frame(new Date().getTime() - animation_start_time)
        // let frame = canvas.toDataURL('image/webp', 1)
        // document.getElementById('frame').src = frame
        video.add(canvas.getContext("2d"));
        total_frames += 1;
    }

    if (create_video) {
        video.compile(false, function(output){
            var end_time = +new Date;
            console.log(total_frames, output)
            var url = (window.webkitURL || window.URL).createObjectURL(output);
            document.getElementById('awesome').src = url; //toString converts it to a URL via Object URLs, falling back to DataURL
            document.getElementById('download').style.display = '';
            document.getElementById('download').href = url;
            document.getElementById('status').innerHTML = "Compiled Video: file size: " + Math.ceil(output.size / 1024) + "KB";
        });
    }
}

function run_video_generator() {
    setInterval(create_video_frame, 66);
}

run_video_generator();
// run_realtime();