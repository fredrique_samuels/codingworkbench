import * as common from "./common";

export type FetchResponseCallback = (r: Response) => any;

/**
 * Builder for constructing a web request.
 *
 * Uses fetch() to execute the requests.
 */
export class RequestAction {
    private readonly _url: string;
    private _method: common.Method;
    private _headers: {[s: string]: string};
    private _body?: BodyInit;
    private _responseCallback?: FetchResponseCallback;

    /**
     * @param url The url to execute.
     *
     * @constructor
     */
    constructor(url: string) {
        this._url = url;
        this._method = 'get';
        this._headers = {};
    }

    /**
     * Callback to capture the raw response
     *
     * @param value
     */
    public setResponseCallback(value: FetchResponseCallback): this {
        this._responseCallback = value;
        return this;
    }

    /**
     * Set the request method to 'post'.
     *
     * @return this
     */
    public post(): this {
        this._method = 'post';
        return this;
    }

    /**
     * The the request body.
     *
     * @param body
     */
    public setBody(body: BodyInit): this {
        this.post();
        this._body = body;
        return this;
    }

    /**
     * Set the request headers to accept json.
     *
     * @return this
     */
    public acceptJson(): this {
        this._headers = {
            ...this._headers,
            ...{
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            }
        };
        return this;
    }

    /**
     * Execute the configured request and return try to parse text content.
     *
     * Attempts to call text() on the fetch result and return the string.
     *
     * @async
     * @return text result.
     */
    public async text(): Promise<string> {
        const parse = async (r: Response) => {
            return await r.text();
        };
        return await this.execute( parse );
    }

    /**
     * Execute the configured request and return try to parse resulting json text to an object .
     *
     * Attempts to call Response.json() on the fetch result and return the parsed object.
     *
     * @async
     * @return object.
     */
    public async json(): Promise<any> {
        this.acceptJson();
        const parse = async (r: Response) => {
            return await r.json();
        };
        return await this.execute( parse );
    }

    /**
     * Called when an exception is thrown during execution.
     *
     * @param message The error message.
     * @param e The error object.
     */
    protected dispatchFailure(message: string, e: Error, r: Response) {
        console.error(message, e)
    }

    /**
     * Called when the excecution succeeds.
     *
     * @param message A success message.
     */
    protected dispatchSuccess(message: string) {}

    private async execute<R>(responseParser: (r: Response) => Promise<R>): Promise<R> {
        let init: RequestInit = {
            method: this._method,
            headers: this._headers
        };
        if(this._method == "post" && this._body) {
            init.body = this._body;
        }
        let r: Response = await fetch(this._url, init);

        /* process the response using the callback method */
        if (this._responseCallback) {
            try {
                this._responseCallback(r);
            } catch (e) {

            }
        }

        /* handle the request failure */
        let maybeError = new Error(`Request failed ${r.url} ${r.status} ${r.statusText}`);
        if(!r.ok) {
            this.safeFailedDispatch("Request failed !OK !", maybeError, r);
            throw maybeError;
        }

        try {
            let res: R = await responseParser(r);
            this.safeSuccessDispatch();
            return res;
        } catch (e) {
            console.error(e.stack);
            this.safeFailedDispatch("Invalid response", maybeError, r);
            throw maybeError;
        }
    }

    private safeFailedDispatch(message: string, maybeError: Error, r: Response) {
        try {
            this.dispatchFailure(message, maybeError, r);
        } catch (e) {
            console.error(e.stack)
        }
    }

    private safeSuccessDispatch() {
        try {
            this.dispatchSuccess("Operation Successful");
        } catch (e) {
            console.error(e.stack)
        }
    }
}

export default RequestAction;
