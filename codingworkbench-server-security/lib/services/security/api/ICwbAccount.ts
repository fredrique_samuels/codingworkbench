import IUnique from "@codingworkbench/data/IUnique";
import IAuditAware from "@codingworkbench/data/IAuditAware";
import IArchiveAware from "@codingworkbench/data/IArchiveAware";


export interface ICwbAccount extends IUnique, IAuditAware, IArchiveAware {
    /* optional username */
    username: string | null;
    /* is the account validated */
    activated: boolean;
    /* the account validation code */
    activation_token: string | null;
}

export default ICwbAccount;
