import CwbLog from "@codingworkbench/log";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
import {ReactViewFactory} from "../lib/html/ReactViewFactory";
import {ExpressApplicationOptions} from "@codingworkbench/server/ExpressApplicationUtils";
import ExpressApplication, {ExpressContextFactory} from "@codingworkbench/server/ExpressApplication";
import {IViewFactory} from "@codingworkbench/server/common";
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";

CwbLog.setup();
CwbLog.enable_console();

interface MyApplicationContext {}

class MyApplicationContextFactory implements ExpressContextFactory<MyApplicationContext> {

    async createContext(options: ExpressApplicationOptions,
                  connectionProperties: IPropertiesReader | null)
        : Promise<MyApplicationContext> {
        return {};
    }
}

const viewFactory: IViewFactory = new ReactViewFactory("/static/js", false);

const home_handler = new RequestHandlerBuilder("/", context => {
    const view = viewFactory.modelAndView("MyPage");
    context.response.view(view);
}).build();

const application = new ExpressApplication<MyApplicationContext>();
application.addHandlers([ home_handler ]);
application.serveStatic("/static", "./web-app" );
application.start({ port: 8080, }, new MyApplicationContextFactory())
    .catch(CwbLog.error);
