import * as crypto from "crypto";
import IEncryptionPolicy from "./IEncryptionPolicy";


namespace Encrypt {

    class Sha256Encryption implements IEncryptionPolicy {
        public async encrypt(input: string): Promise<string> {
            return crypto.createHash("sha256").update(input).digest("base64");
        }
    }

    class NoEncryption implements IEncryptionPolicy {
        public async encrypt(input: string): Promise<string> {
            return input;
        }
    }

    export async function noEncryption(): Promise<IEncryptionPolicy> {
        return new NoEncryption();
    }

    export async function sha256Encryption(): Promise<IEncryptionPolicy> {
        return new Sha256Encryption();
    }

}

export default Encrypt;
