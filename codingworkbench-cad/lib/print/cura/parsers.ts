
type MetaDataParserFunc = (value: string) => any;

function MetaDataParserFunc_Number(value: string): any {
    return parseFloat(value.trim())
}

function MetaDataParserFunc_String(value: string): any {
    return value.trim()
}

function MetaDataParserFunc_Meter(value: string): any {
    return parseFloat(/(\d+?\.\d+)m/.exec(value)[1])
}

export class MetaDataParser {
    private readonly _prefix: string;
    private readonly _parserFunc: MetaDataParserFunc;

    constructor(prefix: string, parserFunc: MetaDataParserFunc) {
        this._prefix = prefix;
        this._parserFunc = parserFunc;
    }

    get prefix(): string { return this._prefix}

    parse(line: string): any {
        return this._parserFunc(line.replace(this._prefix, ""))
    }
}

export const CURA_TIME_PARSER = new MetaDataParser(";TIME:", MetaDataParserFunc_Number);
export const CURA_TIME_FORMATTED_PARSER = new MetaDataParser("Print time (hr|min|s):", MetaDataParserFunc_String);
export const CURA_FILAMENT_USED_PARSER = new MetaDataParser(";Filament used:", MetaDataParserFunc_Meter);
export const CURA_MIN_X_PARSER = new MetaDataParser(";MINX:", MetaDataParserFunc_Number);
export const CURA_MIN_Y_PARSER = new MetaDataParser(";MINY:", MetaDataParserFunc_Number);
export const CURA_MIN_Z_PARSER = new MetaDataParser(";MINZ:", MetaDataParserFunc_Number);
export const CURA_MAX_X_PARSER = new MetaDataParser(";MAXX:", MetaDataParserFunc_Number);
export const CURA_MAX_Y_PARSER = new MetaDataParser(";MAXY:", MetaDataParserFunc_Number);
export const CURA_MAX_Z_PARSER = new MetaDataParser(";MAXZ:", MetaDataParserFunc_Number);
