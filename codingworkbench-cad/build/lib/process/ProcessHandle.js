"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var child_process = require("child_process");
var ProcessOutputCapture_1 = require("./ProcessOutputCapture");
var ProcessHandle = (function () {
    function ProcessHandle() {
    }
    ProcessHandle.prototype.execute = function (command) {
        return new Promise(function (resolve, reject) {
            var pc = new ProcessOutputCapture_1.default();
            child_process.exec(command, { maxBuffer: 1024 * 500 }, function (error, stdout, stderr) {
                var pc = new ProcessOutputCapture_1.default();
                pc.capture(error, stdout, stderr);
                resolve(pc);
            });
        });
    };
    return ProcessHandle;
}());
exports.default = ProcessHandle;
