export declare function setup_log4js(level?: string): void;
export declare function log_enable_console(): void;
export declare function log_error(e: Error): void;
export declare function log_warning(e: Error): void;
export declare function log_info_message(message: string): void;
