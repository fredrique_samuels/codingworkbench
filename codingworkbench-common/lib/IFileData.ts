export interface  IFileData {
    readonly uuid: string;
    readonly name: string;
    readonly size: number;
    readonly mimetype: string;
    readonly md5: string;
    readonly data: Buffer;
}

export default IFileData;
