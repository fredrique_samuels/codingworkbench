"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
Object.defineProperty(exports, "__esModule", { value: true });
var HtmlTemplate = require("./HtmlTemplate");
var HtmlModel_1 = require("./HtmlModel");
/**
 * Builder wrapped arounf HtmlTemplate.
 *
 * @see HtmlTemplate
 */
var HtmlBuilder = /** @class */ (function () {
    /**
     * @constructor
     */
    function HtmlBuilder() {
        this.template = {
            title: "",
            head: [],
            body: [],
            preBody: []
        };
        this.model = {};
    }
    HtmlBuilder.prototype.setModel = function (model) {
        this.model = __assign({}, model);
        return this;
    };
    /**
     * @see HtmlTemplate.head
     */
    HtmlBuilder.prototype.addHead = function (tag) {
        this.template.head.push(tag);
        return this;
    };
    /**
     * @see HtmlTemplate.body
     */
    HtmlBuilder.prototype.addBody = function (bCode) {
        this.template.body.push(bCode);
        return this;
    };
    /**
     * @see HtmlTemplate.preBody
     */
    HtmlBuilder.prototype.addPreBody = function (bCode) {
        this.template.preBody.push(bCode);
        return this;
    };
    HtmlBuilder.prototype.build = function () {
        this.addPreBody("<script>var " + HtmlModel_1.HTML_DATA_MODEL_ID + " = " + JSON.stringify(this.model) + "</script>");
        return HtmlTemplate.build_html(this.template);
    };
    return HtmlBuilder;
}());
exports.HtmlBuilder = HtmlBuilder;
