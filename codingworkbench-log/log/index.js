"use strict";
exports.__esModule = true;
var log4js = require("log4js");
var CwbLog;
(function (CwbLog) {
    var logger = log4js.getLogger();
    var SETTINGS = {
        consoleEnabled: false,
    };
    function setup(level) {
        if (level === void 0) { level = "info"; }
        log4js.configure({
            appenders: { log: {
                    type: "file", filename: "log4js.log",
                    layout: {
                        type: 'pattern',
                        pattern: '%d %p %c %m'
                    }
                } },
            categories: { "default": { appenders: ["log"], level: level } },
        });
    }
    CwbLog.setup = setup;
    function enable_console() {
        SETTINGS.consoleEnabled = true;
    }
    CwbLog.enable_console = enable_console;
    function error(e) {
        if (SETTINGS.consoleEnabled) {
            console.error(e.message, e.stack);
        }
        logger.error(e.message, e.stack);
    }
    CwbLog.error = error;
    function warning(e) {
        if (SETTINGS.consoleEnabled) {
            console.warn(e.message, e.stack);
        }
        logger.warn(e.message, e.stack);
    }
    CwbLog.warning = warning;
    function info(message) {
        if (SETTINGS.consoleEnabled) {
            console.info(message);
        }
        logger.info(message);
    }
    CwbLog.info = info;
})(CwbLog || (CwbLog = {}));
exports["default"] = CwbLog;
//# sourceMappingURL=index.js.map