import * as fs from "fs";
import * as jsonMerger from "json-merger";
import * as path from "path";

import {CURAPaths} from "./CURAPaths";

export class CURADefinitionProvider {
    private readonly enginePaths: CURAPaths;

    constructor(enginePaths: CURAPaths) {
        this.enginePaths = enginePaths;
    }

    public getDefinitionFileObj(definitionTarget: string): any {
        const definitionsObjs: any[] = [this.loadDefObj(definitionTarget)];

        /* build list of files to merge */
        while( definitionsObjs[definitionsObjs.length-1].inherits ) {
            const o = definitionsObjs[definitionsObjs.length-1];
            definitionsObjs.push(this.loadDefObj(`${o.inherits}.def.json`))
        }

        /* merge */
        const merged = jsonMerger.mergeObjects(definitionsObjs.reverse());

        /* remove inheritance */
        merged.inherits = undefined;

        return merged
    }

    private loadDefObj(definitionTarget: string): any {
        return JSON.parse(fs.readFileSync(this.getDefPath(definitionTarget), "utf-8"));
    }

    private getDefPath(definitionTarget: string) {
        const {home, definitions} = this.enginePaths;
        return `${path.join(home, definitions, definitionTarget)}`;
    }
}
