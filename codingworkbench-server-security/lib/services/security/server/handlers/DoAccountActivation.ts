
import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import {RequestContext} from "@codingworkbench/server/common";

import {ISecurityHandlerContext, ISecurityHandlerFactoryContext} from "../../api/context";
import {create_security_model, web_respond_redirect, web_respond_with_500} from "../../api/response";
import CwbLog from "@codingworkbench/log";

async function handlerAsync<C extends ISecurityHandlerContext>(requestContext: RequestContext<C>) {
    const {request, response} = requestContext;
    const {securityService, securityLinks, viewFactory} = requestContext.context;
    const {token} = request.params;

    // const invalidToken = () => !token;
    // const activateAccount = async () => await securityService.activateAccount(token);
    // const gotoSignIn = () => response.redirect(securityLinks.signInPage);
    // const isErrorResponse = (res: CwbAction.IResponse) => res.error;
    // const gotoAccountActivated = () => response.redirect(securityLinks.activateAccountSuccessView);
    //
    // const stream = new WorkStream().error((e) => web_respond_with_500(e, requestContext));
    // stream.terminal(gotoSignIn).predicate(invalidToken).end()
    //       .execute(activateAccount)
    //       .terminal(gotoSignIn).predicate(isErrorResponse).end()
    //       .then(gotoAccountActivated);

    try {
        if (!token) {
            CwbLog.error(new Error(`Account validation attempted with invalid token ${token}`));
            web_respond_redirect(requestContext, securityLinks.signInPage);
            return;
        }
        // register the new user
        const actionResponse = await securityService.activateAccount(token);
        if(actionResponse.error) {
            CwbLog.error(new Error(`Account validation attempted and failed:${token}:${actionResponse.message}`));
            web_respond_redirect(requestContext, securityLinks.signInPage);
            return;
        }

        const model = create_security_model(securityLinks).build();
        const view = viewFactory.modelAndView(securityLinks.activateAccountSuccessView, model);
        response.view(view);
    } catch (e) {
        web_respond_with_500(e, requestContext);
    }
}

export default function DoAccountActivation<C extends ISecurityHandlerContext>(factoryContext: ISecurityHandlerFactoryContext): IRequestHandler<C> {
    return new RequestHandlerBuilder(factoryContext.links.doActivateAccount, () => {}, handlerAsync).setAsync().build();
}

