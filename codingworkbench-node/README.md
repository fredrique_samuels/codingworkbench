@codingworkbench/node
======

Goal
-----

The goal is to wrap a set of objects into a node tree API that reads and writes
data directly to the wrapped objects.

Install
-----
```sh
npm install @codingworkbench/node --save
```

Basic Usage
-----
```typescript
import CwbNode from "@codingworkbench/node";

/**
 * An employee with a name only.
 */
interface Employee {
    name: string;
}

// node data accessors
const get_employee_name = (data: Employee) => data ? data.name : null;

// function to print the name attribute
const print_name = (nameAttribute: CwbNode.IAttribute) => console.log(`Attribute name="${nameAttribute.name}" value="${nameAttribute.value}"`);

// Create a test employee
const employeeObj: Employee = { name: "Jack" };

// Describe the attribute and how to pull the value
const nameAttributeDef = new CwbNode.AttributeBuilder(get_employee_name).build();

// Build the root node from the employee object
const nodeTree = new CwbNode.NodeTreeBuilder("employee-tag", employeeObj)
    .addChild("name", nameAttributeDef)
    .build();

// get the name attribute
nodeTree.get("name").asAttribute<string>(print_name);

```

Output
```console
Attribute name="name" value=Jack
```


Nested Usage
-----
```typescript
import CwbNode from "@codingworkbench/node";

interface Employee {
     name: string;
 }
 
 interface EmployeeStore {
     employees: {[s: string]: Employee }
 }
 
 // node data accessors
 const get_name= (data: Employee) => data ? data.name : null;
 const get_store_entry = (data: EmployeeStore | null, name: string) => data ? data.employees[name] : null;
 const has_store_entry = (data: EmployeeStore | null, name: string): boolean => (data !== null && Object.keys(data.employees).includes(name));
 
 // function to print the name attribute
 const print_name = (nameAttribute: CwbNode.IAttribute) => console.log(`Employee "${nameAttribute.name}"=${nameAttribute.value}`);
 const print_employee = (iElement: CwbNode.IElement) => iElement.get("name").asAttribute(print_name);
 const search_employee = (iElement: CwbNode.IElement) => iElement.get("employee1").asElement(print_employee);
 
 // Create a test employee
 const employeeObj: Employee = { name: "Jack" };
 
 // Create a test store
 const employeeStore: EmployeeStore = {
     employees:  {
         employee1: employeeObj
     }
 };
 
 // Describe the nodes
 const nameAttributeDef = new CwbNode.AttributeBuilder<string, Employee>(get_name).build();
 const employeeElementDef = new CwbNode.ElementBuilder<Employee, EmployeeStore>("employee-tag", get_store_entry)
     .addChild("name", nameAttributeDef)
     .build();
 const employeeGroupDef = new CwbNode.ElementGroupBuilder("employees-tag", employeeElementDef, has_store_entry).build();
 
 // Build the root node from the employee object
 const nodeTree = new CwbNode.NodeTreeBuilder("employee-store-tag", employeeStore)
     .addChild("employees", employeeGroupDef)
     .build();
 
 // get the name attribute
 nodeTree.get("employees").asElement(search_employee);

```

Output
```console
Employee "name"=Jack
```

Errors
-----
```typescript
import CwbNode from "@codingworkbench/node";

const nodeTree = new CwbNode.NodeTreeBuilder("my-node", {})
    .addChild("myAttribute", new CwbNode.AttributeBuilder(data => {}).build())
    .addChild("myElement", new CwbNode.ElementBuilder("my-element", data => {}).build())
    .build();

const missingNode = nodeTree.get("missing-child");

console.log("Attempt to process empty node with standard error:");
missingNode.asAttribute(iAttribute => {});

console.log("\nAttempt to process empty node with custom error:");
missingNode.asElement(iElement => {}, n => { console.log(`Node ${n.name} does not exists`)});

console.log("\nAttempt to process an attribute as an element:");
nodeTree.get("myAttribute").asElement(iElement => {});

console.log("\nAttempt to process an element as an attribute:");
nodeTree.get("myElement").asAttribute(iAttribute => {});

```

Output
```console
Attempt to process empty node with standard error:
Node name="missing-child" does not exists in parent <my-node name="" />!"

Attempt to process empty node with custom error:
Node missing-child does not exists

Attempt to process an attribute as an element:
Node name="myAttribute" type="ATTRIBUTE" parent=<my-node name="" /> is not being consumed!

Attempt to process an element as an attribute:
Node name="myElement" type="ELEMENT" parent=<my-node name="" /> is not being consumed!
```

License
-------
The ISC License

Copyright (c) 2020 Fredrique Samuels (https://codingworkbench.com)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

