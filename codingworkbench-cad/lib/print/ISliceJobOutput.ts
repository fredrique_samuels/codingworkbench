import IProcessStdio, { process_stdio_create, process_stdio_merge } from "../process/IProcessStdio";
import ISliceResults from "./ISliceResults";
import IPrintSummary, { print_summary_create, print_summary_merge } from "./IPrintSummary";

export default interface ISliceJobOutput {
    readonly stdio: IProcessStdio;
    readonly results: ISliceResults[];
    readonly summary: IPrintSummary;
}

export function slice_job_output_create(): ISliceJobOutput {
    return {
        stdio: process_stdio_create(),
        results: [],
        summary: print_summary_create()
    }
}

export function slice_job_output_merge(o1: ISliceJobOutput, o2: ISliceJobOutput) {
    return {
        stdio: process_stdio_merge(o1.stdio, o2.stdio),
        results: [...o1.results, ...o2.results],
        summary: print_summary_merge(o1.summary, o2.summary)
    }
}