const util = require('util');
const path = require('path');
const os = require('os');
const child_process = require('child_process');
const fs = require('fs')



const exec = util.promisify(require('child_process').exec);

// console.log(os.)
// 
const PATH_QUOTE='\"'
const CURA_HOME='C:\\Program Files\\Ultimaker Cura 4.1'

const CURA_ENGINE_FILE=`\\CuraEngine.exe`
const CURA_ENGINE=`${PATH_QUOTE}${CURA_HOME}${CURA_ENGINE_FILE}${PATH_QUOTE}`

const ENDER3_DEF_FILE="\\resources\\definitions\\creality_ender3.def.json"
const ENDER3_DEF=`${PATH_QUOTE}${CURA_HOME}${ENDER3_DEF_FILE}${PATH_QUOTE}`

const STL_FILE="resources\\Marvin.stl"
const GCODE_FILE="resources\\SH_W_Marvin.gcode"
const OUTPUT = "" // `-o ${GCODE_FILE}`


console.log(path.resolve(__dirname))

command = `${CURA_ENGINE} slice -v -j ${ENDER3_DEF} -s material_bed_temperature_layer_0="60" -s material_print_temperature_layer_0="200" -s layer_height="0.2" -s machine_depth=".2" -s layer_height_0="0.2" -s material_diameter="1.75" -e0 -l ${STL_FILE} ${OUTPUT}`

class SlicerCommandBuilder {
  
}

async function sliceFile() {

  let options = {maxBuffer: 1024 * 500}
  let callback = function( error, stdout, stderr) {
    // console.log('stdout:', stdout);
    console.log('stderr:', stderr);

    
    fs.appendFile('stdout.txt', stdout, function (err) {
      if (err) throw err;
      console.log('Saved!');
    });

    fs.appendFile('stderr.txt', stderr, function (err) {
      if (err) throw err;
      console.log('Saved!');
    });
  } 

  child_process.exec(command, options, callback);
  // const { stdout, stderr } = await exec(command, {maxBuffer: 1024 * 500});
  // console.log('stdout:', stdout);
  // console.log('stderr:', stderr);
}

sliceFile();


