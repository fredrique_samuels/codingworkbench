import * as child_process from "child_process";
import IProcessOutput from "./IProcessOutput";

export default class ProcessOutputCapture implements IProcessOutput {

    private _error: child_process.ExecException;
    private _stdout: string;
    private _stderr: string;

    capture(error: child_process.ExecException, stdout: string, stderr: string): void {
        this._error = error;
        this._stdout = stdout;
        this._stderr = stderr
    }

    get errorMessage(): string { return this._error ? this._error.message : "" ;}
    get error(): Error { return this._error; }
    get stdout(): string { return this._stdout; }
    get stderr(): string { return this._stderr; }
}
