import {deepStrictEqual, ok} from "assert";

import CwbTestCase from "@codingworkbench/testcase"
import Optional from "@codingworkbench/common/Optional";

import ICwbAccount from "../../lib/services/security/api/ICwbAccount";
import ICwbUser from "../../lib/services/security/api/ICwbUser";
import SecurityAwareTestCase from "../../lib/services/security/test/SecurityAwareTestCase";
import ISecurityAwareContext from "../../lib/services/security/api/context";
import MockedEmailTransporter from "../../lib/services/email/test/MockedEmailTransporter";
import CwbLog from "@codingworkbench/log";

export default function runTests() {

    return () => {

        let unit: ISecurityAwareContext;

        before(async () => {
            CwbLog.setup();
            CwbLog.enable_console();
            unit = await SecurityAwareTestCase.create_context();
        });

        after(async () => {
            await SecurityAwareTestCase.destroy(unit)
        });


        it("register new user", (done) => {
            async function _e() {
                const email: string = await CwbTestCase.generateEmail();
                const password: string = await CwbTestCase.generatePassword();

                // register user
                const account = (await unit.securityService.registerNewUser(email, password)).data as ICwbAccount;

                // assert user exists
                const expectedUser: Optional<ICwbUser> = Optional.forValue(await unit.securityService.getUserFromEmail(email));
                ok(expectedUser.present);
                deepStrictEqual(email, expectedUser.get().email);
                deepStrictEqual(await unit.encryptionPolicy.encrypt(password), expectedUser.get().password_hash);

                // assert account waiting activation
                ok(!account.activated);
                ok(account.activation_token);

                // assert user linked
                const users: ICwbUser[] = await unit.securityService.getAccountUsers(account);
                deepStrictEqual( users.length, 1);
                const [user] = users;
                deepStrictEqual(user.email, email);

                // assert email sent
                deepStrictEqual(1, (unit.emailTransporter as MockedEmailTransporter).emails.length);
            }

            _e().then(() => done())
                .catch(done);
        });

        it("activate account", (done) => {
            async function _e() {
                const {email, password, account} = await SecurityAwareTestCase.do_signup_test_user(unit);

                // validate account
                await unit.securityService.activateAccount(account.activation_token);

                // assert account activated
                const accountExpected  = Optional.forValue(await unit.securityService.getAccountById(account.id));
                ok(accountExpected.present);
                deepStrictEqual(accountExpected.get().activation_token, null);
                ok(accountExpected.get().activated);
            }

            _e().then(() => done())
                .catch(done);
        });

    }
};


describe('SecurityService.test.integration', runTests());
