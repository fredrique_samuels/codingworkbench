import ISliceResults from "lib/ISliceResults";
export default class CURASliceSummaryExtractor {
    private readonly gcode;
    constructor(gcode: string);
    extractFromOutput(output: string): ISliceResults;
}
