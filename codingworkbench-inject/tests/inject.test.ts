import {deepStrictEqual, ok} from "assert";

import CwbInject from "../inject";
import ITodoModule, {ITodoItem, ITodoServiceArgs, TODO_ITEM_NAME, TodoService, todoServiceInjectionKeys, TodoStore} from "./todoMod";

function assertTodoItem(item: ITodoItem) {
    ok(item);
    deepStrictEqual(item.label, TODO_ITEM_NAME);
}

describe("inject.test.ts", () => {

    it ("create using factory functions", (done) => {

        async function _e() {
            // given
            const injector = new CwbInject.Module<ITodoModule>();
            injector.bindFactoryFunction("todoStore", async () => new TodoStore());
            injector.bindFactoryFunction("todoService", async (args) => new TodoService({todoStore: await args.get("todoStore")}));

            // when
            const [item] = (await injector.get("todoService")).getItems();

            // then
            assertTodoItem(item);
        }

        _e().then(() => done())
            .catch(done);
    });

    it ("error if no types registered", (done) => {

        async function _e() {
            // given
            const injector = new CwbInject.Module<ITodoModule>();

            // when
            try {
                await injector.get("todoService");
                ok(false, "This should fail when no type mapping was set");
            } catch (e) {

            }
        }

        _e().then(() => done())
            .catch(done);
    });

});
