
namespace CwbInject {

    export interface IDependencies<MC> {
        get<K extends keyof MC>(propertyKey: K): Promise<MC[K]>;
    }

    export type TypedPropertyRestriction<T, I> = { [K in keyof T]: T[K] extends I ? K : never }[keyof T];

    export type SuperTypedPropertyRestriction<T, I> = { [K in keyof T]: I extends T[K] ? K : never }[keyof T];

    export type FactoryFunction<R, Args> = (args: IDependencies<Args>) => Promise<R>;

    export type InjectionProperties<T> = { [P in keyof Required<T>]: boolean };

    export type ArgsConstructor<R, Args> = new (args: Args) => R;

    export interface ICreationStrategies<R, MC, Args= any> {
        factoryFunction?: FactoryFunction<R, MC>;
        constructorFunction?: ArgsConstructor<R, Args>;
        injectionKeys: InjectionProperties<Args> | null;
    }

    export interface ICreationStrategiesMap { [s: string]: ICreationStrategies<any, any, any>; }

    /**
     * A module to contain a given collection of dependencies.
     *
     * MC defines all the properties in this module.
     */
    export class Module<MC> {

        private static assertCreationProps(propertyKey: string, creationProps?: ICreationStrategies<any, any, any>) {
            if (!creationProps) {
                throw new Error(`No creation strategy found for property value ${propertyKey}`);
            }
        }
        private readonly target: IStrategiesAndValuesCache<MC>;
        private readonly propertySet: Set<string>;

        constructor(strategiesMap?: ICreationStrategiesMap) {
            this.propertySet = new Set();
            this.target = {
                creationStrategies: { ... strategiesMap || {}  },
                values:  {},
            };
            Object.keys(this.target.creationStrategies).forEach((k) => this.addProperties(k, this.target.creationStrategies[k].injectionKeys));
        }

        /**
         * Combine two modules. Note neither module is allowed to have populated values
         * when merging.
         *
         * @param other The
         */
        public combine<MC2>(other: Module<MC2>): Module<MC & MC2> {
            if (this.hasValues()) {
                throw new Error(`Module 'this' has values already set. Merging is not allowed with modules that have values`);
            }

            if (other.hasValues()) {
                throw new Error(`Module 'other' has values already set. Merging is not allowed with modules that have values`);
            }

            return new Module<MC&MC2>({
                ...this.target.creationStrategies,
                ...other.target.creationStrategies,
            });
        }

        /**
         * Load all registered objects.
         */
        public async createAll(): Promise<void> {
            const propKeys = Array.from(this.propertySet);
            for (const pk of propKeys) {
                await this.createOrGetValue(pk);
            }
        }

        /**
         * Get the current module proxy that will lazy create the property
         * values as they are accessed.
         */
        public async get<K extends keyof MC>(propertyKey: K): Promise<MC[K]> {
            return await this.createOrGetValue(propertyKey.toString()) as MC[K];
        }

        /**
         * Map a constructor of type E to a property (key) in type MC.
         *
         * The type E must have either no args constructor or have a single
         * arg constructor of type Args.
         *
         * Example:
         *
         *  interface ITodoService {}
         *  class TodoService {}
         *
         *  interface IModule {
         *      todoService: ITodoService;
         *  }
         *
         *  const module = new Module<IModule>();
         *  module.add("todoService", TodoService);
         *
         * @param propertyKey
         * @param classType The class type to call on creation.
         * @param injectionKeys Optional keys to be injected into instance constructor call
         * @template E The constructor type to be created when the property is accessed.
         * @template Args Optional argument type that is required by the constructor invocation.
         */
        // public bind<E, Args= Partial<MC>>(
        //         propertyKey: SuperTypedPropertyRestriction<MC, E>,
        //         classType: new (args: Args) => E,
        //         injectionKeys: InjectionProperties<Args> | null = null)
        //     : this {
        //     this.target.creationStrategies[propertyKey.toString()] = {
        //         constructorFunction: classType,
        //         injectionKeys,
        //     };
        //     this.addProperties(propertyKey.toString(), injectionKeys);
        //     return this;
        // }

        /**
         * Add a factory function for a given property on type MC.
         *
         * Example:
         *
         *  interface ITodoService {}
         *  class TodoService {}
         *
         *  interface IModule {
         *      todoService: ITodoService;
         *  }
         *
         *  const module = new Module<IModule>();
         *  module.addFactoryFunction("todoService", (moduleContext: IModule) => new TodoService());
         *
         * @param propertyKey The property to map the result of the factory function to.
         * @param factoryFunction The factory function.
         * @param injectionKeys Optional keys to be injected into instance constructor call
         */
        public bindFactoryFunction<E, Args= Required<MC>>(
                propertyKey: TypedPropertyRestriction<MC, E>,
                factoryFunction: FactoryFunction<E, Args>,
                injectionKeys: InjectionProperties<Args> | null = null,
        ): this {
            this.target.creationStrategies[propertyKey.toString()] = {
                factoryFunction,
                injectionKeys,
            };
            this.addProperties(propertyKey.toString(), injectionKeys);
            return this;
        }

        protected hasValues(): boolean {
            return Object.keys(this.target.values).length > 0;
        }

        private addProperties<Args= Partial<MC>>(propertyKey: string, injectionKeys?: CwbInject.InjectionProperties<Args> | null) {
            this.propertySet.add(propertyKey);
            Object.keys(injectionKeys || {}).forEach((k) => {
                return this.propertySet.add(k);
            });
        }

        private async createOrGetValue(propertyKey: string): Promise<any> {
            if (this.target.values[propertyKey] !== undefined) {
                return this.target.values[propertyKey];
            }

            // todo mark value entry if it was already created, but returned undefined | null.

            const creationProps = this.getCreationStrategies(propertyKey);
            this.target.values[propertyKey] = await this.createNewInstance(creationProps, propertyKey);
            return this.target.values[propertyKey];
        }

        private getCreationStrategies(propertyKey: string) {
            const creationProps = this.target.creationStrategies[propertyKey] as ICreationStrategies<any, any, any>;
            CwbInject.Module.assertCreationProps(propertyKey, creationProps);
            return creationProps;
        }

        private async createNewInstance(creationProps: ICreationStrategies<any, any, any>, propertyKey: string) {
            const me = this;
            const deps: IDependencies<MC> = {

                get: function<K extends keyof MC>(p: K): Promise<MC[K]> {
                    if (propertyKey === p) {
                        throw new Error(`circular injection detected for property ${p}`);
                    }
                    return me.createOrGetValue(p as string);
                }
            };

            let instance: any | undefined;
            if (creationProps.factoryFunction) {
                instance = await creationProps.factoryFunction(deps);
            }
            return instance;
        }
    }

    interface IStrategiesAndValuesCache<C> {
        values: {[s: string]: any};
        creationStrategies: ICreationStrategiesMap;
    }

    function createInstance<T, Args>(c: ArgsConstructor<T, Args>, args: Args): T {
        return new c(args);
    }
}

export default CwbInject;
