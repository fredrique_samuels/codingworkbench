
import {deepStrictEqual, ok} from 'assert'

import IConnectionAware, {connection_aware_close} from "@codingworkbench/data/IConnectionAware";
import CwbLog from "@codingworkbench/log"
import ConnectionAwareTestInjector from "@codingworkbench/data/ConnectionAwareTestInjector";
import {ID} from "@codingworkbench/data/IUnique";
import CwbSearch from '@codingworkbench/search';

import IAutoPersistedObject from "../lib/IAutoPersistedObject";
import {DataStructure} from "../lib/DataStructure";

interface IList<T extends IAutoPersistedObject> {
    add(item: T): Promise<this>;
    remove(item: T): Promise<this>;
    removeById(id: ID): Promise<this>;
    get(index: number): Promise<T>;
    forEach(callback: (item: T) => Promise<any>): Promise<void>;
    map<R>(callback: (item: T) => Promise<R>): Promise<R>;
    search(search: CwbSearch.Search): Promise<CwbSearch.IResults<T>>
}

interface ReferenceType<T> {

}

interface IAutoPersistedObjectProxy<T extends IAutoPersistedObject> {
    setValue<K extends keyof T>(p: K, value: T[K]): Promise<void>;
    getValue<K extends keyof T>(p: K): Promise<T[K]>;
}

interface IProject extends IAutoPersistedObject {
    name: string;
}

interface ITask extends IAutoPersistedObject {
    title: string;
    description: string;
    dueDate: Date;
}

describe('ProjectExample', function() {

    this.timeout(20000);

    let unit: IConnectionAware;

    before(async () => {
        CwbLog.setup();
        CwbLog.enable_console();
        const connectionInjector = new ConnectionAwareTestInjector();
        unit = await connectionInjector.getAll()
    });

    after(async () => {
        await connection_aware_close(unit);
    });

    it("#persists", (done) => {

        async function _e() {
        //     // given
        //     const dataTypeDef = new DataStructure(unit.mongoDao).update({
        //         ...new DataStructure(unit.mongoDao).createParams(),
        //         name: await CwbTestCase.generateUUID()
        //
        //     });
        //     await dataTypeDef.save();
        //
        //     // when
        //     const [obj] = await new DataTypeDao(unit.mongoDao).getByIds([dataTypeDef.id]);
        //
        //     // then
        //     ok(obj);
        //
        //     // const personType = new DynamicDataStructure("cwb.test.Person")
        //     //     .addProperty("name", "text")
        //     //     // .addField("age", "number", {required})
        //     //     .addProperty("birthdate", "date");
        //
        //     // const dynamicDataSource = new DynamicDataSource();
        //
        //     // person = await dynamicDataSource.create(personType, {name: "Paul", birthdate, new Date("1987-10-14")})
        }

        _e().then(done).catch(done)
    });


    async function create_project_type() {
        await new DataStructure(unit.mongoDao)
                .setName("com.test.Project")
                .addProperty("name", "text", {})
                .save();
    }
});
