import * as path from "path"
import * as fs from "fs"
import * as tmp from "tmp"

import CwbLog from "@codingworkbench/log";

import IOption from "../../common/IOption";

import ISlicer from "../ISlicer";
import ProcessHandle from "../../process/ProcessHandle";

import {CURAPaths} from "./CURAPaths";
import CURASliceSummaryExtractor from "./CURASliceSummaryExtractor";
import ISliceJobOutput from "lib/print/ISliceJobOutput";
import ISliceJobInput from "../ISliceJobInput";
import { print_summary_merge, print_summary_create } from "../IPrintSummary";

export default class CURASlicer implements ISlicer {

    private readonly enginePaths: CURAPaths;

    constructor(enginePaths: CURAPaths) {
        this.enginePaths = enginePaths;
    }

    public async getProfileOptions(): Promise<IOption[]> {
        return [];
    }

    public async getPrintOptions(): Promise<IOption[]> {
        return [];
    }

    private async runJob(id: string, stlFile: string, input: ISliceJobInput): Promise<ISliceJobOutput> {
        const tempGcodeFile = tmp.fileSync();

        CwbLog.info(`Slice requested ${stlFile}`);

        let command = this.buildSlicerCommand(stlFile, tempGcodeFile.name, input);

        CwbLog.info(`Executing slicer command \n${command}`);

        return new ProcessHandle().execute(command)
            .then(pr => {
                const {stdout, stderr, errorMessage} = pr;

                const stdio = {
                    stderr: stderr,
                    stdout: stdout,
                    error: errorMessage
                };

                const results = new CURASliceSummaryExtractor(fs.readFileSync(tempGcodeFile.name, "utf-8")).extractFromOutput(stderr);

                tempGcodeFile.removeCallback();

                CwbLog.info(`${id}: Slicer operation summary \n${JSON.stringify(print_summary_create(results.summary), null, 4)}`);
                return {
                    stdio: stdio,
                    results: [results],
                    summary: print_summary_create(results.summary)
                }
            }) 
    }

    public async sliceString(id: string, stlString: string, input: ISliceJobInput): Promise<ISliceJobOutput> {
        const tempStlFile = tmp.fileSync();
        
        fs.writeSync(tempStlFile.fd, stlString);

        const sliceResults = await this.runJob(id, tempStlFile.name, input);

        tempStlFile.removeCallback();
        return sliceResults
    }

    public async sliceFile(path: string, input: ISliceJobInput): Promise<ISliceJobOutput> {
        return await this.runJob(path, path, input);
    }

    private buildSlicerCommand(stlFile: string, gCodeFile: string, input: ISliceJobInput): string {
        const {home, definitions, engine} = this.enginePaths;
        const definitionTarget = input.profile.value;

        const app = `${path.join(home, engine)}`;
        const definition = `${path.join(home, definitions, definitionTarget)}`;

        CwbLog.info(`Using definition ${definition}`);

        // const definitionFileObj = new CURADefinitionProvider(this.enginePaths).getDefinitionFileObj(definitionTarget);
        // const mergedTargetFile = `${path.join(home, definitions, definitionTarget.replace(".def.json", ""))}_merged.def.json`;
        // fs.writeFileSync(mergedTargetFile, JSON.stringify(definitionFileObj, null, 4));

        // console.log('Filedescriptor: ', tmpobj.fd);


        // console.log('File: ', tmpobj.name);
        // const tmpobj = tmp.fileSync();
        // fs.writeSync(tmpobj.fd, JSON.stringify(definitionFileObj));

        const settingString = `-s material_bed_temperature_layer_0="60" -s material_print_temperature_layer_0="200" -s layer_height="0.2" -s machine_depth=".2" -s layer_height_0="0.2" -s material_diameter="1.75"`;
        return `${app} slice -v -p -j ${definition} ${settingString} -e0 -l ${stlFile} -o ${gCodeFile}`
    }
}
