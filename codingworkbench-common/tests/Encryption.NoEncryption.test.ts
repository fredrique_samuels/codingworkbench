import {deepStrictEqual, ok} from 'assert'
import Encrypt from 'lib/Encrypt';

describe('EncryptionPolicy.NoEncryption', function() {

    it ("EncryptionPolicy.none", (done) => {

        async function _e() {
            ok(await Encrypt.noEncryption())
        }

        _e().then(() => done())
            .catch(done);
    });

    it ("encrypt", (done) => {

        async function _e() {
            let policy = await Encrypt.noEncryption();
            deepStrictEqual(await policy.encrypt("input"), "input")
        }

        _e().then(() => done())
            .catch(done);
    });

});
