import * as React from "react";

import {handle_input_state_change, IFormInputsAware} from "@codingworkbench/react/form/FormInputs";
import {ICredentials} from "@codingworkbench/common/ICredentials";
import {ISecurityForm} from "./SecurityForm";
import TextField from '@material-ui/core/TextField';
import {FormControlLabel, Checkbox} from "@material-ui/core";


type FormInputChangeHandler<I> = (name: keyof I, state: IFormInputsAware<I>, setState: (s: any) => void) => void;

export interface IStateControl<S> {
    state: S;
    setState: (s: any) => void;
}

export function SecurityEmailInput(props: IStateControl<ISecurityForm>) {
    return <TextField
        variant="outlined"
        required
        fullWidth
        id="email"
        label="Email Address"
        name="email"
        autoComplete="email"
        disabled={props.state.loading}
        value={props.state.inputs.user}
        onChange={handle_input_state_change<ICredentials>("user", props.state, props.setState)}
    />;
}

export function SecurityPasswordInput(props: IStateControl<ISecurityForm>) {
    return <TextField
        variant="outlined"
        required
        fullWidth
        name="password"
        label="Password"
        type="password"
        id="password"
        autoComplete="current-password"
        disabled={props.state.loading}
        value={props.state.inputs.secret}
        onChange={handle_input_state_change<ICredentials>("secret", props.state, props.setState)}
    />;
}

export function SecurityReCaptcha(props: IStateControl<ISecurityForm>) {
    return <FormControlLabel
        control={<Checkbox value="allowExtraEmails" color="primary" />}
        label="I am not a robot"
    />;
}

