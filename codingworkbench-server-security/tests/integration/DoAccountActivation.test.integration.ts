import {deepStrictEqual, ok} from "assert";

import {mock_request_context} from "../../lib/services/common/test/mock_requests";
import DoAccountActivation from "../../lib/services/security/server/handlers/DoAccountActivation";
import ISecurityAwareContext from "../../lib/services/security/api/context";
import SecurityAwareTestCase from "../../lib/services/security/test/SecurityAwareTestCase";
import CwbLog from "@codingworkbench/log";

export default function runTests() {

    return () => {

        let unit: ISecurityAwareContext;

        before(async () => {
            CwbLog.setup();
            CwbLog.enable_console();
            unit = await SecurityAwareTestCase.create_context();
        });

        after(async () => {
            await SecurityAwareTestCase.destroy(unit)
        });

        it("account activated", (done) => {
            async function _e() {
                // given
                const {account} = await SecurityAwareTestCase.do_signup_test_user(unit);
                const requestContext = mock_request_context(unit, { params: {token: account.activation_token} });

                // when
                await DoAccountActivation({links: unit.securityLinks}).handlerAsync(requestContext);

                // then
                await SecurityAwareTestCase.assert_account_activated(unit, account.id);
                SecurityAwareTestCase.assert_security_view(requestContext, unit.securityLinks.activateAccountSuccessView)
            }

            _e().then(() => done())
                .catch(done);
        });

        it("invalid token redirect to sign page", (done) => {
            async function _e() {
                // given
                const {account} = await SecurityAwareTestCase.do_signup_test_user(unit);
                const requestContext = mock_request_context(unit, { params: {token: "NOT VALID"} });

                // when
                await DoAccountActivation({links: unit.securityLinks}).handlerAsync(requestContext);

                // then
                await SecurityAwareTestCase.assert_account_not_activated(unit, account.id);
                SecurityAwareTestCase.assert_redirect(requestContext, unit.securityLinks.signInPage)
            }

            _e().then(() => done())
                .catch(done);
        });

        it("activated account redirects to sign in page", (done) => {
            async function _e() {
                // given
                const {account} = await SecurityAwareTestCase.do_signup_test_user(unit);
                {
                    const requestContext = mock_request_context(unit, {params: {token: account.activation_token}});
                    await DoAccountActivation({links: unit.securityLinks}).handlerAsync(requestContext);
                }

                {
                    const requestContext = mock_request_context(unit, {params: {token: account.activation_token}});
                    await DoAccountActivation({links: unit.securityLinks}).handlerAsync(requestContext);
                    SecurityAwareTestCase.assert_redirect(requestContext, unit.securityLinks.signInPage)
                }
            }

            _e().then(() => done())
                .catch(done);
        });

    }
};


describe('DoAccountActivation.test.integration', runTests());
