import {HasChildPredicate, IDataAccessor, IElement, INode, NodeType, SearchChildNamesFunction} from "./api";

export type Child<T> = AttributeDefinition<any, T> | ElementDefinition<any, T> | ElementGroupDefinition<any, T>
export type Children<T> = {[s: string]: Child<T> }


export interface AttributeDefinition<T, P=any> extends IDataAccessor<T, P> {
    type: NodeType.ATTRIBUTE;
}

export interface ElementDefinition<T, P=T> extends IDataAccessor<T, P> {
    type: NodeType.ELEMENT;
    tag: string;
    children: Children<T>;
}

export interface ElementGroupDefinition<T, P=T> {
    type: NodeType.ELEMENT_GROUP;
    tag: string;
    hasChildFunction: HasChildPredicate<P>;
    searchChildNamesFunction: SearchChildNamesFunction<P>;
    elementDefinition: ElementDefinition<T, P>;
}


