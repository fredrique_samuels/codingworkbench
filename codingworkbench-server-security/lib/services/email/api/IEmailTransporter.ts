import {ICredentials} from "@codingworkbench/common/ICredentials";


export default interface IEmailTransporter {
    transport(credentials: ICredentials, mail: any): Promise<void>
}
