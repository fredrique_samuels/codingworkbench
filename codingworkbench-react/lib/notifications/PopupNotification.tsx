import * as React from "react";

import Snackbar, {SnackbarOrigin} from "@material-ui/core/Snackbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/core/SvgIcon/SvgIcon";
import IPage from "../page/IPage";
import {IOpenClose} from "../common/OpenClose";


import styles from "./styles";
import Message from "./Message";
import ActionableMessage from "./ActionableMessage";


function fromOptional(origin?: PopupNotification.Origin): SnackbarOrigin {
    return origin
        ? OriginMapping[origin.toString()]
        : OriginMapping[PopupNotification.Origin.TOP_RIGHT.toString()];
}


/**
 * Notification that can be anchored to the corners of the screen with a message.
 *
 * @constructor
 */
function PopupNotification(props: PopupNotification.Props) {
    const classes = styles();
    const {open, onClose, type, autoHideDuration, message, origin} = props;

    return <Snackbar
        anchorOrigin={fromOptional(origin)}
        open={open}
        autoHideDuration={autoHideDuration}
        onClose={onClose}
    >
        <ActionableMessage
            type={Message.fromOptional(type)}
            message={message}
            className={classes.margin}
            actions={[
                <IconButton key="close" aria-label="close" color="inherit" onClick={onClose}>
                    <CloseIcon className={classes.icon} />
                </IconButton>,
            ]}
        />
    </Snackbar>
}


namespace PopupNotification {


    /**
     * Enum to anchor the notification to the screen locations.
     *
     * @see SnackbarOrigin
     */
    export enum Origin {
        TOP_LEFT,
        TOP_CENTER,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_CENTER,
        BOTTOM_RIGHT
    }

    /**
     * Auto hide type declaration.
     *
     * @see SnackbarProps#autoHideDuration
     */
    export type AutoHideDuration = number | null | undefined;

    /**
     * Notification property fieids.
     */
    export interface Props extends IOpenClose {
        /**
         * The notification message.
         */
        message: string;

        /**
         * Optional callback fro when the notification is closed.
         */
        onClose?: () => void;

        /**
         * The message type. Default to info.
         */
        type?: Message.Type;

        /**
         * Audo hide settings. Default to not hiding after 5 seconds.
         */
        autoHideDuration?: AutoHideDuration;

        /**
         * The location of the popup on screen. Default to the top-right.
         * @see Origin
         */
        origin?: Origin;
    }

    export function initProps<S>(page: IPage<S>, target: keyof S): Props {


        const handleClose = (event?: React.SyntheticEvent, reason?: string) => {
            /* ignore clickaway events */
            if (reason === 'clickaway') {
                return;
            }
            page.close(target);
        };

        return {
            open: false,
            message: "",
            onClose: handleClose,
            type: "info",
            autoHideDuration: null
        }
    }

    /**
     * Options for pushing notification to the ViewContext.
     */
    export type Options = {
        autoHideDuration: AutoHideDuration;
    }

    /**
     * Push a warning message to the screen.
     *
     * @param page The view context.
     * @param target the property of the PopupNotification on the state.
     * @param message The message to display
     * @param options Notification options.
     */
    export function pushWarning<S>(page: IPage<S>, target: keyof S, message: string, options?: Options): void {
        pn(page, target, message, "warning", options);
    }

    /**
     * Push an error message to the screen.
     *
     * @param page The view context.
     * @param target the property of the PopupNotification on the state.
     * @param message The message to display
     * @param options Notification options.
     */
    export function pushError<S>(page: IPage<S>, target: keyof S, message: string, options?: Options): void {
        pn(page, target, message, "error", options);
    }

    /**
     * Push an info message to the screen.
     *
     * @param page The view context.
     * @param target the property of the PopupNotification on the state.
     * @param message The message to display
     * @param options Notification options.
     */
    export function pushInfo<S>(page: IPage<S>, target: keyof S, message: string, options?: Options): void {
        pn(page, target, message, "info", options);
    }

    /**
     * Push a success message to the screen.
     *
     * @param page The view context.
     * @param target the property of the PopupNotification on the state.
     * @param message The message to display
     * @param options Notification options.
     */
    export function pushSuccess<S>(page: IPage<S>, target: keyof S, message: string, options?: Options): void {
        pn(page, target, message, "success", options);
    }

    /* not exposed, but does all the work. */
    function pn<S>(page: IPage<S>, target: keyof S, message: string, type: string, options?: Options) {
        const op: Options = {
            autoHideDuration: 5000,
            ...options
        };

        page.open(target, {
            message: message,
            type: type,
            autoHideDuration: op.autoHideDuration
        })
    }

}

export default PopupNotification;


const OriginMapping: {[s: string]: SnackbarOrigin} = {};
OriginMapping[PopupNotification.Origin.TOP_LEFT.toString()] =  { vertical: 'top', horizontal: 'left' };
OriginMapping[PopupNotification.Origin.TOP_CENTER.toString()] =  { vertical: 'top', horizontal: 'center' };
OriginMapping[PopupNotification.Origin.TOP_RIGHT.toString()] =  { vertical: 'top', horizontal: 'right' };
OriginMapping[PopupNotification.Origin.BOTTOM_LEFT.toString()] =  { vertical: 'bottom', horizontal: 'left' };
OriginMapping[PopupNotification.Origin.BOTTOM_CENTER.toString()] =  { vertical: 'bottom', horizontal: 'center' };
OriginMapping[PopupNotification.Origin.BOTTOM_RIGHT.toString()] =  { vertical: 'bottom', horizontal: 'right' };

