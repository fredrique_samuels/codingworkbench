import * as a from "./IActionResponse";
import * as c from "./IClickAction";
import * as _ActionResponseBuilder from "./ActionResponseBuilder";

namespace CwbAction {
    export import IResponse = a.IActionResponse;
    export import IClick = c.IClickAction;

    export import ResponseBuilder = _ActionResponseBuilder.ActionResponseBuilder;
}

export default CwbAction;
