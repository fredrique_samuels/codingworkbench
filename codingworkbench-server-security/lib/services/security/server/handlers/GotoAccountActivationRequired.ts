import {ISecurityHandlerContext, ISecurityHandlerFactoryContext} from "../../api/context";
import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {create_security_model, web_respond_with_500} from "../../api/response";
import {RequestHandlerAsync} from "@codingworkbench/server/common";
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import CwbLog from "@codingworkbench/log";

export default function GotoAccountActivationRequired<C extends ISecurityHandlerContext>(factoryContext: ISecurityHandlerFactoryContext): IRequestHandler<C> {

    const handler: RequestHandlerAsync<C> = async (reqContext) => {
        // todo encrypt email should be encrypted
        const email = decodeURIComponent(reqContext.request.params.email || "");
        const {response} = reqContext;
        const {securityService, securityLinks, viewFactory} = reqContext.context;

        const gotoSignInPage = () => response.redirect(securityLinks.signInPage);
        const view = () => {
            const model = create_security_model(factoryContext.links)
                .put("email", email)
                .build();

            const view = viewFactory.modelAndView(securityLinks.accountActivationRequiredView, model);
            response.view(view);
        };

        if (!email) {
            gotoSignInPage();
            return;
        }

        try {
            const actionResponse = await securityService.hasPendingActivationAccounts(email);
            if(actionResponse.error) {
                CwbLog.error(new Error(actionResponse.message));
                gotoSignInPage();
            } else if (actionResponse.data) {
                view();
            } else {
                gotoSignInPage()
            }
        } catch (e) {
            CwbLog.error(e);
            gotoSignInPage();
        }


    };
    return new RequestHandlerBuilder(factoryContext.links.accountActivationRequiredPage, handler).build();
}
