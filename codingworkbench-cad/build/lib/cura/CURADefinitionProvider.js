"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var jsonMerger = require("json-merger");
var path = require("path");
var CURADefinitionProvider = (function () {
    function CURADefinitionProvider(enginePaths) {
        this.enginePaths = enginePaths;
    }
    CURADefinitionProvider.prototype.getDefinitionFileObj = function (definitionTarget) {
        var definitionsObjs = [this.loadDefObj(definitionTarget)];
        while (definitionsObjs[definitionsObjs.length - 1].inherits) {
            var o = definitionsObjs[definitionsObjs.length - 1];
            definitionsObjs.push(this.loadDefObj(o.inherits + ".def.json"));
        }
        var merged = jsonMerger.mergeObjects(definitionsObjs.reverse());
        merged.inherits = undefined;
        return merged;
    };
    CURADefinitionProvider.prototype.loadDefObj = function (definitionTarget) {
        return JSON.parse(fs.readFileSync(this.getDefPath(definitionTarget), "utf-8"));
    };
    CURADefinitionProvider.prototype.getDefPath = function (definitionTarget) {
        var _a = this.enginePaths, home = _a.home, definitions = _a.definitions;
        return "" + path.join(home, definitions, definitionTarget);
    };
    return CURADefinitionProvider;
}());
exports.CURADefinitionProvider = CURADefinitionProvider;
