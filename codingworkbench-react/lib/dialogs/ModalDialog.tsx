import * as React from 'react';
import PageDialog, {PageDialogProps} from './PageDialog';

export function ModalDialog(props: ModalDialogProps) {
    const dialogProps: PageDialogProps = {
        ... props,
        ... {
            variant: "user"
        }
    };

    return (
        <PageDialog { ... dialogProps } />
    );
}

export interface ModalDialogProps extends PageDialogProps {

}

export default ModalDialog;
