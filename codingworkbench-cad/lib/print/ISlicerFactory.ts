import ISlicer from "./ISlicer";

export interface ISlicerFactory {
    create(): ISlicer;
}
