import IAuditAware from "@codingworkbench/data/IAuditAware";
import IUnique from "@codingworkbench/data/IUnique";
import IArchiveAware from "@codingworkbench/data/IArchiveAware";

export interface IVersionedObject extends IAuditAware, IUnique, IArchiveAware {
    version: number;
    orphaned: boolean;
}
