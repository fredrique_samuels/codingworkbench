import {IEmailContent} from "./IEmailContent";

export interface IEmailService {

    /**
     * Send a raw email from the default sender.
     *
     *
     * @param source {String}
     * @param mail {IEmailContent}
     */
    sendRawHtmlMail(source: string, mail: IEmailContent): Promise<void>;
}

export default IEmailService;
