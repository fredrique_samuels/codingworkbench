/// <reference types="node" />
import * as child_process from "child_process";
import IProcessOutput from "./IProcessOutput";
export default class ProcessOutputCapture implements IProcessOutput {
    private _error;
    private _stdout;
    private _stderr;
    capture(error: child_process.ExecException, stdout: string, stderr: string): void;
    readonly errorMessage: string;
    readonly error: Error;
    readonly stdout: string;
    readonly stderr: string;
}
