import IOption from "../common/IOption";
import ISliceJobOutput from "./ISliceJobOutput";
import ISliceJobInput from "./ISliceJobInput";

export default interface ISlicer {
    getProfileOptions(): Promise<IOption[]>;
    getPrintOptions(): Promise<IOption[]>;
    sliceString(id: string, stlFile: string, input: ISliceJobInput): Promise<ISliceJobOutput>;
    sliceFile(path: string, input: ISliceJobInput): Promise<ISliceJobOutput>;
} 

