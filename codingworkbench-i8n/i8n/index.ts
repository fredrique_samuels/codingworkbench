import * as _IMessageSource from "./IMessageSource";
import * as _DefaultMessageSource from "./DefaultMessageSource";
import * as _IMessageSourceAware from "./IMessageSourceAware";
import * as _MessageSourceUtil from "./MessageSourceUtil";

namespace CwbI8N {

    export import IMessageSource = _IMessageSource.IMessageSource;
    export import MessageKeys = _IMessageSource.MessageKeys;
    export import DefaultMessageSource = _DefaultMessageSource.DefaultMessageSource;
    export import IMessageSourceAware = _IMessageSourceAware.IMessageSourceAware;
    export import actionResponseMessage = _MessageSourceUtil.actionResponseMessage;

}

export default CwbI8N;
