import {read_html_model} from "@codingworkbench/server/HtmlModel";
import {create_secure_page_url, ISecurityLinks, security_links_read} from "../../api/links";
import {ICredentials} from "@codingworkbench/common/ICredentials";
import {IFormValidation} from "@codingworkbench/react/form/FormInputs";
import ViewController from "../../../common/ViewController";


namespace SecurityViewController {

    export function get_site_links(): ISecurityLinks {
        const htmlModel = read_html_model();
        return  security_links_read(htmlModel);
    }

    export function goto_signin() {
        ViewController.redirect(get_site_links().signInPage)
    }

    export function goto_forgot_password() {
        ViewController.redirect(get_site_links().forgotPasswordPage)
    }

    export function  goto_signup() {
        ViewController.redirect(get_site_links().signUpPage)
    }

    export function goto_user_home() {
        ViewController.redirect(create_secure_page_url(get_site_links().userHomePage, get_site_links()));
    }

    export async function do_resend_activation_link(email: string): Promise<IFormValidation> {
        return await ViewController.submit_form(get_site_links().doResendActivationLink, {email});
    }

    export function do_signout() {
        ViewController.do_post_action(get_site_links().doSignOut).catch(console.error);
    }

    export async function do_signin(iCredentials: ICredentials): Promise<IFormValidation> {
        return await ViewController.submit_form(get_site_links().doSignIn, iCredentials);
    }

    export async function do_signup(iCredentials: ICredentials) {
        return await ViewController.submit_form(get_site_links().doSignUp, iCredentials);
    }
}

export default SecurityViewController;
