
/**
 * Simple click action interface.
 */
export interface IClickAction<E = any> {

    /**
     * The action name.
     */
    name: string;

    /**
     * The action execution method.
     *
     * @param e The action event.
     */
    execute: (e?: E) => void;
}

export default IClickAction;
