import * as mysql from "mysql";
import * as process from "process";

import PropertiesFileReader from "@codingworkbench/common/PropertiesFileReader";

import {DbConnection} from "./DbConnection";
import {DbQueryRunner} from "./DbQueryRunner";

import {DbTable} from "./DbTable";
import {DbUpdateRunner} from "./DbUpdateRunner";
import {MysqlQueryParamsFactory} from "./MysqlQueries";
import {DbQuery} from "./DbQuery";
import {DbQueryFactory} from "./DbQueryFactory";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";

class UpdateImpl implements DbUpdateRunner {
    private pool: mysql.Pool;
    private readonly q: DbQuery;

    constructor(pool: mysql.Pool, q: DbQuery) {
        this.pool = pool;
        this.q = q;
    }

    async execute(): Promise<any[]> {
        const {pool, q} = this;

        return new Promise((resolve, reject) => {
            const {command, values} = q;
            pool.query(command, values, function(err, result) {
                if (err) {
                    reject(err);
                } else {
                    resolve([{id: result.insertId}]);
                }
            });
        });
    }
}

function map_columns_to_fields(table: DbTable, rows: any[]): any[] {
    const columns = table.get_columns;
    return rows.map(row => {
        const entry: { [s: string]: any } = {};
        columns.forEach(c => {
            entry[c.field] = row[c.field]
        });
        return entry;
    })
}

class QueryImpl implements DbQueryRunner {
    private pool: mysql.Pool;
    private readonly q: DbQuery;

    constructor(pool: mysql.Pool, q: DbQuery) {
        this.pool = pool;
        this.q = q;
    }

    async query(): Promise<any[]> {
        const {pool, q} = this;

        return new Promise((resolve, reject) => {
            const {command, values} = q;
            pool.query(command, values, function(err, results) {
                if (err) {
                    reject(err);
                } else {
                    if (Array.isArray(results)) {
                        if (q.targetResult === 'RAW') {
                            resolve( results.map((r) => {return {...r};}) );
                        } else if (q.targetResult === 'COUNT') {
                            resolve([{...results[0]}]);
                        } else if(q.table) {
                            resolve(map_columns_to_fields(q.table, results));
                        } else {
                            resolve([])
                        }
                    } else {
                        resolve([])
                    }
                }
            });
        });
    }
}

export class MysqlConnection implements DbConnection {

    private readonly connectionUri: string | mysql.ConnectionConfig;
    private _conn: mysql.Pool | null;
    private _connected: boolean;
    private queryFactory: MysqlQueryParamsFactory;

    constructor(connectionUri: string | mysql.ConnectionConfig) {
        this._conn = null;
        this._connected = false;
        this.connectionUri = connectionUri;
        this.queryFactory = new MysqlQueryParamsFactory();
    }

    async close(): Promise<void> {
        if (this._conn) {
            this._conn.end();
        }
    }

    get connected(): boolean {
        return this._connected;
    }

    public async connect(): Promise<void> {
        await this.testConnection();
        const conn: mysql.Pool = mysql.createPool(this.connectionUri);
        this._conn = conn;
    }

    private testConnection(): Promise<void> {
        const me = this;
        const test = mysql.createConnection(this.connectionUri);
        return new Promise<void>((resolve, reject) => {
            const processError = (err: mysql.MysqlError) => {
                if (err) {
                    me._connected = false;
                } else {
                    me._connected = test.state === "connected" || test.state === "authenticated";
                }
                resolve();
            };

            test.connect(processError);
        }).finally(() => {
            me._connected = test.state === "connected" || test.state === "authenticated";
            test.destroy();
        });
    }

    async createQuery<T>(q: DbQuery): Promise<DbQueryRunner> {
        if (this._conn) {
            return new QueryImpl(this._conn, q);
        } else {
            throw new Error("No Database connection");
        }
    }

    async createUpdate(q: DbQuery): Promise<DbUpdateRunner> {
        if (this._conn) {
            return new UpdateImpl(this._conn, q);
        } else {
            throw new Error("No Database connection");
        }
    }

    async execute(q: DbQuery): Promise<any[]> {
        return (await this.createQuery(q)).query();
    }

    public getQueryFactory(): DbQueryFactory {
        return this.queryFactory;
    }


    /**
     * Read connection params from a properties file.
     *
     * The properties are as follows.
     *
     * database.{env}.user=root
     * database.{env}.password=
     * database.{env}.schema=dev
     * database.{env}.host=127.0.0.1
     * database.{env}.port=5432
     *
     * The placeholder 'env' is read from process.env.NODE_ENV and default to 'test'.
     *
     * @return A connection string 'mysql://{user}:{password}@{host}:{port}/{schema}'
     */
    public static readConnectionString(path: string, scope?: string): string {
        let properties = new PropertiesFileReader(path);
        let propertyEnv = this.extractPropertyEnv(scope);

        let user = properties.getString(`database.${propertyEnv}.user`, "root");
        let password = properties.getString(`database.${propertyEnv}.password`, "");
        let schema = properties.getString(`database.${propertyEnv}.schema`, "");
        let host = properties.getString(`database.${propertyEnv}.host`, "127.0.0.1");
        let port = properties.getNumber(`database.${propertyEnv}.port`);

        return `mysql://${user}${password ? ":" + password : ""}@${host}:${port}${(schema ? `/${schema}` : "")}?createDatabaseIfNotExist=true&autoReconnect=true&maxReconnects=5&useUnicode=true&characterEncoding=UTF-8`;
    }

    public static readConnectionStringNoCredentials(path: string, scope?: string): string {
        return this.getConnectionStringNoCredentialsFromReader(new PropertiesFileReader(path), scope);
    }

    public static getConnectionStringNoCredentialsFromReader(properties: IPropertiesReader, scope?: string) {
        let propertyEnv = this.extractPropertyEnv(scope);
        let schema = properties.getString(`database.${propertyEnv}.schema`, "");
        let host = properties.getString(`database.${propertyEnv}.host`, "127.0.0.1");
        let port = properties.getNumber(`database.${propertyEnv}.port`);

        return `mysql://${host}:${port}/${schema}?createDatabaseIfNotExist=true&autoReconnect=true&maxReconnects=5&useUnicode=true&characterEncoding=UTF-8`;
    }

    private static extractPropertyEnv(scope?: string): string {
        return `${(scope ? `${scope}.` : "")}${(process.env.NODE_ENV || "test")}`
    }

    /**
     * Read connection params from a properties file.
     *
     * The properties are as follows.
     *
     * database.{env}.user=root
     * database.{env}.password=
     * database.{env}.schema=dev
     * database.{env}.host=127.0.0.1
     * database.{env}.port=5432
     *
     * The placeholder 'env' is read from process.env.NODE_ENV and default to 'test'.
     *
     * @return A connection string 'mysql://{user}:{password}@{host}:{port}/{schema}'
     */
    public static readConnectionConfig(path: string, scope?: string): mysql.ConnectionConfig {
        const properties = new PropertiesFileReader(path);
        return this.getConnectionConfigFromReader(properties, scope);
    }

    public static getConnectionConfigFromReader(properties: IPropertiesReader, scope?: string) {

        let propertyEnv = this.extractPropertyEnv(scope);
        let user = properties.getString(`database.${propertyEnv}.user`, "root");
        let password = properties.getString(`database.${propertyEnv}.password`, "");
        let schema = properties.getString(`database.${propertyEnv}.schema`, "");
        let host = properties.getString(`database.${propertyEnv}.host`, "127.0.0.1");
        let port = properties.getNumber(`database.${propertyEnv}.port`);

        return {
            user: user,
            password: password,
            database: schema,
            host: host,
            port: port
        };
    }


}

export default MysqlConnection;

