import IAuditAware from "@codingworkbench/data/IAuditAware";
import IUnique, {ID} from "@codingworkbench/data/IUnique";
import CwbSearch from "@codingworkbench/search";
import {IVersionedObject} from "./IVersionedObject";


export const TEXT_TYPE = "TEXT";
export const DECMAL_TYPE = "DECIMAL";
export const NUMBER_TYPE = "NUMBER";
export const DATE_TYPE = "DATE";
export const TIME_TYPE = "TIME";
export const BOOLEAN = "BOOLEAN";

export const JSON_TYPE = "JSON";
export const EMAIL_TYPE = "EMAIL";
export const ID_TYPE = "ID";
export const CONSTANT_TYPE = "CONSTANT";


export default interface IDataStructure extends IVersionedObject {
    name: string;
    properties: {[s: string]: IDataStructureProperty};
}

export interface IPropertyOptions {

}

export interface IDataStructureProperty extends IVersionedObject {
    parent: ID;
    name: string;
    type: string;
    options: IPropertyOptions;
}

export interface IObjectInstance extends IVersionedObject {
    datatype: ID[];
    data: {};
}
