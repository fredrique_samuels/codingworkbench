import CwbNode from "../../../node";

import {EmployeeStore, IEmployee} from "../employee";
import {addressElement} from "./address_element";


const read_name = (data: IEmployee | null) => data ? data.name : null;
const write_name = (value: string, data: IEmployee | null) => {
    if (data && value) {
        data.name = value
    }
};
const nameAttribute = new CwbNode.AttributeBuilder<string, IEmployee>(read_name).setWriteFunc(write_name).build();

export function employee_add_children(builder: CwbNode.AbstractElementBuilder<IEmployee, any>) {
    return builder.addChild("name", nameAttribute).addChild("address", addressElement);
}

export const EMPLOYEE_TAG = "employee-data";

export function createEmployeeRoot(employee: IEmployee, errorFunction?: CwbNode.ErrorFunction): CwbNode.IRootElement<IEmployee> {
    const builder = new CwbNode.NodeTreeBuilder<IEmployee>(EMPLOYEE_TAG, employee);
    if (errorFunction) {
        builder.setErrorFunction(errorFunction);
    }
    employee_add_children(builder);
    return builder.build();
}

export function createEmployeeStored(): CwbNode.ElementDefinition<IEmployee, EmployeeStore> {
    const builder = new CwbNode.ElementBuilder<IEmployee, EmployeeStore>(EMPLOYEE_TAG, (store, name) => store ? store[name] : null );
    employee_add_children(builder);
    return builder.build();
}
