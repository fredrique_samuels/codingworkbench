import {ID} from "./IUnique";


export interface IUserSession {
    userId: ID;
    accountId: ID;
    created: number;
    ttl: number;
}

export default IUserSession;

