var http = require('http');
var fs = require('fs');

function home(req, res) {
    res.write('Hello World!'); //write a response to the client
    res.end(); //end the response
}

function read_file(req, res, f) {
    console.log(f)  
    fs.readFile(f, function (err,data) {
        if (err) {
          res.writeHead(404);
          res.end(JSON.stringify(err));
          return;
        }
        res.writeHead(200);
        res.end(data);
      });
}

const handlers = {
    "/": function (req, res) {
        res.write('Hello World!'); //write a response to the client
        res.end(); //end the response
    }
}

const STATIC_PATH = "/cdn"

//create a server object:
http.createServer(function (req, res) {
  console.log(req.url)  
  if (req.url.startsWith(STATIC_PATH)) {
    let f = __dirname + req.url.replace(STATIC_PATH, "")
    read_file(req, res, f)
  } else {
    read_file(req, res,"cwb_logo.html")
  }
}).listen(5050); //the server object listens on port 8080