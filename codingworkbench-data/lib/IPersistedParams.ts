import {IUnique} from "./IUnique";
import {IAuditAware} from "./IAuditAware";
import {IArchiveAware} from "./IArchiveAware";

export interface IPersistedParams extends IUnique, IAuditAware, IArchiveAware {

}

export default IPersistedParams;

