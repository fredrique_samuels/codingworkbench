

export default interface IProcessStdio {
    readonly stdout: string;
    readonly stderr: string;
    readonly error: string;
}

export function process_stdio_create(): IProcessStdio {
    return {
        error: "",
        stdout: "",
        stderr: ""
    }
}

export function process_stdio_merge(o1: IProcessStdio, o2: IProcessStdio): IProcessStdio {
    return {
        error: `${o1.error}\n${o2.error}`,
        stderr: `${o1.stderr}\n${o2.stderr}`,
        stdout: `${o1.stdout}\n${o2.stdout}`,
    }
}