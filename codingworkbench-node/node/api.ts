export enum NodeType {
    ELEMENT_ARRAY,
    ELEMENT_GROUP,
    ELEMENT,
    ATTRIBUTE,
    EMPTY,
}

export type ErrorFunction = (n: INode) => void;

export type AttributeConsumer<T=any> = (iAttribute: IAttribute<T>) => void
export type ElementConsumer<T=any> = (iElement: IElement<T>) => void

export interface ConsumableNode {
    asAttribute<T>(consumer: AttributeConsumer<T>, notValidCallback?: (n: INode) => void): this;
    asElement<T>(consumer: ElementConsumer<T>, notValidCallback?: (n: INode) => void): this;
}

export interface INode extends ConsumableNode{
    readonly name: string;
    readonly type: NodeType;
    parent: IElement | null;
}

export interface IAttribute<T=any> extends INode {
    value: T | null;
}

export interface IElement<T=any> extends INode {
    readonly tag: string;
    value: T | null;
    get(name: string): INode;
}

export interface IElementGroup<T=any> extends IElement<T> {
    readonly tag: string;
    get(name: string): INode;
}

export interface IRootElement<T=any> extends IElement<T> {
    select(selector: string): INodeArray;
}

export interface IDataAccessor<T, P = any> {
    getValue(data: P | null, name: string): T | null;
    setValue(value: T | null, data: P | null, name: string): void;
}

export interface INodeArray {
    ifEmpty(callback: () => {}): this;
    values: INode[];
}

export type ReadFunction<T, P> = (data: P | null, key: string) => T | null;
export type WriteFunction<T, P> = (value: T | null, data: P | null, key: string) => void;
export type HasChildPredicate<P> = (parentValue: P | null, name: string) => boolean;
export type SearchChildNamesFunction<P> = (parentValue: P | null, search: string) => string[];
