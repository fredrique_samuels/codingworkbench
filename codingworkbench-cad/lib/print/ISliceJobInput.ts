import IOption from "../common/IOption";

export default interface ISliceJobInput {
    profile: IOption;
    settings: IOption[];
}
