import {deepStrictEqual} from 'assert'
import ArchiveAware, {archive_columns} from "../../lib/ArchiveAware";

describe('ArchiveAwareObject', function() {


    it("#create()", () => {
        deepStrictEqual( {archived: false},  new ArchiveAware().flatten());
    });

    it("#columns()", () => {
        /* given */
        let columns = archive_columns();

        /* then */
        deepStrictEqual(columns.length, 1);

        const [archivedCol] = columns;
        deepStrictEqual(archivedCol.field, "archived");
        deepStrictEqual(archivedCol.column, "archived");
    });

});
