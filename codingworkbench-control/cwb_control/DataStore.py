

class DataStore:

    def __init__(self, key="--default--"):
        self.id = key
        self.data = {}
        self.change_handlers = {}
        self.transformers = {}
        self.enabled_handlers = {}

    def add_change_handler(self, key, handler):
        self.change_handlers[key] = handler

    def set(self, key, value):
        if key in self.transformers.keys():
            v = value
            for func in self.transformers[key]:
                v = func(v)
            self.data[key] = v
        else:
            self.data[key] = value

        if key in self.enabled_handlers.keys():
            for handler in self.enabled_handlers[key].values():
                handler.handle(key, self.data[key], self)

    def get(self, name):
        return self.data[name]

    def has_property(self, property_name):
        return property_name in self.data.keys()

    def property_keys(self):
        return self.data.keys()

    def warning(self, message):
        print(message)

    def fire_handlers(self, target_handlers=[]):
        for prop, handlers in self.enabled_handlers.items():
            for handler_key, handler in handlers.items():
                if handler_key in target_handlers:
                    handler.handle(prop, self.data[prop], self)

    def enable_handler(self, property_name, handler_name):
        if property_name not in self.data.keys():
            self.warning("property '{}' failed to enable handler '{}'. Reason: Property does not exists", property_name, handler_name)

        if handler_name not in self.change_handlers.keys():
            self.warning("property '{}' failed to enable handler '{}'. Reason: Handler does not exists", property_name, handler_name)

        if property_name not in self.enabled_handlers.keys():
            self.enabled_handlers[property_name] = {}

        if handler_name not in self.enabled_handlers[property_name]:
            self.enabled_handlers[property_name][handler_name] = self.change_handlers[handler_name]

    def disable_handler(self, property_name, handler_name):
        if property_name not in self.enabled_handlers.keys():
            return

        if handler_name not in self.enabled_handlers[property_name]:
            return

        del self.enabled_handlers[property_name][handler_name]

    def add_transformer_func(self, property_name, transformer_func):
        if property_name not in self.data.keys():
            self.warning("property '{}' failed to add transformer. Reason: Property does not exists", property_name)

        if property_name not in self.transformers.keys():
            self.transformers[property_name] = []

        self.transformers[property_name].append(transformer_func)
