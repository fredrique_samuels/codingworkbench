"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var body_parser = require("body-parser");
var redisConnect = require("connect-redis");
var cookieParser = require("cookie-parser");
var express = require("express");
var fileUpload = require("express-fileupload");
var session = require("express-session");
var path = require("path");
var process = require("process");
var redis = require("redis");
var log_1 = require("@codingworkbench/log");
var _express_1 = require("./_express");
/**
 * Constant for one megabyte.
 */
exports.ONE_MEGA_BYTE = 1024 * 1024;
/**
 * Bind a all the RequestPath handlers to an  express.Express.
 *
 * @param app The target application
 * @param handlers The list of handlers to bind
 * @param context The web request context.
 */
function bind_handlers_to_app(app, handlers, context) {
    // map handlers.
    handlers.forEach(function (routeDef) {
        var method = routeDef.method, url = routeDef.url, handler = routeDef.handler, isAsync = routeDef.isAsync, handlerAsync = routeDef.handlerAsync;
        log_1.default.info("Adding handler : " + method + " " + url + " ");
        var _method = method ? method : "get";
        if (_method.toLowerCase() === "get") {
            if (isAsync) {
                app.get(url, _express_1.expressTransformHandlerAsync(handlerAsync, context));
            }
            else {
                app.get(url, _express_1.expressTransformHandler(handler, context));
            }
            return;
        }
        if (_method.toLowerCase() === "post") {
            if (isAsync) {
                app.post(url, _express_1.expressTransformHandlerAsync(handlerAsync, context));
            }
            else {
                app.post(url, _express_1.expressTransformHandler(handler, context));
            }
            return;
        }
        throw new Error("Error binding \"" + url + "\". Invalid Method \"" + method + "\"");
    });
}
exports.bind_handlers_to_app = bind_handlers_to_app;
function setup_express(options, app) {
    app.use('/static', express.static(path.resolve("./public")));
    app.use(cookieParser("secretSign#143_!223"));
    app.use(body_parser.urlencoded({ extended: false }));
    app.use(body_parser.json());
    // default options upload options
    // https://www.npmjs.com/package/express-fileupload
    app.use(fileUpload({
        limits: { fileSize: options.fileUploadLimit || exports.ONE_MEGA_BYTE * 50 }
    }));
}
exports.setup_express = setup_express;
function configure_redis(connectionProperties, options, app) {
    if (!options.enabledSessions || !connectionProperties) {
        return;
    }
    var env = process.env.NODE_ENV || "test";
    var host = connectionProperties.getString("redis." + env + ".host", "127.0.0.1");
    var port = connectionProperties.getNumber("redis." + env + ".port", 6379);
    var ttl = connectionProperties.getNumber("redis." + env + ".ttl", 86400);
    var password = connectionProperties.getString("redis." + env + ".password", "");
    log_1.default.info("connecting to redis server " + host + ":" + port + " ttl=" + ttl);
    var redisStore = redisConnect(session);
    var redisClient = redis.createClient({ host: host, port: port, password: password ? password : undefined });
    redisClient.on('error', function (err) {
        if (err) {
            log_1.default.error(err);
        }
    });
    var sessionOptions = {
        secret: 'ThisIsHowYouUseRedisSessionStorage',
        resave: false,
        saveUninitialized: false,
        store: new redisStore({ client: redisClient, ttl: ttl }),
    };
    app.use(session(sessionOptions));
}
exports.configure_redis = configure_redis;
function bind_user_defined_middleware(middlewares, app) {
    var httpFactory = new _express_1.ExpressHttpFactory();
    middlewares.map(function (mw) {
        return function (req, res, next) {
            mw(httpFactory.createRequest(req), httpFactory.createResponse(res), next);
        };
    })
        .forEach(function (mw) {
        app.use(mw);
    });
}
exports.bind_user_defined_middleware = bind_user_defined_middleware;
