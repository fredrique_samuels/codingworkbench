import CwbSearch from "@codingworkbench/search";

import {IPersistedParams} from "./IPersistedParams";
import {ArchiveAware} from "./ArchiveAware";
import {Unique} from "./Unique";
import {AuditAware} from "./AuditAware";
import {ID} from "./IUnique";
import {CrudDataAccessor} from "./CrudDataAccessor";
import {DbTable} from "./DbTable";
import {IUserSession} from "./IUserSession";

export abstract class PersistedObject<P extends IPersistedParams> {
    protected _params: P;
    protected readonly dao: CrudDataAccessor;
    private readonly _table: DbTable;

    protected constructor(dao: CrudDataAccessor, table: DbTable) {
        this.dao = dao;
        this._table = table;
        this._params = this.createParams();
    }

    public get id(): ID { return this._params.id; }
    public get persisted(): boolean { return this._params.id !== null; }
    public get params(): P { return {...this._params}; }

    public validatePersisted() {
        if (!this.persisted) {
            throw new Error("Object not yet persisted");
        }
    }

    public async fromId(id: ID): Promise<this> {
        this.update(await this.dao.getObject(this._table, id) as P);
        return this;
    }

    public async save(session?: IUserSession): Promise<this> {
        this._params.id = await this.dao.saveObject(this._table, this._params, session ? session.userId : null);
        return this;
    }

    public update(params: P): this {
        this._params = {
            ... this._params,
            ... (new Unique(this._params)).update(params.id).flatten(),
            ... (new ArchiveAware(this._params)).update(params).flatten(),
        };
        return this;
    }

    public abstract createParams(): P;

    protected static createPersistedObject(): IPersistedParams {
        return {
            ...new Unique().flatten(),
            ...new ArchiveAware().flatten(),
            ...new AuditAware({}).flatten(),
        }
    }

    public async search(searchParams: CwbSearch.Search): Promise<P[]> {
        return await this.dao.searchObjects(this._table, searchParams);
    }

    public async searchPaged(searchParams: CwbSearch.Search): Promise< CwbSearch.IResults<P>> {
        return await this.dao.searchObjectsPaginated(this._table, searchParams);
    }

    public static mapParams<P extends IPersistedParams>(obj: PersistedObject<P>): P {
        return obj.params;
    }

    public static mapId<P extends IPersistedParams>(obj: PersistedObject<P>): ID {
        return obj.id;
    }
}

export default PersistedObject;
