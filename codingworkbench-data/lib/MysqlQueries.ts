import CwbSearch from "@codingworkbench/search";

import {DbTable} from "./DbTable";
import {DbQuery, TargetResult} from "./DbQuery";
import {DbQueryFactory} from "./DbQueryFactory";


export class MysqlQueryParamsFactory implements DbQueryFactory {

    createInsertQuery(table: DbTable, record: any): DbQuery {
        return insertQ(table, record).build();
    }

    public createUpdateQuery(table: DbTable, record: any): DbQuery {
        return updateQ(table, record).build();
    }

    public createSearchQuery(table: DbTable, searchParams: CwbSearch.Search): DbQuery {
        return readQ(table, searchParams).build();
    }

    public createDeleteObjectQuery(table: DbTable, record: any): DbQuery {
        return deleteQ(table, record).build();
    }

    createCountQuery(table: DbTable, search: CwbSearch.Search): DbQuery {
        return countQ(table, search).build();
    }
}


/**
 * Query params builder. Builds a collection of values needed for the query
 * and placeholder variables to be used in the query string.
 *
 * This object is inteneded for extension overriding the generateSql()
 * to create the sql string and then invoking build() to create the QueryParams.
 */
export class MysqlQBuilder<T = any> {
    private argCounter: number = 0;
    private _values: any[] = [];
    private _targetResult?: TargetResult;

    protected table: DbTable;
    protected data: T;

    constructor(table: DbTable, data: T) {
        this.table = table;
        this.data = data;
    }

    public setTargetResult(t: TargetResult): this {
        this._targetResult = t;
        return this;
    }

    public addValue(obj: any, prop?: string): string {
        this._values.push(prop ? obj[prop] : obj);
        this.argCounter = this.argCounter + 1;
        return `?`
    }

    public generateSql(): string {
        const { table, data } = this;
        return this.generateSqlInternal(table, data);
    }

    generateSqlInternal(table: DbTable, data: T): string {
        throw new Error("Method not implemented")
    }

    /**
     * @deprecated
     */
    get values(): any[] {
        return [...this._values]
    }

    build(): DbQuery {
        let sql = this.generateSql();
        return {
            table: this.table,
            command: sql,
            values: [...this._values],
            targetResult: this._targetResult,
        }
    }
}

/**
 * Generate the query needed to insert the target object.
 *
 * @param table The target insert table
 * @param data ProjectInfo The project item to insert
 */
export function insertQ(table: DbTable, data: any): MysqlQBuilder {
    return new InsertQueryParamsBuilder(table, data);
}


/**
 * Generate the query needed to delete the target object.
 *
 * @param table The target table
 * @param data The data to be deleted.
 */
export function deleteQ(table: DbTable, data: any): MysqlQBuilder {
    return new DeleteQueryParamsBuilder(table, data);
}


/**
 * Generate the update needed to update the given record.
 *
 * @param table The target table
 * @param data The data to be deleted.
 */
export function updateQ(table: DbTable, data: any): MysqlQBuilder {
    return new UpdateQueryParamsBuilder(table, data);
}


/**
 * Generate the get sql needed to retrieve records.
 *
 * @param table The target table
 * @param ids The data to be deleted.
 */
export function readQ(table: DbTable, searchParams: CwbSearch.Search): MysqlQBuilder<CwbSearch.Search> {
    return new ReadObjectQueryParamsBuilder(table, searchParams);
}

export function searchIdQ(table: DbTable, searchParams: CwbSearch.Search): MysqlQBuilder<CwbSearch.Search> {
    return new SearchIdQueryParamsBuilder(table, searchParams);
}

export function countQ(table: DbTable, searchParams: CwbSearch.Search): MysqlQBuilder<CwbSearch.Search> {
    return new CountQueryParamsBuilder(table, searchParams).setTargetResult("COUNT");
}


export type ConditionGeneratorFunc = (field: string, value: any, args: MysqlQBuilder, tableDef: DbTable) => string;

/**
 * A query condition builder.
 */
class ConditionStringGenerator {

    eq(field: string, value: any, args: MysqlQBuilder, tableDef: DbTable): string {
        return Array.isArray(value)
            ? ` ${tableDef.get(field).column} IN (${value.map((v) => args.addValue(v)).join(",")})`
            : ` ${tableDef.get(field).column}=${args.addValue(value)}`
    }

    in(field: string, value: any, args: MysqlQBuilder, tableDef: DbTable): string {
        return Array.isArray(value)
            ? ` ${tableDef.get(field).column} IN (${value.map((v) => args.addValue(v)).join(",")})`
            : ` ${tableDef.get(field).column} IN (${args.addValue(value)})`
    }

    lt(field: string, value: any, args: MysqlQBuilder, tableDef: DbTable): string {
        return ` ${tableDef.get(field).column} < ${args.addValue(value)}`
    }

    lte(field: string, value: any, args: MysqlQBuilder, tableDef: DbTable): string {
        return ` ${tableDef.get(field).column} <= ${args.addValue(value)}`
    }

    gt(field: string, value: any, args: MysqlQBuilder, tableDef: DbTable): string {
        return ` ${tableDef.get(field).column} > ${args.addValue(value)}`
    }

    gte(field: string, value: any, args: MysqlQBuilder, tableDef: DbTable): string {
        return ` ${tableDef.get(field).column} >= ${args.addValue(value)}`
    }

    like(field: string, value: any, args: MysqlQBuilder, tableDef: DbTable): string {
        return ` ${tableDef.get(field).column} LIKE ${args.addValue(`%${value}%`)}`
    }

    ne(field: string, value: any, args: MysqlQBuilder, tableDef: DbTable): string {
        return ` ${tableDef.get(field).column} != ${args.addValue(value)}`
    }

    nin(field: string, value: any, args: MysqlQBuilder, tableDef: DbTable): string {
        return Array.isArray(value)
            ? ` ${tableDef.get(field).column} NOT IN (${value.map((v) => args.addValue(v)).join(",")})`
            : ` ${tableDef.get(field).column} NOT IN (${args.addValue(value)})`
    }
}

class InsertQueryParamsBuilder extends MysqlQBuilder {

    constructor(table: DbTable, data: any) {
        super(table, data);
    }

    generateSqlInternal(table: DbTable, data: any): string {
        return `
            INSERT INTO ${table.name}
                (${table.insert_column_names.join(',')}) 
            VALUES 
                (${table.insert_columns.map( c => this.addValue(data, c.field) )})
--             RETURNING id
            ;
        `
    }
}


class DeleteQueryParamsBuilder  extends MysqlQBuilder {

    constructor(table: DbTable, data: any) {
        super(table, data);
    }

    generateSqlInternal(table: DbTable, data: any): string {
        let args = this;
        return table.archived
            ?  `
            UPDATE  ${table.name}
            SET     archived    =   ${args.addValue(true)}
            WHERE
                    id          =   ${args.addValue(data.id)}
            AND     archived    =   ${args.addValue(false)}
            -- RETURNING id;`
            : `
                DELETE FROM ${table.name}
                WHERE
                    id=${args.addValue(data.id)}
            `
    }

}

class UpdateQueryParamsBuilder  extends MysqlQBuilder {

    constructor(table: DbTable, data: any) {
        super(table, data);
    }

    generateSqlInternal(table: DbTable, data: any): string {
        let args = this;
        return `
            UPDATE  ${table.name}
            SET     ${table.insert_columns.map( c => ` ${c.column}=${args.addValue(data, c.field)}` ).join(",")}
            WHERE
                    id  =   ${args.addValue(data.id)}
                    ${ table.archived ? `AND archived=${args.addValue(false)}` : "" }
--          RETURNING   id;
            `
    }

}

class ReadObjectQueryParamsBuilder  extends MysqlQBuilder<CwbSearch.Search> {

    constructor(table: DbTable, searchParams: CwbSearch.Search) {
        super(table, searchParams);
    }

    generateSqlInternal(table: DbTable, searchParams: CwbSearch.Search): string {
        const conditionsStr: string[] = create_condition_queries(table, searchParams, this);
        return `
            SELECT  ${table.get_columns_as_alias_sql.join(",")}
            FROM    ${table.name}
            WHERE   id
            ${conditionsStr.length ? "AND" : ""}
            ${conditionsStr.join(" AND ")}
            ${create_sort_limit_offset_query(table, searchParams, this)}
        `;
    }
}

/**
 * Generate the search sql needed to retrieve ids based on filters.
 *
 * @param table The target table
 * @param searchParams filters.
 */
class SearchIdQueryParamsBuilder extends MysqlQBuilder<CwbSearch.Search> {

    constructor(table: DbTable, searchParams: CwbSearch.Search) {
        super(table, searchParams);
    }

    generateSqlInternal(table: DbTable, searchParams: CwbSearch.Search): string {
        const conditionsStr: string[] = create_condition_queries(table, searchParams, this);
        return `SELECT  id
                FROM    ${table.name} 
                WHERE   id  
                ${conditionsStr.length ? "AND" : ""}
                ${conditionsStr.join(" AND ")}
                ${create_sort_limit_offset_query(table, searchParams, this)}`;
    }
}

class CountQueryParamsBuilder extends MysqlQBuilder<CwbSearch.Search> {

    constructor(table: DbTable, searchParams: CwbSearch.Search) {
        super(table, searchParams);
    }

    generateSqlInternal(table: DbTable, searchParams: CwbSearch.Search): string {
        const conditionsStr: string[] = create_condition_queries(table, searchParams, this);
        return `SELECT  COUNT(id) as count
                FROM    ${table.name} 
                WHERE   id  
                ${conditionsStr.length ? "AND" : ""}
                ${conditionsStr.join(" AND ")}
                ${create_sort_limit_offset_query(table, searchParams, this)}`;
    }
}


export function create_filter_queries<T>(table: DbTable, searchParams: CwbSearch.Search, builder: MysqlQBuilder<T>): string[] {

    const conditionStringGenerator = new ConditionStringGenerator();
    const OperationFunctionDict: {[s: string]: ConditionGeneratorFunc} = {};
    OperationFunctionDict['eq'] = conditionStringGenerator.eq.bind(conditionStringGenerator);
    OperationFunctionDict['lt'] = conditionStringGenerator.lt.bind(conditionStringGenerator);
    OperationFunctionDict['lte'] = conditionStringGenerator.lte.bind(conditionStringGenerator);
    OperationFunctionDict['gt'] = conditionStringGenerator.gt.bind(conditionStringGenerator);
    OperationFunctionDict['gte'] = conditionStringGenerator.gte.bind(conditionStringGenerator);
    OperationFunctionDict['in'] = conditionStringGenerator.in.bind(conditionStringGenerator);
    OperationFunctionDict['like'] = conditionStringGenerator.like.bind(conditionStringGenerator);
    OperationFunctionDict['ne'] = conditionStringGenerator.ne.bind(conditionStringGenerator);
    OperationFunctionDict['nin'] = conditionStringGenerator.nin.bind(conditionStringGenerator);

    const conditionsStr: string[] = [];
    const filters = searchParams.filters || {};
    Object.keys(filters)
        .forEach( f => {
            const operation = filters[f].operation || 'eq';
            conditionsStr.push( OperationFunctionDict[operation](f, filters[f].value, builder, table) );
        });
    return conditionsStr;
}

export function create_not_archived_query<T>(table: DbTable, builder: MysqlQBuilder<T>): string[] {
    const conditionsStr: string[] = [];
    if(table.archived) {
        conditionsStr.push( ` archived=${builder.addValue(false)}` );
    }
    return conditionsStr;
}

export function create_condition_queries<T>(table: DbTable, searchParams: CwbSearch.Search, builder: MysqlQBuilder<T>): string[] {
    let conditionsStr: string[] = [];
    conditionsStr = [...conditionsStr, ...create_filter_queries(table, searchParams, builder)];
    conditionsStr = [...conditionsStr, ...create_not_archived_query(table, builder)];
    return conditionsStr;
}

export function create_sort_limit_offset_query<T>(table: DbTable, searchParams: CwbSearch.Search, builder: MysqlQBuilder<T>): string {
    let order_by = "";
    let limit = "";
    let offset = "";

    if (searchParams.pagination) {
        if (searchParams.pagination.sortField !== null) {
            const {sortField, sortOrder} = searchParams.pagination;
            order_by = ` ORDER BY ${sortField} ${sortOrder}`
        }

        limit = ` LIMIT ${builder.addValue(searchParams.pagination.pageSize)}`;

        if (searchParams.pagination.offset > 0) {
            offset = ` OFFSET ${builder.addValue(searchParams.pagination.offset)}`;
        }
    }

    return ` ${order_by} ${limit} ${offset}`
}
