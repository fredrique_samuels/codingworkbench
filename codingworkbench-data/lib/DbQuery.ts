import {DbTable} from "./DbTable";
import {ID} from "./IUnique";

export interface DbQuery {
    table: DbTable,
    command: string,
    values?: any,
    targetResult?: TargetResult
}

export type TargetResult = 'COUNT' | 'OBJECT_ID' | 'RAW';

export interface CountResult {
    count: number;
}

export type InsertedIdResult = ID;

export default DbQuery;
