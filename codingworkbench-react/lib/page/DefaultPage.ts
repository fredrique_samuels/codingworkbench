import IPage from "./IPage";
import {PageAction} from "./PageAction";


export class DefaultPage<IState> implements IPage<IState> {
    state: any;

    close<K extends keyof IState>(target: keyof IState, targetUpdates?: any, stateUpdates?: Pick<IState, K>): void {
    }

    closeHandler<K extends keyof IState>(target: keyof IState, targetUpdates?: any, stateUpdates?: Pick<IState, K>): () => void {
        return function () {
        };
    }

    logError(e: Error): void {
    }

    open<K extends keyof IState>(target: keyof IState, targetUpdates?: any, stateUpdates?: Pick<IState, K>): void {
    }

    openHandler<K extends keyof IState>(target: keyof IState, targetUpdates?: any, stateUpdates?: Pick<IState, K>): () => void {
        return function () {
        };
    }

    pushErrorNotification(message: string): void {
    }

    pushInfoNotification(message: string): void {
    }

    pushSuccessNotification(message: string): void {
    }

    pushWarningNotification(message: string): void {
    }

    setState<K extends keyof IState>(state: ((prevState: Readonly<IState>, props: Readonly<{}>) => (Pick<IState, K> | IState | null)) | Pick<IState, K> | IState | null, callback?: () => void): void {
    }

    startLoading(title: string): void {
    }

    stopLoading(): void {
    }

    updateState<K extends keyof IState>(updates: Pick<IState, K> | null, property?: keyof IState, propertyUpdates?: any): void {
    }

    updateStateProperty<K extends keyof IState>(property: K, updates?: any): void {
    }

    webAction(url: string): PageAction {
        return new PageAction(url, this);
    }

}
