import {IFormInputsAware, IFormValidationAware, IFormValidation, validation_passed} from "@codingworkbench/react/form/FormInputs";
import {ICredentials} from "@codingworkbench/common/ICredentials";
import {AuthenticationMessageCodes} from "../../../api/messages";

export interface ISecurityInputs extends ICredentials {
}

export interface ISecurityForm extends IFormInputsAware<ISecurityInputs>, IFormValidationAware {
    loading: boolean;
}

export class SecurityForm {

    public static createForm()
        : ISecurityForm
    {
        return {
            inputs: {user: "", secret: ""},
            validation: validation_passed(),
            loading: false
        }
    }

    public static updateValidation(state: ISecurityForm, validation: IFormValidation)
        : ISecurityForm {
        return {
            ...state,
            validation: validation
        }
    }

    public static startLoading(state: ISecurityForm)
        : ISecurityForm {
        return {
            ...state,
            loading: true
        }
    }

    public static stopLoading(state: ISecurityForm)
        : ISecurityForm {
        return {
            ...state,
            loading: false
        }
    }

    public static processValidationResponse(state: ISecurityForm, validation: IFormValidation)
        : ISecurityForm {
        return SecurityForm.updateValidation(SecurityForm.stopLoading(state), validation)
    }

    public static getMessage(validation: IFormValidation)
        : string {
        if (validation.passed) {
            return "";
        }

        const messageKey: string = validation.message || "";
        const message = AuthenticationMessageCodes[messageKey];

        return message || "We are unable to service your request at this time";
    }

}
