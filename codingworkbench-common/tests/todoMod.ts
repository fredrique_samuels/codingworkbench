import CwbInject from "lib/inject";


export interface ITodoItem { label: string; }

export interface ITodoStore {
    getItems(): ITodoItem[];
}

export const TODO_ITEM_NAME = "todoItem0";

export class TodoStore implements ITodoStore {

    public getItems(): ITodoItem[] {
        return [
            {
                label: TODO_ITEM_NAME,
            },
        ];
    }
}

export interface ITodoServiceArgs {
    todoStore: ITodoStore;
}

export const todoServiceInjectionKeys: CwbInject.InjectionProperties<ITodoServiceArgs> = {
    todoStore: true,
};

export interface ITodoService {
    getItems(): ITodoItem[];
}

export class TodoService implements ITodoService {
    private readonly _id: number;
    private store: ITodoStore;

    constructor(args: ITodoServiceArgs) {
        this.store = args.todoStore;
        this._id = new Date().getTime();
    }

    get id(): number {
        return this._id;
    }

    public getItems(): ITodoItem[] {
        return this.store.getItems();
    }
}

export default interface ITodoModule {
    todoStore: ITodoStore;
    todoService: TodoService;
}
