import {IActionResponse} from "./IActionResponse";

export class ActionResponseBuilder<T = any> {

    private a: IActionResponse<T>;

    constructor() {
        this.a = { error: false }
    }

    public error(message?: string): this {
        this.a = {
            ... this.a,
            error: true
        };

        if (message) {
            this.a.message = message
        } else {
            this.a.message = undefined;
        }

        return this;
    }

    public redirect(url: string): this {
        this.a = {
            ... this.a,
            redirect: url
        };
        return this;
    }

    public data(data: T): this {
        this.a = {
            ... this.a,
            data: data
        };
        return this;
    }

    public json(): string {
        return JSON.stringify(this.a);
    }

    public build(): IActionResponse<T> {
        return {... this.a};
    }
}
