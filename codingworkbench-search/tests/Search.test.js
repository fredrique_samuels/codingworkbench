"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
exports.__esModule = true;
var assert_1 = require("assert");
var search_1 = require("../../lib/search");
function createTestPaginated() {
    var searchBuilder = new search_1["default"].SearchBuilder();
    searchBuilder.pagination
        .setPageSize(1)
        .setOffset(2)
        .setSortField("field")
        .setSortOrder('DESC');
    var searchParams = searchBuilder.build();
    return searchParams;
}
function assertPaginatedSearch(iPagination) {
    assert_1.deepStrictEqual(iPagination.pageSize, 1);
    assert_1.deepStrictEqual(iPagination.offset, 2);
    assert_1.deepStrictEqual(iPagination.sortField, 'field');
    assert_1.deepStrictEqual(iPagination.sortOrder, 'DESC');
}
describe('PaginationParamsBuilder', function () {
    it("#init()", function () {
        var iPagination = new search_1["default"].SearchBuilder().build().pagination;
        assert_1.deepStrictEqual(iPagination.pageSize, 25);
        assert_1.deepStrictEqual(iPagination.offset, 0);
        assert_1.deepStrictEqual(iPagination.sortField, null);
        assert_1.deepStrictEqual(iPagination.sortOrder, 'ASC');
    });
    it("#setters()", function () {
        var paginationParams = createTestPaginated().pagination;
        assertPaginatedSearch(paginationParams);
    });
    it("#with arg()", function () {
        var paginationParams = new search_1["default"].SearchBuilder(createTestPaginated()).build().pagination;
        assertPaginatedSearch(paginationParams);
    });
});
function createTestFilters() {
    var searchBuilder = new search_1["default"].SearchBuilder();
    searchBuilder.filters
        .addFilter("field0", 5)
        .addFilter("field1", [10], "in");
    return searchBuilder.build();
}
function assertTestFilters(dict) {
    assert_1.deepStrictEqual(dict, {
        field0: { value: 5, operation: "eq" },
        field1: { value: [10], operation: "in" },
    });
}
describe('FiltersBuilder', function () {
    it("#init()", function () {
        var dict = new search_1["default"].SearchBuilder().build();
        assert_1.deepStrictEqual(dict.filters, {});
    });
    it("#setters()", function () {
        var dict = createTestFilters();
        assertTestFilters(dict.filters);
    });
    it("#with arg()", function () {
        var dict = new search_1["default"].SearchBuilder(createTestFilters()).build();
        assertTestFilters(dict.filters);
    });
});
var data_functions = {
    dataSupplier: function (s) { return __awaiter(_this, void 0, void 0, function () {
        var pageParams, resultsLeft, make_results;
        return __generator(this, function (_a) {
            pageParams = s.pagination;
            resultsLeft = 15 - pageParams.offset;
            make_results = function (c, offset) {
                var r = [];
                for (var idx = 0; idx < c; idx++) {
                    r.push(idx + offset);
                }
                return r;
            };
            return [2 /*return*/, pageParams.pageSize <= resultsLeft
                    ? make_results(pageParams.pageSize, pageParams.offset)
                    : make_results(resultsLeft, pageParams.offset)];
        });
    }); },
    totalSupplier: function () { return __awaiter(_this, void 0, void 0, function () { return __generator(this, function (_a) {
        return [2 /*return*/, 15];
    }); }); }
};
describe('do_paged_search', function () {
    it("#from empty search()", function (done) {
        function _e() {
            return __awaiter(this, void 0, void 0, function () {
                var searchResults;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, search_1["default"].do_paged_search(new search_1["default"].SearchBuilder().build(), data_functions)];
                        case 1:
                            searchResults = _a.sent();
                            assert_1.deepStrictEqual(searchResults.currentPage, 1);
                            assert_1.deepStrictEqual(searchResults.pages, 1);
                            assert_1.deepStrictEqual(searchResults.results.length, 15);
                            assert_1.deepStrictEqual(searchResults.next, undefined);
                            assert_1.deepStrictEqual(searchResults.prev, undefined);
                            assert_1.deepStrictEqual(searchResults.refresh.pagination.totalRecords, 15);
                            return [2 /*return*/];
                    }
                });
            });
        }
        _e().then(function () { return done(); })["catch"](done);
    });
    it("#from paged", function (done) {
        function _e() {
            return __awaiter(this, void 0, void 0, function () {
                var searchBuilder, searchResults;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0:
                            searchBuilder = new search_1["default"].SearchBuilder();
                            searchBuilder.pagination.setPageSize(5);
                            return [4 /*yield*/, search_1["default"].do_paged_search(searchBuilder.build(), data_functions)];
                        case 1:
                            searchResults = _a.sent();
                            assert_page(searchResults, 1, 3, [0, 1, 2, 3, 4], 15);
                            return [4 /*yield*/, search_1["default"].do_paged_search(searchResults.next, data_functions)];
                        case 2:
                            searchResults = _a.sent();
                            assert_page(searchResults, 2, 3, [5, 6, 7, 8, 9], 15);
                            return [4 /*yield*/, search_1["default"].do_paged_search(searchResults.next, data_functions)];
                        case 3:
                            searchResults = _a.sent();
                            assert_page(searchResults, 3, 3, [10, 11, 12, 13, 14], 15);
                            return [4 /*yield*/, search_1["default"].do_paged_search(searchResults.prev, data_functions)];
                        case 4:
                            searchResults = _a.sent();
                            assert_page(searchResults, 2, 3, [5, 6, 7, 8, 9], 15);
                            return [4 /*yield*/, search_1["default"].do_paged_search(searchResults.prev, data_functions)];
                        case 5:
                            searchResults = _a.sent();
                            assert_page(searchResults, 1, 3, [0, 1, 2, 3, 4], 15);
                            return [2 /*return*/];
                    }
                });
            });
        }
        _e().then(function () { return done(); })["catch"](done);
    });
    function assert_page(searchResults, currentPage, totalPages, results, totalRecords) {
        assert_1.deepStrictEqual(searchResults.currentPage, currentPage);
        assert_1.deepStrictEqual(searchResults.pages, totalPages);
        assert_1.deepStrictEqual(searchResults.results.length, results.length);
        assert_1.deepStrictEqual(searchResults.results, results);
        assert_1.deepStrictEqual(searchResults.refresh.pagination.totalRecords, totalRecords);
        if (currentPage > 1) {
            assert_1.ok(searchResults.prev);
        }
        else {
            assert_1.ok(!searchResults.prev);
        }
        if (currentPage === totalPages) {
            assert_1.ok(!searchResults.next);
        }
        else {
            assert_1.ok(searchResults.next);
        }
    }
});
//# sourceMappingURL=Search.test.js.map