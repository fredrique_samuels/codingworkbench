import {IAddress, IEmployee} from "../employee";
import CwbNode from "../../../node";

const readStreet = (data: IAddress) => data ? data.street : null;
const streetAttribute = new CwbNode.AttributeBuilder(readStreet).build();

const readAddress = (data: IEmployee) => data ? data.address : null;
export const ADDRESS_TAG = "address-data";
export const addressElement = new CwbNode.ElementBuilder<IAddress, IEmployee>(ADDRESS_TAG, readAddress)
    .addChild("street", streetAttribute)
    .build();
