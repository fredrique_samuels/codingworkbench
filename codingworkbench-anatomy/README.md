
## Setup

```sh
python3.7 -m pip install numpy
python3.7 -m pip install matplotlib
python3.7 -m pip install pyfirmata
```

##Install

```
python setup.py install 
```

## List TTY AND CU devices

```sh
ls /dev/tty.*
ls /dev/cu.*
```

## Dataflow editor 

see [rete.js](https://rete.js.org/#/examples/task#lang=en&tosearch=move)


## Cherry Py Docs

https://docs.cherrypy.org/en/latest/basics.html 

## Potentially needing to control a motor with the Rasberry Pi

https://www.youtube.com/watch?v=2bganVdLg5Q
