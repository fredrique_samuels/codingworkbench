export default interface ProcessStdio {
    readonly stdout: string;
    readonly stderr: string;
    readonly error: string;
}
