import * as React from "react";
import * as ReactDOM from "react-dom";

import {IPageProps, IPageState, Page} from "../../lib/page/Page";
import SignIn from "./SignIn";


interface IMyPageState extends IPageState {}
interface IMyPageProps extends IPageProps {}

class MyPage extends Page<IMyPageProps, IMyPageState> {

    constructor(props: any) {
        super(props);
        this.state = {
            ...Page.createState(this),
            title: "My Test Page",
            subtitle: `subtitle`
        }
    }

    renderChildren(): React.ReactNode[] {
        return [<SignIn />]
    }

}

ReactDOM.render(<MyPage />, document.getElementById("root"));
