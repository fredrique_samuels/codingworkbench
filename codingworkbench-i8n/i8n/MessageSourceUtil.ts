import IMessageSource from "./IMessageSource";
import CwbAction from "@codingworkbench/action";

export async function actionResponseMessage<C>(res: CwbAction.IResponse<C>, messageSource: IMessageSource): Promise<CwbAction.IResponse<C>> {
    let message: string | undefined = res.message;
    if (message) {
        const s = await messageSource.get(message);
        message = s ? s : message;
    }
    return {
        ...res,
        message
    };
}
