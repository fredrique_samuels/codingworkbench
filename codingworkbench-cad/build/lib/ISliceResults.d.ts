import ISliceSummary from "./ISliceSummary";
export default interface ISliceResults {
    readonly gcode: string;
    readonly summary: ISliceSummary;
}
