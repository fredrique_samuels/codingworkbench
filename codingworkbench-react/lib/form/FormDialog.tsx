import * as React from 'react';

import Button from '@material-ui/core/Button';
import IPage, {safe_page} from "../page/IPage";
import PageDialog, {PageDialogProps} from "../dialogs/PageDialog";
import CwbAction from '@codingworkbench/action';
import Optional from '@codingworkbench/common/Optional';
import {IFormValidation, handle_input_state_change, IFormInputsAware, IFormValidationAware, validation_failed, validation_passed} from "./FormInputs";

export type FormDialogActionParams<F, P> = {
    page: IPage,
    form: F,
    props: P
}
export type FormDialogAction<F, P> = (params: FormDialogActionParams<F, P>) => Promise<CwbAction.IResponse>

/**
 * @template F The form inputs type internal state. This should be extended to include the form field data.
 * @template Prop The FormDialog properties when when used as a JSX tag.
 */
class FormDialog
        <
            I extends IFormValidationAware,
            S extends FormDialogState<I> = FormDialogState<I>,
            P extends FormDialogProps = FormDialogProps
        >
    extends React.Component<P, S>
{
    constructor(props: P) {
        super(props);
    }

    public get page(): IPage { return safe_page(this.props.page) }

    /**
     * Override the render method to draw a dialog with properties that simulate a submission form.
     */
    render() {
        const dialogProps: PageDialogProps = {
            ... this.props,
            ... {
                title: this.props.title || "",
                text: this.props.text || "",
                content: this.renderForm(),
                actions: this.prepareActions(),
                closeOnAwayClick: false
            }
        };
        
        return (
            <PageDialog { ...dialogProps } />
        )
    }

    /**
     * Called when the dialog closes. Should be overridden to
     * reset the component state after the form is used.
     */
    reset(): void {}

    /**
     * Render the form content.
     */
    renderForm(): React.ReactNode {
        return this.props.content;
    }

    /**
     * Override this method to implement validation.
     */
    async validate(): Promise<IFormValidation> {
        return {
            passed: true
        };
    }

    /**
     * Called when one of the inputs value changes.
     *
     * @param name The inputs field name.
     */
    handleChange(name: keyof I) {
        const { state } = this;
        handle_input_state_change(name, state, this.setState.bind(this));
    }


    disableInputs() {
        this.setState({ disableActions: true})
    }

    enableInputs() {
        this.setState({ disableActions: false})
    }

    ifViewContext(handler: (page: IPage) => void) {
        const { page } = this.props;
        Optional.forValue<IPage | undefined>(page)
            .ifPresent(handler)
    }

    protected async executeAction(formAction: FormDialogAction<I, P>): Promise<CwbAction.IResponse> {
        const {state, props} = this;
        let actionResponse = await formAction({
                page: this.page,
                form: state.inputs,
                props: props
            });
        if (actionResponse.error) {
            this.setState({inputs: {...state.inputs, validation: validation_failed(actionResponse.message)}});
        }
        return actionResponse;
    }

    protected get disableActions(): boolean {
        return this.state.disableActions;
    }

    protected async onSubmit(state: I): Promise<CwbAction.IResponse> {
        return new CwbAction.ResponseBuilder()
            .error("Submission Failed")
            .build();
    }

    protected renderActions(fa: FormActionProps): JSX.Element[] {
        return [
            <FormCancelButton  key="cancel-button" label={"Cancel"} {...fa} />,
            <FormSubmitButton  key="submit-button" label={"Submit"} {...fa} />
        ]
    }

    protected prepareActions(): JSX.Element[]  {

        const submitAsync = async () => {
            let validated = validation_passed();
            try {
                validated = await this.validate()
            } catch (e) {
                this.ifViewContext((page: IPage) => {
                    page.logError(e);
                    page.pushWarningNotification("Error validating form");
                });
                return;
            }

            if(!validated.passed) {
                this.ifViewContext((page: IPage) => {
                    page.pushWarningNotification(validated.message || "Form inputs not valid");
                });
                return
            }

            try {
                this.disableInputs();
                let res = await this.onSubmit(this.state.inputs);
                if(res.error) {
                    this.ifViewContext((page: IPage) => {
                        if(res.message) {
                            page.pushWarningNotification(res.message);
                        }
                    });
                } else if(this.state.options.closeOnDone) {
                    this.close();
                }
            } catch (e) {
                this.ifViewContext((page: IPage) => {
                    page.logError(e);
                    page.pushErrorNotification("Submission Error");
                });
            } finally {
                this.enableInputs();
            }
        };

        const handleSubmit = () => {
            submitAsync().catch(console.error)
        };
    
        const handleCancel = () => {
            this.close();
        };

        const fa: FormActionProps = {
            cancel: handleCancel,
            submit: handleSubmit,
            disable: this.disableActions
        };

        return this.renderActions(fa);
    }

    private close() {
        const { props } = this;
        if (props.onClose) {
            props.onClose()
        }
        this.reset()
    }
}

export interface FormDialogState<I> extends IFormInputsAware<I> {
    /**
     *  value used to disable all action on the form while performing operations
     */
    disableActions: boolean;

    /**
     * Form behaviour settings.
     */
    options: FormOptions;
}

export interface FormDialogProps extends PageDialogProps {
    page?: IPage;
}

export interface FormOptions {

    /**
     * Setting to
     */
    closeOnDone?: boolean;
}

/**
 * Create a new state. Used to construct the state of the FormDialog implementation.
 *
 * @param options Optional options.
 */
export function form_dialog_init_state<S, I>(inputs: I, options?: FormOptions): FormDialogState<I> {
    return {
        disableActions: false,
        options: {
            closeOnDone: true,
            ...options
        },
        inputs: inputs
    }
}

export type HandleInputChangeFunc<F> = (name: keyof F) => (event: React.ChangeEvent<HTMLInputElement>) => any;

export interface FormActionProps {
    cancel: () => void,
    submit: () => void,
    disable: boolean
}

export interface FormButtonProps extends FormActionProps {
    label: string;
}

export function FormCancelButton(props: FormButtonProps) {
    return <Button
        key={props.label}
        disabled={props.disable}
        onClick={props.cancel}
        color="secondary"
    >
        {props.label}
    </Button>;
}

export function FormSubmitButton(props: FormButtonProps) {
    return <Button
        key={props.label}
        disabled={props.disable}
        onClick={props.submit}
        color="default"
        variant={"outlined"}
    >
        {props.label}
    </Button>;
}


export default FormDialog;
