import * as express from "express"
import * as fileUpload from "express-fileupload"
import * as common from "./common";

import {IncomingHttpHeaders} from "http";
import IFileData from "@codingworkbench/common/IFileData";
import IUploadedFiles from "@codingworkbench/common/IUploadedFiles";

class FileDataImpl implements IFileData {
    private _file: fileUpload.UploadedFile;

    constructor(file: fileUpload.UploadedFile) {
        this._file = file
    }

    get uuid(): string { return this._file.md5 }
    get md5(): string { return this._file.md5 }
    get name(): string { return this._file.name }
    get mimetype(): string { return this._file.mimetype }
    get data(): Buffer { return this._file.data }
    get size(): number { return this._file.size }
}

class UploadedFilesImpl implements IUploadedFiles {
    private readonly _files: fileUpload.FileArray;

    constructor(files: fileUpload.FileArray) {
        this._files = files;
    }

    get(alias: string): IFileData[] {
        let f = this._files[alias];
        if(!f) { return [] }

        if( Array.isArray(f) ) {
            return (f as fileUpload.UploadedFile[])
                .map( file =>  new FileDataImpl(file))
        }

        return [new FileDataImpl(<fileUpload.UploadedFile>f)]
    }

}


class SessionImpl implements common.Session {
    private readonly _session?: Express.Session;

    constructor(session?: Express.Session) {
        this._session = session;
    }

    get id(): string {
        if(this._session) {
            return this._session.id;
        }
        return "";
    }

    get(name: string): any {
        if(this._session) {
            return this._session[name];
        }
    }

    set(name: string, value: any): void {
        if(this._session && name !== "id") {
            this._session[name] = value;
        }
    }

    destroy(err?: (err: any) => void) {
        if(this._session) {
            this._session.destroy(err ? err : console.error)
        }
    }
}


class RequestWrapper implements common.Request {
    private readonly req: express.Request;

    constructor(req: express.Request) {
        this.req = req;
    }

    get sessionID(): string {
        return this.req.sessionID || "";
    }

    get body(): any {
        return this.req.body
    }

    get params(): any {
        return this.req.params
    }

    get files(): IUploadedFiles {
        const {files} = this.req;

        if (files === undefined || files === null || Object.keys(files).length == 0) {
            return new UploadedFilesImpl({})
        }

        return new UploadedFilesImpl(files)
    }

    get headers(): IncomingHttpHeaders {
        return this.req.headers
    }

    get method(): common.Method {
        let lowerCase = this.req.method.toLowerCase();
        if( lowerCase === 'get' ) return 'get';
        if( lowerCase === 'post' ) return 'post';
        throw new Error("Unspported method")
    }

    get url(): string {
        return this.req.url
    }

    get session(): common.Session {
        return new SessionImpl(this.req.session);
    }
}

class ResponseWrapper implements common.Response {
    private _terminated = false;
    private res: express.Response;

    constructor(res: express.Response) {
        this.res = res;
    }

    sendText(text: string): void {
        this.res.send(text);
        this.terminate();
    }

    sendJson(str: string): void {
        this.res.setHeader('Content-Type', 'application/json');
        this.res.end(str);
        // this.terminate();
    }

    sendJsonObj(obj: any, pretty?: boolean): void {
        this.sendJson(JSON.stringify(obj, null,  pretty ? 4 : 0))
    }

    sendStatus(code: number, text?: string): void {
        if(text) {
            this.res.status(code).send(text);
        } else {
            this.res.sendStatus(code)
        }
        this.terminate()
    }

    redirect(target: string): void {
        this.res.redirect(target);
        this.terminate();
    }

    view(view: common.View): void {
        this.res.send(view.html);
        this.terminate();
    }

    get terminated(): boolean { return this._terminated; }
    private terminate(): void { this._terminated = true}


}

export class ExpressHttpFactory implements common.RequestFactory<express.Request, express.Response> {

    createRequest(input: express.Request): common.Request {
        return new RequestWrapper(input)
    }

    createResponse(input: express.Response): common.Response {
        return new ResponseWrapper(input);
    }

}

export function expressTransformHandler<C={}>(handler: common.RequestHandlerFunc, context: C )
    : express.RequestHandler {
    return (req: express.Request, res: express.Response) => {
        handler({
            request: new RequestWrapper(req),
            response: new ResponseWrapper(res),
            context: context
        })
    }
}

export function expressTransformHandlerAsync<C={}>( handler: common.RequestHandlerAsync, context: C )
    : express.RequestHandler {
    return async (req: express.Request, res: express.Response) => {
        return await  handler({
            request: new RequestWrapper(req),
            response: new ResponseWrapper(res),
            context: context
        })
    }
}
