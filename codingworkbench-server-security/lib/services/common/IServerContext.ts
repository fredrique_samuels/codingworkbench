import {IViewAware} from "@codingworkbench/server/IViewAware";
import IEncryptionAware from "./IEncryptionAware";
import {IServerPropertiesAware} from "./IServerPropertiesAware";
import {IMessagesContext} from "./IMessagesContext";


export default interface IServerContext extends IViewAware,IEncryptionAware, IServerPropertiesAware, IMessagesContext {

}
