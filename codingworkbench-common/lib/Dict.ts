import IMapLike, {EntrySet} from "./IMapLike";
import Optional from "./Optional";


export class Dict<V> implements IMapLike<V> {

    protected entrySet: EntrySet<V>;

    constructor(other?: IMapLike<V>) {
        this.entrySet = { ... ( other ? other.entries : {}) }
    }

    get entries(): EntrySet<V> { return {...this.entrySet}}

    contains(key: string): boolean {
        return Object.keys(this.entrySet).includes(key);
    }

    get(key: string, params?: any): V | undefined {
        return this.entrySet[key];
    }

    getOptional(key: string, params?: any): Optional<V> {
        return Optional.forNullableValue(this.get(key, params));
    }

    keys(): string[] {
        return Object.keys(this.entrySet);
    }

    put(key: string, value: V): this {
        this.entrySet[key] = value;
        return this;
    }

    remove(key: string): this {
        delete this.entrySet[key];
        return this;
    }

    addAll(other: IMapLike<V>): this {
        this.entrySet = {
            ... this.entries,
            ... other.entries
        };
        return this;
    }

}

export default Dict;
