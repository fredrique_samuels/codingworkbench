import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {RequestHandlerFunc} from "@codingworkbench/server/common";
import {ISecurityHandlerContext, ISecurityHandlerFactoryContext} from "../../api/context";
import {create_security_model} from "../../api/response";
import {create_secure_page_url} from "../../api/links";


export default function GotoUserHome<C extends ISecurityHandlerContext>(factoryContext: ISecurityHandlerFactoryContext): IRequestHandler<C> {
    const handler: RequestHandlerFunc<C> = reqContext => {
        const view = reqContext.context.viewFactory.modelAndView(factoryContext.links.userHomePageView, create_security_model(factoryContext.links).build());
        return reqContext.response.view(view);
    };
    return new RequestHandlerBuilder(create_secure_page_url(factoryContext.links.userHomePage, factoryContext.links), handler).build();
}
