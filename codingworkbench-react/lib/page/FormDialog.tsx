// import * as React from 'react';
//
//
// export type FormDialogActionParams<F, P> = {
//     page: IPage,
//     form: F,
//     props: P
// }
// export type FormDialogAction<F, P> = (params: FormDialogActionParams<F, P>) => Promise<CwbAction.IResponse>
//
// /**
//  * @template F The form inputs type internal state. This should be extended to include the form field data.
//  * @template Prop The FormDialog properties when when used as a JSX tag.
//  */
// class FormDialog
//         <
//             I extends FormInputs,
//             S extends FormDialog.State<I> = FormDialog.State<I>,
//             P extends FormDialog.Props = FormDialog.Props
//         >
//     extends React.Component<P, S>
// {
//     constructor(props: P) {
//         super(props);
//     }
//
//     public get page(): IPage { return safe_page(this.props.page) }
//
//     /**
//      * Override the render method to draw a dialog with properties that simulate a submission form.
//      */
//     render() {
//         const dialogProps: Dialog.Props = {
//             ... this.props,
//             ... {
//                 title: this.props.title || "",
//                 text: this.props.text || "",
//                 content: this.renderForm(),
//                 actions: this.prepareActions(),
//                 closeOnAwayClick: false
//             }
//         };
//
//         return (
//             <Dialog { ...dialogProps } />
//         )
//     }
//
//     /**
//      * Called when the dialog closes. Should be overridden to
//      * reset the component state after the form is used.
//      */
//     reset(): void {}
//
//     /**
//      * Render the form content.
//      */
//     renderForm(): React.ReactNode {
//         return this.props.content;
//     }
//
//     /**
//      * Override this method to implement validation.
//      */
//     async validate(): Promise<FormInputs.Validation> {
//         return {
//             passed: true
//         };
//     }
//
//     /**
//      * Called when one of the inputs value changes.
//      *
//      * @param name The inputs field name.
//      */
//     handleChange(name: keyof I) {
//         const { state } = this;
//
//         return (event: React.ChangeEvent<HTMLInputElement>) => {
//
//             /* update the form property with the new input value */
//             let inputs: I = {
//                 ...state.inputs,
//                 [name]: event.target.value
//             };
//
//             /* update the main state with the new form */
//             this.setState({...state, inputs: inputs} );
//         };
//     }
//
//
//     disableInputs() {
//         this.setState({ disableActions: true})
//     }
//
//     enableInputs() {
//         this.setState({ disableActions: false})
//     }
//
//     ifViewContext(handler: (page: IPage) => void) {
//         const { page } = this.props;
//         Optional.forValue<IPage | undefined>(page)
//             .ifPresent(handler)
//     }
//
//     protected async executeAction(formAction: FormDialogAction<I, P>): Promise<CwbAction.IResponse> {
//         const {state, props} = this;
//         let actionResponse = await formAction({
//                 page: this.page,
//                 form: state.inputs,
//                 props: props
//             });
//         if (actionResponse.error) {
//             this.setState({inputs: {...state.inputs, validation: FormInputs.Validation.failed(actionResponse.message)}});
//         }
//         return actionResponse;
//     }
//
//     protected get disableActions(): boolean {
//         return this.state.disableActions;
//     }
//
//     protected async onSubmit(state: I): Promise<CwbAction.IResponse> {
//         return new CwbAction.ResponseBuilder()
//             .error("Submission Failed")
//             .build();
//     }
//
//     protected renderActions(fa: FormDialog.ActionProps): JSX.Element[] {
//         return [
//             <FormDialog.CancelButton  key="cancel-button" label={"Cancel"} {...fa} />,
//             <FormDialog.SubmitButton  key="submit-button" label={"Submit"} {...fa} />
//         ]
//     }
//
//     protected prepareActions(): JSX.Element[]  {
//
//         const submitAsync = async () => {
//             let validated: FormInputs.Validation = {
//                 passed: false
//             };
//
//             try {
//                 validated = await this.validate()
//             } catch (e) {
//                 this.ifViewContext((page: IPage) => {
//                     page.logError(e);
//                     page.pushWarningNotification("Error validating form");
//                 });
//                 return;
//             }
//
//             if(!validated.passed) {
//                 this.ifViewContext((page: IPage) => {
//                     page.pushWarningNotification(validated.message || "Form inputs not valid");
//                 });
//                 return
//             }
//
//             try {
//                 this.disableInputs();
//                 let res = await this.onSubmit(this.state.inputs);
//                 if(res.error) {
//                     this.ifViewContext((page: IPage) => {
//                         if(res.message) {
//                             page.pushWarningNotification(res.message);
//                         }
//                     });
//                 } else if(this.state.options.closeOnDone) {
//                     this.close();
//                 }
//             } catch (e) {
//                 this.ifViewContext((page: IPage) => {
//                     page.logError(e);
//                     page.pushErrorNotification("Submission Error");
//                 });
//             } finally {
//                 this.enableInputs();
//             }
//         };
//
//         const handleSubmit = () => {
//             submitAsync().catch(console.error)
//         };
//
//         const handleCancel = () => {
//             this.close();
//         };
//
//         const fa: FormDialog.ActionProps = {
//             cancel: handleCancel,
//             submit: handleSubmit,
//             disable: this.disableActions
//         };
//
//         return this.renderActions(fa);
//     }
//
//     private close() {
//         const { props } = this;
//         if (props.onClose) {
//             props.onClose()
//         }
//         this.reset()
//     }
// }
//
// export type FormDialogState<I> = FormDialog.State<I>;
// export type FormDialogProps = FormDialog.Props;
//
//
// namespace FormDialog {
//
//     export interface State<I> {
//         /**
//          * The form input state.
//          */
//         inputs: I;
//
//         /**
//          *  value used to disable all action on the form while performing operations
//          */
//         disableActions: boolean;
//
//         /**
//          * Form behaviour settings.
//          */
//         options: Options;
//     }
//
//     export interface Props extends Dialog.Props {
//         page?: IPage;
//     }
//
//     export interface Options {
//
//         /**
//          * Setting to
//          */
//         closeOnDone?: boolean;
//     }
//
//     /**
//      * Create a new state. Used to construct the state of the FormDialog implementation.
//      *
//      * @param options Optional options.
//      */
//     export function initState<S, I>(inputs: I, options?: Options): State<I> {
//         return {
//             disableActions: false,
//             options: {
//                 closeOnDone: true,
//                 ...options
//             },
//             inputs: inputs
//         }
//     }
//
//     export type HandleInputChangeFunc<F> = (name: keyof F) => (event: React.ChangeEvent<HTMLInputElement>) => any;
//
//     export interface ActionProps {
//         cancel: () => void,
//         submit: () => void,
//         disable: boolean
//     }
//
//
//     export interface ButtonProps extends ActionProps {
//         label: string;
//     }
//
//     export function CancelButton(props: ButtonProps) {
//         return <Button
//             key={props.label}
//             disabled={props.disable}
//             onClick={props.cancel}
//             color="secondary"
//         >
//             {props.label}
//         </Button>;
//     }
//
//     export function SubmitButton(props: ButtonProps) {
//         return <Button
//             key={props.label}
//             disabled={props.disable}
//             onClick={props.submit}
//             color="default"
//             variant={"outlined"}
//         >
//             {props.label}
//         </Button>;
//     }
//
// }
//
// export default FormDialog;
