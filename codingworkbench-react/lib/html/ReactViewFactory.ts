
import {PathResolver, ReactHtmlBuilder} from "./ReactHtmlBuilder";
import {IViewFactory, View} from "@codingworkbench/server/common";
import {HtmlModel} from "@codingworkbench/server/HtmlModel";
import {HtmlBuilder} from "@codingworkbench/server/HtmlBuilder";

export class ReactViewFactory implements IViewFactory {

    private readonly _pathResolver: PathResolver;
    private readonly _isProd: boolean;

    constructor(jsPath: string, isProd: boolean = false) {
        this._pathResolver = {
            resolve: (p: string) =>  {
                return  `${jsPath}/${p}`
            }
        };
        this._isProd = isProd;
    }

    public createHtmlBuilder(): HtmlBuilder {
        return new ReactHtmlBuilder(this._pathResolver, this._isProd);
    }

    public modelAndView(viewName: string, model?: HtmlModel): View {
        const builder = this.createHtmlBuilder() as ReactHtmlBuilder;
        builder.addReactViewJs( viewName.endsWith(".js") ? viewName : `${viewName}.js`);
        builder.setModel(model || {});
        return { html: builder.build() };
    }

}
