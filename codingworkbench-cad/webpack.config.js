const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyPlugin = require('copy-webpack-plugin');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const os =  require('os')

// calculated paths
const DIST_DIR = path.resolve(__dirname, 'build');
const SRC_ROOT_DIR = path.resolve(__dirname, '.');
const PLATFORM = os.platform()

//https://www.typescriptlang.org/docs/handbook/react-&-webpack.html

module.exports = {
    entry: {
        slice_demo: ['./lib/examples/slice_demo.ts']
    },
    target: 'node',
    output: {
        path: DIST_DIR,
        filename: '[name].js',
    },
    devtool: 'source-map',
    externals: {
        "react": "React",
        "react-dom": "ReactDOM",
    },
    resolve: {
        extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
        modules: [
            SRC_ROOT_DIR,
            path.join(__dirname, './node_modules/'),
        ]
    },
    module: {
        rules: [
            {
                // this is so that we can compile any React,
                // ES6 and above into normal ES5 syntax
                test: /\.(js|jsx)$/,
                // we do not want anything from node_modules to be compiled
                exclude: /node_modules/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        // presets: ['es2015', 'react'],
                        // plugins: ['react-html-attrs', 'transform-decorators-legacy2', 'transform-class-properties', 'transform-object-rest-spread']
                    },
                }]


            },
            {
                test: /\.(ts|tsx)$/,
                loader: 'ts-loader'
            },
            { enforce: "pre", test: /\.(js|jsx)$/, loader: "source-map-loader" },
            {
                test: /\.txt$/,
                use: 'raw-loader'
            },
            {
                test: /\.glsl/,
                use: 'raw-loader'
            }
        ]
    },
    resolve: {
        plugins: [
            new TsconfigPathsPlugin({ configFile: "./tsconfig.json" })
        ]
    }
    //
};
