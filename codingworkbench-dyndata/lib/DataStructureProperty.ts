import VersionedObject from "./VersionedObject";
import IDataStructure, {IDataStructureProperty, IPropertyOptions} from "./IDataStructure";
import CrudDataAccessor from "@codingworkbench/data/CrudDataAccessor";
import {DataStructureTable} from "./DataStructure";


export default class DataStructureProperty extends VersionedObject<IDataStructureProperty> {

    constructor(dao: CrudDataAccessor) {
        super(dao, DataStructureTable);
    }

    createParams(): IDataStructureProperty {
        return {
            ...this.createVersionObjectParams(),
            name: "",
            options: {},
            type: "",
            parent: null
        };
    }

    public update(params: IDataStructureProperty): this {
        super.update(params);
        this._params.name = params.name;
        this._params.type = params.type;
        this._params.parent = params.parent;
        this._params.options = {...params.options};
        return this;
    }

}
