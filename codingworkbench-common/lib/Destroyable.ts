/**
 * <p>Interface providing an object with destruction operations.</p>
 *
 * <p>The context of 'destroy' is relative to the caller. For example <code>ObjectArray</code> calls destroy when an object is removed from the array.</p>
 */
export interface Destroyable {

    /**
     * Called when the object is destroyed.
     */
    destroy(): Promise<void>
}
