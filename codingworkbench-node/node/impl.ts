import {ErrorFunction, IAttribute, IDataAccessor, IElement, IElementGroup, INode, INodeArray, IRootElement, NodeType} from "./api";
import {ElementDefinition, ElementGroupDefinition} from "./definition";
import CwbNode from "./index";

interface IElementOptions<T, P=any> {
    value?: T | null,
    name?: string,
    parent?: IElement<P>;
    errorFunction?: ErrorFunction | null;
}

function parentToString(e: IElement) {
    return `<${e.tag} name="${e.name}" />`;
}

export function getErrorMessage(n: INode) {
    if (n.type === NodeType.EMPTY) {
        const parentMessage: string = n.parent
            ? ` in parent ${parentToString(n.parent)}`
            : "";
        return`Node name="${n.name}" does not exists${parentMessage}!"`;
    }
    return`Node name="${n.name}" type="${NodeType[n.type]}" ${n.parent ? `parent=${parentToString(n.parent)}` : ""} is not being consumed!`;
}

const defaultErrorFunction: ErrorFunction = n => console.warn(getErrorMessage(n));

interface NodeOptions {
    parent: IElement<any> | null,
    errorFunction: ErrorFunction | null
}

class NodeImpl implements INode {
    private readonly _parent: IElement | null;
    private readonly _name: string;
    private readonly _type: NodeType;
    protected readonly errorFunction: ErrorFunction | null;

    constructor(name: string, options: NodeOptions, type: NodeType) {
        this._name = name;
        this._parent = options.parent;
        this._type = type;
        this.errorFunction = options.errorFunction;
    }

    get parent(): IElement<any> | null { return this._parent; }
    get name(): string { return this._name; }
    get type(): NodeType { return this._type; }

    public asAttribute<T>(handler: (iAttribute: IAttribute<T>) => void, notValidCallback?: ErrorFunction): this {
        if (this._type === NodeType.ATTRIBUTE) {
            handler((this as unknown) as IAttribute<T>);
        } else {
            this.processError(notValidCallback);
        }
        return this;
    }

    public asElement<T>(handler: (iElement: IElement<T>) => void, notValidCallback?: ErrorFunction): this {
        if (this.isElement()) {
            handler((this as unknown) as IElement<T>);
        } else {
            this.processError(notValidCallback);
        }
        return this;
    }

    public get(name: string): INode { return this.createEmptyChild(name) }

    public getChildrenNames(search: string): string[] {
        return [];
    }

    public searchChildren(search: string = ""): INode[] {
        if (!this.isElement()) {
            return []
        }

        const me = this;
        return this.getChildrenNames(search).map(n => me.get(n)).filter(n => n.type !== NodeType.EMPTY);
    }

    private processError(notValidCallback?: ErrorFunction) {
        if(notValidCallback) {
            notValidCallback(this);
        } else if(this.errorFunction) {
            this.errorFunction(this);
        } else {
            defaultErrorFunction(this);
        }
    }

    private isElement() {
        return this._type === NodeType.ELEMENT || this._type === NodeType.ELEMENT_GROUP;
    }

    protected createEmptyChild(name: string) {
        return new NodeImpl(name, this.createNodeOptions(), NodeType.EMPTY);
    }

    private createNodeOptions() {
        return {
            parent: this.isElement() ? this as unknown as IElement : null,
            errorFunction: this.errorFunction,
        };
    }
}

class ElementGroupImpl<T, P=T> extends NodeImpl implements IElementGroup<T> {
    private _value: T | null;
    private readonly definition: ElementGroupDefinition<T, P>;

    public constructor(definition: ElementGroupDefinition<T, P>, options: IElementOptions<T> = {}) {
        super(options.name || "", {
            parent: options.parent || null,
            errorFunction: options.errorFunction || null,
        }, NodeType.ELEMENT_GROUP);
        this._value = options.value || null;
        this.definition = definition;
    }

    get tag(): string {return this.definition.tag; }
    get value(): T | null { return null;}
    set value(_v: T | null) {}

    get(name: string): INode {
        if (!this.parent) {
            return this.createEmptyChild(name);
        }

        if (this.definition.hasChildFunction(this.parent.value, name)) {
            return new ElementImpl(this.definition.elementDefinition, {
                parent: this.parent,
                name: name,
                errorFunction: this.errorFunction,
            });
        }

        return this.createEmptyChild(name);
    }

    public getChildrenNames(search: string): string[] {
        if (!this.parent) {
            return [];
        }
        return this.definition.searchChildNamesFunction(this.parent.value, search);
    }

}

export class ElementImpl<T, P=T> extends NodeImpl implements IElement<T> {
    private _value: T | null;
    private readonly definition: ElementDefinition<T, P>;

    public constructor(definition: ElementDefinition<T, P>, options: IElementOptions<T> = {}) {
        super(options.name || "", {
            parent: options.parent || null,
            errorFunction: options.errorFunction || null,
        }, NodeType.ELEMENT);
        this._value = options.value || null;
        this.definition = definition;
    }

    public get(name: string): INode {
        const child = this.definition.children[name];

        if (!child) {
            return this.createEmptyChild(name);
        }

        if (child.type === NodeType.ELEMENT_GROUP) {
            return new ElementGroupImpl<any, T>(child, {
                name: name,
                parent: this,
                errorFunction: this.errorFunction
            });
        }

        if (child.type === NodeType.ELEMENT) {
            return new ElementImpl<any, T>(child, {
                name: name,
                parent: this,
                errorFunction: this.errorFunction
            });
        }

        if (child.type === NodeType.ATTRIBUTE) {
            return new Attribute<T, any>(name, this, this.errorFunction, child);
        }

        return this.createEmptyChild(name);
    }

    public getChildrenNames(search: string): string[] {
        return Object.keys(this.definition.children).filter( k => CwbNode.matchesSearch(search, k));
    }

    get tag(): string { return this.definition.tag; }

    get value(): T | null {
        if (this.parent && !this._value) {
            return this.definition.getValue(this.parent.value, this.name);
        }
        return this._value;
    }

    set value(v : T | null) {
        if (this.parent && !this._value) {
            this.definition.setValue(v, this.parent.value, this.name);
        } else {
            this._value = v;
        }
    }
}

export class Attribute<E, T=any> extends NodeImpl implements IAttribute<T> {
    private readonly accessor: IDataAccessor<T, E>;

    constructor(name: string, parent: IElement<E>, errorFunction: ErrorFunction | null, accessor: IDataAccessor<T, E>) {
        super(name, {
            parent,
            errorFunction
        } , NodeType.ATTRIBUTE);
        this.accessor = {
            setValue: accessor.setValue,
            getValue: accessor.getValue
        };
    }

    get value(): T | null {
        return this.parent ? this.accessor.getValue(this.parent.value, this.name) : null;
    }

    set value(v: T | null) {
        if (this.parent) {
            this.accessor.setValue(v, this.parent.value, this.name);
        }
    }
}

export class NodeArrayImpl implements INodeArray {
    private readonly _arr: INode[];

    constructor(arr: INode[]) {
        this._arr = arr;
    }

    get values(): INode[] {
        return [... this._arr];
    }

    public ifEmpty(callback: () => {}): this {
        if (this._arr.length === 0) {
            callback();
        }
        return this;
    }
}

interface SearchContext {
    workingArr: INode[]
}

export class RootImpl<T> extends ElementImpl<T, T> implements IRootElement<T> {

    constructor(elementDefinition: ElementDefinition<T, T>, value: T | null, errorFunc?: ErrorFunction) {
        super(elementDefinition, {value: value, errorFunction: errorFunc});
    }

    public select(selector: string): INodeArray {
        const context: SearchContext = {
            workingArr: []
        };

        if (selector && selector.length) {
            const nameRegex: string[] = selector.split(".");
            context.workingArr = [this];
            nameRegex.forEach((s) => {
                const arr: INode[] = [];
                context.workingArr.forEach( p => {
                    (p as NodeImpl).searchChildren(s).forEach( c =>  arr.push(c));
                });
                context.workingArr = arr;
            });
        }

        return new NodeArrayImpl(context.workingArr);
    }
}
