import * as React from "react";

export interface IFormInputsAware<I> {
    inputs: I
}

export interface IFormValidationAware {
    validation: IFormValidation;
}

export type FormErrors = {[s: string]: string};

export interface IFormValidation {
    passed: boolean;
    message?: string;
    errors?: FormErrors;
}

export function validation_passed(): IFormValidation {
    return {passed: true};
}

export function validation_failed(message?: string, errors?: FormErrors): IFormValidation {
    return {
        passed: false,
        message: message,
        errors: errors || {}
    };
}

export function handle_input_state_change<I>(name: keyof I, state: IFormInputsAware<I>, setState: (s: any) => void) {
    return (event: React.ChangeEvent<HTMLInputElement>) => {

        /* update the form property with the new input value */
        let inputs: I = {
            ...state.inputs,
            [name]: event.target.value
        };

        /* update the main state with the new form */
        setState({...state, inputs: inputs} );
    };
}
