export default interface ISettingOption {
    name: string;
    label: string;
    value: any;
}
