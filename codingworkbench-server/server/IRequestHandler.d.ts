import * as common from "./common";
/**
 * Structure to store the basic route information
 * for incoming requests.
 */
export interface IRequestHandler<ContextType = {}> {
    isAsync: boolean;
    url: string;
    handler: common.RequestHandlerFunc<ContextType>;
    handlerAsync: common.RequestHandlerAsync<ContextType>;
    method?: common.Method;
}
export default IRequestHandler;
