import DbTable from "@codingworkbench/data/DbTable";
import DbColumn from "@codingworkbench/data/DbColumn";


export class VersionedDbTable extends DbTable {

    constructor(name: string, defs: DbColumn[]) {
        super(name, [
            ...defs,
            new DbColumn("version"),
            new DbColumn("orphaned"),
        ]);

        this.setUnique()
            .setArchived()
            .setAudited();
    }
}
