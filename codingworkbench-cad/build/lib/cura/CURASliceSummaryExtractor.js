"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var parsers_1 = require("./parsers");
var CURASliceSummaryExtractor = (function () {
    function CURASliceSummaryExtractor(gcode) {
        this.gcode = gcode;
    }
    CURASliceSummaryExtractor.prototype.extractFromOutput = function (output) {
        var summary = {};
        output.split("\n")
            .forEach(function (line) {
            if (line.startsWith(parsers_1.CURA_TIME_PARSER.prefix)) {
                summary.timeInSeconds = parsers_1.CURA_TIME_PARSER.parse(line);
            }
            else if (line.startsWith(parsers_1.CURA_TIME_FORMATTED_PARSER.prefix)) {
                summary.timeFormatted = parsers_1.CURA_TIME_FORMATTED_PARSER.parse(line);
            }
            else if (line.startsWith(parsers_1.CURA_FILAMENT_USED_PARSER.prefix)) {
                summary.filamentUsedInMeters = parsers_1.CURA_FILAMENT_USED_PARSER.parse(line);
            }
            else if (line.startsWith(parsers_1.CURA_MIN_X_PARSER.prefix)) {
                summary.minx = parsers_1.CURA_MIN_X_PARSER.parse(line);
            }
            else if (line.startsWith(parsers_1.CURA_MIN_Y_PARSER.prefix)) {
                summary.miny = parsers_1.CURA_MIN_Y_PARSER.parse(line);
            }
            else if (line.startsWith(parsers_1.CURA_MIN_Z_PARSER.prefix)) {
                summary.minz = parsers_1.CURA_MIN_Z_PARSER.parse(line);
            }
            else if (line.startsWith(parsers_1.CURA_MAX_X_PARSER.prefix)) {
                summary.maxx = parsers_1.CURA_MAX_X_PARSER.parse(line);
            }
            else if (line.startsWith(parsers_1.CURA_MAX_Y_PARSER.prefix)) {
                summary.maxy = parsers_1.CURA_MAX_Y_PARSER.parse(line);
            }
            else if (line.startsWith(parsers_1.CURA_MAX_Z_PARSER.prefix)) {
                summary.maxz = parsers_1.CURA_MAX_Z_PARSER.parse(line);
            }
        });
        if (summary.minx !== undefined && summary.maxx !== undefined) {
            summary.dimx = summary.maxx - summary.minx;
            summary.centerx = summary.minx + 0.5 * summary.dimx;
        }
        if (summary.miny !== undefined && summary.maxy !== undefined) {
            summary.dimy = summary.maxy - summary.miny;
            summary.centery = summary.miny + 0.5 * summary.dimy;
        }
        if (summary.minz !== undefined && summary.maxz !== undefined) {
            summary.dimz = summary.maxz - summary.minz;
            summary.centerz = summary.minz + 0.5 * summary.dimz;
        }
        return {
            gcode: this.gcode,
            summary: summary
        };
    };
    return CURASliceSummaryExtractor;
}());
exports.default = CURASliceSummaryExtractor;
