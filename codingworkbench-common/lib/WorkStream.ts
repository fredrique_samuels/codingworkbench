
// const invalidToken = () => !token;
// const activateAccount = async () => await securityService.activateAccount(token);
// const gotoSignIn = () => response.redirect(securityLinks.signInPage);
// const isErrorResponse = (res: CwbAction.IResponse) => res.error;
// const gotoAccountActivated = () => response.redirect(securityLinks.activateAccountSuccessView);
//
// const stream = new WorkStream().error((e) => web_respond_with_500(e, requestContext));
// stream.terminal(gotoSignIn).predicate(invalidToken).end()
//       .execute(activateAccount)
//       .terminal(gotoSignIn).predicate(isErrorResponse).end()
//       .then(gotoAccountActivated);

interface IWorkInputs<C=any, R=any> {
    context: C;
    input: R;
}

interface IWorkItem<C=any, I=any, P=any> {
    // errors
    errorSync(callback: (e: Error) => any): this;
    error(callback: (e: Error) => Promise<any>): this;

    // terminal
    // terminateSync(predicate: () => boolean, callback?: (wi: IWorkInputs<C, I>) => any): IWorkItem<C, R, I>;
    terminate(predicate: () => Promise<boolean>, callback?: (wi: IWorkInputs<C, I>) => Promise<any>): this;

    // runners
    runSync<R=any>(callback?: (wi: IWorkInputs<C, I>) => R): IWorkItem<C, R, I>
    run<R=any>(callback?: (wi: IWorkInputs<C, I>) => Promise<R>): IWorkItem<C, R, I>

    // terminate definition end return parent flow
    end(): IWorkItem<C, P>
}

interface IWorkStream<C=any, I=any> extends IWorkItem<C, I, I> {

}



