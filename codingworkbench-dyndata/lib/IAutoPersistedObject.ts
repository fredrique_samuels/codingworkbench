import {IVersionedObject} from "./IVersionedObject";
import {ID} from "@codingworkbench/data/IUnique";


export default interface IAutoPersistedObject extends IVersionedObject {
    _data_structure: ID;
    _reference_stack: ID[];
}
