from cwb_control.Plant import Plant
from cwb_control.Transformers import min_max_transformer, clamp

MAX_PEDAL_DISTANCE = 60.0
MAX_ACCELERATION = 100
MAX_VELOCITY = 100


class Car(Plant):

    def __init__(self, name, ds):
        Plant.__init__(self, name, ds)
        self.velocity = self.create_data_point("velocity", 0).add_transformer_func(min_max_transformer(0, MAX_VELOCITY))
        self.pedal = self.create_data_point("pedal", 0).add_transformer_func(min_max_transformer(0, MAX_PEDAL_DISTANCE))
        self.acceleration = self.create_data_point("acceleration", 0)\
            .add_transformer_func(min_max_transformer(-MAX_ACCELERATION, MAX_ACCELERATION))
        self.acceleration_factor = self.create_data_point("acceleration_factor", 1)

    def update(self, dt):
        # max speed for the current pedal position
        tv = (MAX_VELOCITY / MAX_PEDAL_DISTANCE) * self.pedal.get()

        current_velocity = self.velocity.get()
        acceleration = min(MAX_ACCELERATION, abs(tv - current_velocity) * self.acceleration_factor.get())
        acceleration_dir = 1 if tv >= current_velocity else -1

        self.acceleration.set(acceleration * acceleration_dir)

        # update velocity
        new_vel = current_velocity + (self.acceleration.get() * dt)
        self.velocity.set(new_vel)
