import  * as React from "react";

import { LinearProgress, makeStyles } from "@material-ui/core";
import PageDialog, {PageDialogProps} from "./PageDialog";


const useStyles = makeStyles({
    root: {
      flexGrow: 1,
    },
  });
  
function LinearIndeterminate() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <LinearProgress />
        </div>
    );
}

export function LoadingDialog(props: PageDialogProps) {
    const classes = useStyles();

    const dialogProps: PageDialogProps = {
        ... props,
        ... { 
            title: props.title,
            maxWidth: "md",
            content: <div key="loading" className={classes.root}><LinearIndeterminate key="linear" /></div>,
            closeOnAwayClick: false
        }
    };
    return <PageDialog { ... dialogProps } />
}

export default LoadingDialog;
