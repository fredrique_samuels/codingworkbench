export interface IEmailContent {
    email: string;
    title: string;
    body: string;
}
