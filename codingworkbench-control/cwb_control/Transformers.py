

def clamp(min_value, max_value, val):
    return max(min_value, min(max_value, val))


def min_max_transformer(min_value, max_value):
    return lambda x: clamp(min_value, max_value, x)
