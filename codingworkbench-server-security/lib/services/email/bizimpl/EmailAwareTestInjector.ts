import EmailAwareInjector from "../inject/EmailAwareInjector";
import IEmailTransporter from "../api/IEmailTransporter";
import MockedEmailTransporter from "../test/MockedEmailTransporter";


export default class EmailAwareTestInjector extends EmailAwareInjector {

    constructor() {
        super();
        this.bind("emailTransporter", async (): Promise<IEmailTransporter> => new MockedEmailTransporter())
    }
}
