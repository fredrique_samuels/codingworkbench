import {ID} from "./IUnique";

export interface ILastUpdateAware {
    /* date that the record was last updated */
    readonly date_last_updated: Date;

    /* user that last updated the record */
    readonly user_last_updated: ID;
}

export default ILastUpdateAware;
