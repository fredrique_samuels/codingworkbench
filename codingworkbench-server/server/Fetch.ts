
export class Fetch <R=any> {

    private readonly requestInit: RequestInit;

    constructor() {
        this.requestInit = {
            method: "GET",
            headers: {},
            body: JSON.stringify({}),
        }
    }

    public setBody(b: any): this {
        this.requestInit["body"] = b;
        return this;
    }

    public setJSONBody(b: any): this {
        this.requestInit["body"] = JSON.stringify(b);
        this.addHeader("Content-Type", "application/json");
        return this;
    }

    public setPost(): this {
        this.requestInit["method"] = "POST";
        return this;
    }

    private addHeader(key: string, value: string): this {
        const v:HeadersInit = {};
        v[key] = value;
        this.requestInit.headers = {
            ... this.requestInit.headers || {},
            ...v
        };
        return this;
    }

    public async json(url: string): Promise<R> {
        this.addHeader("Accept", "application/json");
        const r = await fetch(url, this.requestInit);
        return (await r.json()) as R;
    }
}
