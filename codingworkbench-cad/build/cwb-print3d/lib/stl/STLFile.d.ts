export default class STLFile {
    _filename: string;
    constructor(filename: string);
    isBinary(): boolean;
}
