## Create run scripts
PROJECT_ID=server-security node manifest.js

## RUN AND PUBLISH
```sh
sh publish.sh <project_id>
```

## Latest package version
```sh
npm show @codingworkbench/data version
```

output
```
1.1.9
```

## project config example

```typescript
{
  "name": "common",
  "basePath": "../codingworkbench-common",
  "srcRoot": "lib",
  "files": [
    "package.json",
    "package-lock.json",
    "tsconfig.json",
    "tslint.json",
    ".eslintrc.json",
    "LICENSE",
    "README.md"
  ]
}

```
