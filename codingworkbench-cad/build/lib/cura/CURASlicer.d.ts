import ISlicer from "../ISlicer";
import ICURAOptions from "./CURAOptions";
import { CURAPaths } from "./CURAPaths";
import ISliceJobOutput from "lib/ISliceJobOutput";
export default class CURASlicer implements ISlicer {
    private readonly enginePaths;
    constructor(enginePaths: CURAPaths);
    slice(stlFile: string, gCodeFile: string, options: ICURAOptions): Promise<ISliceJobOutput>;
    sliceStlString(stlString: string, options: ICURAOptions): Promise<ISliceJobOutput>;
    private buildSlicerCommand;
}
