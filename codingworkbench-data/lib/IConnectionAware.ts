import IConnectionPropertiesAware from "./IConnectionPropertiesAware";
import CwbLog from "@codingworkbench/log";
import MysqlConnection from "./MysqlConnection";
import CrudDataAccessor from "./CrudDataAccessor";
import MongoConnection from "./MongoConnection";


export default interface IConnectionAware extends IConnectionPropertiesAware {
    sqlConnection: MysqlConnection;
    sqlDao: CrudDataAccessor;

    mongoConnection: MongoConnection;
    mongoDao: CrudDataAccessor;
}

export async function connection_aware_close(connectionAware: IConnectionAware) {
    try {
        if (connectionAware.sqlConnection)
            await connectionAware.sqlConnection.close();
    } catch (e) {
        CwbLog.warning(e)
    }

    try {
        if (connectionAware.mongoConnection)
            await connectionAware.mongoConnection.close();
    } catch (e) {
        CwbLog.warning(e)
    }
}
