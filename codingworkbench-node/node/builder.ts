import {AttributeDefinition, Child, ElementDefinition, ElementGroupDefinition} from "./definition";
import {ErrorFunction, HasChildPredicate, IRootElement, NodeType, ReadFunction, SearchChildNamesFunction, WriteFunction} from "./api";
import {readOnlyData} from "./common";
import {RootImpl} from "./impl";

export class AttributeBuilder<T, P=any> {
    private definition: AttributeDefinition<T, P>;

    constructor(readFunction: ReadFunction<T, P>) {
        this.definition = {
            type: NodeType.ATTRIBUTE,
            ...readOnlyData(readFunction)
        }
    }

    public setWriteFunc(writeFunction: WriteFunction<T, P>): this {
        this.definition = {
            ...this.definition,
            setValue: writeFunction,
        };
        return this;
    }

    public build(): AttributeDefinition<T, P> {
        return {...this.definition}
    }
}

export abstract class AbstractElementBuilder<T, P=any> {
    protected definition: ElementDefinition<T, P>;

    protected constructor(tag: string, readFunction: ReadFunction<T, P>) {
        this.definition = {
            type: NodeType.ELEMENT,
            tag,
            children: {},
            ... readOnlyData(readFunction),
        }
    }

    public setWriteFunc(writeFunction: WriteFunction<T, P>): this {
        this.definition = {
            ...this.definition,
            setValue: writeFunction,
        };
        return this;
    }

    public addChild(name: string, child: Child<T>): this {
        this.definition.children[name] = child;
        return this;
    }
}

export class ElementBuilder<T, P=any> extends AbstractElementBuilder<T, P> {

    constructor(tag: string, readFunction: ReadFunction<T, P>) {
        super(tag, readFunction);
    }

    public build(): ElementDefinition<T, P> {
        return {...this.definition}
    }
}

export class ElementGroupBuilder<T, P=any> {

    private readonly definition: ElementGroupDefinition<T, P>;
    private searchChildNamesFunction: SearchChildNamesFunction<P>;

    constructor(tag: string, elementDefinition: ElementDefinition<T, P>,  hasChildFunction: HasChildPredicate<P>,) {
        this.definition = {
            type: NodeType.ELEMENT_GROUP,
            tag,
            hasChildFunction: hasChildFunction,
            searchChildNamesFunction: () => [],
            elementDefinition: {...elementDefinition},
        };

        this.searchChildNamesFunction = () => [];
    }

    public setSearchChildNamesFunction(func: SearchChildNamesFunction<P>): this {
        this.searchChildNamesFunction = func;
        return this;
    }

    public build(): ElementGroupDefinition<T, P> {
        return {
            ...this.definition,
            searchChildNamesFunction: this.searchChildNamesFunction
        }
    }
}

export class NodeTreeBuilder<T> extends AbstractElementBuilder<T, T>  {
    private readonly value: T;
    private _errorFunc?: ErrorFunction;

    constructor(tag: string, value: T) {
        super(tag, (data) => data);
        this.value = value;
    }

    public setErrorFunction(errorFunc: ErrorFunction): this {
        this._errorFunc = errorFunc;
        return this;

    }

    public build(): IRootElement<T> {
        return new RootImpl<T>(this.definition, this.value, this._errorFunc);
    }
}
