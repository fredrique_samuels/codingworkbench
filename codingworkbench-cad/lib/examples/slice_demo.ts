import * as fs from "fs";
import * as path from "path";
import CwbLog from "@codingworkbench/log";
import {CURAPaths} from "../print/cura/CURAPaths";
import CURASlicer from "../print/cura/CURASlicer";
import { ICURA_PROFILE_CREALITY_ENDER3 } from "../print/cura/CURAOptions";
import { SlicerServiceImpl } from "../print/SlicerServiceImpl";
import ISliceJobInput from "../print/ISliceJobInput";

CwbLog.setup();
CwbLog.enable_console();

// create slicer
const curaPaths = CURAPaths.fromPropertiesFile("./config/printer.properties");
const curaSlicer = new CURASlicer(curaPaths);

// setupe the service 
const service = new SlicerServiceImpl(curaSlicer);  

const rootDir = "./resources/XYZ_da_Vinci_1.0_E3D_V6_Bowden/files";
const files = fs.readdirSync(rootDir)
    .map(f => path.join(rootDir, f))
    .filter(f => f.toLowerCase().endsWith(".stl"))
    ;


//var jsonMerger = require("json-merger");
//var result = jsonMerger.mergeFiles(["a.json", "b.json"]);


// create the slice paramters
const file = "./resources/XYZ_da_Vinci_1.0_E3D_V6_Bowden/files/chassis.STL";
const input: ISliceJobInput = { profile: ICURA_PROFILE_CREALITY_ENDER3, settings:[] };

// slice all files
service.sliceBatch(files, input)
    .then(jr => console.log(jr.summary))
    .catch(console.error)
