import {IDataAccessor, ReadFunction} from "./api";

const READONLY_ERROR = () => { throw new Error(`Error trying to write to a readonly value`)};

export function readOnlyData<T, P>(readFunc: ReadFunction<T, P>): IDataAccessor<T, P> {
    return {
        getValue: readFunc,
        setValue: READONLY_ERROR
    }
}

export function matchesSearch(search: string, key: string): boolean {
    return new RegExp(`\\b${search.replace("*", ".*")}\\b`).test(key);
}
