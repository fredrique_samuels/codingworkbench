import {deepStrictEqual} from "assert";
import DisplayFormat from "lib/DisplayFormat";

describe("common.test.ts", () => {

    it ("dateToMDY", () => {
        const date = new Date(1591028195857);
        deepStrictEqual(DisplayFormat.dateToMDY(date), "June 1, 2020");
    });

    it ("dateToMDYT", () => {
        const date = new Date(1591028195857);
        deepStrictEqual(DisplayFormat.dateToMDYT(date), "June 1, 2020 18:16");
    });

});
