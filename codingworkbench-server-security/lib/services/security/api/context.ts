
import ISecurityService from "./ISecurityService";
import {ISecurityLinks} from "./links";
import {IViewAware} from "@codingworkbench/server/IViewAware";
import {RequestContext} from "@codingworkbench/server/common";
import CwbI8N from "@codingworkbench/i8n";
import IEmailAwareContext from "../../email/api/IEmailAwareContext";
import IServerContext from "../../common/IServerContext";
import IConnectionAware from "@codingworkbench/data/IConnectionAware";

export interface ISecurityAwareContext extends IServerContext, IEmailAwareContext, IConnectionAware {
    securityLinks: ISecurityLinks;
    securityService: ISecurityService;
    securityCallbackHost: string,
    messageSource: CwbI8N.IMessageSource;
}

export type OnAuthenticatedCallback = (webContext: RequestContext<any>) => any;

export interface ISecurityHandlerContext extends ISecurityAwareContext, IViewAware {
    onAuthenticatedCallback?: OnAuthenticatedCallback;
}

export interface ISecurityHandlerFactoryContext {
    links: ISecurityLinks;
}

export default ISecurityAwareContext;
