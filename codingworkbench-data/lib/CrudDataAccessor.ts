
import CwbSearch from "@codingworkbench/search";
import CwbLog from "@codingworkbench/log";

import {ArchiveAware} from "./ArchiveAware";
import {AuditAware} from "./AuditAware";

import {DataAccessor} from "./DataAccessor";
import {DbConnection} from "./DbConnection";
import {DbTable} from "./DbTable";
import {DbQuery} from "./DbQuery";
import {IAuditAware} from "./IAuditAware";
import {ID, IUnique} from "./IUnique";

export class CrudDataAccessor implements DataAccessor {
    protected db: DbConnection;
    private _debug: boolean;

    constructor(db: DbConnection) {
        this.db = db;
        this._debug = false;
    }

    public setDebug(): this {
        this._debug = true;
        return this;
    }

    public async execute(q: DbQuery): Promise<any[]> {
        return await this.db.execute(q);
    }

    /**
     * @see DataAccessor.saveObject()
     */
    public async saveObject<T extends IUnique>(table: DbTable, obj: T, userId: ID = null): Promise<ID> {

        if (obj.id !== null) {
            const record = {
                ...obj,
                ...table.audited
                    ? new AuditAware((obj as unknown) as IAuditAware, userId).setLastUpdated(userId).flatten()
                    : {}
            };

            const q = this.db.getQueryFactory().createUpdateQuery(table, record);
            this.debugQ(q);

            await (await this.db.createUpdate(q)).execute();
            return obj.id;
        } else {
            const record = {
                ...obj,
                ...table.audited
                    ? new AuditAware({}, userId).flatten()
                    : {},
                ...table.archived
                    ? new ArchiveAware().flatten()
                    : {}
            };

            const q = this.db.getQueryFactory().createInsertQuery(table, record);
            this.debugQ(q);

            let update = await this.db.createUpdate(q);
            const [rows] = await update.execute();
            return (rows.id) as ID;
        }
    }

    /**
     * Get an SavedObject by its id.
     *
     * @param table The target table.
     * @param id The object id.
     * @throws SystemError if the object could not be found.
     */
    public async getObject<T extends IUnique>(table: DbTable, id: ID): Promise<T> {
        const searchBuilder = new CwbSearch.SearchBuilder();
        searchBuilder.filters.addFilter("id", id);

        const q = this.db.getQueryFactory().createSearchQuery(table, searchBuilder.build());
        this.debugQ(q);

        let getQ = await this.db.createQuery(q);
        const objects = await getQ.query();
        const [obj] = objects;
        if (obj) {
            return obj;
        }
        throw new Error(`Unable to find object with id ${id} in table ${table.name}`);
    }

    /**
     *  Delete or archive a given record.
     *
     * @param table The target table.
     * @param id The object id.
     */
    public async deleteObject(table: DbTable, id: ID) {
        const record = await this.getObject(table, id);

        const q = this.db.getQueryFactory().createDeleteObjectQuery(table, record);
        this.debugQ(q);

        await (await this.db.createQuery(q)).query();
    }

    /**
     * Search the table for all records matching the given search params.
     *
     * @param table The target table.
     * @param searchParams Search parameters.
     * @param enhanceSearchFunc optional Callback used to update the search parameters if needed.
     */
    public async searchObjects<T extends IUnique>(table: DbTable,
                                                               searchParams: CwbSearch.Search
    ): Promise<T[]> {
        const q = this.db.getQueryFactory().createSearchQuery(table, searchParams);
        this.debugQ(q);

        return await (await this.db.createQuery(q)).query();
    }

    public async searchObjectsPaginated<T extends IUnique>(table: DbTable, searchParams: CwbSearch.Search): Promise<CwbSearch.IResults<T>> {
        const me = this;
        const dataFunctions: CwbSearch.PaginatedDataFunctions<T> = {
            dataSupplier: async (s: CwbSearch.Search) => {
                return await me.searchObjects(table, s);
            },
            totalSupplier: async (s: CwbSearch.Search) => {
                const searchBuilder = new CwbSearch.SearchBuilder(s);
                searchBuilder.pagination.setPageSize(CwbSearch.INF_PAGE_SIZE).setOffset(0);
                return await me.countRecords(table, searchBuilder.build());
            }
        };
        return await CwbSearch.do_paged_search(searchParams, dataFunctions);
    }

    private async countRecords(
        table: DbTable,
        searchParams: CwbSearch.Search
    ): Promise<number> {

        const q = this.db.getQueryFactory().createCountQuery(table, searchParams);
        this.debugQ(q);

        const [c] = await (await this.db.createQuery(q)).query();
        return c.count;
    }

    private debugQ(q: DbQuery) {
        if (this._debug) {
            CwbLog.info(q.command);
            CwbLog.info(q.values);
        }
    }

}

export default CrudDataAccessor;
