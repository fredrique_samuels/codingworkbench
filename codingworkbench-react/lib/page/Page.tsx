import * as React from "react"

import CwbAction from "@codingworkbench/action";
import HasStateComponent from "../state/HasStateComponent";

import PopupNotification from "../notifications/PopupNotification";
import LoadingDialog from "../dialogs/LoadingDialog";
import TitlePanel from "../TitlePanel";
import IPage from "./IPage";
import * as OpenClose from "../common/OpenClose";
import {PageAction} from "./PageAction";
import PageDialog, {PageDialogProps, PageDialogOptions} from "../dialogs/PageDialog";
import {HTML_DATA_MODEL_ID, HtmlModel, ModelSupplier} from "@codingworkbench/server/HtmlModel";
import {IFormValidation, validation_failed, validation_passed} from "../form/FormInputs";


/**
 * Basic page implementation that implements the ViewContext interface.
 */
export class Page
        <
            P extends IPageProps = IPageProps,
            S extends IPageState = IPageState
        > 

    extends HasStateComponent<P, S>
    implements IPage<S>, ModelSupplier {

    constructor(props: any) {
      super(props);
    }

    render() {
      return <div key={"page-root"} style={{flexGrow: 1}} >
            <TitlePanel key={"base-title-panel"}
                title={this.state.title}
                subtitle={this.state.subtitle}
            />
            { this.props.children }
            { this.renderChildren() }
            <PopupNotification key={"base-popup-notification"} { ...this.state._popupNotification } />
            <LoadingDialog key={"base-loading-dialog"}  { ...this.state._loadingDialog } />
        </div>
    }

    renderChildren(): React.ReactNode[] { return [] }
    logError(e: Error): void { console.error(e.stack); }
    webAction(url: string): PageAction { return new PageAction(url, this); }

    async submitForm(url: string, body?: any): Promise<IFormValidation> {
        try {
            const response: CwbAction.IResponse = await this.webAction(url).post().setBody(JSON.stringify(body || {})).json();
            if (response.error) {
                return validation_failed(response.message);
            }
            if (response.redirect) {
                window.location.href = response.redirect;
            }
            return validation_passed();
        } catch (e) {
            return validation_failed("We are unable to process your request at this time");
        }
    }

    open(target: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, keyof S>) { OpenClose.open(this, target, targetUpdates, stateUpdates); }
    close(target: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, keyof S>) { OpenClose.close(this, target, targetUpdates, stateUpdates); }
    openHandler(target: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, keyof S>): () => void { return OpenClose.openHandler(this, target, targetUpdates, stateUpdates); }
    closeHandler(target: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, keyof S>): () => void { return OpenClose.closeHandler(this, target, targetUpdates, stateUpdates); }
    pushErrorNotification(message: string): void { PopupNotification.pushError<S>(this, "_popupNotification", message); }
    pushWarningNotification(message: string): void { PopupNotification.pushWarning<S>(this, "_popupNotification", message); }
    pushInfoNotification(message: string): void { PopupNotification.pushInfo<S>(this, "_popupNotification", message); }
    pushSuccessNotification(message: string): void { PopupNotification.pushSuccess<S>(this, "_popupNotification", message); }
    startLoading(title: string): void { this.open("_loadingDialog", {title: title}); }
    stopLoading(): void { this.close("_loadingDialog"); }

    public static createState<S extends IPageState>(page: IPage<S>): IPageState {
        return {
            /* page titles */
            title: "",
            subtitle: "",

            /* Popup notification implementation  */
            _popupNotification: PopupNotification.initProps(page, "_popupNotification"),

            /* loading dialog implementation  */
            _loadingDialog: PageDialog.createPropsForPage(page, "_loadingDialog", {})
        }
    }

    protected getBaseDialogOptions(dialogKey: keyof S): PageDialogOptions<S> {
        return {
            page: this,
            dialogKey: dialogKey
        }
    }

    /**
     * Get values stored in the "data-" attributes of the model div.
     */
    public getModelProperty(property: string): any | null {
        const model = this.getModel();
        if(model) {
            const namedItem = model[property];
            if (namedItem) {
                return namedItem;
            }
        }
        return null;
    }

    get model(): HtmlModel {
        return this.getModel();
    }

    public getModel(): HtmlModel {
        const properties: HtmlModel = eval(`${HTML_DATA_MODEL_ID}`) as HtmlModel;
        return properties || {};
    }
}


export interface IPageProps {

}

export interface IPageState {
    title: string,
    subtitle: string,
    _popupNotification: PopupNotification.Props,
    _loadingDialog: PageDialogProps
}
