const fs = require("fs");
const path = require("path");
const { spawn } = require('child_process');

function executeProcess(cmd, options, callback) {
    console.log(`executing ${cmd}`, options || []);
    const ls = spawn(cmd, options || []);

    ls.stdout.on('data', (data) => {
        console.log(`${data}`);
    });

    ls.stderr.on('data', (data) => {
        console.error(`${data}`);
        // callback(data);
    });

    ls.on('close', (code) => {
        console.log(`child process exited with code ${code}`);
        if(callback) {
            callback()
        }
    });
}

function executeProcessSync(cmd, options) {
    return new Promise((resolve, reject) => {
        executeProcess(cmd, options, function(error) {
            if (error){
                reject(error);
            } else {
                resolve();
            }
        })
    });
}

function loadJsonFromFile(f) {
    return JSON.parse(fs.readFileSync(f));
}

function overrideTsConfigOutDir(project) {
    const {basePath} = project;
    const f = path.resolve(basePath, "tsconfig.json");
    if (!fs.existsSync(f)) {
        console.log(`Ignoring tsconfig.json. ${f} not found.`);
        return;
    }
    const obj = loadJsonFromFile(f);
    obj.compilerOptions.outDir = ".";

    const copyRoot = getProjectCopyRoot(project);
    fs.writeFileSync(path.resolve(copyRoot, "tsconfig.json"), JSON.stringify(obj));
}

function removeTsConfigOutDir(project) {
    const {basePath} = project;
    const f = path.resolve(basePath, "tsconfig.json");
    if (!fs.existsSync(f)) {
        console.log(`Ignoring tsconfig.json. ${f} not found.`);
        return;
    }
    const obj = loadJsonFromFile(f);
    obj.compilerOptions.outDir = undefined;

    const copyRoot = getProjectCopyRoot(project);
    let resolve = path.resolve(basePath, "tsconfig_publish.json");
    fs.writeFileSync(resolve, JSON.stringify(obj));
    return resolve;
}


function createPublishTsConfig(project) {
    const {basePath} = project;
    const f = path.resolve(basePath, "tsconfig.json");
    if (!fs.existsSync(f)) {
        console.log(`Ignoring tsconfig.json. ${f} not found.`);
        return;
    }
    const obj = loadJsonFromFile(f);
    obj.compilerOptions.outDir = undefined;

    const copyRoot = getProjectCopyRoot(project);
    let resolve = path.resolve(copyRoot, "tsconfig_publish.json");
    fs.writeFileSync(resolve, JSON.stringify(obj), {flag: "w+"});
    return resolve;
}

function listFiles(dirPath, arrayOfFiles) {
    const files = fs.readdirSync(dirPath);

    arrayOfFiles = arrayOfFiles || [];

    files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            arrayOfFiles = listFiles(dirPath + "/" + file, arrayOfFiles)
        } else {
            arrayOfFiles.push(path.join(dirPath, "/", file))
        }
    });

    return arrayOfFiles
}

function getSourceList(project) {
    const {basePath, srcRoot} = project;
    const dirPath = path.resolve(basePath, srcRoot);
    return listFiles(dirPath).map(f => {
        if (f.endsWith(".ts") || f.endsWith(".tsx")) {
            return {
                tsFile: f,
                copyName: path.relative(dirPath, f),
            }
        }
        return null;
    }).filter(e => e !== null);
}

function cleanProject(project) {
    const copyRoot = getProjectCopyRoot(project);
    if (!fs.existsSync(copyRoot)) { return }

    listFiles(copyRoot).forEach(f => {
        try {
            fs.unlinkSync(f)
        } catch(err) {
            console.error(err)
        }
    });

    try {
        fs.rmdirSync(copyRoot, { recursive: true })
    } catch (e) {
        console.error(e)
    }
    console.log(`${copyRoot} is deleted!`);
}

function getProjectCopyRoot(project) {
    return project.name;
}

function mkdirSafe(d) {
    try {
        fs.mkdirSync(d, { recursive: true })
    } catch (e) { }
}

function createProjectFolder(project) {
    const projectCopyRoot = getProjectCopyRoot(project);
    mkdirSafe(projectCopyRoot);
    console.log(`${projectCopyRoot} created!`);
}

function copyProject(project) {
    createProjectFolder(project)
    const {basePath, files} = project;
    const copyRoot = path.resolve(getProjectCopyRoot(project));
    project.tsFiles = getSourceList(project);
    project.tsFiles.forEach(
        f => {
            const copyDest = path.resolve(copyRoot, f.copyName);
            const copyDestParent = path.dirname(copyDest).split(path.sep).pop();
            mkdirSafe(copyDestParent);
            fs.copyFileSync(f.tsFile, copyDest);
        }
    );

    files.forEach(f => {
        const srcFile = path.resolve(basePath, f);
        const copyDest = path.resolve(copyRoot, f);
        const copyDestParent = path.dirname(copyDest).split(path.sep).pop();
        mkdirSafe(copyDestParent);
        fs.copyFileSync(srcFile, copyDest);
    });

    removeTsConfigOutDir(project);

}

function removeTsFiles(project) {
    project.tsFiles.forEach(
        f => {
            const copyRoot = path.resolve(getProjectCopyRoot(project));
            const copyDest = path.resolve(copyRoot, f.copyName);
            try {
                fs.unlinkSync(copyDest)
            } catch(err) {
                console.error(err)
            }
        }
    );
}

function loadProject(projectId) {
    return loadJsonFromFile(`configs/${projectId}.json`);
}

module.exports = {
    loadProject: loadProject,
    getProjectCopyRoot: getProjectCopyRoot,
    getSourceList: getSourceList,
    removeTsConfigOutDir: removeTsConfigOutDir,
    createPublishTsConfig: createPublishTsConfig,
    cleanProject: cleanProject,
    copyProject: copyProject,
    executeProcess: executeProcess,
    executeProcessSync: executeProcessSync,
    removeTsFiles: removeTsFiles,
};
