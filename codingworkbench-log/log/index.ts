import * as log4js from "log4js";

namespace CwbLog {

    const logger = log4js.getLogger();

    const SETTINGS = {
        consoleEnabled: false,
    };

    export function setup(level: string = "info") {
        log4js.configure({
            appenders: { log:  {
                    type: "file", filename: "log4js.log",
                    layout: {
                        type: 'pattern',
                        pattern: '%d %p %c %m'
                    }
                } },
            categories: { default: { appenders: ["log"], level: level } },
        });
    }

    export function enable_console() {
        SETTINGS.consoleEnabled = true;
    }

    export function error(e: Error) {
        if (SETTINGS.consoleEnabled) {
            console.error(e.message, e.stack);
        }
        logger.error(e.message, e.stack);
    }

    export function warning(e: Error) {
        if (SETTINGS.consoleEnabled) {
            console.warn(e.message, e.stack);
        }
        logger.warn(e.message, e.stack);
    }

    export function info(message: string) {
        if (SETTINGS.consoleEnabled) {
            console.info(message);
        }
        logger.info(message);
    }
}

export default CwbLog;
