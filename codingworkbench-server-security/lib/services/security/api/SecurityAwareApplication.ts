import ExpressApplication, {ExpressContextFactory} from "@codingworkbench/server/ExpressApplication";
import {IServerProperties} from "../../common/IServerPropertiesAware";
import {ExpressApplicationMiddleware, ExpressApplicationOptions, IExpressApplication} from "@codingworkbench/server/ExpressApplicationUtils";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import {has_active_session} from "@codingworkbench/server/Session";
import ConnectionAwareInjector from "@codingworkbench/data/ConnectionAwareInjector";

import {ServerInjector} from "../../common/ServerInjector";
import EmailAwareInjector from "../../email/inject/EmailAwareInjector";
import SecurityAwareInjector from "../inject/SecurityAwareInjector";
import {create_security_links, ISecurityLinks} from "./links";
import ISecurityAwareContext from "./context";
import SecurityServiceHandlers from "../server/handlers";


import * as StatusMonitor from "express-status-monitor";

export abstract class SecureApplicationContextFactory<C extends ISecurityAwareContext> implements ExpressContextFactory<C> {
    protected serverProperties: IServerProperties;

    protected constructor(serverProps: IServerProperties) {
        this.serverProperties = serverProps;
    }

    protected async createContextInternal(connectionProperties: IPropertiesReader | null)
        : Promise<ISecurityAwareContext> {
        return await (this.createSecurityServerInjector(connectionProperties)).getAll();
    }

    protected createSecurityServerInjector(connectionProperties: IPropertiesReader | null) {
        if (!connectionProperties) {
            throw "Connection properties as required";
        }
        const serverInjector = new ServerInjector(this.serverProperties);
        const connectionAwareInjector = new ConnectionAwareInjector(connectionProperties);
        const emailServiceInjector = new EmailAwareInjector();
        const securityInjector = new SecurityAwareInjector();

        return serverInjector
            .merge(connectionAwareInjector)
            .merge(emailServiceInjector)
            .merge(securityInjector);
    }

    abstract createContext(options: ExpressApplicationOptions, connectionProperties: IPropertiesReader | null): Promise<C>;
    abstract configureApplication(context: C, app: IExpressApplication<C>): Promise<void>;
}


function add_secure_user_data_validation<C>(securityLinks: ISecurityLinks, expressApp: ExpressApplication<C>) {
    const mw: ExpressApplicationMiddleware = (req, res, next) => {
        if (req.url.includes(securityLinks.secureDataPattern)) {
            if (has_active_session(req)) {
                next();
                return;
            } else {
                req.session.destroy();
                res.sendStatus(403, "Access Denied");
                return;
            }
        } else {
            next();
        }
    };
    expressApp.addMiddleware(mw);
}

function add_secure_user_page_validation<C>(securityLinks: ISecurityLinks, expressApp: ExpressApplication<C>) {
    const mw: ExpressApplicationMiddleware = (req, res, next) => {
        if (req.url.includes(securityLinks.securePagePattern)) {
            if (has_active_session(req)) {
                next();
                return;
            } else {
                req.session.destroy();
                res.redirect(`${securityLinks.signInPage}`);
                return;
            }
        } else {
            next();
        }
    };
    expressApp.addMiddleware(mw);
}

interface ISecurityAwareApplicationProperties extends IServerProperties {

}

export default class SecurityAwareApplication<C extends ISecurityAwareContext> extends ExpressApplication<C> {
    private serverProperties: IServerProperties;

    constructor(serverProperties: ISecurityAwareApplicationProperties) {
        super();
        this.serverProperties = { ...serverProperties };
    }

    async start(options: ExpressApplicationOptions, contextFactory: ExpressContextFactory<C>): Promise<void> {
        this.getApp().use(StatusMonitor());

        const securityLinks = this.serverProperties.securityLinks;

        this.addHandlers(SecurityServiceHandlers.create<C>(securityLinks));
        add_secure_user_page_validation(securityLinks, this);
        add_secure_user_data_validation(securityLinks, this);

        this.addHandlers([new RequestHandlerBuilder("/", context => context.response.redirect(securityLinks.signInPage)).build()]);
        this.serveStatic("/static", this.serverProperties.staticRoot);

        return super.start( {
            ...options,
            port: options.port || 8080,
            connectionPropertiesFile: options.connectionPropertiesFile || "./web-app/conf/connection.properties",
        }, contextFactory);
    }

}
