import ISlicer from "../ISlicer";
import ISliceResults from "../ISliceResults";
import ICURAOptions from "./CURAOptions";
import { CURAPaths } from "./CURAPaths";
export default class CURASlicer implements ISlicer {
    private readonly enginePaths;
    constructor(enginePaths: CURAPaths);
    slice(stlFile: string, gCodeFile: string, options: ICURAOptions): Promise<ISliceResults>;
    sliceStlString(stlString: string, options: ICURAOptions): Promise<ISliceResults>;
    private buildSlicerCommand;
}
