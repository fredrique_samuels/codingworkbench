
import STLVertexDataScaleVisitor from "./STLVertexDataScaleVisitor"


export default class STLUtils {

    /**
     * Given the string  contents of an STL file, return the scaled version of the file
     * by the given factor. 
     * @param stlFileContent
     */
    static scaleModel(stlFileContent: string, scaleFactor: number=1.0): string {
        const visitor = new STLVertexDataScaleVisitor(scaleFactor);
        stlFileContent.split("\n").forEach(visitor.visit.bind(visitor))
        return visitor.content;
    }
}
