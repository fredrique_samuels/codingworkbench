import CwbInject from "@codingworkbench/common/inject";
import {IServerProperties, IServerPropertiesAware} from "./IServerPropertiesAware";
import Encrypt from "@codingworkbench/common/Encrypt";
import CwbI8N from "@codingworkbench/i8n";
import {IViewFactory} from "@codingworkbench/server/common";
import {ReactViewFactory} from "@codingworkbench/react/html/ReactViewFactory";
import IServerContext from "./IServerContext";


async function create_viewFactory(dependencies: CwbInject.Injector<IServerPropertiesAware>): Promise<IViewFactory> {
    return new ReactViewFactory((await dependencies.get("serverProperties")).jsPath, false);
}

export class ServerInjector extends CwbInject.Module<IServerContext> {

    constructor(serverProperties: IServerProperties) {
        super();
        this.bind("encryptionPolicy", async () => { return await Encrypt.sha256Encryption(); })
            .bind("serverProperties", async (): Promise<IServerProperties> => { return serverProperties; })
            .bind("messageSource", async (): Promise<CwbI8N.IMessageSource> => new CwbI8N.DefaultMessageSource({}))
            .bind("viewFactory", create_viewFactory);
    }
}
