import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
export default interface IConnectionPropertiesAware {
    connectionProperties: IPropertiesReader;
}
