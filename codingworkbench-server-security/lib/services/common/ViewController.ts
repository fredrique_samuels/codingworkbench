import {IFormValidation, validation_failed, validation_passed} from "@codingworkbench/react/form/FormInputs";
import CwbAction from "@codingworkbench/action";
import RequestAction from "@codingworkbench/server/RequestAction";


namespace ViewController {

    export function redirect(url: string) {
        window.location.href = url;
    }

    export async function submit_form(url: string, body?: any): Promise<IFormValidation> {
        try {
            const response: CwbAction.IResponse = await new RequestAction(url).post().setBody(JSON.stringify(body || {})).json();
            if (response.error) {
                return validation_failed(response.message);
            }
            redirect_action(response);
            return validation_passed();
        } catch (e) {
            console.log(e);
            return validation_failed("We are unable to process your request at this time");
        }
    }

    export async function do_post_action<R=any>(url: string, body?: any ): Promise<CwbAction.IResponse<R>> {
        const res = (await new RequestAction(url).post().setBody(JSON.stringify(body || {})).json()) as CwbAction.IResponse<R>;
        return redirect_action(res);
    }

    export function redirect_action<R=any>(res: CwbAction.IResponse<R>): CwbAction.IResponse<R> {
        if (res.redirect) {
            ViewController.redirect(res.redirect);
        }
        return res;
    }

    export async function do_action<R=any>(url: string): Promise<CwbAction.IResponse<R>> {
        return redirect_action(await new RequestAction(url).json() as CwbAction.IResponse<R>);
    }

}


export default ViewController;
