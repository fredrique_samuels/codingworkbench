import { CURAPaths } from "./CURAPaths";
export declare class CURADefinitionProvider {
    private readonly enginePaths;
    constructor(enginePaths: CURAPaths);
    getDefinitionFileObj(definitionTarget: string): any;
    private loadDefObj;
    private getDefPath;
}
