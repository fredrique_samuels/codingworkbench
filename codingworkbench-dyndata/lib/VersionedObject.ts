import PersistedObject from "@codingworkbench/data/PersistedObject";
import IUserSession from "@codingworkbench/data/IUserSession";
import Unique from "@codingworkbench/data/Unique";
import ArchiveAware from "@codingworkbench/data/ArchiveAware";
import AuditAware from "@codingworkbench/data/AuditAware";
import CwbSearch from "@codingworkbench/search";
import {IVersionedObject} from "./IVersionedObject";
import {DataStructureTable} from "./DataStructure";
import CrudDataAccessor from "@codingworkbench/data/CrudDataAccessor";
import DbTable from "@codingworkbench/data/DbTable";



export default abstract class VersionedObject<T extends IVersionedObject> extends PersistedObject<T> {

    private dirty: boolean;

    protected constructor(dao: CrudDataAccessor, table: DbTable) {
        super(dao, table);
        this.dirty = false;
    }

    public async save(session?: IUserSession): Promise<this> {
        if (this.persisted) {
            await this.assert_not_out_of_sync()
        }

        this._params.version += 1;
        await super.save(session);
        return this;
    }

    protected setDirty(): this {
        this.dirty = true;
        return this;
    }

    public update(params: T): this {
        super.update(params);
        this._params.orphaned = false;
        return this;
    }

    public createVersionObjectParams(): IVersionedObject {
        return {
            ...new Unique().flatten(),
            ...new ArchiveAware().flatten(),
            ...new AuditAware({}).flatten(),
            version: 0,
            orphaned: false
        };
    }

    private async assert_not_out_of_sync() {
        const searchBuilder = new CwbSearch.SearchBuilder();
        searchBuilder.filters.addFilter("id", this._params.id);
        const [found] = await this.dao.searchObjects<T>(DataStructureTable, searchBuilder.build());
        if (!found || found.archived) {
            return this;
        } else {
            if (found.version != this._params.version) {
                throw Error(`Unable to save object id='${this._params.id}'. The edited version is out of date.`)
            }
        }
    }
}
