import * as express from "express";
import * as common from "./common";
export declare class ExpressHttpFactory implements common.RequestFactory<express.Request, express.Response> {
    createRequest(input: express.Request): common.Request;
    createResponse(input: express.Response): common.Response;
}
export declare function expressTransformHandler<C = {}>(handler: common.RequestHandlerFunc, context: C): express.RequestHandler;
export declare function expressTransformHandlerAsync<C = {}>(handler: common.RequestHandlerAsync, context: C): express.RequestHandler;
