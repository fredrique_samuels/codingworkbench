const fs = require("fs");
const path = require("path");
const process = require("process");
const utils = require("./utils");

const projectId = process.env.PROJECT_ID;
const project = utils.loadProject(projectId);
utils.createPublishTsConfig(project);
