import CwbNode from "../node";

const nodeTree = new CwbNode.NodeTreeBuilder("my-node", {})
    .addChild("myAttribute", new CwbNode.AttributeBuilder(data => {}).build())
    .addChild("myElement", new CwbNode.ElementBuilder("my-element", data => {}).build())
    .build();

const missingNode = nodeTree.get("missing-child");

console.log("Attempt to process empty node with standard error:");
missingNode.asAttribute(iAttribute => {});

console.log("\nAttempt to process empty node with custom error:");
missingNode.asElement(iElement => {}, n => { console.log(`Node ${n.name} does not exists`)});

console.log("\nAttempt to process an attribute as an element:");
nodeTree.get("myAttribute").asElement(iElement => {});

console.log("\nAttempt to process an element as an attribute:");
nodeTree.get("myElement").asAttribute(iAttribute => {});
