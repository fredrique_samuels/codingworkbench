"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var fs = require("fs");
var tmp = require("tmp");
var log_1 = require("cwb-core/log");
var ProcessHandle_1 = require("../process/ProcessHandle");
var CURASliceSummaryExtractor_1 = require("./CURASliceSummaryExtractor");
var CURASlicer = (function () {
    function CURASlicer(enginePaths) {
        this.enginePaths = enginePaths;
    }
    CURASlicer.prototype.slice = function (stlFile, gCodeFile, options) {
        return __awaiter(this, void 0, void 0, function () {
            var command;
            return __generator(this, function (_a) {
                log_1.log_info_message("Slice requested " + stlFile);
                command = this.buildSlicerCommand(stlFile, gCodeFile, options);
                log_1.log_info_message("Executing slicer command \n" + command);
                return [2, new ProcessHandle_1.default().execute(command)
                        .then(function (pr) {
                        var stdout = pr.stdout, stderr = pr.stderr, errorMessage = pr.errorMessage;
                        var stdio = {
                            stderr: stderr,
                            stdout: stdout,
                            error: errorMessage
                        };
                        var summary = new CURASliceSummaryExtractor_1.default(fs.readFileSync(gCodeFile, "utf-8")).extractFromOutput(stderr);
                        log_1.log_info_message("Slicer operation summary \n" + JSON.stringify(summary));
                        return {
                            stdio: stdio,
                            summary: summary
                        };
                    })];
            });
        });
    };
    CURASlicer.prototype.sliceStlString = function (stlString, options) {
        return __awaiter(this, void 0, void 0, function () {
            var tempStlFile, tempGcodeFile, sliceResults;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        tempStlFile = tmp.fileSync();
                        tempGcodeFile = tmp.fileSync();
                        fs.writeSync(tempStlFile.fd, stlString);
                        return [4, this.slice(tempStlFile.name, tempGcodeFile.name, options)];
                    case 1:
                        sliceResults = _a.sent();
                        tempStlFile.removeCallback();
                        tempGcodeFile.removeCallback();
                        return [2, sliceResults];
                }
            });
        });
    };
    CURASlicer.prototype.buildSlicerCommand = function (stlFile, gCodeFile, options) {
        var _a = this.enginePaths, home = _a.home, definitions = _a.definitions, engine = _a.engine;
        var definitionTarget = options.definitionTarget;
        var app = "" + path.join(home, engine);
        var definition = "" + path.join(home, definitions, definitionTarget);
        log_1.log_info_message("Using definition " + definition);
        var settings = "-s material_bed_temperature_layer_0=\"60\" -s material_print_temperature_layer_0=\"200\" -s layer_height=\"0.2\" -s machine_depth=\".2\" -s layer_height_0=\"0.2\" -s material_diameter=\"1.75\"";
        return app + " slice -v -j " + definition + " " + settings + " -e0 -l " + stlFile + " -o " + gCodeFile;
    };
    return CURASlicer;
}());
exports.default = CURASlicer;
