@codingworkbench/server-security
======
     
     
Tests
-----
```sh
npm run liquibase:test
npm run liquibase:dev
```

Install
-----
```sh
npm install @codingworkbench/server-security --save
```

Setup DB Schema
-----
The schema is managed using liquibase. 

```sh
npm run liquibase:test
npm run liquibase:dev
```

Run Demo
-----

```sh
npm run demo:server
```

Example Server
-----

```typescript
import CwbLog from "@codingworkbench/log";
import {ExpressApplicationOptions} from "@codingworkbench/server/ExpressApplicationUtils";
import ISecurityContext from "@codingworkbench/server-security/services/security/api/context";
import {ISecurityServerProperties} from "@codingworkbench/server-security/services/common/ISecurityServerPropertiesAware";
import SecurityAwareApplication, {SecureApplicationContextFactory} from "@codingworkbench/server-security/services/security/api/SecurityAwareApplication";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";

export interface ApplicationContext extends ISecurityContext {}

class ApplicationContextFactory extends SecureApplicationContextFactory<ApplicationContext> {

    constructor(serverProps: ISecurityServerProperties) {
        super(serverProps);
    }

    createContext(options: ExpressApplicationOptions, connectionProperties: IPropertiesReader | null): Promise<ApplicationContext> {
        return this.createContextInternal(connectionProperties);
    }
}

try {

    CwbLog.setup();
    CwbLog.enable_console();

    const securityAwareApplicationProperties = SecurityAwareApplication.createProperties();
    const application = new SecurityAwareApplication<ApplicationContext>(securityAwareApplicationProperties);
    const contextFactory = new ApplicationContextFactory(securityAwareApplicationProperties);

    const options: ExpressApplicationOptions = {
        port: 8080,
        enabledSessions: true,
    };
    application.start(options, contextFactory).catch(CwbLog.error);
} catch (e) {
    CwbLog.error(e)
}

``` 

Status View
-----
http://localhost:8080/status

License
-------
The ISC License

Copyright (c) 2020 Fredrique Samuels (https://codingworkbench.com)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

