from cwb_control.DataPointChangeHandler import DataPointChangeHandler


class PlotDataHandler(DataPointChangeHandler):
    id = "PlotDataHandler"

    def __init__(self):
        self.timestamp = 0
        self.processed_keys = []

    def handle(self, key, value, ds):
        k = "{}.{}".format(key, PlotDataHandler.id)
        if k in self.processed_keys:
            return

        if not ds.has_property(k):
            ds.set(k, [])
        ds.set(k, ds.get(k) + [(self.timestamp, value)])

        self.processed_keys.append(k)

    def set_timestamp(self, ts):
        self.timestamp = ts
        self.processed_keys = []

    @staticmethod
    def get_plot_keys(ds):
        return list(filter(lambda k: k.endswith(".{}".format(PlotDataHandler.id)), ds.property_keys()))

