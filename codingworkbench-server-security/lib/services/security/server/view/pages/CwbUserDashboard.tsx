import * as React from "react"
import * as ReactDOM from "react-dom";

import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';

import {IPageProps, IPageState, Page} from "@codingworkbench/react/page/Page";
import SessionAwarePage from "../components/SessionAwarePage";
import SecurityViewController from "../SecurityViewController";

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
        },
        menuButton: {
            marginRight: theme.spacing(2),
        },
        title: {
            flexGrow: 1,
        },
    }),
);

function UserHome() {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <AppBar position="static">
                <Toolbar>
                    <IconButton edge="start" className={classes.menuButton} color="inherit" aria-label="menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        Dashboard
                    </Typography>
                    <Button color="inherit" onClick={SecurityViewController.do_signout}>Log Out</Button>
                </Toolbar>
            </AppBar>
        </div>
    );
}


export default class CwbUserDashboard extends SessionAwarePage<IPageProps, IPageState> {

    constructor(props: IPageProps) {
        super(props);
        this.state = Page.createState(this);
    }

    renderChildren() {
        return [
            <UserHome />
        ]
    }
}

ReactDOM.render(<CwbUserDashboard />, document.getElementById("root"));
