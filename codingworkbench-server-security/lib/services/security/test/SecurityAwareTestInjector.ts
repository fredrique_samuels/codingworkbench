
import SecurityAwareInjector from "../inject/SecurityAwareInjector";

export default class SecurityAwareTestInjector extends SecurityAwareInjector {

    constructor() {
        super();
    }
}
