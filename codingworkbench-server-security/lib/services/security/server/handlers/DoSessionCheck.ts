import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import {RequestContext} from "@codingworkbench/server/common";

import {ISecurityHandlerContext, ISecurityHandlerFactoryContext} from "../../api/context";
import {request_context_unpack, session_expired} from "@codingworkbench/server/Session";

async function handlerAsync<C extends ISecurityHandlerContext>(requestContext: RequestContext<C>) {
    const {session} = request_context_unpack(requestContext);
    if (!session || session_expired(session)) {
        requestContext.response.sendStatus(403, "Session Expired");
    } else {
        requestContext.response.sendText("");
    }
}

export default function DoSessionCheck<C extends ISecurityHandlerContext>(factoryContext: ISecurityHandlerFactoryContext): IRequestHandler<C> {
    return new RequestHandlerBuilder(factoryContext.links.sessionCheck, () => {}, handlerAsync).setAsync().build();
}

