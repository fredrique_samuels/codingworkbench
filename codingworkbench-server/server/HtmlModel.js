"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Dict_1 = require("@codingworkbench/common/Dict");
exports.HTML_DATA_MODEL_ID = "__CwbPageDataModel";
function read_html_model() {
    var properties = eval("" + exports.HTML_DATA_MODEL_ID);
    return properties || {};
}
exports.read_html_model = read_html_model;
var ModelBuilder = /** @class */ (function () {
    function ModelBuilder(other) {
        this._internal = new Dict_1.default(other);
    }
    ModelBuilder.prototype.put = function (key, value) {
        this._internal.put(key, value);
        return this;
    };
    ModelBuilder.prototype.build = function () {
        return this._internal.entries;
    };
    return ModelBuilder;
}());
exports.ModelBuilder = ModelBuilder;
