import * as common from "./common";

import IRequestHandler from "./IRequestHandler";
import {IViewAware} from "./IViewAware";
import {ModelBuilder} from "./HtmlModel";
import {RequestContext} from "./common";

export class RequestHandlerBuilder<C> {
    private readonly url: string;
    private handler: common.RequestHandlerFunc<C>;
    private handlerAsync: common.RequestHandlerAsync<C>;
    private isAsync: boolean;
    private method: common.Method;
    private timeout: number;

    constructor(
        url: string,
        handler: common.RequestHandlerFunc<C> = () => {},
        handlerAsync: common.RequestHandlerAsync<C> = async () => {}
    ) {
        this.url = url;
        this.handler = handler;
        this.handlerAsync = handlerAsync;
        this.isAsync = false;
        this.method = 'get';
        this.timeout = 10000;
    }

    public setOnError(e: Error, requestContext: RequestContext<C>) {

    }

    public setHandler(handler: common.RequestHandlerFunc<C>): this {
        this.handler = handler;
        this.isAsync = false;
        return this;
    }

    public setHandlerAsync(handlerAsync: common.RequestHandlerAsync<C>): this {
        this.handlerAsync = handlerAsync;
        this.isAsync = true;
        return this;
    }

    public setAsync(): this {
        this.isAsync = true;
        return this;
    }

    /**
     * Set the max request running time.
     *
     * @param ms Time in milliseconds.
     */
    setTimeout(ms: number): this {
        this.timeout = ms;
        return this;
    }

    /**
     * Set the request method.
     *
     * @param method
     * @see Web.Method
     */
    setMethod(method: common.Method): this {
        this.method = method;
        return this;
    }

    /**
     * Build the path object.
     */
    build(): IRequestHandler<C> {
        return {
            isAsync: this.isAsync,
            url: this.url,
            handler: this.handler,
            handlerAsync: this.handlerAsync,
            method: this.method
        };
    }

    public static redirect<ContextType>(path: string, redirectUrl: string): IRequestHandler<ContextType> {
        const handler: common.RequestHandlerFunc<ContextType> = (context: common.RequestContext<ContextType>) => {
            context.response.redirect(redirectUrl);
        };
        return new RequestHandlerBuilder<ContextType>(path, handler).build();
    }

    private static _view<C extends IViewAware>(
        requestContext: common.RequestContext<C>,
        viewName: string,
        modelBuilder?: ModelBuilder
    ) {
        const model = modelBuilder
            ? modelBuilder.build()
            : {};
        requestContext.response.view(requestContext.context.viewFactory.modelAndView(viewName, model));
    }


    public static view<C extends IViewAware>(
        path: string,
        viewName: string,
        modelBuilder?: ModelBuilder,
        callback?: (requestContext: common.RequestContext<C>, modelBuilder: ModelBuilder) => void
    ): IRequestHandler<C> {
        const handler: common.RequestHandlerFunc<C> = (requestContext: common.RequestContext<C>) => {
            const mb = modelBuilder || new ModelBuilder();
            if (callback) {
                callback(requestContext, mb)
            }
            RequestHandlerBuilder._view<C>(requestContext, viewName, mb);
        };
        return new RequestHandlerBuilder<C>(path, handler).build();
    }

    public static viewAsync<C extends IViewAware>(
        path: string,
        viewName: string,
        modelBuilder?: ModelBuilder,
        callback?: (requestContext: common.RequestContext<C>, modelBuilder: ModelBuilder) => Promise<void>
    ): IRequestHandler<C> {
        const handlerAsync: common.RequestHandlerAsync<C> = async (requestContext: common.RequestContext<C>) => {
            const mb = modelBuilder || new ModelBuilder();
            if (callback) {
                await callback(requestContext, mb)
            }
            RequestHandlerBuilder._view<C>(requestContext, viewName, mb);
        };
        return new RequestHandlerBuilder<C>(path, () => {}, handlerAsync).setAsync().build();
    }

    public static post<C extends IViewAware>(path: string, handler: common.RequestHandlerFunc<C>): IRequestHandler<C> {
        return new RequestHandlerBuilder<C>(path, handler).setMethod("post").build();
    }

    public static postAsync<C extends IViewAware>(path: string, handler: common.RequestHandlerAsync<C>): IRequestHandler<C> {
        return new RequestHandlerBuilder<C>(path, () => {}, handler).setAsync().setMethod("post").build();
    }

    public static get<C extends IViewAware>(path: string, handler: common.RequestHandlerFunc<C>): IRequestHandler<C> {
        return new RequestHandlerBuilder<C>(path, handler).build();
    }

    public static getAsync<C extends IViewAware>(path: string, handler: common.RequestHandlerAsync<C>): IRequestHandler<C> {
        return new RequestHandlerBuilder<C>(path, () => {}, handler).setAsync().build();
    }
}
