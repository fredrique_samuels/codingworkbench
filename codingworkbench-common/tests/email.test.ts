import {deepStrictEqual, ok} from 'assert'
import CwbTestCase from "@codingworkbench/testcase";
import Password from "../lib/Password";


describe('email', function() {

    it ("isEmailString# f@g.com ok", () => {
        ok(Password.isEmailString("f@g.com"));
    });

    it ("isEmailString# '' false", () => {
        ok(!Password.isEmailString(""));
    });

    it ("isEmailString# 'assd' false", () => {
        ok(!Password.isEmailString(CwbTestCase.MALFORMED_EMAIL));
    });

});
