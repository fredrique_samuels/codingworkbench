// import * as path from "path"
// import * as os from "os"
// import * as fs from "fs"
// import * as express from "express"
// import * as uuid from "uuid/v1"
//
// import { PropertiesFileReader, PropertiesReader, mkdirRecursive } from "genius-common"
//
// import { STLUtil, SlicerBuilder, CuraSlicerBuilder, CuraEnginePaths, SliceResults } from "..";
// import {DiskFile, FileHandle} from "genius-common"
//
// const properties: PropertiesReader = new PropertiesFileReader(path.join('config', "printer.properties"));
// const CURA_ENGINE_PATHS = CuraEnginePaths.fromProperties(properties)
//
// const TEMP_DIR = "slicer-temp"
//
// async function scaleAndSlice(inputStlFile: string, scale: number = 1): Promise<SliceResults> {
//
//   // create unique name for temp files
//   const uniqueTempName = uuid()
//
//   // create file handle for source stl
//   const inputStl_Fh: FileHandle = new DiskFile(inputStlFile)
//
//   // load stl code
//   let inputStlCode = await inputStl_Fh.readString()
//
//   // scale stl code
//   let scaledStlCode = STLUtil.scaleModel(inputStlCode, scale)
//
//   // save scale stl file
//   let scaledStl_fh = new DiskFile(path.join(TEMP_DIR, `scaled-stl-${uniqueTempName}.stl`))
//   await scaledStl_fh.writeString(scaledStlCode);
//
//   // create gcode file handle
//   let gcode_fh = new DiskFile(path.join(TEMP_DIR, `gcode-${uniqueTempName}.stl`))
//
//   // slice the stl file
//   let sliceData: SliceResults = await new CuraSlicerBuilder(CURA_ENGINE_PATHS)
//       .create()
//       .slice(scaledStl_fh.path, gcode_fh.path)
//
//   // cleanup files
//   scaledStl_fh.delete()
//   gcode_fh.delete()
//
//   return sliceData;
// }
//
//
// async function printSliceResults(inputStl: string, scale: number, sliceData: SliceResults): Promise<any> {
//   const {dimx, dimy, dimz, timeFormatted} = sliceData.gcodeData
//   const ok = dimx < 200 && dimy < 200 && dimz < 200
//   console.log(` ok=${ok} ${inputStl} | scale=${scale} | x=${dimx.toPrecision(3)}mm | y=${dimy.toPrecision(3)}mm | z=${dimz.toPrecision(3)}mm | time=${timeFormatted}`)
//   return;
// }
//
//
// async function main() {
//
//   // Create temp directory
//   let tempDirExists: boolean = fs.existsSync(TEMP_DIR);
//   if(!tempDirExists) {
//     await mkdirRecursive(TEMP_DIR)
//   }
//
//   // scale
//   let scale = 0.25;
//
//   let files: string[] = [
//     "resources/Marvin.stl"
//   ]
//
//   let sourceDir = "C:\\Users\\Fredrique\\workspace\\projects\\manneqiun\\The_Fay_Mannequin\\stl";
//   const fileList = fs.readdirSync(sourceDir)
//
//   async function do_slicing(f: string): Promise<any> {
//     let inputStl = path.join(sourceDir, f);
//     let sliceData = await scaleAndSlice(inputStl, scale)
//     printSliceResults(f, scale, sliceData);
//   }
//
//   for (let f of fileList) {
//     if(f.endsWith(".stl")) {
//       await do_slicing(f)
//     }
//   }
// }
//
// main()
