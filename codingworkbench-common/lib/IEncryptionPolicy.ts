export interface IEncryptionPolicy {
    encrypt(input: string): Promise<string>;
}

export default IEncryptionPolicy;
