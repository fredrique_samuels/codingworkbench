const fs = require("fs");
const path = require("path");
const process = require("process");

const utils = require("./utils");

const projectId = process.env.PROJECT_ID;
const project = utils.loadProject(projectId);
const {basePath, srcRoot} = project;
const copyRoot = utils.getProjectCopyRoot(project);
const tsList = utils.getSourceList(project);
utils.removeTsConfigOutDir(project);

let commands = [];

const getAllFiles = function(dirPath, arrayOfFiles) {
    let files = fs.readdirSync(dirPath);

    arrayOfFiles = arrayOfFiles || [];

    files.forEach(function(file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
        } else {
            arrayOfFiles.push(path.join(dirPath, "/", file))
        }
    });

    return arrayOfFiles
};

commands.push(`rm -Rf ${copyRoot}`);
tsList.forEach(f => {
    const copyDest = path.resolve(copyRoot, f.copyName);
    const copyDestParent = path.dirname(copyDest);
    commands.push(`mkdir -p ${copyDestParent}`);
    commands.push(`cp ${f.tsFile} ${copyDestParent}`);
});

project.files.forEach( f => {
    const copyDest = path.resolve(copyRoot, f);
    const copyDestParent = path.dirname(copyDest);
    const fullPath = path.resolve(basePath, f);
    if (fs.lstatSync(fullPath).isDirectory()) {
        const files = getAllFiles(fullPath);

        files.forEach(f => {
            const relativePath = path.relative(basePath, f);
            const destFile = path.join(copyDestParent, relativePath);
            commands.push(`mkdir -p ${path.dirname(destFile)}`);
            commands.push(`cp ${f} ${destFile}`);
        })
    } else {
        commands.push(`mkdir -p ${copyDestParent}`);
        commands.push(`cp ${fullPath} ${copyDestParent}`);
    }
});
commands.push(`rm -Rf ./${copyRoot}/**/**/*.d.ts`);
commands.push(`rm -Rf ./${copyRoot}/**/*.d.ts`);
commands.push(`rm -Rf ./${copyRoot}/*.d.ts`);
fs.writeFileSync(`copy_${projectId}.sh`, commands.join("\n"));

commands = [];
commands.push(`npm install --prefix ${copyRoot} `);
commands.push(`npm --prefix ${copyRoot} run tsc:publish`);
if (project.commands) {
    project.commands.forEach(c => commands.push(c))
}
tsList.forEach(f => { commands.push(`rm ${path.resolve(copyRoot, f.copyName)}`); });
commands.push(`rm -Rf ${path.resolve(copyRoot, "node_modules")}`);
fs.writeFileSync(`build_${projectId}.sh`, commands.join("\n"));

commands = [];
commands.push(`npm publish --folder ${copyRoot} `);
commands.push(`rm -Rf ${copyRoot}`);
fs.writeFileSync(`publish_${projectId}.sh`, commands.join("\n"));
