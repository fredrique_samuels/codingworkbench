import IEncryptionPolicy from "@codingworkbench/common/IEncryptionPolicy";


export default interface IEncryptionAware {
    encryptionPolicy: IEncryptionPolicy;
}
