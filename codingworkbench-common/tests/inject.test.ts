import {deepStrictEqual, ok} from "assert";

import CwbInject from "../lib/inject";
import ITodoModule, {ITodoItem, ITodoServiceArgs, TODO_ITEM_NAME, TodoService, todoServiceInjectionKeys, TodoStore} from "./todoMod";

function assertTodoItem(item: ITodoItem) {
    ok(item);
    deepStrictEqual(item.label, TODO_ITEM_NAME);
}

interface IUserData {
    name: string;
    age: number;
}

interface IEmployeeData {
    employeeNumber: string;
}

describe("inject.test.ts", () => {

    it ("create using factory functions", (done) => {

        async function _e() {
            // given
            const injector = new CwbInject.Module<ITodoModule>();
            injector.bind("todoStore", async () => new TodoStore());
            injector.bind("todoService", async (args) => new TodoService({todoStore: await args.get("todoStore")}));

            // when
            const [item] = (await injector.get("todoService")).getItems();

            // then
            assertTodoItem(item);
        }

        _e().then(() => done())
            .catch(done);
    });

    it ("error if no types registered", (done) => {

        async function _e() {
            // given
            const injector = new CwbInject.Module<ITodoModule>();

            // when
            try {
                await injector.get("todoService");
                ok(false, "This should fail when no type mapping was set");
            } catch (e) {

            }
        }

        _e().then(() => done())
            .catch(done);
    });

    it ("get all", (done) => {

        async function _e() {
            // given
            const injector = new CwbInject.Module<IUserData>()
                .bind("name", async (args) => "John")
                .bind("age", async (args) => 55);

            deepStrictEqual(await injector.getAll(), {name: "John", age: 55});
        }

        _e().then(() => done())
            .catch(done);
    });

    it ("combine", (done) => {

        async function _e() {
            // given
            const injector = new CwbInject.Module<IUserData>()
                .bind("name", async (args) => "John")
                .bind("age", async (args) => 55)
                .merge(
                    new CwbInject.Module<IEmployeeData>()
                        .bind("employeeNumber", async (args) => "employee-1")
                );

            deepStrictEqual(await injector.getAll(), {employeeNumber: 'employee-1', name: "John", age: 55});
        }

        _e().then(() => done())
            .catch(done);
    });
});
