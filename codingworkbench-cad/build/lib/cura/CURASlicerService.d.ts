import SettingItem from "../common/SettingItem";
import ISettingOption from "../common/SettingOption";
import ISlicerService from "../service/ISlicerService";
import { CURAPaths } from "./CURAPaths";
import ISliceJobOutput from "lib/ISliceJobOutput";
export default class CURASlicerService implements ISlicerService {
    private slicer;
    constructor(enginePaths: CURAPaths);
    getDefinitions(): Promise<ISettingOption[]>;
    getOptions(): Promise<SettingItem[]>;
    slice(stlString: string, definition: string, options: SettingItem[]): Promise<ISliceJobOutput>;
}
