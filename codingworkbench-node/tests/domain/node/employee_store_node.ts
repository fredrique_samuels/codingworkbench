import CwbNode from "../../../node";

import {EmployeeStore, IEmployee} from "../employee";
import {createEmployeeStored} from "./employee_element";


const storeHasChild: CwbNode.HasChildPredicate<EmployeeStore> = (store, name) => store ? store[name] !== undefined : false;
const storeSearchChild: CwbNode.SearchChildNamesFunction<EmployeeStore> =
    (store, search) => store
        ? Object.keys(store).filter(k => CwbNode.matchesSearch(search, k))
        : [];

const employees_group = new CwbNode.ElementGroupBuilder<IEmployee, EmployeeStore>(
    "employees-data",
        createEmployeeStored(),
        storeHasChild,
    )
    .setSearchChildNamesFunction(storeSearchChild)
.build();

export const EMPLOYEE_STORE_TAG = "employee-store";
export function createEmployeeStoreNode(store: EmployeeStore, errorFunction?: CwbNode.ErrorFunction): CwbNode.IRootElement<EmployeeStore> {
    const builder = new CwbNode.NodeTreeBuilder<EmployeeStore>(EMPLOYEE_STORE_TAG, store);
    if (errorFunction) { builder.setErrorFunction(errorFunction); }
    return builder
        .addChild("employees", employees_group)
        .build();
}
