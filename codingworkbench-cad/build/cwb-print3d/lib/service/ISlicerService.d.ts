import SettingItem from "cwb-core/settings/SettingItem";
import ISettingOption from "cwb-core/settings/SettingOption";
import ISliceResults from "../ISliceResults";
export default interface ISlicerService {
    getOptions(): Promise<SettingItem[]>;
    getDefinitions(): Promise<ISettingOption[]>;
    slice(stlString: string, definition: string, options: SettingItem[]): Promise<ISliceResults>;
}
