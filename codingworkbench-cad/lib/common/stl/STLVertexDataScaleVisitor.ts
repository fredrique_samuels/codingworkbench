

function line_has_vertex_data(line: string): boolean {
    return line.trim().startsWith("vertex")
}

function visit_line(scale: number, line: string, lineNumber: number): string {
    const scaleFunc = (f: number) =>  scale * f;
    const toStringFunc = (f: number) => f.toFixed(15);

    if (!line_has_vertex_data(line)) {
        return line;
    }

    const [leadingSpace, valueArray] = line.split("vertex");
    const newValuesString = valueArray.trim()
        .split(" ")
        .map(parseFloat)
        .map(scaleFunc)
        .map(toStringFunc)
        .join(" ");
    return `${leadingSpace}vertex ${newValuesString}\r`;
}

export default class STLVertexDataScaleVisitor {
    data = "";
    scale = 1.0;

    constructor(scale: number = 1.0) {
        this.scale = scale;
    }

    public visit(line: string, lineNumber: number): void {
        this.data = this.data + visit_line(this.scale, line, lineNumber) + "\n"
    }

    get content(): string {
        return this.data;
    }
}
