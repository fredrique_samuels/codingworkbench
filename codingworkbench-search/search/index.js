"use strict";
exports.__esModule = true;
var search = require("./Search");
var CwbSearch;
(function (CwbSearch) {
    CwbSearch.SearchBuilder = search.SearchBuilder;
    CwbSearch.results_from_array = search.search_results_from_array;
    CwbSearch.mapResults = search.mapResults;
    CwbSearch.PageDataSupplier = search.SearchResultBuilder;
    CwbSearch.INF_PAGE_SIZE = search.INF_PAGE_SIZE;
    CwbSearch.do_paged_search = search.do_paged_search;
})(CwbSearch || (CwbSearch = {}));
exports["default"] = CwbSearch;
//# sourceMappingURL=index.js.map