"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * Contruct an html string using the provided template values.
 *
 * @param template The template to construct html from.
 */
function build_html(template) {
    return "\n            <html lang=\"en\">\n            <head>\n            <title>" + template.title + "</title>\n            " + template.head.join("\n") + "\n            </head>\n            <body>\n              " + template.preBody.join("\n") + "\n              " + template.body.join("\n") + "\n            </body>\n            </html>\n        ";
}
exports.build_html = build_html;
