import {ID} from "./IUnique";
import IUserSession from "./IUserSession";

const TIME_30_MINUTES = 1800000;

export class UserSessionBuilder {
    private userId: ID;
    private accountId: ID;
    private ttl: number;

    constructor() {
        this.userId = 0;
        this.accountId = 0;
        this.ttl = TIME_30_MINUTES;
    }

    public setUserId(userId: ID): this {
        this.userId = userId;
        return this;
    }

    public setAccountId(accountId: ID): this {
        this.accountId = accountId;
        return this;
    }

    public setTTL(ttl: number): this {
        this.ttl = ttl;
        return this;
    }

    public build(): IUserSession {
        return {
            userId: this.userId,
            accountId: this.accountId,
            created: new Date().getTime(),
            ttl: this.ttl,
        };
    }
}

export default UserSessionBuilder;
