import CwbAction from "@codingworkbench/action";
import CwbLog from "@codingworkbench/log";
import CrudDataAccessor from "@codingworkbench/data/CrudDataAccessor";
import IEncryptionPolicy from "@codingworkbench/common/IEncryptionPolicy";
import Dict from "@codingworkbench/common/Dict";
import Password from "@codingworkbench/common/Password";

import ISecurityService from "../api/ISecurityService";
import SecurityErrors from "../api/security_errors";
import ICwbAccount from "../api/ICwbAccount";
import {ICwbUser} from "../api/ICwbUser";

import CwbUser from "./CwbUser";
import CwbAccount from "./CwbAccount";
import {SecurityEmailKeys} from "./emails";
import {ID} from "@codingworkbench/data/IUnique";
import {IEmailTemplateProvider} from "../../email/api/IEmailTemplateProvider";
import {ISecurityLinks} from "../api/links";
import IEmailService from "../../email/api/IEmailService";
import {build_mail_from_template} from "../../email/api/IEmailTemplate";
import {EmailSource} from "../../email/api/EmailSource";


class Validator<E extends Object> {
    _errors: string[];

    constructor() {
        this._errors = [];
    }

    add(code: E) {
        this._errors.push(code.toString());
    }

    get hasError(): boolean {
        return this._errors.length > 0;
    }

    get action(): CwbAction.IResponse {
        if (this.hasError) {
            const [e] = this._errors;
            return new CwbAction.ResponseBuilder().error(e).build();
        }
        return new CwbAction.ResponseBuilder().build();
    }
}

async function validate_email_format(email: string, validator: Validator<SecurityErrors>) {
    if (!Password.isEmailString(email)) {
        validator.add(SecurityErrors.EMAIL_INVALID);
    }
}

async function validate_password_strength(password: string, validator: Validator<SecurityErrors>) {
    if (!Password.isPasswordStrengthMediumToHigh(password)) {
        validator.add(SecurityErrors.PASSWORD_WEAK);
    }
}

async function validate_password_validated(password: string, passwordVerified: string, validator: Validator<SecurityErrors>) {
    if (password !== passwordVerified) {
        validator.add(SecurityErrors.PASSWORD_VERIFICATION_FAILED);
    }
}

async function validate_email_available(email: string, dao: CrudDataAccessor, validator: Validator<SecurityErrors>) {
    try {
        const user = new CwbUser(dao);
        await user.fromEmail(email);
    } catch (e) {
        return;
    }
    validator.add(SecurityErrors.EMAIL_IN_USE);
}

async function validate_email_registered(email: string, dao: CrudDataAccessor, validator: Validator<SecurityErrors>) {
    try {
        const user = new CwbUser(dao);
        return user.fromEmail(email);
    } catch (e) {
        validator.add(SecurityErrors.EMAIL_NOT_REGISTERED);
    }
}

export default class SecurityService implements ISecurityService {

    private readonly _dao: CrudDataAccessor;
    private _encryptionPolicy: IEncryptionPolicy;
    private _emailService: IEmailService;
    private emailTemplateProvider: IEmailTemplateProvider;
    private securityCallbackHost: string;
    private securityLink: ISecurityLinks;

    constructor(dao: CrudDataAccessor,
                securityCallbackHost: string,
                securityLink: ISecurityLinks,
                encryptionPolicy: IEncryptionPolicy,
                emailService: IEmailService,
                emailTemplates: IEmailTemplateProvider
    ) {
        this.securityCallbackHost = securityCallbackHost;
        this.securityLink = securityLink;
        this._dao = dao;
        this._encryptionPolicy = encryptionPolicy;
        this._emailService = emailService;
        this.emailTemplateProvider = emailTemplates;
    }

    public async authenticate(email: string, password: string): Promise<CwbAction.IResponse<ICwbAccount>> {
        const user = new CwbUser(this._dao);
        try {
            await user.fromEmail(email);
        } catch (e) {
            return new CwbAction.ResponseBuilder().error(SecurityErrors.EMAIL_OR_PASS_INVALID).build();
        }

        if (user.params.password_hash !== (await this._encryptionPolicy.encrypt(password))) {
            return new CwbAction.ResponseBuilder().error(SecurityErrors.EMAIL_OR_PASS_INVALID).build();
        }

        const [account] = await user.getAccounts();
        if (account) {
            return new CwbAction.ResponseBuilder().data(account.params).build();
        }
        return new CwbAction.ResponseBuilder().error(SecurityErrors.EMAIL_OR_PASS_INVALID).build();
    }

    public async getAccountUsers(account: ICwbAccount): Promise<ICwbUser[]> {
        return (await
                    (await new CwbAccount(this._dao).fromId(account.id))
                .getUsers())
                .map(u => u.params);
    }

    async registerNewUser(email: string, password: string): Promise<CwbAction.IResponse<ICwbAccount>> {
        const validator = new Validator<SecurityErrors>();
        await validate_email_format(email, validator);
        await validate_password_strength(password, validator);
        await validate_email_available(email, this._dao, validator);
        if (validator.hasError) {
            return validator.action;
        }

        // create user
        const user = new CwbUser(this._dao);
        {
            const params = user.params;
            params.email = email;
            params.password_hash = await this._encryptionPolicy.encrypt(password);
            user.update(params);
            await user.save();
        }

        // create account
        const account = new CwbAccount(this._dao);
        await account.save();
        {
            const params = account.params;
            params.activation_token = (await this._encryptionPolicy.encrypt(`${user.params.id}:${account.params.id}:${new Date().getTime()}`)).replace("=", "");
            account.update(params);
            await account.save();
        }

        await account.linkUser(user);

        // TODO Check if there is other account linked to the same user that is already activated then activated this account automatically
        await this.sendActivationLink(email);

        return new CwbAction.ResponseBuilder().data(account.params).build();
    }

    public async resetPassword(email: string, pw: string, verifyPw: string): Promise<CwbAction.IResponse> {
        const validator = new Validator<SecurityErrors>();
        await validate_email_registered(email, this._dao, validator);
        await validate_password_strength(pw, validator);
        await validate_password_validated(pw, verifyPw, validator);
        if (validator.hasError) {
            return validator.action;
        }

        const user = new CwbUser(this._dao);
        await user.fromEmail(email);

        let params = user.params;
        params.password_hash = await this._encryptionPolicy.encrypt(pw);
        params.password_reset_token = null;
        await user.update(params).save();

        return new CwbAction.ResponseBuilder().build();
    }

    async sendPasswordResetLink(email: string): Promise<CwbAction.IResponse> {
        const validator = new Validator<SecurityErrors>();
        await validate_email_registered(email, this._dao, validator);
        if (validator.hasError) {
            return validator.action;
        }

        let user = await new CwbUser(this._dao).fromEmail(email);
        let params = user.params;

        params.password_reset_token = await this._encryptionPolicy.encrypt(`${user.params.id}:${new Date().getTime()}`);
        await user.update(params).save();

        const link = await this.createUserPasswordResetRequestLink(params.password_reset_token);
        const mailTemplate = this.emailTemplateProvider.get(SecurityEmailKeys.PASSWORD_RESET_REQUEST.toString());
        if (!mailTemplate) {
            CwbLog.error(new Error(`Email template for ${SecurityEmailKeys.PASSWORD_RESET_REQUEST.toString()} is not specified!`));
            return new CwbAction.ResponseBuilder().error(SecurityErrors.SERVER_ERROR).build()
        }

        const emailParams = build_mail_from_template(mailTemplate, new Dict<string>().put("link", link));
        await this._emailService.sendRawHtmlMail(EmailSource.NO_REPLY.toString(), {...emailParams, email});

        return new CwbAction.ResponseBuilder().build();
    }

    async activateAccount(token: string | null): Promise<CwbAction.IResponse> {
        const validator = new Validator<SecurityErrors>();
        if (token === null) {
            validator.add(SecurityErrors.INVALID_TOKEN);
            return validator.action;
        }

        const cwbAccount = await new CwbAccount(this._dao).searchForActivationAccount(token as string);
        if (cwbAccount) {
            const params = cwbAccount.params;
            params.activated = true;
            params.activation_token = null;
            await cwbAccount.update(params).save();
        } else {
            validator.add(SecurityErrors.INVALID_TOKEN);
        }

        return validator.action;
    }

    async hasPendingActivationAccounts(email: string): Promise<CwbAction.IResponse<boolean>> {
        const validator = new Validator<SecurityErrors>();
        await validate_email_format(email, validator);
        if (validator.hasError) {
            return new CwbAction.ResponseBuilder().data(false).build();
        }

        const user = new CwbUser(this._dao);
        await user.fromEmail(email);
        const accounts: CwbAccount[] = await user.getAccounts();
        const notActivated = (a: CwbAccount) => !a.params.activated;
        return new CwbAction.ResponseBuilder().data(accounts.filter(notActivated).length > 0).build();
    }

    async sendActivationLink(email: string): Promise<CwbAction.IResponse> {
        try {
            const user = new CwbUser(this._dao);
            await user.fromEmail(email);
            const accounts = await user.getAccounts();

            const [account] = accounts.filter(acc => !acc.params.activated);
            if (account) {
                const link = await this.createAccountActivationLink(account.params.activation_token);
                const mailTemplate = this.emailTemplateProvider.get(SecurityEmailKeys.ACCOUNT_ACTIVATION_REQUIRED.toString());
                if (!mailTemplate) {
                    CwbLog.error(new Error(`Email template for ${SecurityEmailKeys.ACCOUNT_ACTIVATION_REQUIRED.toString()} is not specified!`));
                    return new CwbAction.ResponseBuilder().error(SecurityErrors.SERVER_ERROR).build()
                }

                const emailParams = build_mail_from_template(mailTemplate, new Dict<string>().put("link", link));
                await this._emailService.sendRawHtmlMail(EmailSource.NO_REPLY.toString(), {...emailParams, email});
                return new CwbAction.ResponseBuilder().build();
            } else {
                return new CwbAction.ResponseBuilder().error(SecurityErrors.EMAIL_ALREADY_REGISTERED).build();
            }

        } catch (e) {
            CwbLog.error(e);
            return new CwbAction.ResponseBuilder().error(SecurityErrors.EMAIL_INVALID).build();
        }
    }

    async getUserFromEmail(email: string): Promise<ICwbUser | null> {
        try {
            return (await new CwbUser(this._dao).fromEmail(email)).params;
        } catch (e) {
            return null;
        }
    }

    async getAccountById(id: ID): Promise<ICwbAccount | null> {
        const [account] = (await new CwbAccount(this._dao).searchByIds([id])).map(a => a.params);
        return account || null;
    }



    private async createAccountActivationLink(activation_token: string | null): Promise<string> {
        if (activation_token) {
            return `${this.securityCallbackHost}${this.securityLink.doActivateAccount.replace(":token", encodeURIComponent(activation_token))}`;
        }
        throw new Error("No activation token provided");
    }

    private async createUserPasswordResetRequestLink(token: string | null): Promise<string> {
        if (token) {
            return `${this.securityCallbackHost}${this.securityLink.resetPasswordPage}/${encodeURIComponent(token)}`;
        }
        throw new Error("No reset token provided");
    }


}
