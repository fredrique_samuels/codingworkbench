import IPrintSummary from "./IPrintSummary";

export default interface ISliceSummary extends IPrintSummary {
    readonly minx?: number;
    readonly miny?: number;
    readonly minz?: number;
    readonly maxx?: number;
    readonly maxy?: number;
    readonly maxz?: number;
    readonly dimx?: number;
    readonly dimy?: number;
    readonly dimz?: number;
    readonly centerx?: number;
    readonly centery?: number;
    readonly centerz?: number;
}
