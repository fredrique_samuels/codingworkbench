
export default class STLFile {
    _filename: string;

    constructor(filename: string) {
        this._filename = filename;
    }

    public isBinary(): boolean {
        return false
    }
}
