


import ICwbAccount from "../api/ICwbAccount";
import CwbUser from "./CwbUser";
import CwbSearch from "@codingworkbench/search";
import PersistedObject from "@codingworkbench/data/PersistedObject";
import CrudDataAccessor from "@codingworkbench/data/CrudDataAccessor";
import DbColumn from "@codingworkbench/data/DbColumn";
import DbTable from "@codingworkbench/data/DbTable";
import {ID} from "@codingworkbench/data/IUnique";
import IUserSession from "@codingworkbench/data/IUserSession";

const TABLE: DbTable = new DbTable(
    "cwb_auth_account",
    [
        new DbColumn("username"),
        new DbColumn("activated"),
        new DbColumn("activation_token"),
    ])
    .setUnique()
    .setArchived()
    .setAudited();


const ACCOUNT_USERS_TABLE: DbTable = new DbTable(
    "cwb_auth_user_account",
    [
        new DbColumn("cwb_auth_user_id"),
        new DbColumn("cwb_auth_account_id"),
    ]);

export default class CwbAccount extends PersistedObject<ICwbAccount> {

    private _users: CwbUser[];

    constructor(dao: CrudDataAccessor) {
        super(dao, TABLE);
        this._users = []
    }

    public createParams(): ICwbAccount {
        return {
            ...PersistedObject.createPersistedObject(),
            username: null,
            activated: false,
            activation_token: null
        };
    }

    public async linkUser(user: CwbUser) {
        this.validatePersisted();
        user.validatePersisted();
        const [ {count} ] = await this.dao.execute({
            table: ACCOUNT_USERS_TABLE,
            command: `SELECT count(*) as count from cwb_auth_user_account WHERE cwb_auth_user_id=? AND cwb_auth_account_id=?;`,
            targetResult: "RAW",
            values: [user.id, this.id]
        });
        if (count === 0) {
            await this.dao.execute({
                table: ACCOUNT_USERS_TABLE,
                command: `INSERT INTO cwb_auth_user_account ( cwb_auth_user_id, cwb_auth_account_id) VALUES (?, ?);`,
                values: [user.id, this.id]
            });
        }
    }

    public async getUsers(): Promise<CwbUser[]> {
        const rows = await this.dao.execute({
            table: ACCOUNT_USERS_TABLE,
            command: `SELECT cwb_auth_user_id as id from cwb_auth_user_account WHERE cwb_auth_account_id=?;`,
            targetResult: "RAW",
            values: [this._params.id]
        });

        return rows.length > 0
            ? await new CwbUser(this.dao).searchByIds(rows.map(r => r.id))
            : [];
    }

    async getForUser(userId: ID): Promise<CwbAccount[]> {
        const rows = await this.dao.execute({
            table: ACCOUNT_USERS_TABLE,
            command: `SELECT cwb_auth_account_id as id from cwb_auth_user_account WHERE cwb_auth_user_id =?;`,
            targetResult: "RAW",
            values: [userId]
        });
        return rows.length > 0
            ? await new CwbAccount(this.dao).searchByIds(rows.map(r => r.id))
            : [];
    }

    public async searchByIds(ids: ID[]): Promise<CwbAccount[]> {
        const {dao} = this;
        const searchBuilder = new CwbSearch.SearchBuilder();
        searchBuilder.filters.addFilter("id", ids, "in");
        return (await this.search(searchBuilder.build())).map(p =>  new CwbAccount(dao).update(p));
    }

    public async save(userSession?: IUserSession): Promise<this> {
        await super.save(userSession);
        return this;
    }

    public update(account: ICwbAccount): this {
        super.update(account);
        this._params.activated = account.activated;
        this._params.activation_token = account.activation_token;
        this._params.username = account.username;
        return this;
    }

    public async searchForActivationAccount(token: string): Promise<CwbAccount> {
        const {dao} = this;

        const searchBuilder = new CwbSearch.SearchBuilder();
        searchBuilder.filters.addFilter("activation_token", token);
        searchBuilder.filters.addFilter("activated", false);

        const [account] = (await this.search(searchBuilder.build())).map(p =>  new CwbAccount(dao).update(p));
        return account;
    }
}
