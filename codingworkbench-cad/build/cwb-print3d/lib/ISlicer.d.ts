import ISliceResults from "./ISliceResults";
export default interface ISlicer {
    slice(stlFile: string, gCodeFile: string, options: any): Promise<ISliceResults>;
}
