

export function force_cast<T>(d: T | undefined): T {
    return d as T;
}
