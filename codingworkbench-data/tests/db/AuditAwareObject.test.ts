
import {deepStrictEqual, ok} from 'assert'
import AuditAware, {audit_columns} from "../../lib/AuditAware";
import ILastUpdateAware from "../../lib/ILastUpdateAware";


describe('AuditAwareObject', function() {


    it("#create() defaults", () => {
        let audited = new AuditAware({}).flatten();
        let curDate = new Date();


        deepStrictEqual( null,  audited.user_of_entry);
        deepStrictEqual( null,  audited.user_last_updated);
        ok(audited.date_of_entry.getTime() >= curDate.getTime());
        ok(audited.date_last_updated.getTime() >= curDate.getTime());
    });

    it("#create() with userId", () => {
        let audited = new AuditAware({}, 33).flatten();

        deepStrictEqual( 33,  audited.user_of_entry);
        deepStrictEqual( 33,  audited.user_last_updated);
    });

    it("#update() defaults", () => {
        let audited: ILastUpdateAware = new AuditAware({}).setLastUpdated().flatten();
        let curDate = new Date();

        deepStrictEqual( null,  audited.user_last_updated);
        ok(audited.date_last_updated.getTime() >= curDate.getTime());
    });

    it("#update() with userId", () => {
        let curDate = new Date();
        let audited: ILastUpdateAware = new AuditAware({}).setLastUpdated(44).flatten();

        deepStrictEqual( 44,  audited.user_last_updated);
        ok(audited.date_last_updated.getTime() >= curDate.getTime());
    });

    it("#columns()", () => {
        /* given */
        let columns = audit_columns();

        /* then */
        deepStrictEqual(columns.length, 4);

        const [doe, uoe, dlu, ulu] = columns;
        deepStrictEqual(doe.field, "date_of_entry");
        deepStrictEqual(doe.column, "doe");

        deepStrictEqual(uoe.field, "user_of_entry");
        deepStrictEqual(uoe.column, "uoe");

        deepStrictEqual(dlu.field, "date_last_updated");
        deepStrictEqual(dlu.column, "dlu");

        deepStrictEqual(ulu.field, "user_last_updated");
        deepStrictEqual(ulu.column, "ulu");
    });
    
});
