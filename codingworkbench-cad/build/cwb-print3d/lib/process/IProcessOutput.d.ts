export default interface IProcessOutput {
    readonly error: Error;
    readonly errorMessage: string;
    readonly stdout: string;
    readonly stderr: string;
}
