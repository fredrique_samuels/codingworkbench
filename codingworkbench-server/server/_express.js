"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var FileDataImpl = /** @class */ (function () {
    function FileDataImpl(file) {
        this._file = file;
    }
    Object.defineProperty(FileDataImpl.prototype, "uuid", {
        get: function () { return this._file.md5; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FileDataImpl.prototype, "md5", {
        get: function () { return this._file.md5; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FileDataImpl.prototype, "name", {
        get: function () { return this._file.name; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FileDataImpl.prototype, "mimetype", {
        get: function () { return this._file.mimetype; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FileDataImpl.prototype, "data", {
        get: function () { return this._file.data; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FileDataImpl.prototype, "size", {
        get: function () { return this._file.size; },
        enumerable: true,
        configurable: true
    });
    return FileDataImpl;
}());
var UploadedFilesImpl = /** @class */ (function () {
    function UploadedFilesImpl(files) {
        this._files = files;
    }
    UploadedFilesImpl.prototype.get = function (alias) {
        var f = this._files[alias];
        if (!f) {
            return [];
        }
        if (Array.isArray(f)) {
            return f
                .map(function (file) { return new FileDataImpl(file); });
        }
        return [new FileDataImpl(f)];
    };
    return UploadedFilesImpl;
}());
var SessionImpl = /** @class */ (function () {
    function SessionImpl(session) {
        this._session = session;
    }
    Object.defineProperty(SessionImpl.prototype, "id", {
        get: function () {
            if (this._session) {
                return this._session.id;
            }
            return "";
        },
        enumerable: true,
        configurable: true
    });
    SessionImpl.prototype.get = function (name) {
        if (this._session) {
            return this._session[name];
        }
    };
    SessionImpl.prototype.set = function (name, value) {
        if (this._session && name !== "id") {
            this._session[name] = value;
        }
    };
    SessionImpl.prototype.destroy = function (err) {
        if (this._session) {
            this._session.destroy(err ? err : console.error);
        }
    };
    return SessionImpl;
}());
var RequestWrapper = /** @class */ (function () {
    function RequestWrapper(req) {
        this.req = req;
    }
    Object.defineProperty(RequestWrapper.prototype, "sessionID", {
        get: function () {
            return this.req.sessionID || "";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestWrapper.prototype, "body", {
        get: function () {
            return this.req.body;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestWrapper.prototype, "params", {
        get: function () {
            return this.req.params;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestWrapper.prototype, "files", {
        get: function () {
            var files = this.req.files;
            if (files === undefined || files === null || Object.keys(files).length == 0) {
                return new UploadedFilesImpl({});
            }
            return new UploadedFilesImpl(files);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestWrapper.prototype, "headers", {
        get: function () {
            return this.req.headers;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestWrapper.prototype, "method", {
        get: function () {
            var lowerCase = this.req.method.toLowerCase();
            if (lowerCase === 'get')
                return 'get';
            if (lowerCase === 'post')
                return 'post';
            throw new Error("Unspported method");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestWrapper.prototype, "url", {
        get: function () {
            return this.req.url;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RequestWrapper.prototype, "session", {
        get: function () {
            return new SessionImpl(this.req.session);
        },
        enumerable: true,
        configurable: true
    });
    return RequestWrapper;
}());
var ResponseWrapper = /** @class */ (function () {
    function ResponseWrapper(res) {
        this._terminated = false;
        this.res = res;
    }
    ResponseWrapper.prototype.sendText = function (text) {
        this.res.send(text);
        this.terminate();
    };
    ResponseWrapper.prototype.sendJson = function (str) {
        this.res.setHeader('Content-Type', 'application/json');
        this.res.end(str);
        // this.terminate();
    };
    ResponseWrapper.prototype.sendJsonObj = function (obj, pretty) {
        this.sendJson(JSON.stringify(obj, null, pretty ? 4 : 0));
    };
    ResponseWrapper.prototype.sendStatus = function (code, text) {
        if (text) {
            this.res.status(code).send(text);
        }
        else {
            this.res.sendStatus(code);
        }
        this.terminate();
    };
    ResponseWrapper.prototype.redirect = function (target) {
        this.res.redirect(target);
        this.terminate();
    };
    ResponseWrapper.prototype.view = function (view) {
        this.res.send(view.html);
        this.terminate();
    };
    Object.defineProperty(ResponseWrapper.prototype, "terminated", {
        get: function () { return this._terminated; },
        enumerable: true,
        configurable: true
    });
    ResponseWrapper.prototype.terminate = function () { this._terminated = true; };
    return ResponseWrapper;
}());
var ExpressHttpFactory = /** @class */ (function () {
    function ExpressHttpFactory() {
    }
    ExpressHttpFactory.prototype.createRequest = function (input) {
        return new RequestWrapper(input);
    };
    ExpressHttpFactory.prototype.createResponse = function (input) {
        return new ResponseWrapper(input);
    };
    return ExpressHttpFactory;
}());
exports.ExpressHttpFactory = ExpressHttpFactory;
function expressTransformHandler(handler, context) {
    return function (req, res) {
        handler({
            request: new RequestWrapper(req),
            response: new ResponseWrapper(res),
            context: context
        });
    };
}
exports.expressTransformHandler = expressTransformHandler;
function expressTransformHandlerAsync(handler, context) {
    var _this = this;
    return function (req, res) { return __awaiter(_this, void 0, void 0, function () {
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0: return [4 /*yield*/, handler({
                        request: new RequestWrapper(req),
                        response: new ResponseWrapper(res),
                        context: context
                    })];
                case 1: return [2 /*return*/, _a.sent()];
            }
        });
    }); };
}
exports.expressTransformHandlerAsync = expressTransformHandlerAsync;
