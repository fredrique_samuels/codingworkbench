import * as PropertiesReader_Dep from "properties-reader"
import IPropertiesReader from "./IPropertiesReader";


export class PropertiesFileReader implements IPropertiesReader {
    private _reader: PropertiesReader_Dep.Reader;

    constructor(path: string) {
        this._reader = PropertiesReader_Dep(path)
    }

    getString(property: string, fallback?: string): string {
        let p = this._reader.getRaw(property);
        if(p === null) {
            if(fallback !== undefined) {
                return fallback
            }
            throw new Error(`Property '${property}' not found with not fallback provided`)
        }
        return p
    }

    getNumber(property: string, fallback?: number): number {
        try {
            let p = this._reader.getRaw(property);
            if(p === null) {
                if(fallback !== undefined) {
                    return fallback
                }
                throw new Error(`Property '${property}' not found with not fallback provided`)
            }
            return parseFloat(p)
        } catch {
            throw new Error(`Property '${property}' expected to be a number`)
        }
    }
}


export default PropertiesFileReader;
