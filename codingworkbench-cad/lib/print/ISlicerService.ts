import IOption from "../common/IOption";
import ISliceJobInput from "./ISliceJobInput";
import ISliceJobOutput from "lib/print/ISliceJobOutput";

export default interface ISlicerService {
    getPrintOptions(): Promise<IOption[]>;
    getProfileOptions(): Promise<IOption[]>;
    slice(path: string, input: ISliceJobInput): Promise<ISliceJobOutput>;
    sliceBatch(path: string[], input: ISliceJobInput): Promise<ISliceJobOutput>;
    sliceString(id: string, stlFile: string, input: ISliceJobInput): Promise<ISliceJobOutput>;
}
