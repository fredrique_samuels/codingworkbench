import {EmailSourceCredentials} from "../bizimpl/EmailService";
import IEmailService from "./IEmailService";
import {IEmailTemplateProvider} from "./IEmailTemplateProvider";
import IEmailTransporter from "./IEmailTransporter";
import IConnectionPropertiesAware from "@codingworkbench/data/IConnectionPropertiesAware";

export default interface IEmailAwareContext extends IConnectionPropertiesAware {
    emailSourceCredentials: EmailSourceCredentials;
    emailService: IEmailService;
    emailTransporter: IEmailTransporter;
    emailTemplateProvider: IEmailTemplateProvider
}
