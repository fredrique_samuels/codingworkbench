"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var PropertiesReader_1 = require("cwb-core/common/PropertiesReader");
var os = require("os");
var CURAPaths = (function () {
    function CURAPaths() {
    }
    CURAPaths.fromProperties = function (properties) {
        var platform = os.platform();
        return {
            home: properties.getString("slicer." + platform + ".cura.home"),
            definitions: properties.getString("slicer." + platform + ".cura.definitions"),
            engine: properties.getString("slicer." + platform + ".cura.engine")
        };
    };
    CURAPaths.fromPropertiesFile = function (path) {
        return CURAPaths.fromProperties(PropertiesReader_1.default.readFile(path));
    };
    return CURAPaths;
}());
exports.CURAPaths = CURAPaths;
