
import {IHasState} from "../state/IHasState";


/**
 * Interface to define properties used to open and close components
 */
export interface IOpenClose {
    /**
     * Boolean to toggle the open state of a component.
     */
    open: boolean,
}


/**
 * Set the opens state of nested property on the state.
 *
 * Example:
 *  If state = { dialog: { open: false} }
 *      calling > OpenClose.open(comp, 'dialog')
 *   will result in the new state being
 *      state = { dialog: { open: true} }
 */
export function open<S>(hasState: IHasState<any, S>, property: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, keyof S> | null): void {
    setOpenState(hasState, true, property, targetUpdates, stateUpdates);
}

/**
 * Set the opens state of nested property on the state.
 *
 * Example:
 *  If state = { dialog: { open: true } }
 *      calling > OpenClose.close(comp, 'dialog')
 *   will result in the new state being
 *      state = { dialog: { open: false } }
 */
export function close<S>(hasState: IHasState<any, S>, property: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, keyof S> | null): void {
    setOpenState(hasState, false, property, targetUpdates, stateUpdates);
}

/**
 * Create a zero argument callback function to the #open() function. Useful for binding to events.
 * @see OpenClose.open
 */
export function openHandler<S>(hasState: IHasState<any, S>, property: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, keyof S>): () => void {
    return (() => { open(hasState, property, targetUpdates, stateUpdates) }) ;
}

/**
 * Create a zero argument callback function to the #close() function. Useful for binding to events.
 * @see OpenClose.close
 */
export function closeHandler<S>(hasState: IHasState<any, S>, property: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, keyof S>): () => void {
    return (() => { close(hasState, property, targetUpdates, stateUpdates) }) ;
}


/* main state update method. This is not exported */
function setOpenState<S, K extends keyof S>(hasState: IHasState<any, S>, open: boolean, target: keyof S, targetUpdates?: any, stateUpdates?: Pick<S, keyof S> | null) {
    let propertyUpdates = {...targetUpdates, ...{open: open}};
    if(stateUpdates !== undefined) {
        hasState.updateState(stateUpdates, target, propertyUpdates)
    } else {
        hasState.updateStateProperty(target, propertyUpdates);
    }
}
