
namespace ProxyUtil {

    class PHandler implements ProxyHandler<any> {

        private h: Handler;

        constructor(h: Handler) {
            this.h = h;
        }

        set(target: any, p: PropertyKey, value: any, receiver: any): boolean {
            this.h.set(target, p, value);
            return true
        }

        get(target: any, p: PropertyKey, receiver: any): any {
            return this.h.get(target, p);
        }
    }

    class EmptyHandler implements ProxyHandler<any> {
        set(target: any, p: PropertyKey, value: any, receiver: any): boolean {
            return true;
        }

        get(target: any, p: PropertyKey, receiver: any): any {}
    }

    export interface Handler {
        set(target: any, p: PropertyKey, value: any): void;
        get(target: any, p: PropertyKey): any;
    }

    export function handlerWrapper<T extends object>(h: Handler): ProxyHandler<T> {
        return new PHandler(h);
    }

    export function create<T>(handler?: Handler): T {
        return createForTarget({}, handler);
    }

    export function createForTarget<T>(target: any, handler?: Handler): T {
        if (handler) {
            return new Proxy<any>(target, handlerWrapper(handler));
        }
        return new Proxy<any>(target, new EmptyHandler());
    }
}


export default ProxyUtil;