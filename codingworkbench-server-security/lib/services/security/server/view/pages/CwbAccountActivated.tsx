import * as React from "react"
import * as ReactDOM from "react-dom";

import CssBaseline from '@material-ui/core/CssBaseline';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {Link} from '@material-ui/core';
import Copyright from "../components/Copyright";
import SecurityViewController from "../SecurityViewController";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function CwbAccountActivated() {
    const classes = useStyles();

    return (
        <Container component="main" maxWidth="sm">
            <CssBaseline />
            <div className={classes.paper}>
                <Typography component="h1" variant="h4">
                    Your account has been activated.
                </Typography>
                <br />
                <Link href="#" variant="body2" onClick={SecurityViewController.goto_signin} >
                    {"Process to Sign In"}
                </Link>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
}

ReactDOM.render(<CwbAccountActivated />, document.getElementById("root"));
