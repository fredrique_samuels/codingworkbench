import CwbI8N from "@codingworkbench/i8n";
import SecurityErrors from "./security_errors";


export const AuthenticationMessageCodes: CwbI8N.MessageKeys = {};
AuthenticationMessageCodes[SecurityErrors.EMAIL_IN_USE] = "The email provided is already in use";
AuthenticationMessageCodes[SecurityErrors.EMAIL_OR_PASS_INVALID] = "The username or password is not valid";
AuthenticationMessageCodes[SecurityErrors.EMAIL_INVALID] = "Invalid email";
AuthenticationMessageCodes[SecurityErrors.EMAIL_NOT_REGISTERED] = "The email provided is not registered";
AuthenticationMessageCodes[SecurityErrors.PASSWORD_WEAK] = "The password is not strong enough";
AuthenticationMessageCodes[SecurityErrors.INVALID_TOKEN] = "Invalid token provided";
AuthenticationMessageCodes[SecurityErrors.PASSWORD_VERIFICATION_FAILED] = "Password verification failed";
AuthenticationMessageCodes[SecurityErrors.SERVER_ERROR] = "Your request could not be processed at this time";

export default class AuthenticationMessageSource extends CwbI8N.DefaultMessageSource {

    constructor(messageSource?: CwbI8N.IMessageSource) {
        super(AuthenticationMessageCodes, messageSource);
    }
}
