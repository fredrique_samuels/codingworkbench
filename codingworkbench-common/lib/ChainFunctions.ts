
export enum LinkFunctionResult {
    NEXT,
    BREAK,
}

export type LinkFunction = () => LinkFunctionResult
export type LinkFunctionAsync = () => Promise<LinkFunctionResult>

export default class ChainedFunctions {

    private readonly functions: LinkFunctionAsync[];

    constructor() {
        this.functions = [];
    }

    public addSync(func: LinkFunction) {
        this.functions.push(ChainedFunctions.toAsync(func));
    }

    public add(func: LinkFunctionAsync) {
        this.functions.push(func);
    }

    public async execute(): Promise<void> {
        await ChainedFunctions.executeAll(this.functions);
    }

    public static executeAllSync(functions: LinkFunction[]) {
        for (let func of functions) {
            let result = func();
            if (result ===  LinkFunctionResult.BREAK) {
                return;
            }
        }
    }

    public static async executeAll(functions: LinkFunctionAsync[]): Promise<void> {
        for (let func of functions) {
            let result = await func();
            if (result ===  LinkFunctionResult.BREAK) {
                return;
            }
        }
    }

    public static toAsync(func: LinkFunction): LinkFunctionAsync {
        return async () =>  { return func() } ;
    }

}
