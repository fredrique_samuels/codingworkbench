import * as builder from "./builder";
import * as api from "./api";
import * as definition from "./definition";
import * as impl from "./impl";
import * as common from "./common";

namespace CwbNode {

    export import NodeTreeBuilder = builder.NodeTreeBuilder;
    export import ElementBuilder = builder.ElementBuilder;
    export import AttributeBuilder = builder.AttributeBuilder;
    export import ElementGroupBuilder = builder.ElementGroupBuilder;
    export import AbstractElementBuilder = builder.AbstractElementBuilder;

    export import NodeType = api.NodeType;
    export import HasChildPredicate = api.HasChildPredicate;
    export import SearchChildNamesFunction = api.SearchChildNamesFunction;

    export import ErrorFunction = api.ErrorFunction;
    export import AttributeConsumer = api.AttributeConsumer;
    export import ElementConsumer = api.ElementConsumer;

    export import AttributeDefinition = definition.AttributeDefinition;
    export import ElementDefinition = definition.ElementDefinition;
    export import ElementGroupDefinition = definition.ElementGroupDefinition;
    export import Child = definition.Child;
    export import Children = definition.Children;

    export import getErrorMessage = impl.getErrorMessage;

    export import INode = api.INode;
    export import IAttribute = api.IAttribute;
    export import IElement = api.IElement;
    export import IRootElement = api.IRootElement;
    export import IDataAccessor = api.IDataAccessor;

    export import ReadFunction = api.ReadFunction;
    export import WriteFunction = api.WriteFunction;


    export import matchesSearch = common.matchesSearch;
}



export default CwbNode;
