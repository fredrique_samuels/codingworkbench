// import CwbAction from "@cwb-action";
// import CwbWeb from "@cwb-web";
//
// import IMessageSource from "cwb-core/i8n/IMessageSource";
// import MessageSourceUtil from "cwb-core/i8n/MessageSourceUtil";
// import ICwbAccount from "../../../domain/ICwbAccount";
// import {AuthenticationControllerContext} from "../controller/AuthenticationControllerContext";
// import AuthenticationMessageSource from "../messages/AuthenticationMessageSource";
// import AuthenticationRequestPathContext from "../AuthenticationRequestPathContext";
// import AuthenticationRequestPathViewContext, {OnAuthenticatedCallback} from "../AuthenticationRequestPathViewContext";
//
//
//
// export async function doLoginAsync<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext, webContext: CwbWeb.RequestContext<C>) {
//     const {email, password} = webContext.request.body;
//     try {
//         // authenticate
//         const actionResponse: CwbAction.IResponse<ICwbAccount> = await webContext.context.authenticationService.authenticate(email, password);
//         if (actionResponse.error) {
//             await web_respond_with_error(actionResponse, webContext, viewContext.messageSource);
//             return
//         }
//
//         // extract data
//         const account: ICwbAccount = actionResponse.data as ICwbAccount;
//         const [user] = (await webContext.context.authenticationService.getAccountUsers(account)).filter(u => u.email === email);
//
//         if(!account.activated) {
//             webContext.response.sendJson(
//                 new CwbAction.ResponseBuilder().redirect(`${viewContext.htmlViewContext.siteMap.activateAccountPage}/${encodeURIComponent(email)}`).json()
//             );
//             return;
//         }
//
//         // update session info
//         const userSession: CwbWeb.IUserSession = new CwbWeb.UserSessionBuilder().setUserId(user.id).setAccountId(account.id).build();
//         webContext.request.session.set("key", userSession);
//
//         // callback to notify that the user was authenticated
//         viewContext.onAuthenticated(webContext);
//
//         /* TODO check "dest" parameter for destination url AND check "validateOnly" parameter for response type */
//         // redirect to user home page
//         webContext.response.sendJson(new CwbAction.ResponseBuilder().redirect(viewContext.htmlViewContext.siteMap.userHomePage).json());
//     } catch (e) {
//         web_respond_with_500(e, webContext);
//     }
// }
//
// export function doLogin<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//     const handler: CwbWeb.RequestHandlerAsync<C> = async (webContext: CwbWeb.RequestContext<C>) => {
//         await doLoginAsync(viewContext, webContext);
//     };
//     return CwbWeb.RequestHandlerBuilder.postAsync(viewContext.htmlViewContext.siteMap.doLogin, handler);
// }
//
// export function doResendActivationLink<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//     const handler: CwbWeb.RequestHandlerAsync<C> = async (webContext: CwbWeb.RequestContext<C>) => {
//         const {email} = webContext.request.body;
//         try {
//             // authenticate
//             const actionResponse: CwbAction.IResponse<ICwbAccount> = await webContext.context.authenticationService.sendActivationLink(email);
//             if (actionResponse.error) {
//                 await web_respond_with_error(actionResponse, webContext, viewContext.messageSource);
//                 return
//             }
//             web_respond_with_ok(webContext);
//         } catch (e) {
//             web_respond_with_500(e, webContext);
//         }
//     };
//     return CwbWeb.RequestHandlerBuilder.postAsync(viewContext.htmlViewContext.siteMap.doResendActivationLink, handler);
// }
//
// export function registerPage<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//     return CwbWeb.RequestHandlerBuilder.view(
//         viewContext.htmlViewContext.siteMap.registerPage,
//         viewContext.htmlViewContext.siteMap.registerPageView,
//         viewContext.htmlViewContext.createModelBuilder());
// }
//
// export function accountActivationSuccessPage<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//     return CwbWeb.RequestHandlerBuilder.view(
//         viewContext.htmlViewContext.siteMap.activateAccountSuccessPage,
//         viewContext.htmlViewContext.siteMap.activateAccountSuccessView,
//         viewContext.htmlViewContext.createModelBuilder());
// }
//
// export function doRegistration<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//
//     const handler: CwbWeb.RequestHandlerAsync<C> = async (webContext: CwbWeb.RequestContext<C>) => {
//
//         const {email, password} = webContext.request.body;
//         try {
//             // register the new user
//             const actionResponse = await webContext.context.authenticationService.registerNewUser(email, password);
//             if(actionResponse.error) {
//                 await web_respond_with_error(actionResponse, webContext, viewContext.messageSource);
//                 return;
//             }
//
//             let url = `${viewContext.htmlViewContext.siteMap.activateAccountPage}/${encodeURIComponent(email)}`;
//             webContext.response.sendJson(new CwbAction.ResponseBuilder().redirect(url).json());
//         } catch (e) {
//             web_respond_with_500(e, webContext);
//         }
//     };
//
//     return CwbWeb.RequestHandlerBuilder.postAsync(viewContext.htmlViewContext.siteMap.doRegistration, handler);
// }
//
// export function doAccountActivation<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//
//     const handler: CwbWeb.RequestHandlerAsync<C> = async (webContext: CwbWeb.RequestContext<C>) => {
//
//         const {token} = webContext.request.params;
//         try {
//             // register the new user
//             const actionResponse = await webContext.context.authenticationService.activateAccount(token);
//             if(actionResponse.error) {
//                 webContext.response.redirect(viewContext.htmlViewContext.siteMap.loginPage);
//                 return;
//             }
//             webContext.response.redirect(viewContext.htmlViewContext.siteMap.activateAccountSuccessPage);
//         } catch (e) {
//             web_respond_with_500(e, webContext);
//         }
//     };
//
//     let path = `${viewContext.htmlViewContext.siteMap.doActivateAccount}/:token`;
//     return CwbWeb.RequestHandlerBuilder.get(path, handler);
// }
//
// export function activateAccountPage<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//
//     const handler: CwbWeb.RequestHandlerAsync<C> = async (webContext: CwbWeb.RequestContext<C>) => {
//         const email = decodeURIComponent(webContext.request.params.email);
//
//         try {
//             const actionResponse = await webContext.context.authenticationService.hasPendingActivationAccounts(email);
//             if(actionResponse.error) {
//                 webContext.response.redirect(viewContext.htmlViewContext.siteMap.loginPage);
//                 return;
//             }
//
//             if (actionResponse.data) {
//                 webContext.response.view(
//                     webContext.context.viewFactory.modelAndView(
//                         viewContext.htmlViewContext.siteMap.activateAccountView,
//                         viewContext.htmlViewContext.createModelBuilder().put("email", email).build()
//                     )
//                 );
//             } else {
//                 webContext.response.redirect(viewContext.htmlViewContext.siteMap.loginPage);
//             }
//         } catch (e) {
//             web_respond_with_500(e, webContext);
//         }
//     };
//
//     return CwbWeb.RequestHandlerBuilder.get(`${viewContext.htmlViewContext.siteMap.activateAccountPage}/:email`, handler);
// }
//
//
// export function doLogout<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//     const handler: CwbWeb.RequestHandlerFunc<C> = (webContext: CwbWeb.RequestContext<C>) => {
//         // destroy session
//         webContext.request.session.destroy();
//
//         // redirect to login page
//         webContext.response.sendJson(new CwbAction.ResponseBuilder().redirect(viewContext.htmlViewContext.siteMap.loginPage).json());
//     };
//     return CwbWeb.RequestHandlerBuilder.post(viewContext.htmlViewContext.siteMap.doLogout, handler);
// }
//
// export function userHomePage<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//
//     const handler: CwbWeb.RequestHandlerAsync<C> = async (webContext: CwbWeb.RequestContext<C>) => {
//
//         try {
//             const {session} = CwbWeb.unpackWebContext(webContext);
//             if (session) {
//                 webContext.response.view(
//                     webContext.context.viewFactory.modelAndView(
//                         viewContext.htmlViewContext.siteMap.userHomePageView,
//                         viewContext.htmlViewContext.createModelBuilder().build()
//                     )
//                 );
//             } else {
//                 webContext.response.sendJson(new CwbAction.ResponseBuilder().redirect(viewContext.htmlViewContext.siteMap.loginPage).json());
//             }
//         } catch (e) {
//             web_respond_with_500(e, webContext);
//         }
//     };
//
//     return CwbWeb.RequestHandlerBuilder.get(viewContext.htmlViewContext.siteMap.userHomePage, handler);
// }
//
// export function siteHomePage<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//     return CwbWeb.RequestHandlerBuilder.view(
//         viewContext.htmlViewContext.siteMap.siteHomePage,
//         viewContext.htmlViewContext.siteMap.siteHomePageView,
//         viewContext.htmlViewContext.createModelBuilder()
//     );
// }
//
//
// export function userSessionHeartbeatHandler<C extends AuthenticationRequestPathContext>(viewContext: AuthenticationRequestPathViewContext):  CwbWeb.IRequestHandler<C> {
//     return new CwbWeb.RequestHandlerBuilder<C>(viewContext.htmlViewContext.siteMap.sessionCheck,
//         (webContext) => {
//             const {session} = CwbWeb.unpackWebContext(webContext);
//             if (!session) {
//                 webContext.response.sendStatus(403, "Session Expired");
//             } else {
//                 webContext.response.sendText("");
//             }
//         })
//         .build();
// }
//
// export default class AuthenticationRequestHandlerFactory<C extends AuthenticationRequestPathContext> {
//     private htmlViewContext: AuthenticationControllerContext;
//     private messageSource: AuthenticationMessageSource;
//
//     constructor(htmlViewContext: AuthenticationControllerContext) {
//         this.htmlViewContext = htmlViewContext;
//         this.messageSource = new AuthenticationMessageSource();
//     }
//
//     public createPaths(onAuthenticated: OnAuthenticatedCallback = () => {}):  CwbWeb.IRequestHandler<C>[] {
//         const {htmlViewContext, messageSource} = this;
//         const reqSiteMap: AuthenticationRequestPathViewContext = {
//             htmlViewContext,
//             onAuthenticated,
//             messageSource,
//         };
//         return [
//             loginPage<C>(reqSiteMap),
//             doLogin<C>(reqSiteMap),
//
//             registerPage<C>(reqSiteMap),
//             doRegistration<C>(reqSiteMap),
//
//             doLogout<C>(reqSiteMap),
//
//             activateAccountPage<C>(reqSiteMap),
//             accountActivationSuccessPage<C>(reqSiteMap),
//             doAccountActivation<C>(reqSiteMap),
//             doResendActivationLink<C>(reqSiteMap),
//
//             siteHomePage<C>(reqSiteMap),
//             userHomePage<C>(reqSiteMap),
//
//             userSessionHeartbeatHandler<C>(reqSiteMap)
//         ]
//     }
// }
