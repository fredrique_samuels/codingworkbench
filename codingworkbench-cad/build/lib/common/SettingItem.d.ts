export default interface SettingItem {
    displayName: string;
    property: string;
    type: string;
    meta: any;
    category?: string;
}
