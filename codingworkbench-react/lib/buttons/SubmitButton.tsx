import * as React from "react";
import Button from "@material-ui/core/Button";
import {IButtonProps} from "./IButtonProps";


export function SubmitButton(props: IButtonProps) {
    return <Button
        key={props.label}
        disabled={props.disabled}
        onClick={props.onClick}
        color="default"
        variant={"outlined"}
    >
        {props.label}
    </Button>;
}
