import {DbColumn} from "./DbColumn";
import {IAuditAware} from "./IAuditAware";
import {ID} from "./IUnique";
import ArchiveAware from "./ArchiveAware";

export class AuditAware {

    private o: IAuditAware;

    constructor(o: Partial<IAuditAware>, userId?: ID) {
        this.o = {
            date_last_updated: new Date(),
            user_last_updated: userId || null,
            date_of_entry: new Date(),
            user_of_entry: userId || null,
            ...o
        }
    }

    public flatten(): IAuditAware {
        return { ...this.o };
    }

    public setLastUpdated(userId?: ID): this {
        this.o = {
            ... this.o,
            date_last_updated: new Date(),
            user_last_updated: userId || null
        };
        return this;
    }

}

const _audit_columns = [
    new DbColumn("date_of_entry", "doe"),
    new DbColumn("user_of_entry", "uoe"),
    new DbColumn("date_last_updated", "dlu"),
    new DbColumn("user_last_updated", "ulu")
];

export function audit_columns(): DbColumn[] {
    return [... _audit_columns];
}

export default AuditAware;
