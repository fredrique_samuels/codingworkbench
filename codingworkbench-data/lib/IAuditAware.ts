import {ILastUpdateAware} from "./ILastUpdateAware";
import {ID} from "./IUnique";

/**
 * Fields that contain audit data.
 */
export interface IAuditAware extends ILastUpdateAware {

    /* Date that the record was created  */
    date_of_entry: Date;

    /* user that created the record */
    user_of_entry: ID;
}

export default IAuditAware;
