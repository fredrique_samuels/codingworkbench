import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="cwb_control", # Replace with your own username
    version="0.0.4",
    author="Fredrique Samuels",
    author_email="fredriquesamuels@gmail.com",
    description="A package to help code control systems",
    long_description=long_description,
    long_description_content_type="text/markdown",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 2",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.7',
)


