
import DbTable from "@codingworkbench/data/DbTable";
import DbColumn from "@codingworkbench/data/DbColumn";
import IUserSession from "@codingworkbench/data/IUserSession";
import CrudDataAccessor from "@codingworkbench/data/CrudDataAccessor";
import CwbSearch from "@codingworkbench/search";
import {ID} from "@codingworkbench/data/IUnique";

import VersionedObject from "./VersionedObject";
import {VersionedDbTable} from "./VersionedDbTable";
import IDataStructure from "./IDataStructure";
import {IPropertyOptions} from "./IDataStructure";
import DataStructureProperty from "./DataStructureProperty";


export const DataStructureTable: DbTable = new VersionedDbTable (
    "CwbDataStructureTable",
    [ new DbColumn("name") ]);


export const DataStructurePropertyTable: DbTable = new VersionedDbTable (
    "CwbDataStructurePropertyTable",
    [ new DbColumn("name") ]);


export class DataStructure extends VersionedObject<IDataStructure> {

    constructor(dao: CrudDataAccessor) {
        super(dao, DataStructureTable);
    }

    public setName(n: string)
        : this
    {
        if (this.persisted) {
            throw new Error("Cannot change name name of a type that has been saved")
        }
        this._params.name = n;
        return this;
    }

    public addProperty(property_name: string, type: string, options: IPropertyOptions)
        : this
    {
        if (!this._params.properties[property_name]) {
            this._params.properties[property_name] = {
                ...new DataStructureProperty(this.dao).params,
                name: property_name,
                type,
                options
            };
        } else {
            // support property updates
        }
        return this;
    }

    async save(session?: IUserSession): Promise<this> {
        await this.assert_unique();
        const id = await super.save(session);

        for (const k in Object.keys(this._params.properties)) {
            const p = this._params.properties[k];
            p.parent = p;
            const structureProperty = new DataStructureProperty(this.dao);
        }
        return id;
    }

    public createParams(): IDataStructure {
        return {
            ...this.createVersionObjectParams(),
            name: "",
            properties: {}
        };
    }

    public update(params: IDataStructure): this {
        super.update(params);
        this._params.name = params.name;
        return this;
    }

    private async assert_unique() {
        if (!this._params.name) {
            throw new Error("DataType name is required.")
        }
        const searchBuilder = new CwbSearch.SearchBuilder();
        searchBuilder.filters.addFilter("name", this._params.name);

        const [found] = await this.dao.searchObjects(DataStructureTable, searchBuilder.build());
        if (found) {
            throw Error(`Unable to create the Type '${this._params.name}'. The type already exists' `)
        }
    }
}

export class DataStructureDao {
    private dao: CrudDataAccessor;

    constructor(dao: CrudDataAccessor) {
        this.dao = dao;
    }

    public async getByIds(ids: ID[]): Promise<IDataStructure[]> {
        const searchBuilder = new CwbSearch.SearchBuilder();
        searchBuilder.filters.addFilter("id", ids);
        return (await this.dao.searchObjects<IDataStructure>(DataStructureTable, searchBuilder.build()))
            .map(ds => {
                ds.properties = {};
                return ds;
            })
        ;
    }
}
