

import {ISecurityLinks} from "../../api/links";

import GotoSignIn from "./GotoSignIn";
import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {ISecurityHandlerContext} from "../../api/context";

import DoSignIn from "./DoSignIn";
import GotoSignUp from "./GotoSignUp";
import DoSignUp from "./DoSignUp";
import GotoAccountActivationRequired from "./GotoAccountActivationRequired";
import DoAccountActivation from "./DoAccountActivation";
import DoResendLink from "./DoResendLink";
import GotoUserHome from "./GotoUserHome";
import DoSignOut from "./DoSignOut";
import DoSessionCheck from "./DoSessionCheck";

namespace SecurityServiceHandlers {

    export function create<C extends ISecurityHandlerContext>
    (
        links: ISecurityLinks
    ): Array<IRequestHandler<C>> {

        const factoryContext = {links,};
        return [
            DoSignIn(factoryContext),
            DoSignUp(factoryContext),
            DoSignOut(factoryContext),
            DoAccountActivation(factoryContext),
            DoResendLink(factoryContext),
            DoSessionCheck(factoryContext),

            GotoSignIn(factoryContext),
            GotoSignUp(factoryContext),
            GotoUserHome(factoryContext),
            GotoAccountActivationRequired(factoryContext),
        ];
    }

}

export default SecurityServiceHandlers;
