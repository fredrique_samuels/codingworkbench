export default class STLUtils {
    static scaleModel(stlFileContent: string, scaleFactor?: number): string;
}
