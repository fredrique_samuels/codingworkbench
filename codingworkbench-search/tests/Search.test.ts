import {deepStrictEqual, ok} from 'assert';

import CwbSearch from "../search";

function createTestPaginated() {
    const searchBuilder = new CwbSearch.SearchBuilder();
    searchBuilder.pagination
        .setPageSize(1)
        .setOffset(2)
        .setSortField("field")
        .setSortOrder('DESC');

    const searchParams = searchBuilder.build();
    return searchParams;
}

function assertPaginatedSearch(iPagination: CwbSearch.IPagination) {
    deepStrictEqual(iPagination.pageSize, 1);
    deepStrictEqual(iPagination.offset, 2);
    deepStrictEqual(iPagination.sortField, 'field');
    deepStrictEqual(iPagination.sortOrder, 'DESC');
}

describe('PaginationParamsBuilder', function() {

    it("#init()", () => {
        const iPagination = new CwbSearch.SearchBuilder().build().pagination;

        deepStrictEqual(iPagination.pageSize, 25);
        deepStrictEqual(iPagination.offset, 0);
        deepStrictEqual(iPagination.sortField, null);
        deepStrictEqual(iPagination.sortOrder, 'ASC');
    });

    it("#setters()", () => {
        const paginationParams = createTestPaginated().pagination;
        assertPaginatedSearch(paginationParams);
    });


    it("#with arg()", () => {
        const paginationParams = new CwbSearch.SearchBuilder(createTestPaginated()).build().pagination;
        assertPaginatedSearch(paginationParams);
    });

});

function createTestFilters() {
    const searchBuilder = new CwbSearch.SearchBuilder();
    searchBuilder.filters
        .addFilter("field0", 5)
        .addFilter("field1", [10], "in");
    return searchBuilder.build();
}

function assertTestFilters(dict: CwbSearch.Filters) {
    deepStrictEqual(dict, {
        field0: {value: 5, operation: "eq"},
        field1: {value: [10], operation: "in"},
    });
}

describe('FiltersBuilder', function() {

    it("#init()", () => {
        const dict = new CwbSearch.SearchBuilder().build();
        deepStrictEqual(dict.filters, {});
    });

    it("#setters()", () => {
        const dict = createTestFilters();
        assertTestFilters(dict.filters);
    });

    it("#with arg()", () => {
        const dict = new CwbSearch.SearchBuilder(createTestFilters()).build();
        assertTestFilters(dict.filters);
    });

});


const data_functions: CwbSearch.PaginatedDataFunctions<number> = {
    dataSupplier: async (s) => {
        const pageParams = s.pagination as CwbSearch.IPagination;
        const resultsLeft = 15 - pageParams.offset;

        const make_results = (c: number, offset: number) => {
            const r: number[] = [];
            for (let idx = 0; idx < c; idx ++) {
                r.push(idx + offset);
            }
            return r;
        };

        return pageParams.pageSize <= resultsLeft
            ? make_results(pageParams.pageSize, pageParams.offset)
            : make_results(resultsLeft, pageParams.offset)
    },
    totalSupplier: async () => 15
};


describe('do_paged_search', function() {

    it("#from empty search()", (done) => {

        async function _e() {
            const searchResults = await CwbSearch.do_paged_search(new CwbSearch.SearchBuilder().build(), data_functions);

            deepStrictEqual(searchResults.currentPage, 1);
            deepStrictEqual(searchResults.pages, 1);
            deepStrictEqual(searchResults.results.length, 15);
            deepStrictEqual(searchResults.next, undefined);
            deepStrictEqual(searchResults.prev, undefined);
            deepStrictEqual((searchResults.refresh as CwbSearch.Search).pagination.totalRecords, 15);
        }

        _e().then(() => done())
            .catch(done)
    });

    it("#from paged", (done) => {
        async function _e() {
            const searchBuilder = new CwbSearch.SearchBuilder();
            searchBuilder.pagination.setPageSize(5);

            let searchResults = await CwbSearch.do_paged_search(searchBuilder.build(), data_functions);
            assert_page(searchResults, 1, 3, [0, 1, 2, 3, 4], 15);

            searchResults = await CwbSearch.do_paged_search(searchResults.next as CwbSearch.Search, data_functions);
            assert_page(searchResults, 2, 3, [5, 6, 7, 8, 9], 15);

            searchResults = await CwbSearch.do_paged_search(searchResults.next as CwbSearch.Search, data_functions);
            assert_page(searchResults, 3, 3, [10, 11, 12, 13, 14], 15);

            searchResults = await CwbSearch.do_paged_search(searchResults.prev as CwbSearch.Search, data_functions);
            assert_page(searchResults, 2, 3, [5, 6, 7, 8, 9], 15);

            searchResults = await CwbSearch.do_paged_search(searchResults.prev as CwbSearch.Search, data_functions);
            assert_page(searchResults, 1, 3, [0, 1, 2, 3, 4], 15);
        }

        _e().then(() => done())
            .catch(done)
    });

    function assert_page(searchResults: CwbSearch.IResults<number>, currentPage: number, totalPages: number, results: number[], totalRecords: number) {
        deepStrictEqual(searchResults.currentPage, currentPage);
        deepStrictEqual(searchResults.pages, totalPages);
        deepStrictEqual(searchResults.results.length, results.length);
        deepStrictEqual(searchResults.results, results);
        deepStrictEqual(((searchResults.refresh as CwbSearch.Search).pagination as CwbSearch.IPagination).totalRecords, totalRecords);

        if (currentPage > 1) {
            ok(searchResults.prev);
        } else {
            ok(!searchResults.prev);
        }

        if (currentPage === totalPages) {
            ok(!searchResults.next);
        } else {
            ok(searchResults.next);
        }
    }

});
