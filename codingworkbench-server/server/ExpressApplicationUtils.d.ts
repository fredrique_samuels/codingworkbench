import * as express from "express";
import * as common from "./common";
import IRequestHandler from "./IRequestHandler";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
/**
 * Simple next function.
 */
export declare type ExpressApplicationNextFunction = express.NextFunction;
/**
 * Middleware function called before handlers passed to ExpressApplication#addHandlers.
 * Useful for filtering expires sessions or access control requests or
 */
export declare type ExpressApplicationMiddleware = (req: common.Request, res: common.Response, next: ExpressApplicationNextFunction) => any;
export interface IExpressApplication<ContextType> {
    getApp(): express.Express;
    addMiddleware(mw: ExpressApplicationMiddleware): this;
    addHandlers(handlers: IRequestHandler<ContextType>[]): this;
    serveStatic(path: string, resolve: string): this;
}
/**
 * Options needed to run express.
 */
export interface ExpressApplicationOptions<Context = any> {
    /**
     * Path to the a connection.properties file.
     */
    connectionPropertiesFile?: string;
    /**
     * The port the server should listen on.
     */
    port?: number;
    /**
     * File upload size limit in bytes. Defaults to 50MB.
     */
    fileUploadLimit?: number;
    /**
     * A function called just before  app.listen() is called.
     * Useful for added custom setup on the underlying express.Express object.
     */
    setupFunc?: (app: express.Express) => void;
    /**
     * Function to be called when the server starts up.
     */
    onStartFunc?: (applicationContext: any, expressApplication: IExpressApplication<Context>) => Promise<void>;
    /**
     * Indicate if session middleware settings.
     */
    enabledSessions?: boolean;
    /**
     * Flag to switch on in memeory data access for testing
     */
    useMemoryStorage?: boolean;
}
/**
 * Constant for one megabyte.
 */
export declare const ONE_MEGA_BYTE: number;
/**
 * Bind a all the RequestPath handlers to an  express.Express.
 *
 * @param app The target application
 * @param handlers The list of handlers to bind
 * @param context The web request context.
 */
export declare function bind_handlers_to_app<C = {}>(app: express.Express, handlers: IRequestHandler<C>[], context: C): void;
export declare function setup_express(options: ExpressApplicationOptions, app: express.Express): void;
export declare function configure_redis(connectionProperties: IPropertiesReader | null, options: ExpressApplicationOptions, app: express.Express): void;
export declare function bind_user_defined_middleware(middlewares: ExpressApplicationMiddleware[], app: express.Express): void;
