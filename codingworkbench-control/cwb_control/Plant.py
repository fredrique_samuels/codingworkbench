from cwb_control.DataPoint import DataPoint


class Plant:

    def __init__(self, id, data_store):
        self.data_store = data_store
        self.id = id

    def create_data_point(self, key, value=None):
        return DataPoint("{}.{}".format(self.id, key), self.data_store, value)

