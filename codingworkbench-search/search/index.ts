import * as search from "./Search"

namespace CwbSearch {
    export import IFilter = search.SearchFilter;
    export import Filters = search.SearchFilterDict;

    export import Search = search.SearchParams;
    export import Pagination = search.IPagination;
    export import IResults = search.SearchResults;
    export import SearchBuilder = search.SearchBuilder;
    export import IPagination = search.IPagination;
    export import IFiltersBuilder = search.ISearchFiltersBuilder;
    export import IPaginationBuilder = search.ISearchPaginationBuilder;
    export import ResultBuilder = search.PageDataSupplier;
    export import PageTotalSupplier = search.PageTotalSupplier;
    export import PaginatedDataFunctions = search.PaginatedDataFunctions;

    export import results_from_array = search.search_results_from_array;
    export import mapResults = search.mapResults;

    export import PageDataSupplier = search.SearchResultBuilder;

    export import SortOrder = search.SortOrder;
    export import Operation = search.Operation;

    export import INF_PAGE_SIZE = search.INF_PAGE_SIZE;

    export import do_paged_search = search.do_paged_search;
}

export default CwbSearch;
