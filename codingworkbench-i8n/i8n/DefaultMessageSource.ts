import IMessageSource, {MessageKeys} from "./IMessageSource";

export class DefaultMessageSource implements IMessageSource {
    private readonly keys: MessageKeys;
    private readonly messageSource: IMessageSource | undefined;

    constructor(keys: MessageKeys, messageSource?: IMessageSource) {
        this.keys = keys;
        this.messageSource = messageSource;
    }

    public async get(code: string, ...args: any): Promise<string> {
        const msg = this.keys[code];
        if (msg) {
            return msg;
        }

        if (this.messageSource) {
            return this.messageSource.get(code, ...args);
        }

        return code;
    }

}
