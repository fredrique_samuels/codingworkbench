import {deepStrictEqual, ok} from "assert";

namespace CwbTestCase {

    export type DestroyCallback = (unit: any) => Promise<void>;

    export interface IDestroyable {
        destroyCallbacks: DestroyCallback[];
    }

    export type Functions = {
        before: () => Promise<any>
        after: (g: any) => Promise<void>
        beforeEach: (g?: any) => Promise<any>
        afterEach: (unit: any, g?: any) => Promise<void>
    }

    export namespace Functions {
        export function empty(): Functions {
            return {
                beforeEach: async () => {},
                afterEach: async unit => {},
                before: async () => {},
                after: async unit => {}
            }
        }
    }

    export function createDestroyable(destroyFunctions: DestroyCallback[], pick: Partial<IDestroyable>) {
        return [
            ...destroyFunctions,
            ...pick.destroyCallbacks || [],
        ];
    }

    export async function destroy(destroyable: IDestroyable) {
        if (!destroyable) { return; }
        for (const dc of destroyable.destroyCallbacks) {
            try {
                await dc(destroyable);
            } catch (e) {
                console.error(e);
            }
        }
    }

    export async function generateEmail(): Promise<string> {
        return new Promise<string>((resolve) => {
            setTimeout(() => resolve(`test_${new Date().getTime()}@g.com`), 10)
        })
    }

    export async function generatePassword(): Promise<string> {
        return new Promise<string>((resolve) => {
            setTimeout(() => resolve(`secret_${new Date().getTime()}_${PASSWORD_MEDIUM}`), 10)
        })
    }

    export async function generateUUID(): Promise<string> {
        return new Promise<string>((resolve) => {
            setTimeout(() => resolve(`id_${new Date().getTime()}`), 10)
        })
    }

    export function generateNumericID(): Promise<number> {
        return new Promise<number>((resolve) => {
            setTimeout(() => resolve(new Date().getTime()), 10);
        })
    }

    export async function assertThrows(func: () => Promise<any>, messages?: assertThrows.Messages) {
        let error: Error | null = null;

        try {
            await func();
        } catch (e) {
            error = e;
        }

        ok(error, ( messages ? messages.noExceptionMessage : "") || "Expected an exception");
        if(messages && messages.errorMessage && error) {
            deepStrictEqual(error.message, messages.errorMessage)
        }
    }

    export namespace assertThrows {
        export type Messages = {
            errorMessage?: string;
            noExceptionMessage?: string;
        }
    }

    export const MALFORMED_EMAIL: string = "1232dd";
    export const PASSWORD_WEAK: string = "#$6ja";
    export const PASSWORD_MEDIUM: string = "#1234dfsasd";

}

export default CwbTestCase;
