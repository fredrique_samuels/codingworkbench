
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {RequestHandlerFunc} from "@codingworkbench/server/common";
import {ISecurityHandlerContext, ISecurityHandlerFactoryContext} from "../../api/context";
import {create_security_model} from "../../api/response";


export default function GotoAccountActivationRequired<C extends ISecurityHandlerContext>(factoryContext: ISecurityHandlerFactoryContext): IRequestHandler<C> {
    const url = factoryContext.links.signUpPage;
    const model = create_security_model(factoryContext.links).build();
    const handler: RequestHandlerFunc<C> = reqContext => {
        const view = reqContext.context.viewFactory.modelAndView(factoryContext.links.signUpPageView, model);
        return reqContext.response.view(view);
    };
    return new RequestHandlerBuilder(url, handler).build();
}
