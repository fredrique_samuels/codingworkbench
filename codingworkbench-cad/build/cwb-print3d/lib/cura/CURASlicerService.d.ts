import SettingItem from "cwb-core/settings/SettingItem";
import ISettingOption from "cwb-core/settings/SettingOption";
import ISliceResults from "../ISliceResults";
import ISlicerService from "../service/ISlicerService";
import { CURAPaths } from "./CURAPaths";
export default class CURASlicerService implements ISlicerService {
    private slicer;
    constructor(enginePaths: CURAPaths);
    getDefinitions(): Promise<ISettingOption[]>;
    getOptions(): Promise<SettingItem[]>;
    slice(stlString: string, definition: string, options: SettingItem[]): Promise<ISliceResults>;
}
