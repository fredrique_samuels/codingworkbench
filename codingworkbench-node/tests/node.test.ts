import {deepStrictEqual, fail, ok} from "assert";

import CwbNode from "../node";

import {EMPLOYEE1, employee1, employeeStore, IAddress, IEmployee} from "./domain/employee";
import {createEmployeeRoot} from "./domain/node/employee_element";
import {ADDRESS_TAG} from "./domain/node/address_element";
import {createEmployeeStoreNode} from "./domain/node/employee_store_node";
import {INode} from "../node/api";


describe('CwbNode.Element', function() {


    it ("root#constructor WithValue", () => {
        const root = createEmployeeRoot(employee1);
        deepStrictEqual(root.value, employee1);
    });

    it ("root#getAttribute Returns Empty Node", () => {
        const root = createEmployeeRoot(employee1);
        deepStrictEqual(root.get("doesNotExists").type, CwbNode.NodeType.EMPTY);
    });

    it ("root#getAttrbute", () => {
        const root = createEmployeeRoot(employee1);

        deepStrictEqual(root.get("doesNotExists").type, CwbNode.NodeType.EMPTY);
        ok(root.get("name").name);
        assertEmployeeName(root);
        assertEmployeeAddress(root);
    });

    it ("root#writeProperty", () => {
        const root = createEmployeeRoot(employee1);
        root.get("name").asAttribute((iAttribute) => {
            const newValue = iAttribute.value + "-changed";
            iAttribute.value = newValue;

            assertEmployeeName(root, newValue);
        });
    });

    it ("root#as wrong type", () => {
        const root = createEmployeeRoot(employee1);

        const failTest = () => {
            ok(false, `name should be an attribute`)
        };

        const assertTypeError = (n: CwbNode.INode) => {
            deepStrictEqual(n.type, CwbNode.NodeType.ATTRIBUTE);
            deepStrictEqual(n.name, "name");
        };

        root.get("name").asElement(failTest, assertTypeError);
    });

    it ("root#element group", () => {
        const root = createEmployeeStoreNode(employeeStore, failOnNodeError);

        const processEmployee1: CwbNode.ElementConsumer = iElement => {
            assertEmployeeName(iElement);
            assertEmployeeAddress(iElement);
        };

        const processEmployees: CwbNode.ElementConsumer = iElement => {
            iElement.get(EMPLOYEE1).asElement(processEmployee1);
        };

        root.get("employees").asElement(processEmployees);
    });

    it ("setDefaultErrorFunction", (done) => {
        const errorFunc = () => {
            done();
        };

        async function _e() {
            new CwbNode.NodeTreeBuilder<any>("root", {})
                .setErrorFunction(errorFunc)
                .build()
                .asAttribute(() => {});
        }

        _e().then(() => {})
            .catch(done);

    });

    it ("select$tier1", () => {
        const root = createEmployeeRoot(employee1, failOnNodeError);
        assertEmployeeNameSearch(root, "name");
        assertEmployeeNameSearch(root, "*me");
        assertEmployeeNameSearch(root, "na*e");
        assertEmployeeNameSearch(root, "na*");
        assertEmployeeNameSearch(root, "*a*");

        assertAddressSearch(root, "address");
        assertAddressSearch(root, "addr*");
        assertAddressSearch(root, "*ress");
        assertAddressSearch(root, "ad*ss");

        assertStreetSearch(root.select("address.street").ifEmpty(fail).values);
        assertStreetSearch(root.select("address*.str*").ifEmpty(fail).values);
        assertStreetSearch(root.select("addr*.stre*").ifEmpty(fail).values);
        assertStreetSearch(root.select("address.*").ifEmpty(fail).values);
    });

    it ("select$employee store select", () => {
        const root = createEmployeeStoreNode(employeeStore, failOnNodeError);
        assertStreetSearch(root.select("employees.employee1.address.street").ifEmpty(fail).values);
    })

});

const failOnNodeError = (node: CwbNode.INode) => {
    fail(CwbNode.getErrorMessage(node));
};


function assertAddressSearch(root: CwbNode.IRootElement<IEmployee>, selector: string) {
    const [addressEl] = root.select(selector).ifEmpty(fail).values;
    const assertElement: CwbNode.ElementConsumer = iElement => iElement.get("street").asAttribute(iAttribute => deepStrictEqual(employee1.address.street, iAttribute.value), failOnNodeError);
    addressEl.asElement(assertElement, failOnNodeError)
}

function assertStreetSearch(nodes: INode[]) {
    const [streetAttr] = nodes;
    const assertAttribute: CwbNode.AttributeConsumer = iAttribute => deepStrictEqual(employee1.address.street, iAttribute.value);
    streetAttr.asAttribute(assertAttribute, failOnNodeError)
}

function assertEmployeeNameSearch(root: CwbNode.IRootElement<IEmployee>, selector: string) {
    const [nameAttr] = root.select(selector).ifEmpty(fail).values;

    const assertAttribute: CwbNode.AttributeConsumer = iAttribute => deepStrictEqual(iAttribute.value, employee1.name);
    nameAttr.asAttribute(assertAttribute, failOnNodeError);
}


function assertEmployeeName(root:  CwbNode.IElement<IEmployee>, name?: string) {
    const iNode = root.get("name");
    deepStrictEqual(iNode.name, "name");
    iNode.asAttribute<string>(a => deepStrictEqual(a.value, name || employee1.name), () => ok(false, `attribute 'name' does not exists`) )

}

function assertAddressStreet(iElement: CwbNode.IElement<IAddress>) {
    const iNode = iElement.get("street");
    deepStrictEqual(iNode.name, "street");

    iNode.asAttribute<string>((iAttribute) => {
            deepStrictEqual(iAttribute.value, employee1.address.street);
        },
        () => ok(false, `element 'address' does not exists`)
    );
}

function assertEmployeeAddress(root: CwbNode.IElement<IEmployee>) {
    const iNode = root.get("address");
    deepStrictEqual(iNode.name, "address");

    iNode.asElement<IAddress>((iElement) => {
            deepStrictEqual(iElement.value, employee1.address);
            deepStrictEqual(iElement.tag, ADDRESS_TAG);
            assertAddressStreet(iElement)
        },
        () => ok(false, `element 'address' does not exists`)
    );
}
