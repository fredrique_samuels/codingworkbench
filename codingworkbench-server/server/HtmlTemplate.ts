
/**
 * Representation of all variable parts of a html file.
 *
 * Currently supports title, head tags and body.
 * The body is split info the pre-body and body.
 *
 */
export interface HtmlTemplate {
    /**
     * The title of the html file.
     *
     * Wil be placed in a <title/> tag.
     */
    title: string;

    /**
     * Array of tags to place in the head of the html after the title.
     */
    head: string[];

    /**
     * Array of tags to place before the main body content.
     */
    preBody: string[];

    /**
     * Array of tags as the main body.
     */
    body: string[];
}


/**
 * Contruct an html string using the provided template values.
 *
 * @param template The template to construct html from.
 */
export function build_html(template: HtmlTemplate): string {
    return `
            <html lang="en">
            <head>
            <title>${template.title}</title>
            ${template.head.join("\n")}
            </head>
            <body>
              ${template.preBody.join("\n")}
              ${template.body.join("\n")}
            </body>
            </html>
        `
}


