import ISlicerService from "./ISlicerService";
import IOption from "../common/IOption";
import ISliceJobOutput, { slice_job_output_merge, slice_job_output_create } from "./ISliceJobOutput";
import ISlicer from "./ISlicer";
import ISliceJobInput from "./ISliceJobInput";


export class SlicerServiceImpl implements ISlicerService {

    private slicer: ISlicer;

    constructor(slicer: ISlicer) {
        this.slicer = slicer;
    }

    async getProfileOptions(): Promise<IOption[]> {
        return this.slicer.getProfileOptions();
    }
    
    async getPrintOptions(): Promise<IOption[]> {
        return this.slicer.getProfileOptions();
    }

    async sliceString(id: string, stlString: string, input: ISliceJobInput): Promise<ISliceJobOutput> {
        return this.slicer.sliceString(id, stlString, input);
    }

    async slice(path: string, input: ISliceJobInput): Promise<ISliceJobOutput> {
        return this.slicer.sliceFile(path, input);
    }

    async sliceBatch(paths: string[], input: ISliceJobInput): Promise<ISliceJobOutput> {
        let o: ISliceJobOutput = slice_job_output_create();
        for (const path of paths) {
            const r = await this.slice(path, input);
            o = slice_job_output_merge(o, r);
        }
        return o;
    }

}