export default class STLVertexDataScaleVisitor {
    data: string;
    scale: number;
    constructor(scale?: number);
    visit(line: string, lineNumber: number): void;
    readonly content: string;
}
