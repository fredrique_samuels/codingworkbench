
import {ModelBuilder} from "@codingworkbench/server/HtmlModel";
import {RequestContext} from "@codingworkbench/server/common";
import CwbAction from "@codingworkbench/action";
import CwbI8N from "@codingworkbench/i8n";

import {SecureLinkData, SECURITY_LINKS_MODEL} from "./links";
import {ISecurityHandlerContext} from "./context";
import CwbLog from "@codingworkbench/log";

export function create_security_model<L extends SecureLinkData>(bc: L) {
    return new ModelBuilder().put(SECURITY_LINKS_MODEL, bc);
}

export function web_respond_redirect<C extends ISecurityHandlerContext>(requestContext: RequestContext<C>, link: string) {
    requestContext.response.sendJson(new CwbAction.ResponseBuilder().redirect(link).json());
}

export async function web_respond_with_error<C extends ISecurityHandlerContext>(
    actionResponse: CwbAction.IResponse,
    webContext: RequestContext<C>,
    messageSource: CwbI8N.IMessageSource
) {
    const responseError = new CwbAction.ResponseBuilder().error(actionResponse.message).build();
    const i8nResponse = await CwbI8N.actionResponseMessage(responseError, messageSource);
    webContext.response.sendJson(JSON.stringify(i8nResponse));
}

export function web_respond_with_ok(webContext: RequestContext) {
    webContext.response.sendJson(new CwbAction.ResponseBuilder().json());
}

export function web_respond_with_500(e: Error, webContext: RequestContext) {
    CwbLog.error(e);
    webContext.response.sendStatus(500, "Internal Server Error");
}
