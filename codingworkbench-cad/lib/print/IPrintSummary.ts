import { process_stdio_merge } from "lib/process/IProcessStdio";


export default interface IPrintSummary {
    readonly timeInSeconds?: number;
    readonly timeFormatted?: string;
    readonly filamentUsedInMeters?: number;
}

export function print_summary_create(o: IPrintSummary = {}): IPrintSummary  {
    const d: IPrintSummary = {
        timeInSeconds: 0,
        filamentUsedInMeters: 0,
        timeFormatted: sec_to_time_string(0)
    }
    return print_summary_merge(d, o);
}

export function print_summary_merge(p1: IPrintSummary, p2: IPrintSummary): IPrintSummary {
    const timeInSeconds: number = sum_optional_numbers(p1.timeInSeconds, p2.timeInSeconds);
    return {
        filamentUsedInMeters: sum_optional_numbers(p1.filamentUsedInMeters, p2.filamentUsedInMeters),
        timeInSeconds: timeInSeconds,
        timeFormatted: sec_to_time_string(timeInSeconds)
    } 
}

function sum_optional_numbers(n1?: number, n2?: number): number {
    return ( n1 === undefined ? 0 : n1 ) + ( n2 === undefined ? 0 : n2 )
}

function sec_to_time_string(sec_num: number) {
    let hours: number   = Math.floor(sec_num / 3600);
    let minutes: number = Math.floor((sec_num - (hours * 3600)) / 60);
    let seconds: number = sec_num - (hours * 3600) - (minutes * 60);

    return `${hours}h ${minutes}m ${seconds}s`
}