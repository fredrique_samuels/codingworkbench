import {deepStrictEqual} from "assert";
import {HtmlBuilder} from "../server/HtmlBuilder";

describe("HtmlBuilder", () => {

    it ("build", () => {
        const expected = `
            <html lang="en">
            <head>
            <title></title>
            
            </head>
            <body>
              <script>var __CwbPageDataModel = {}</script>
              
            </body>
            </html>
        `
        deepStrictEqual(new HtmlBuilder().build(), expected);
    });

});
