import Optional from "./Optional";

export type EntrySet<V> = {[s: string]: V};

export interface IMapLike<V> {
    readonly entries: EntrySet<V>;
    getOptional(key: string, params?: any): Optional<V>;
    get(key: string, params?: any): V | undefined;
    put(key: string, value: V): this;
    keys(): string[];
    remove(key: string): this;
    contains(key: string): boolean;
    addAll(other: IMapLike<V>): this;
}

export default IMapLike;
