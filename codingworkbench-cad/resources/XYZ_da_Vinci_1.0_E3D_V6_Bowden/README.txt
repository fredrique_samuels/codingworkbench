                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:1510425
XYZ da Vinci 1.0 E3D V6 Bowden complete conversion (no drilling, no cutting) (updated 3/8/17) by AndrewT111 is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

UPDATE!
============================

**3/8/2017**: I added an updated version of the hotend_base file, called "hotend_base_v2." After struggling with some part corner lifting (more than I would have thought, even with ABS) I did some flow visualization and determined that a slight amount of air was blowing across the print bed and the printed part. This air was circulating around the underside of the carriage below the rear side of the heat sink fins. The new hotend_base file shrouds the rear of the heat sink, leaving a duct for the air to escape through the original stepper motor bore hole and onto the top side of the carriage, away from the print bed. While this is an improvement in restricting airflow, it didn't make things air tight (to my eye anyway), so I wrapped the underside (between the fan duct/base of the heat sink and the hot end heat block) in a little aluminum foil, secured with some electrical tape (far from the hot parts), to make sure no air was escaping and blowing onto the print bed. I've run the printer for about 15 hours so far on three medium-large solid infill parts and it seems to have reduced some of my curling problems, even for these challenging parts. 

hotend_base_v2 is a drop-in swap for the original design, so if you're already running this Bowden conversion you should be able to print the new part in 20-30 minutes and swap it out pretty easily.

I will probably redesign this entire hot end assembly at some point and post it as a new thing. It has always bothered me that the fan duct isn't securely attached to the hotend base before installation, it makes removing and reinstalling this thing pretty fiddly, and I usually feel like I need three hands to do a good job. Aside from that I intend to design a layer cooling fan mount and duct that is easily removable with thumbscrews (so you can quickly remove that extra weight and bulk when not printing PLA). I'm hoping to be able to make one that doesn't have the belt passing through it (like another design does on Thingiverse), as I think that makes installation and removal a huge PITA. I'll also put more consideration into how I vent the hot end heat sink cooling airflow, probably leaving a larger exit hole through the original stepper motor bore hole and forcing it to blow straight up.


Original summary
==========================

This is an all original design for converting an XYZ da Vinci 1.0 to use an E3D V6 1.75mm hot end and a Bowden style extruder. It does not require any drilling, bending, or other permanent modification, except you may have to draw a 12V line from the power supply to drive the stock always-on E3D V6 fan that blows over the heat sink. You should be able to install this mod without removing the carriage, cutting up any plastic panels, or anything similarly inconvenient. Note that this mod will require you to run another firmware like Repetier, as it will require you to reverse the extrusion axis direction (easy to change in the Repetier configuration header file). The hardware side of the mod is completely reversible, although I don't personally know of a way to reflash the stock da Vinci firmware if you decided you wanted to go all the way back.

I designed this to be made of mostly small parts that are easier to print successfully on a marginal setup (like my da Vinci with the stock extruder and clog-prone hotend). The only real challenging part to print is the main extruder mount chassis, which has three areas requiring supports and a fairly long footprint that can be prone to lifting. Because a lot of these parts are under considerable compressive stress when assembled, I suggest printing this with a very high infill percentage. A sparse infill can lead to internal wall buckling that'll make the part fail. I printed mine as solid infill, as I do with any serious structural part.


This mod would technically work in a da Vinci 1.0a, but the 1.0a has holes drilled for a dual Bowden extruder mount on the top shelf of the chassis. You could do a more compact build by using the other Bowden extruder mount on Thingiverse (http://www.thingiverse.com/thing:846434). Despite the title that user posted, I believe his printer is actually a da Vinci 1.0A. Mine is a vanilla 1.0 and I do not have the holes for dual extruders drilled in the chassis, just a single center placed hole used as a cable mount in the stock design.

You may be interested in the E3D V6 hotend mount even if you're using a commercial Bowden extruder with another mounting method. Reusing the stock carriage without modification has several advantages. First, you won't have to attempt printing a gigantic carriage on an ailing da Vinci stock hotend. Secondly, the stock carriage is injection molded and has a far greater strength to weight ratio of anything you could ever print on your stock printer. Additionally, the envelope of the mount and stock carriage is such that you won't have problems with the carriage crashing into the Z axis rods or other obstacles in the build area. And finally, it's always nice to know you can go back to some degree if you change your mind. I can completely revert to the original hotend configuration in about 10 minutes with a few basic tools without even removing the carriage from the printer. 

I see this design as pretty basic and straightforward, but I'm pretty happy with the results. Having the extruder output a little further forward lets me run a slightly shorter Bowden tube than I would otherwise, which generally improves print quality, all other things being equal. I'm currently running my infills at 80mm/s without issue, and I could probably go faster but the printer gets super loud, so I tend to run it on the slower side. I'm glad to not hear my printer sound like it's shaking itself to death doing infills on narrow regions, slinging that stepper motor around is a serious problem in cases like that. 

As usual, use this design at your own risk, I am not responsible for any damages or injuries that may result from using these parts.


# Print Settings

Printer: XYZ da Vinci 1.0, borosilicate bed
Rafts: No
Supports: Yes
Resolution: 0.2mm vertical
Infill: 100% (solid)

# Post-Printing

Parts list--------------------


Big stuff:

-1x Stock XYZ Da Vinci 1.0 
-1x E3D V6 1.75mm universal Bowden kit w/ PTFE Bowden tube and screw-in extruder-end quick connect coupler. (i.e. https://www.amazon.com/Genuine-E3D-All-Metal-Universal-V6-175-B/dp/B00NAK9L6Q (updated link 2018/4/27))


Fasteners: 

I used mostly metric but also a few SAE fasteners (the plastic thread cutting screws) because I live in the United States, and a much wider variety of SAE fasteners are available to me locally. You can substitute with suitable metric fasteners if you're not in the US, and re-form the holes from the STEP file to suit what you have available.

Aside from the brass heat set threaded inserts, the rest of the fasteners below may be available at your local hardware store (I found most of it at my local Ace Hardware). I used these threaded inserts because I had them on hand from previous builds, and in my experience they are superior to captive nuts in that they distribute stress throughout the surrounding material rather than as a concentrated shear stress at the edges of the nut face and floor of the hex cutout. I always keep some on hand. You should be able to modify the part from the STEP files to accommodate captive nuts if you wish, but give these heat set inserts a try if you're ever ordering a bunch of stuff from McMaster-Carr -- they're great for higher stress applications! Note that you need a soldering iron with a medium tapered/fine tip to install these. You can find instructional videos on YouTube

I put McMaster-Carr part numbers for all the fasteners in the design. I'm not suggesting you go out and buy every single item (they only come in 25-50 packs usually, and at a high price), but they'll give you an idea what you should be looking for (length, maybe head style, etc).


Bowden extruder:
-3x M3 brass heat set threaded insert for plastic (McMaster No. 94180A331)
-8x M3 washer (i.e. McMaster No. 90965A130)
-1x M3x25mm socket cap machine screw (i.e. McMaster No. 91292A020)
-4x M3x30mm socket cap machine screw (i.e. McMaster No. 91292A022)
-2x M3x35mm socket cap machine screw (i.e. McMaster No. 91292A033)

Hotend mount:
-6x M3 brass heat set threaded insert for plastic (McMaster No. 94180A331)
-6x M3 washer (i.e. McMaster No. 90965A130)
-4x M3x16mm phillips machine screw (i.e. McMaster No. 92005A126)
-2x M3x25mm phillips machine screw (i.e. McMaster No. 92005A130)
-2x #4-20x0.625 thread forming screw for plastic (i.e. McMaster No. 99461A140)
-2x #4x0.5 thread forming screw for brittle plastic (i.e. McMaster No. 97975A115)
-2x #4 plastic washer 0.25" outer diam (i.e. McMaster No. 90295A045)
-4x #6 washer (i.e. McMaster No. 90107A007)


Notes-------------------------	
Hopefully the pictures have been sufficient, but here are a few things that may not be readily apparent:

Extruder assembly notes:
-There needs to be an M3 washer above the swivel joint on the extruder idler arm. It's shown in one of the photos, but this is very important as it lets the idler arm rotate freely and if you forget it you could crack the chassis when you tighten down the motor screws.
-The thumbscrews are designed to have the screws epoxied into them (i.e. JB Weld, Devcon, etc). I do this because the thumbscrews make assembly much easier, but if you follow suit you should make sure to leave the socket clear of epoxy so that it can still be turned using a hex key -- use the thumbscrews to tighten it down most of the way, and the hex key to tighten the rest of the way it to keep it from moving around when you're using it. 
-There is a trick to epoxying screws into thumbscrews like this so they're aligned properly. Put a layer of saran wrap over the hole the thumbscrew will go through, then push the thumbscrew and screw through it, poking a hole in the saran wrap. Screw it down moderately tight. Mix your epoxy, carefully use a toothpick to apply the epoxy between the screw head and the wall of the thumbscrew, being careful to let keep the hex socket clear. The saran wrap makes sure you don't have any epoxy leaking down to glue the thumbscrew to the rest of your parts. Being mounted in the hole lets you ensure the screw and face of the thumbscrew will be perpendicular. 
-As an installation tip, put some electrical tape on the top of the metal shelf around the hole, just a strip on either side will do. The texture on the top layer of the shelf spacer will grab into the tape and prevent the extruder from rotating. 
-Wrap a layer of electrical tape around the bowden tube and the bowden fitting where they go under the secondary and tertiary guide caps (if necessary). The rubberyness of the vinyl will help the caps grip the parts better than just hard ABS on PTFE/ABS.
-The spring mount hooks onto the free hanging M3x25 screw. Make sure to mount the bowden tube fitting so the hex flats are horizontal on top and bottom. It's not shown this way in the CAD assembly pictures, but it occurred to me during assembly. 
-The black thing under the primary guide cap is the rubber fitting that comes stock in the da Vinci printer, it holds the little filament feed tube in the plastic support above the filament source. If you lost this, you may want to take the STEP files and fill the cutouts back in so you can just have it grab onto the PTFE bowden tube material.


Hotend assembly notes
-The clearances are pretty tight between the fan duct and the hotend cap, which is why I selected phillips head screws that are lower profile than a standard socket cap screw. I would not try to make much of a substitution here.
-I would suggest positioning the hotend as shown. The heater and thermistor cables can run up toward the back of the printer next to the fan duct, over the fan duct, then around to the thermistor ports, etc.
-The little loop on the backplate is for cable tying your wiring harness to the carriage.

Finally, here's a video of this printer in action:

https://www.youtube.com/watch?v=dyvJHKajml8

![Alt text](https://cdn.thingiverse.com/assets/38/3a/64/ca/da/2016-04-21_20.45.44.jpg)
The screws I ended up using were slightly different than my CAD assembly, I switched to some things that were available locally as individual parts

![Alt text](https://cdn.thingiverse.com/assets/46/3b/af/81/75/2016-04-21_20.46.03.jpg)
I recommend orienting the extruder stepper as shown. It gives some room to coil up and tie down the excess stepper cabling

![Alt text](https://cdn.thingiverse.com/assets/22/a3/db/d1/a5/2016-04-21_20.46.20.jpg)
Top down view. These prints were somewhat rough as my stock da Vinci hot end was on its last legs

![Alt text](https://cdn.thingiverse.com/assets/85/22/20/71/f9/2016-04-21_20.46.35.jpg)

![Alt text](https://cdn.thingiverse.com/assets/c3/97/4a/d6/32/2016-04-21_20.46.42.jpg)

![Alt text](https://cdn.thingiverse.com/assets/e6/02/6b/38/42/2016-04-21_20.47.13.jpg)

![Alt text](https://cdn.thingiverse.com/assets/f7/ab/7b/e2/f6/2016-08-07_13.46.45.jpg)
Just a quick shot showing how I chose to wire the 30mm 12V fan into my system. I chose not to cut up my wiring harness and instead soldered directly onto the hardlines coming off the sockets. The +12V and GND on the top and bottom sockets are easily accessible to soldering without having to remove anything from the system. 

![Alt text](https://cdn.thingiverse.com/assets/4c/0f/cf/8a/76/2016-04-21_20.46.51.jpg)
This image shows the original design of hotend_base, note that the heatsink fins are visible. This allows cooling air to flow down onto the print bed and part. This has been corrected in hotend_base_v2

![Alt text](https://cdn.thingiverse.com/assets/07/24/fc/71/5a/2017-03-08_04.12.45.jpg)
Using the new hotend_base_v2 design, I further sealed heatsink airflow using two pieces of aluminum foil. They're secured with some electrical tape further up the duct (not visible).

![Alt text](https://cdn.thingiverse.com/assets/a9/a1/bc/0d/a5/2017-03-08_04.12.53.jpg)
Using the new hotend_base_v2 design, I further sealed heatsink airflow using two pieces of aluminum foil. They're secured with some electrical tape further up the duct (not visible).