import IMapLike, { EntrySet } from "@codingworkbench/common/IMapLike";
export declare const HTML_DATA_MODEL_ID = "__CwbPageDataModel";
export declare type HtmlModel = EntrySet<any>;
export declare type ModelSupplierFunc = () => HtmlModel;
export declare function read_html_model(): EntrySet<any>;
export interface ModelSupplier {
    getModel(): HtmlModel;
}
export declare class ModelBuilder {
    private _internal;
    constructor(other?: IMapLike<any>);
    put(key: string, value: any): this;
    build(): HtmlModel;
}
