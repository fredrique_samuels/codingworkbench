import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import {create_secure_data_url, create_secure_page_url} from "../../api/links";
import CwbAction from "@codingworkbench/action";
import {RequestContext} from "@codingworkbench/server/common";
import ICwbAccount from "../../api/ICwbAccount";
import {ISecurityHandlerContext, ISecurityHandlerFactoryContext} from "../../api/context";
import IUserSession from "@codingworkbench/data/IUserSession";
import UserSessionBuilder from "@codingworkbench/data/UserSessionBuilder";
import {web_respond_redirect, web_respond_with_500, web_respond_with_error} from "../../api/response";
import {create_activation_required_link} from "./common";
import {ICredentials} from "@codingworkbench/common/ICredentials";


async function handlerAsync<C extends ISecurityHandlerContext>(requestContext: RequestContext<C>) {
    const iCredentials = requestContext.request.body as ICredentials;
    const {securityService, messageSource, securityLinks, onAuthenticatedCallback} = requestContext.context;
    const {request, response} = requestContext;

    try {
        // authenticate
        const actionResponse: CwbAction.IResponse<ICwbAccount> = await securityService.authenticate(iCredentials.user, iCredentials.secret);
        if (actionResponse.error) {
            await web_respond_with_error(actionResponse, requestContext, messageSource);
            return
        }

        // extract data
        const account: ICwbAccount = actionResponse.data as ICwbAccount;
        const [user] = (await securityService.getAccountUsers(account)).filter(u => u.email === iCredentials.user);

        if(!account.activated) {
            web_respond_redirect(requestContext, create_activation_required_link(securityLinks, iCredentials.user));
            return;
        }

        // update session info
        const userSession: IUserSession = new UserSessionBuilder().setUserId(user.id).setAccountId(account.id).build();
        request.session.set("key", userSession);

        // callback to notify that the user was authenticated
        if (onAuthenticatedCallback) {
            onAuthenticatedCallback(requestContext);
        }

        /* TODO check "dest" parameter for destination url AND check "validateOnly" parameter for response type */
        // redirect to user home page
        web_respond_redirect(requestContext, create_secure_page_url(securityLinks.userHomePage, securityLinks));
    } catch (e) {
        web_respond_with_500(e, requestContext);
    }
}

export default function DoSignIn<C extends ISecurityHandlerContext>(factoryContext: ISecurityHandlerFactoryContext): IRequestHandler<C> {
    return new RequestHandlerBuilder(factoryContext.links.doSignIn, () => {}, handlerAsync).setMethod("post").setAsync().build();
}
