import {DbColumn} from "./DbColumn";
import {ID, IUnique, UNSAVED_ID} from "./IUnique";

export class Unique {

    private o: IUnique;

    constructor(o?: IUnique) {
        this.o = {
            ...(o || {id: UNSAVED_ID})
        }
    }

    public flatten(): IUnique {
        return { ...this.o };
    }

    public update(id?: ID): this {
        this.o = {
            id: id || this.o.id
        };
        return this;
    }
}

export default Unique;

export function unique_columns(): DbColumn[] {
    return [
        new DbColumn("id", "id").setPrimary()
    ];
}
