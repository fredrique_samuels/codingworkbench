"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function line_has_vertex_data(line) {
    return line.trim().startsWith("vertex");
}
function visit_line(scale, line, lineNumber) {
    var scaleFunc = function (f) { return scale * f; };
    var toStringFunc = function (f) { return f.toFixed(15); };
    if (!line_has_vertex_data(line)) {
        return line;
    }
    var _a = line.split("vertex"), leadingSpace = _a[0], valueArray = _a[1];
    var newValuesString = valueArray.trim()
        .split(" ")
        .map(parseFloat)
        .map(scaleFunc)
        .map(toStringFunc)
        .join(" ");
    return leadingSpace + "vertex " + newValuesString + "\r";
}
var STLVertexDataScaleVisitor = (function () {
    function STLVertexDataScaleVisitor(scale) {
        if (scale === void 0) { scale = 1.0; }
        this.data = "";
        this.scale = 1.0;
        this.scale = scale;
    }
    STLVertexDataScaleVisitor.prototype.visit = function (line, lineNumber) {
        this.data = this.data + visit_line(this.scale, line, lineNumber) + "\n";
    };
    Object.defineProperty(STLVertexDataScaleVisitor.prototype, "content", {
        get: function () {
            return this.data;
        },
        enumerable: true,
        configurable: true
    });
    return STLVertexDataScaleVisitor;
}());
exports.default = STLVertexDataScaleVisitor;
