declare type MetaDataParserFunc = (value: string) => any;
export declare class MetaDataParser {
    private readonly _prefix;
    private readonly _parserFunc;
    constructor(prefix: string, parserFunc: MetaDataParserFunc);
    readonly prefix: string;
    parse(line: string): any;
}
export declare const CURA_TIME_PARSER: MetaDataParser;
export declare const CURA_TIME_FORMATTED_PARSER: MetaDataParser;
export declare const CURA_FILAMENT_USED_PARSER: MetaDataParser;
export declare const CURA_MIN_X_PARSER: MetaDataParser;
export declare const CURA_MIN_Y_PARSER: MetaDataParser;
export declare const CURA_MIN_Z_PARSER: MetaDataParser;
export declare const CURA_MAX_X_PARSER: MetaDataParser;
export declare const CURA_MAX_Y_PARSER: MetaDataParser;
export declare const CURA_MAX_Z_PARSER: MetaDataParser;
export {};
