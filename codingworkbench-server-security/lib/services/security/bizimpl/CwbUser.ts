

import CwbSearch from "@codingworkbench/search";

import CwbAccount from "./CwbAccount";
import ICwbUser from "../api/ICwbUser";
import PersistedObject from "@codingworkbench/data/PersistedObject";
import CrudDataAccessor from "@codingworkbench/data/CrudDataAccessor";
import DbTable from "@codingworkbench/data/DbTable";
import {ID} from "@codingworkbench/data/IUnique";
import DbColumn from "@codingworkbench/data/DbColumn";

const TABLE: DbTable = new DbTable(
    "cwb_auth_user",
    [
        // new DbColumn("username"),
        new DbColumn("email"),
        new DbColumn("password_hash"),
        new DbColumn("password_reset_token"),
    ])
    .setUnique()
    .setArchived()
    .setAudited();

export default class CwbUser extends PersistedObject<ICwbUser> {

    constructor(dao: CrudDataAccessor) {
        super(dao, TABLE);
    }

    public async fromEmail(email: string): Promise<CwbUser> {
        const searchBuilder = new CwbSearch.SearchBuilder();
        searchBuilder.filters.addFilter("email", email);
        const [object] = await this.dao.searchObjects(TABLE, searchBuilder.build());
        if (object) {
            this.update(object as ICwbUser);
            return this;
        }
        throw new Error(`Unable to locate user with email ${email}`)
    }

    public async getAccounts(): Promise<CwbAccount[]> {
        return new CwbAccount(this.dao).getForUser(this.id);
    }

    public async searchByIds(ids: ID[]): Promise<CwbUser[]> {
        const {dao} = this;

        const searchBuilder = new CwbSearch.SearchBuilder();
        searchBuilder.filters.addFilter("id", ids, "in");
        return (await this.search(searchBuilder.build())).map(p =>  new CwbUser(dao).update(p));
    }

    public createParams(): ICwbUser {
        return {
            ...PersistedObject.createPersistedObject(),
            username:  "",
            email: "",
            password_hash: "",
            password_reset_token: null,
        }
    }

    public update(params: ICwbUser): this {
        super.update(params);
        this._params.password_hash = params.password_hash;
        this._params.email = params.email;
        this._params.password_reset_token = params.password_reset_token;
        return this;
    }
}
