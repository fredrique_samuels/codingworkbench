import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import {RequestContext} from "@codingworkbench/server/common";

import {ISecurityHandlerContext, ISecurityHandlerFactoryContext} from "../../api/context";
import {web_respond_redirect, web_respond_with_500, web_respond_with_error} from "../../api/response";
import {create_activation_required_link} from "./common";
import {ICredentials} from "@codingworkbench/common/ICredentials";

async function handlerAsync<C extends ISecurityHandlerContext>(requestContext: RequestContext<C>) {
    try {
        const iCredentials = requestContext.request.body as ICredentials;
        const actionResponse = await requestContext.context.securityService.registerNewUser(iCredentials.user, iCredentials.secret);
        if(actionResponse.error) {
            await web_respond_with_error(actionResponse, requestContext, requestContext.context.messageSource);
        } else {
            // todo encrypt email
            web_respond_redirect(requestContext, create_activation_required_link(requestContext.context.securityLinks, iCredentials.user));
        }
    } catch (e) {
        web_respond_with_500(e, requestContext);
    }
}

export default function DoSignUp<C extends ISecurityHandlerContext>(factoryContext: ISecurityHandlerFactoryContext): IRequestHandler<C> {
    return new RequestHandlerBuilder(factoryContext.links.doSignUp, () => {}, handlerAsync).setMethod("post").setAsync().build();
}
