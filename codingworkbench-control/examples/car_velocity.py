import numpy as np

from cwb_control.Control import Control
from cwb_control.DataStore import DataStore
from cwb_control.PlotDataHandler import PlotDataHandler
from cwb_control.MatPlotLibDataHandler import MatPlotLibDataHandler


# simulation parameters
from examples.Car import Car

simulation_duration = 10.0
time_step = 0.01

# plot cache
plotHandler = PlotDataHandler()

# data storage
data_store = DataStore()
data_store.add_change_handler(PlotDataHandler.id, plotHandler)

# car object
car = Car("car0", data_store)
car.acceleration_factor.set(10)
car.velocity.enable_handler(PlotDataHandler.id)
car.pedal.enable_handler(PlotDataHandler.id)
car.acceleration.enable_handler(PlotDataHandler.id)


# control logic
control = Control("car0", data_store, car.velocity.id, car.pedal.id)
# control.set_point.enable_handler(PlotDataHandler.id)
# control.error.enable_handler(PlotDataHandler.id)
# control.error_dt.enable_handler(PlotDataHandler.id)
control.kp.set(0.025)
# control.ki.set(0)
control.kd.set(20)
# control.p.enable_handler(PlotDataHandler.id)
# control.i.enable_handler(PlotDataHandler.id)
# control.d.enable_handler(PlotDataHandler.id)
control.enable_set_point_change_error_delta_ignore()

# simulation loop
for t in np.arange(0.0, simulation_duration, time_step):
    plotHandler.set_timestamp(t)

    if t == 1.0:
        pass
        # car.pedal.set(30)
        control.set_point.set(50)

    if t == 4.0:
        pass
        # car.pedal.set(50)
        control.set_point.set(25)

    if t == 8.0:
        pass
        # car.pedal.set(20)
        control.set_point.set(75)

    control.update(time_step)
    car.update(time_step)
    data_store.fire_handlers([PlotDataHandler.id])

MatPlotLibDataHandler.show_plot(data_store)
