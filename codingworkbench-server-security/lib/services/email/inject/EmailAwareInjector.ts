import EmailService, {EmailSourceCredentials} from "../bizimpl/EmailService";
import IEmailService from "../api/IEmailService";
import CwbInject from "@codingworkbench/common/inject";
import IEmailAwareContext from "../api/IEmailAwareContext";
import {IEmailTemplateProvider} from "../api/IEmailTemplateProvider";
import {IEmailTemplate} from "../api/IEmailTemplate";
import GmailTransporter from "../bizimpl/GmailTransporter";



class EmptyEmailTemplates implements IEmailTemplateProvider {
    get(key: string): IEmailTemplate | null {
        return null;
    }
}

export async function create_emailService(deps: CwbInject.Injector<IEmailAwareContext>): Promise<IEmailService> {
    return new EmailService(await deps.get("emailSourceCredentials"), await deps.get("emailTransporter"));
}

async function create_emailSourceCredentials()
    : Promise<EmailSourceCredentials>
{
    return {}
}

async function create_emailTemplateProvider()
    : Promise<IEmailTemplateProvider>
{
    return new EmptyEmailTemplates();
}

export default class EmailAwareInjector extends CwbInject.Module<IEmailAwareContext> {

    constructor() {
        super();
        this.bind("emailSourceCredentials", create_emailSourceCredentials );
        this.bind("emailService", create_emailService);
        this.bind("emailTransporter", async () => new GmailTransporter());
        this.bind("emailTemplateProvider", create_emailTemplateProvider);
    }
}
