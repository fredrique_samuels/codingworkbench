
/**
 * File data accessor.
 */
export interface IFileHandle {

    /**
     * The file path.
     */
    readonly path: string;

    /**
     * Read the file contents as a string.
     */
    readString(): Promise<string>;

    /**
     * Write a string to the file.
     * @param data
     */
    writeString(data: string): Promise<any>;

    /**
     * Delete the file if it exists.
     */
    delete(): Promise<void>;

    /**
     * Check if the file exists.
     */
    exists(): boolean;
}

export default IFileHandle;
