import {deepStrictEqual, ok} from 'assert'
import CwbTestCase from '@codingworkbench/testcase';
import Password from "../lib/Password";


describe('Password', function() {

    it ("getStrength# empty string is weak", () => {
        let strength = Password.getPasswordStrength("");
        deepStrictEqual(strength, 'weak')
    });

    it ("getStrength# less than 6 length is weak", () => {
        let strength = Password.getPasswordStrength(CwbTestCase.PASSWORD_WEAK);
        deepStrictEqual(strength, 'weak')
    });

    it ("getStrength# less than 8 length is medium with special chars", () => {
        let strength = Password.getPasswordStrength("#$6jaas");
        deepStrictEqual(strength, 'medium')
    });

    it ("getStrength# less than 8 length is medium", () => {
        let strength = Password.getPasswordStrength("1234dfs");
        deepStrictEqual(strength, 'medium')
    });

    it ("getStrength# 8 or more length is medium no special chars", () => {
        let strength = Password.getPasswordStrength(CwbTestCase.PASSWORD_MEDIUM);
        deepStrictEqual(strength, 'medium')
    });

    it ("getStrength# 8 or more length is strong with special chars", () => {
        let strength = Password.getPasswordStrength("A1234df#$sasd");
        deepStrictEqual(strength, 'strong')
    });

});
