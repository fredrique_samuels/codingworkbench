import {IMessageSource} from "./IMessageSource";


export interface IMessageSourceAware {
    readonly messageSource: IMessageSource;
}
