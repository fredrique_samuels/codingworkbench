



/**
 * Wrapper for any value to control access to it.
 * Has methods to check if the value is undefined or null.
 */
export class Optional<T=any> {

    private readonly _value: any | null | undefined;
    private readonly _nullable: boolean;

    /**
     * @param value The target value
     * @param nullable
     */
    constructor(value: T | null | undefined, nullable: boolean) {
        this._value = value;
        this._nullable = nullable;
    }

    /**
     * Create an Optional instance that is always empty.
     */
    static empty<T=any>() {
        return new Optional<T>(undefined, true);
    }

    /**
     * Check if the value is present.
     */
    isPresent() {
        if(this.isUndefined()) {
            return false;
        }

        /* check if this value is allowed to be null */
        if (this._nullable) return true;

        /* make sure this value is not null */
        return !this.isNull();
    }

    /**
     * @see Optional.isPresent
     */
    get present() {
        return this.isPresent()
    }

    /**
     * Invoke the callback method if the target isPresent returns true.
     *
     * @param callback
     * @param fallback
     */
    ifPresent(callback: (value: T) => void, fallback?: () => void) {
        if(this.present) {
            if(this._value !== undefined) {
                callback(this._value);
            }
        } else if(fallback) {
            fallback();
        }
    }

    /**
     * Get the internal value.
     * Throw an exception if isPresent() returns false.
     */
    get(): T {
        if(this.present) {
            return this._value;
        }
        throw new Error("Cannot get value if it is not present.")
    }

    /**
     * Convert from to another optional object
     */
    map<R>( transformer: (value: T) => R ): Optional<R> {
        if(this.present) {
            if(this._value !== undefined) {
                return Optional.forValue( transformer(this._value) );
            }
        }
        return Optional.empty();
    }


    /**
     * @see Optional.flatten()
     */
    get value() {
        return this.get();
    }

    /**
     * Create a new Optional instance for the given value.
     * The return Optional.present will be false if the value is undefined or null.
     *
     * @param value
     */
    static forValue<T>(value: T | null | undefined) : Optional<T> {
        if(value instanceof Optional) return value;
        return new Optional(value, false);
    }

    /**
     * Create a new Optional instance for the given value.
     * The return Optional.present will be false only if the value is undefined.
     *
     * @param value
     */
    static forNullableValue<T>(value: T | null | undefined) : Optional<T> {
        if(value instanceof Optional) return value;
        return new Optional(value, true);
    }

    /**
     * Invokes the callback function if the optionalObject provided is not null or undefined.
     *
     * @param optionalObject An object of type T that can be null or undefined.
     * @param callback The function to call with an object of T that is guaranteed to not be null or undefined.
     */
    public static ifObject<T>(optionalObject: T | null | undefined, callback: (obj: T) => any): boolean {
        let optional = Optional.forValue<T>(optionalObject);
        if(optional.present) {
            optional.ifPresent(callback);
            return true;
        }
        return false;
    }

    /**
     * Invokes the callback function if the optionalObject provided is not undefined.
     *
     * @param optionalObject An object of type T that can be null or undefined.
     * @param callback The function to call with an object of T that is guaranteed to not be undefined.
     */
    public static ifNullableObject<T>(optionalObject: T | null | undefined, callback: (obj: T | null) => any) {
        let optional = Optional.forNullableValue<T | null>(optionalObject);
        if(optional.present) {
            optional.ifPresent(callback);
            return true;
        }
        return false;
    }

    private isUndefined() {
        return this._value === undefined;
    }

    private isNull() {
        return this._value === null;
    }

}

export default Optional;
