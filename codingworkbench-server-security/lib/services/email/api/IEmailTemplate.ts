

import {IEmailContent} from "./IEmailContent";
import IMapLike from "@codingworkbench/common/IMapLike";

export enum EmailTemplateType {
    FUNCTION,
    STRING
}

export type EmailTemplateFunction = (params: IMapLike<any>) => string;


export function build_mail_from_template(template: IEmailTemplate | undefined, params: IMapLike<any>) : IEmailContent {
    if (!template) {
        throw new Error("No email template provided")
    }

    if (template.type === EmailTemplateType.STRING) {
        return {
            email: "",
            title: template.title,
            body: template.templateString || ""
        }
    }

    return {
        email: "",
        title: template.title,
        body: template.templateFunction ? template.templateFunction(params) : ""
    }
}

export interface IEmailTemplate {
    title: string;
    type: EmailTemplateType;
    templateString?: string;
    templateFunction?: EmailTemplateFunction;
}


