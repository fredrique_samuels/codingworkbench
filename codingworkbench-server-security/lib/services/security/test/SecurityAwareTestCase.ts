import CwbInject from "@codingworkbench/common/inject";
import ISecurityAwareContext from "../api/context";
import ServerTestInjector from "../../common/test/ServerTestInjector";
import EmailAwareTestInjector from "../../email/bizimpl/EmailAwareTestInjector";
import SecurityAwareTestInjector from "./SecurityAwareTestInjector";
import ICwbAccount from "../api/ICwbAccount";
import {ID} from "@codingworkbench/data/IUnique";
import Optional from "@codingworkbench/common/Optional";
import {deepStrictEqual, ok} from "assert";
import CwbTestCase from "@codingworkbench/testcase";
import {RequestContext, ResponseBuilder} from "@codingworkbench/server/common";
import {create_security_model} from "../api/response";
import {MockResponse} from "../../common/test/mock_requests";
import {connection_aware_close} from "@codingworkbench/data/IConnectionAware";
import ConnectionAwareTestInjector from "@codingworkbench/data/test/ConnectionAwareTestInjector";

namespace SecurityAwareTestCase {
    export async function create_injector(): Promise<CwbInject.Module<ISecurityAwareContext>> {
        const serverInjector = new ServerTestInjector();
        const connectionInjector = new ConnectionAwareTestInjector();
        const emailServiceInjector = new EmailAwareTestInjector();
        const securityInjector = new SecurityAwareTestInjector();
        return serverInjector.merge(connectionInjector).merge(emailServiceInjector).merge(securityInjector);
    }

    export async function create_context(): Promise<ISecurityAwareContext> {
        return await (await create_injector()).getAll();
    }

    export async function destroy(context: ISecurityAwareContext) {
        await connection_aware_close(context);
    }

    export async function do_signup_test_user(unit: ISecurityAwareContext, overrides: any = {}) {
        const email: string = overrides.email || await CwbTestCase.generateEmail();
        const password: string = await CwbTestCase.generatePassword();
        const actionResponse = await unit.securityService.registerNewUser(email, password);
        const account = actionResponse.data as ICwbAccount;
        return {email, password, account}
    }

    export async function get_account_by_id(unit: ISecurityAwareContext, accountId: ID)
        : Promise<ICwbAccount> {
        const a = Optional.forValue(await unit.securityService.getAccountById(accountId));
        ok(a.present);
        return a.get()
    }

    export async function assert_account_activated(unit: ISecurityAwareContext, accountId: ID) {
        const assertAccount = await get_account_by_id(unit, accountId);
        deepStrictEqual(assertAccount.activation_token, null);
        ok(assertAccount.activated);
    }

    export async function assert_account_not_activated(unit: ISecurityAwareContext, accountId: ID) {
        const assertAccount = await get_account_by_id(unit, accountId);
        ok(assertAccount.activation_token !== null);
        ok(!assertAccount.activated);
    }

    export function assert_security_view<C extends ISecurityAwareContext>(requestContext: RequestContext<C>, viewName: string) {
        const {viewFactory, securityLinks} = requestContext.context;
        const model = create_security_model(securityLinks).build();
        const view = viewFactory.modelAndView(viewName, model);
        deepStrictEqual((requestContext.response as MockResponse).data, view)
    }

    export function assert_redirect<C extends ISecurityAwareContext>(requestContext: RequestContext<C>, url: string) {
        deepStrictEqual((requestContext.response as MockResponse).data, new ResponseBuilder().redirect(url).json())
    }

}


export default SecurityAwareTestCase;
