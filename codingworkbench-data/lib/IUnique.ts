export const UNSAVED_ID = null;

export type ID = number | string | null;

/**
 * Interface for persistence data with a numeric id.
 */
export interface IUnique {
    id: ID;
}

export default IUnique;
