import * as child_process from "child_process"
import IProcessOutput from "./IProcessOutput";
import ProcessOutputCapture from "./ProcessOutputCapture";

export default class ProcessHandle {

    execute(command: string): Promise<IProcessOutput> {
        return new Promise((resolve, reject) => {
            const pc = new ProcessOutputCapture();
            child_process.exec(command, {maxBuffer: 1024 * 500},
                (error: child_process.ExecException, stdout: string, stderr: string) => {
                    let pc = new ProcessOutputCapture();
                    pc.capture(error, stdout, stderr);
                    resolve(pc)
                })
        })
    }
}
