import ISliceSummary from "./ISliceSummary";
import ProcessStdio from "./process/ProcessStdio";
export default interface ISliceResults {
    readonly stdio: ProcessStdio;
    readonly summary: ISliceSummary;
}
