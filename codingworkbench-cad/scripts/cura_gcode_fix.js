const fs = require('fs');

// https://en.wikipedia.org/wiki/G-code

class GVec {
    constructor() {
        this.x = null
        this.y = null
        this.z = null
    }

    add(aGVec) {
        let ret = new GVec()
        ret.x = aGVec.x === null ? this.x : aGVec.x;
        ret.y = aGVec.y === null ? this.x : aGVec.y;
        ret.z = aGVec.z === null ? this.x : aGVec.z;
        return ret;
    }

    static zero() {
        let ret = new GVec();
        ret.x = 0;
        ret.y = 0;
        ret.z = 0;
        return ret;
    }
}

class GCodeContext {
    move(aGVec) {}
    linearInterpolate(aGVec) {}
}

class GCodeFunction {
    execute(gcodeContext) {}
}

class AbstractFunctionG {
    constructor(code, x, y, z) {
        this.code = code;
        this.x = x;
        this.y = y;
        this.z = z;
    }
}

function extract_g_data(array) {
    let code;
    let x = null
    let y = null
    let z = null
    array.forEach(function(p){
        
    })
}

function extractVecComponentValue(s) {
    if(s === undefined) return {}
    if(s.startsWith("X")) return {x: parseFloat(s.replace("X", ""))}
    if(s.startsWith("Y")) return {y: parseFloat(s.replace("Y", ""))}
    if(s.startsWith("Z")) return {z: parseFloat(s.replace("Z", ""))}
    return {}
}

function parse_layer_number(layerLine) {
    return parseInt(layerLine.replace(";LAYER:", ""))
}

function parse_g_line(gLine) {
    let parts = gLine.split(";")[0].split(" ")
    const [name, a, b, c, d, e] = parts.slice(0)

        let val = {};
        let arr = [a, b, c, d, e];
        arr.forEach(
                function(i) {
                    val = Object.assign({}, val, extractVecComponentValue(i))
                }
            )
    return {
        name: name,
        vec: val
    }
}

async function load_gcode_paths(filename) {
    let data = fs.readFileSync(filename, 'utf8');

    let gArr = []
    let layer = -1;
    data.split("\n")
        .forEach(
            function(line) {
                if(line.startsWith(";LAYER:")) {
                    layer = parse_layer_number(line)
                } else if(line.startsWith("G") && layer >= 0)
                {
                    gArr.push(parse_g_line(line))
                }
            }
        )

    console.log("OPERATION COUNT", gArr.length)
}

load_gcode_paths("resources/CE3_square.gcode")