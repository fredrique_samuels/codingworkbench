import {deepStrictEqual, fail, ok} from 'assert';

import CwbLog from "@codingworkbench/log";
import CwbSearch from "@codingworkbench/search";
import ConnectionTestCase from "../../lib/ConnectionTestCase";
import DbColumn from "../../lib/DbColumn";
import Unique, {unique_columns} from "../../lib/Unique";
import DbTable from "../../lib/DbTable";
import IUnique, {ID} from "../../lib/IUnique";
import IConnectionAware, {connection_aware_close} from "../../lib/IConnectionAware";
import ConnectionAwareTestInjector from "../../lib/ConnectionAwareTestInjector";
import CrudDataAccessor from "../../lib/CrudDataAccessor";
import {db_connection_debug_mode} from "../../lib/DbConnection";

interface Employee extends IUnique {
    name: string;
}

const TABLE_NAME = "unit_tests_employee";

namespace Employee {

    export const table = new DbTable(TABLE_NAME)
        .addColumns(unique_columns())
        .addColumn(new DbColumn("name"));

    export function create(name: string): Employee {
        return {
            ... new Unique().flatten(),
            name: name
        }
    }
}

interface IUnit extends ConnectionTestCase.IMongoConnectionTestCase {}

describe('MongoDataAccessor', function() {

    this.timeout(20000);

    let unit: IConnectionAware;

    before(async () => {
        CwbLog.setup();
        CwbLog.enable_console();

        unit = await new ConnectionAwareTestInjector().getAll();
        try {
            await unit.mongoConnection.createCollection(TABLE_NAME);
        } catch (e) {
            CwbLog.error(e);
        }

        db_connection_debug_mode(true);
    });


    after(async () => {
        await unit.mongoConnection.deleteCollection(TABLE_NAME);
        await connection_aware_close(unit);
    });

    function get_dao(unit: IConnectionAware): CrudDataAccessor {
        return unit.mongoDao;
    }

    it("connected", (done) => {
        async function _e() {
            ok(unit.mongoConnection.connected);
        }

        _e().then(() => done())
            .catch(done)
    });

    it("saveObject()", (done) => {

        async function _e() {
            /* when */
            let id = await get_dao(unit).saveObject(Employee.table, Employee.create("John"));

            /* then */
            ok(id !== null);
        }

        _e().then(() => done())
            .catch(done)
    });

    it("getObject()", (done) => {

        async function _e() {
            /* given */
            let id = await get_dao(unit).saveObject(Employee.table, Employee.create("John"));

            /* when */
            let employee: Employee = await get_dao(unit).getObject(Employee.table, id);

            /* then */
            deepStrictEqual(employee.id, id);
            deepStrictEqual(employee.name, "John");
        }

        _e().then(() => done())
            .catch(done)
    });

    it("update#updateObject()", (done) => {

        async function _e() {
            /* given */
            let id = await get_dao(unit).saveObject(Employee.table, Employee.create("John"));
            const employee: Employee = await get_dao(unit).getObject(Employee.table, id);

            /* when */
            await get_dao(unit).saveObject(Employee.table, {
                ...employee,
                name: "Peter"
            });

            /* then */
            let employeeUpdated: Employee = await get_dao(unit).getObject(Employee.table, id);
            deepStrictEqual(employeeUpdated.id, id);
            deepStrictEqual(employeeUpdated.name, "Peter");
        }

        _e().then(() => done())
            .catch(done)
    });

    it("searchObject()", (done) => {

        async function _e() {
            /* given */
            let id = await get_dao(unit).saveObject(Employee.table, Employee.create("John"));

            /* when */
            const searchBuilder = new CwbSearch.SearchBuilder();
            searchBuilder.filters.addFilter("id", id);

            const employees: Employee[] = await get_dao(unit).searchObjects(Employee.table, searchBuilder.build());

            /* then */
            deepStrictEqual(employees.length, 1);
            deepStrictEqual(employees[0].id, id);
            deepStrictEqual(employees[0].name, "John");
        }

        _e().then(() => done())
            .catch(done)
    });

    it("searchObjectByIds()", (done) => {

        async function _e() {
            /* given */
            let id = await get_dao(unit).saveObject(Employee.table, Employee.create("John"));
            let id2 = await get_dao(unit).saveObject(Employee.table, Employee.create("Jack"));

            /* when */
            const searchBuilder = new CwbSearch.SearchBuilder();
            searchBuilder.filters.addFilter("id", [id, id2]);

            const employees: Employee[] = await get_dao(unit).searchObjects(Employee.table, searchBuilder.build());

            /* then */
            deepStrictEqual(employees.length, 2);
        }

        _e().then(() => done())
            .catch(done)
    });

    it("deleteObject()", (done) => {

        async function _e() {
            /* given */
            let id = await get_dao(unit).saveObject(Employee.table, Employee.create("John"));

            /* when */
            await get_dao(unit).deleteObject(Employee.table, id);

            /* then */
            try {
                await get_dao(unit).getObject(Employee.table, id);
                fail("item not deleted");
            } catch (e) {
            }

        }

        _e().then(() => done())
            .catch(done)
    });

    it("pagination()", (done) => {

        async function _e() {
            /* given */
            const ids: ID[] = [
                await get_dao(unit).saveObject(Employee.table, Employee.create("John")),
                await get_dao(unit).saveObject(Employee.table, Employee.create("Ben")),
                await get_dao(unit).saveObject(Employee.table, Employee.create("Rick")),
                await get_dao(unit).saveObject(Employee.table, Employee.create("Sara")),
                await get_dao(unit).saveObject(Employee.table, Employee.create("Vernon")),
                await get_dao(unit).saveObject(Employee.table, Employee.create("kara")),
                await get_dao(unit).saveObject(Employee.table, Employee.create("Peter")),
                await get_dao(unit).saveObject(Employee.table, Employee.create("Chris")),
                await get_dao(unit).saveObject(Employee.table, Employee.create("Leon")),
            ];

            const searchBuilder = new CwbSearch.SearchBuilder();
            searchBuilder.filters.addFilter("id", ids, "in");
            searchBuilder.pagination.setPageSize(3)
                .setSortField("id")
                .setSortOrder("ASC");

            const search = searchBuilder.build();

            /* then */
            let searchResults: CwbSearch.IResults<Employee> = await get_dao(unit).searchObjectsPaginated<Employee>(Employee.table, search);
            assert_page(searchResults, 1, 3, ids.slice(0, 3), 9);

            searchResults = await get_dao(unit).searchObjectsPaginated<Employee>(Employee.table, searchResults.next as CwbSearch.Search);
            assert_page(searchResults, 2, 3, ids.slice(3, 6), 9);

            searchResults = await get_dao(unit).searchObjectsPaginated<Employee>(Employee.table, searchResults.next as CwbSearch.Search);
            assert_page(searchResults, 3, 3, ids.slice(6, 9), 9);

            const searchBuilder2 = new CwbSearch.SearchBuilder(searchResults.prev);
            searchBuilder2.pagination.setSortOrder('DESC');

            searchResults = await get_dao(unit).searchObjectsPaginated<Employee>( Employee.table, searchBuilder2.build() );
            assert_page(searchResults, 2, 3, ids.reverse().slice(3, 6), 9);

        }

        _e().then(() => done())
            .catch(done)
    });

    function assert_page(searchResults: CwbSearch.IResults<Employee>, currentPage: number, totalPages: number, results: ID[], totalRecords: number) {
        deepStrictEqual(searchResults.currentPage, currentPage);
        deepStrictEqual(searchResults.pages, totalPages);
        deepStrictEqual(searchResults.results.length, results.length);
        deepStrictEqual(searchResults.results.map(e => e.id), results);
        deepStrictEqual(((searchResults.refresh as CwbSearch.Search).pagination as CwbSearch.IPagination).totalRecords, totalRecords);

        if (currentPage > 1) {
            ok(searchResults.prev);
        } else {
            ok(!searchResults.prev);
        }

        if (currentPage === totalPages) {
            ok(!searchResults.next);
        } else {
            ok(searchResults.next);
        }
    }

});
