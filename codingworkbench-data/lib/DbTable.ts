/**
 * Object to define the structure of a table.
 * Also contains methods to extract column names in
 * various formats.
 */

import {ArchiveAware, archive_columns} from "./ArchiveAware";
import {audit_columns, AuditAware} from "./AuditAware";
import {DbColumn} from "./DbColumn";
import {Unique, unique_columns} from "./Unique";

export class DbTable {

    private _defs: DbColumn[];
    private _audited: boolean = false;
    private _archived: boolean = false;
    private _unique: boolean = false;
    private readonly _name: string;

    /**
     * Create a new table definition.
     * 
     * @param name The table name
     * @param defs An list of column definitions.
     */
    constructor(name: string, defs: DbColumn[] = []) {
        this._defs = [...defs];
        this._name = name;
    }

    /**
     * Invoke to indicate that this table contains AuditData columns
     * 
     * @returns this
     */
    setAudited(): this {
        this._audited = true;
        return this;
    }

    /**
     * Invoke to indicate that each entry should have a unique id.
     *
     * @returns this
     */
    setUnique(): this {
        this._unique = true;
        return this;
    }

    /**
     * Invoke to indicate that this table contains
     * KeepForever columns.
     * 
     * @returns this
     */
    setArchived(): this {
        this._archived = true;
        return this;
    }

    /**
     * The table name.
     * @readonly
     */
    get name(): string {return this._name}

    /**
     * True if the table contains audit data.
     * 
     * @see setAudited
     * @readonly
     */
    get audited(): boolean {return this._audited}

    /**
     * True if the table contains KeepForever data.
     * 
     * @see setKeepForever
     * @readonly
     */
    get archived(): boolean {return this._archived}

    /**
     * True if all entries in the table should have a unique id.
     */
    get unique(): boolean {
        return this._unique;
    }

    /**
     * Get the definition of a JS Object field by its name.
     * 
     * @param field The field definition
     * @throws Error if the fields has no definition defined.
     */
    get(field: string): DbColumn {
        const [d] = this.get_columns.filter(d => d.field===field)
        if(d) return d;
        throw Error(`No definition setup for field='${field}'`)
    }

    /**
     * Add a column to the table definition.
     * @param column
     */
    addColumn(column: DbColumn): this {
        this.addColumns([column]);
        return this;
    }

    /**
     * Add an array columns to the table definition.
     * @param columns
     */
    addColumns(columns: DbColumn[]): this {
        this._defs = [...this._defs, ...columns];
        return this;
    }

    /**
     * Get all the definitions in this table
     * excluding primary key columns.
     * 
     * Useful for generating insert statements.
     */
    get insert_columns(): DbColumn[] {
        return [ 
            ... this._defs.filter(p => !p.primary),
            ... this._unique ? unique_columns() : [],
            ... this._audited ? audit_columns() : [],
            ... this._archived ? archive_columns() : []
        ]
    }

    /**
     * Get all the column names in this table
     * excluding primary key columns.
     * 
     * @see insert_columns
     */
    get insert_column_names(): string[] { return this.insert_columns.map( d => d.column) }

    /**
     * Get all the definitions
     */
    get get_columns(): DbColumn[] {
        return [ 
            ... this._defs,
            ... this._unique ? unique_columns() : [],
            ... this._audited ? audit_columns() : [],
            ... this._archived ? archive_columns() : []
        ]
    }

    /**
     * Get all the 
     */
    get get_columns_as_alias_sql(): string[] { return this.get_columns.map(p => `${p.column} as ${p.field}`) }


    get column_names(): string[] { return this._defs.map(p => p.column) }
    get columns_as_alias_sql(): string[] { return this._defs.map(p => `${p.column} as ${p.field}`) }
    get fields(): string[] { return this._defs.map(p => p.field) }

    public static empty(): DbTable {
        return new DbTable("", []);
    }
}

export default DbTable;

