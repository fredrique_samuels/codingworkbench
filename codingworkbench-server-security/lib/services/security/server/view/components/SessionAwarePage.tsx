import {IPageProps, IPageState, Page} from "@codingworkbench/react/page/Page";
import {RequestAction} from "@codingworkbench/server/RequestAction";
import SecurityViewController from "../SecurityViewController";

export default class SessionAwarePage
    <
        P extends IPageProps = IPageProps,
        S extends IPageState = IPageState
    >
    extends Page<P, S> {

    constructor(props: P) {
        super(props);
    }

    componentDidMount(): void {
        setInterval(this.validateSession.bind(this), 10000)
    }

    validateSession() {
        new RequestAction(SecurityViewController.get_site_links().sessionCheck)
            .setResponseCallback((r: Response) => {
                if(r.status == 403) {
                    SecurityViewController.goto_signin();
                }
            })
            .text()
            .then(() => {});
    }

}
