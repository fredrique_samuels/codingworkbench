import {deepStrictEqual, ok} from 'assert'
import Encrypt from 'lib/Encrypt';


describe('EncryptionPolicy.sha256', function() {

    it ("EncryptionPolicy.sha256", (done) => {

        async function _e() {
            ok(await Encrypt.sha256Encryption())
        }

        _e().then(() => done())
            .catch(done);
    });

    it ("encrypt", (done) => {

        async function _e() {
            let policy = await Encrypt.sha256Encryption();
            deepStrictEqual(await policy.encrypt("input"), "yWxtW+jQihLntc3Bsgf6ayQwl0yGgD2IkWdedv2ZLCA=")
        }

        _e().then(() => done())
            .catch(done);
    });

});
