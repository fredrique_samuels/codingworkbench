import * as mongo from "mongodb";

import CwbSearch from "@codingworkbench/search";
import CwbLog from "@codingworkbench/log";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";



import {DbConnection, is_db_connection_debug_mode} from "./DbConnection";
import {DbQueryRunner} from "./DbQueryRunner";
import {DbTable} from "./DbTable";
import {DbUpdateRunner} from "./DbUpdateRunner";
import {DbQuery} from "./DbQuery";
import {DbQueryFactory} from "./DbQueryFactory";



export class MongoConnection implements DbConnection {

    private _hostUrl: string;
    private authentication: string;
    private _connected: boolean;
    private _dbName: string;
    private _client: mongo.MongoClient | null;
    private _mdb: mongo.Db | null;

    constructor(connectionProperties: IPropertiesReader, scope?: string) {
        const env: string = process.env.NODE_ENV || "test";
        const scopePart = scope ? "." + scope : "";
        const prefix = `mongodb${scopePart}.${env}`;

        const host = connectionProperties.getString(`${prefix}.host`, "127.0.0.1");
        const port = connectionProperties.getNumber(`${prefix}.port`, 0);
        const user = connectionProperties.getString(`${prefix}.user`, "");
        const password = encodeURIComponent(connectionProperties.getString(`${prefix}.password`, ""));
        const protocol = encodeURIComponent(connectionProperties.getString(`${prefix}.protocol`, "mongodb"));
        this._dbName = connectionProperties.getString(`${prefix}.db`, "");
        this._hostUrl = `${host}${port > 0 ? `:${port}` : ""}/${this._dbName}?retryWrites=true&w=majority`;
        this.authentication = `${protocol}://${user}:${password}`;
        this._connected = false;
        this._client = null;
        this._mdb = null;

        CwbLog.info(`Loaded configs for mongo db at ${this._hostUrl}`);
    }

    get dbName(): string {
        return this._dbName;
    }

    get hostUrl(): string {
        return this._hostUrl;
    }

    get connected(): boolean {
        return this._connected;
    }

    public async createCollection(name: string) {
        const mdb = await this.get_mdb();
        const find = (await mdb.collections()).find(c => c.collectionName === name);
        if (!find) {
            await mdb.createCollection(name);
        }
    }

    public async deleteCollection(name: string) {
        const mdb = await this.get_mdb();
        await mdb.dropCollection(name);
    }

    public async connect(): Promise<void> {
        CwbLog.info(`Connecting to Mongo ${this._hostUrl}`);
        try {
            await this.get_mdb();
            this._connected = true;
        } catch (e) {
            CwbLog.error(e);
            this._connected = false;
        }
        CwbLog.info(`Connection Status connected=${this._connected}`);
    }

    public async close(): Promise<void> {
        this.close_connection_safe(this._client);
    }

    async createQuery<T>(q: DbQuery): Promise<DbQueryRunner> {
        return new QueryImpl(this, q);
    }

    async createUpdate(q: DbQuery): Promise<DbUpdateRunner> {
        return new UpdateImpl(this, q);
    }

    async execute(q: DbQuery): Promise<any[]> {
        throw new Error("Method not implemented.");
    }

    public getQueryFactory(): DbQueryFactory {
        return new MongoQueryParamsFactory();
    }

    public async get_mdb(): Promise<mongo.Db> {
        await this.get_connection();
        return this._mdb as mongo.Db;
    }

    private async get_connection(): Promise<mongo.MongoClient> {
        let refresh_db_conn = false;
        if (this._client === null || !this._connected) {
            this.close_connection_safe(this._client);
            this._client = await this.create_connection(`${this.authentication}@${this._hostUrl}`);
            this._client as mongo.MongoClient;
            refresh_db_conn = true;
        }

        if (refresh_db_conn) {
            this._mdb = this._client.db(this._dbName);
        }

        return this._client;
    }

    private close_connection_safe(conn: mongo.MongoClient | null) {
        try {
            if (conn) {
                conn.close().catch(CwbLog.error);
            }
        } catch (e) {
            CwbLog.error(e);
        }
    }

    private async create_connection(url: string): Promise<mongo.MongoClient> {
        return await mongo.MongoClient.connect(url, {useUnifiedTopology: true});
    }

}

class UpdateImpl implements DbUpdateRunner {
    private conn: MongoConnection;
    private readonly q: DbQuery;

    constructor(conn: MongoConnection, q: DbQuery) {
        this.conn = conn;
        this.q = q;
    }

    async execute(): Promise<any[]> {
        const {conn, q} = this;
        const {table, command, values} = q;

        const updateFunc = async (mdb: mongo.Db): Promise<any[]> => {
            const fieldKeys = Object.keys(values);
            const updates: any = {};

            fieldKeys.forEach(
                k => {
                    if (k === "id") {
                        return;
                    }

                    if (k === "_id") {
                        return;
                    }

                    updates[k] = values[k];
                }
            );

            const id = values["id"];
            if (!id) {
                throw new Error("Cannot update record. No id provided");
            }


            const filter = {_id: {$eq: id}};
            const update = {$set: updates};

            if(is_db_connection_debug_mode()) {
                CwbLog.info(`mongo update -> ${table.name} ${JSON.stringify(filter)} ${JSON.stringify(update)} `)
            }

            await mdb.collection(table.name).updateOne(filter, update);
            return [{id: id}];
        };

        const insertFunc = async (mdb:mongo. Db): Promise<any[]> => {
            const op: mongo.CollectionInsertOneOptions = {};

            if(is_db_connection_debug_mode()) {
                CwbLog.info(`mongo insert -> ${table.name} ${JSON.stringify(values)} ${JSON.stringify(op)} `)
            }

            const insertRes = await mdb.collection<any[]>(table.name).insertOne(values, op);
            return [{id: insertRes.insertedId}];
        };

        const deleteFunc = async (mdb: mongo.Db): Promise<any[]> => {
            const op: mongo.CollectionInsertOneOptions = {};

            const id = values["id"];
            if (!id) {
                return [];
            }

            if(is_db_connection_debug_mode()) {
                CwbLog.info(`mongo insert -> ${table.name} ${JSON.stringify(values)} ${JSON.stringify(op)} `)
            }

            await mdb.collection<any[]>(table.name).deleteOne(values, op);
            return [];
        };

        if (command === "insert") {
            return await insertFunc(await conn.get_mdb()) as any[];
        } else if (command === "update") {
            return await updateFunc(await conn.get_mdb()) as any[];
        } else if (command === "delete") {
            return await deleteFunc(await conn.get_mdb()) as any[];
        }

        return [];
    }
}

function map_columns_to_fields(table: DbTable, rows: any[]): any[] {
    const columns = table.get_columns;
    return rows.map(row => {
        const entry: { [s: string]: any } = {};
        columns.forEach(c => {
            if (c.field === "id") {
                entry["id"] = row["_id"];
            } else {
                entry[c.field] = row[c.field];
            }
        });
        return entry;
    });
}

class QueryImpl implements DbQueryRunner {
    private conn: MongoConnection;
    private readonly q: DbQuery;

    constructor(conn: MongoConnection, q: DbQuery) {
        this.conn = conn;
        this.q = q;
    }

    async query(): Promise<any[]> {
        const {conn, q} = this;
        const {table, command, values, targetResult} = q;

        const searchFunc = async (mdb: mongo.Db): Promise<any[]> => {
            const searchParams: CwbSearch.Search = values as CwbSearch.Search;
            const cursor = create_mongo_search_cursor(searchParams, table, mdb);
            const results = await cursor.toArray();

            if (targetResult === "RAW") {
                return results;
            }

            return map_columns_to_fields(table, results);
        };

        const countFunc = async (mdb: mongo.Db): Promise<any[]> => {
            const searchParams: CwbSearch.Search = values as CwbSearch.Search;
            const cursor = create_mongo_search_cursor(searchParams, table, mdb);
            const count = await cursor.count();
            return [{count: count}];
        };

        if (command === "search") {
            return await searchFunc(await conn.get_mdb()) as any[];
        }

        if (command === "count") {
            return await countFunc(await conn.get_mdb()) as any[];
        }

        return [];
    }
}

function create_mongo_search_cursor(searchParams: CwbSearch.Search, table: DbTable, mdb: mongo.Db)
    : mongo.Cursor<any>
{
    const query = create_mongo_query(searchParams);

    if(is_db_connection_debug_mode()) {
        CwbLog.info(`mongo query -> ${table.name} ${JSON.stringify(query)} `)
    }

    const cursor: mongo.Cursor<any> = mdb.collection(table.name).find(query);

    if (searchParams.pagination) {
        if (searchParams.pagination.sortField !== null) {
            const {sortField, sortOrder} = searchParams.pagination;
            if (sortField) {
                const sort: any = {};
                sort[sortField === "id" ? "_id" : sortField] = sortOrder === "ASC" ? 1 : -1;
                cursor.sort(sort);
            }
        }

        cursor.limit(searchParams.pagination.pageSize);

        if (searchParams.pagination.offset > 0) {
            cursor.skip(searchParams.pagination.offset);
        }
    }

    return cursor
}

function create_mongo_query(s: CwbSearch.Search): { [s: string]: any } {
    const filters: CwbSearch.Filters = s.filters || {};
    const filterKeys = Object.keys(filters);

    const MongoComparisonKeyMap: { [s: string]: string } = {
        eq  : "$eq",
        lt  : "$lt",
        lte : "$lte",
        gt  : "$gt",
        gte : "$gte",
        ne  : "$ne",
        nin : "$nin",
        in  : "$in",
    };

    // https://docs.mongodb.com/manual/reference/operator/query-comparison/
    const query: { [s: string]: any } = {};

    filterKeys.forEach(fk => {
        const fil: CwbSearch.IFilter = filters[fk];

        const operation: string = fil.operation || "$eq";
        let mkey: string = MongoComparisonKeyMap[operation] || "$eq";

        if (mkey === "$eq" && Array.isArray(fil.value)) {
            mkey = "$in";
        }

        const mfil: { [s: string]: any } = {};
        mfil[mkey] = fil.value;

        if (fk === "id") {
            query["_id"] = mkey === "$eq" ? fil.value : mfil;
        } else {
            query[fk] = mkey === "$eq" ? fil.value : mfil;
        }
    });

    return query;
}

class MongoQueryParamsFactory implements DbQueryFactory {

    createUpdateQuery(table: DbTable, record: any): DbQuery {
        return {
            command: "update",
            table,
            targetResult: "OBJECT_ID",
            values: record,
        };
    }

    createInsertQuery(table: DbTable, record: any): DbQuery {
        return {
            command: "insert",
            table,
            targetResult: "OBJECT_ID",
            values: record,
        };
    }

    createSearchQuery(table: DbTable, search: CwbSearch.Search): DbQuery {
        return {
            command: "search",
            table,
            values: search,
        };
    }

    createCountQuery(table: DbTable, search: CwbSearch.Search): DbQuery {
        return {
            command: "count",
            table,
            values: search,
        };
    }

    createDeleteObjectQuery(table: DbTable, record: any): DbQuery {
        return {
            command: "delete",
            table,
            values: record,
        };
    }

}


export default MongoConnection;

