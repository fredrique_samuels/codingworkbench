

/**
 * Interface defining methods useful methods for objects that have state.
 */
export interface IHasState<IProps=any, IState=any> {

    /**
     * State property accessor
     */
    state: IState;

    /**
     * Update a specific property on the state.
     *
     * @param property The property name
     * @param updates The updates to apply to the property.
     * @see updateComponentStateProperty
     */
    updateStateProperty<IKey extends keyof IState>(property: IKey, updates?: any): void;

    /**
     * Update the object state. Works the save as React.Component#setState().
     *
     * Also as extra parameters for updating a nested property state. Note this property is not typed.
     *
     * @param updates The updates for the state.
     * @param property Optional name of a specific property on the state.
     * @param propertyUpdates Optional updates to apply to the property provided.
     */
    updateState<IKey extends keyof IState>(updates: Pick<IState, IKey> | null, property?: keyof IState, propertyUpdates?: any): void;

    /**
     * Method definition of React.Component#setState()
     *
     * @param state The state updates to apply.
     * @param callback Optional callback method.
     * @see updateComponentState
     */
    setState<IKey extends keyof IState>(state: ReactUpdateState<IProps, IState,IKey>, callback?: () => void): void
}


/**
 * A shorthand type declaration used for setState() on react components.
 */
export type ReactUpdateState<P, S, K extends keyof S> = ((prevState: Readonly<S>, props: Readonly<P>) => (Pick<S, K> | S | null)) | Pick<S, K> | S | null

/**
 * Given a  HasState instance, update the state using updates provided.
 * If property is provided, update the specific  property on the state using propertyUpdates.
 *
 * @param hasState The target state object.
 * @param updates The state updates to apply.
 * @param property Optional property to apply specific changes to.
 * @param propertyUpdates Optional update to the property provided.
 */
export function updateComponentState<P, S, K extends keyof S>(hasState: IHasState<P, S>, updates: Pick<S, keyof S> | null, property?: keyof S, propertyUpdates: any = {}) {
    const newState: S = {...hasState.state, ...(updates == null ? {} : updates)};
    if (property) {
        newState[property] = {...newState[property], ...propertyUpdates};
    }
    hasState.setState(newState);
}

/**
 * Given a  HasState instance, update the specific property on the state using the updates provided.
 *
 * @param hasState The target state object.
 * @param property Optional property to apply specific changes to.
 * @param updates The state updates to apply.
 */
export function updateComponentStateProperty<P, S, K extends keyof S>(hasState: IHasState<P, S>, property: K, updates: any = {}) {
    updateComponentState(hasState, null, property, updates)
}

/**
 * A function that updates a value on the state and returns the new state.
 */
export type PropertySetter<T, S> = (value: T) => S;

/**
 * An interface so access and update a property on a state object
 */
export interface IStateAccessor<S, T = any> {

    /**
     * Set the property value and return the new state.
     */
    set(value: T): S;

    /**
     * get the value of a property this accessor
     * is permitted to.
     */
    get(): T;
}

