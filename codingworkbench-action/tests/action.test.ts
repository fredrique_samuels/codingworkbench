import {deepStrictEqual} from "assert";

import CwbAction from "../action";

describe("action.test.ts", () => {

    it ("ok", () => {
        const expected = {
            error: false
        };
        deepStrictEqual(new CwbAction.ResponseBuilder().build(), expected);
    });

    it ("redirect", () => {
        const expected = {
            error: false,
            redirect: "http://aurl"
        };
        deepStrictEqual(new CwbAction.ResponseBuilder().redirect("http://aurl").build(), expected);
    });

    it ("error", () => {
        const expected: CwbAction.IResponse = {
            error: true,
            message: "An Error Message"
        };
        deepStrictEqual(new CwbAction.ResponseBuilder().error("An Error Message").build(), expected);
    });

    it ("json", () => {
        const expected = JSON.stringify({
            error: true,
            message: "An Error Message"
        });
        deepStrictEqual(new CwbAction.ResponseBuilder().error("An Error Message").json(), expected);
    });


});
