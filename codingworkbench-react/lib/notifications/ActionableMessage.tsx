import * as React from 'react';
import clsx from 'clsx';

import SnackbarContent from '@material-ui/core/SnackbarContent';

import styles from "./styles"
import Message from "./Message";


/**
 * A message that supports user action buttons.
 *
 * @constructor
 */
function ActionableMessage(props: ActionableMessage.Props) {
    const classes = styles();
    const { className, actions,  message, type, ...other } = props;

    return (
        <SnackbarContent
            className={clsx(classes[type], className)}
            aria-describedby="client-snackbar"
            message={ <Message {...props} /> }
            action={actions}
            {...other}
        />
    );
}


namespace ActionableMessage {

    export interface Props extends Message.Props {
        /**
         * user class names to be added to the standard styling.
         */
        className?: string;

        /**
         * Action components. While not enforced, IconButton components work best.
         */
        actions: React.ReactNode
    }

}

export default ActionableMessage;
