import * as fs from "fs";
import IFileHandle from "./IFileHandle";


namespace FileIO {


    /**
     * Load a file from disk.
     *
     * @param path The file path.
     */
    export function loadFromDisk(path: string): IFileHandle {
        return new DiskFile(path);
    }

    /**
     * Recursively create a directory and it's parents.
     *
     * @param path
     */
    export async function mkdirRecursive(path: string): Promise<any> {
        return new Promise((res, rej) => {
            fs.mkdir(path,
                {recursive: true},
                err => err ? rej(err) : res()
            );
        })
    }


    class DiskFile implements IFileHandle {

        private readonly _path: string;

        constructor(path: string) {
            this._path = path;
        }

        get path(): string {
            return this._path
        }

        readString(): Promise<string> {
            return new Promise((res, rej) => {
                fs.readFile(this.path, {encoding: "utf8"}, (err, data) => {
                    if (err) rej(err);
                    else res(data)
                })
            })
        }

        writeString(data: string): Promise<any> {
            return new Promise((res, rej) => {
                fs.writeFile(this.path, data, (err) => {
                    if (err) rej(err);
                    else res(data)
                })
            });
        }

        delete(): Promise<void> {
            return new Promise((res, rej) => {
                fs.unlink(this.path, (err) => {
                    if (err) rej(err);
                    else res()
                })
            });
        }

        exists(): boolean {
            return fs.existsSync(this.path);
        }

    }
}

export default FileIO;
