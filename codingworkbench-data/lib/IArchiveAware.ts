/**
 * Interface for any object that should not be delete
 * from storage. archived should be set to true to indicate
 * that it should not be include in updates or query results.
 */
import {DbColumn} from "./DbColumn";

export interface IArchiveAware {
    archived: boolean;
}

/**
* Create data for an archived record.
*/
export function archive(): IArchiveAware {
    return { archived: true }
}

/**
 * Create new Archived data.
 */
export function createArchive(): IArchiveAware {
    return { archived: false }
}

/**
 * Create new Archived data.
 */
export function updateArchive(dst: IArchiveAware, src: IArchiveAware): void {
    dst.archived = src.archived;
}

/**
 * Get the columns mapping for the audit fields.
 */
export function archive_columns(): DbColumn[] {
    return [
        new DbColumn("archived"),
    ];
}

export default IArchiveAware;
