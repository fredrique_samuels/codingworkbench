
// imports
const {
    Avatar,
    Button,
    colors,
    createMuiTheme,
    CssBaseline,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Icon,
    IconButton,
    MuiThemeProvider,
    Paper,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Toolbar,
    Typography,
    withStyles,
} = window['material-ui'];



// Enable the dark theme
const theme = createMuiTheme({
    palette: {
        type: 'dark',
    }
});

class AbstractRoot extends React.Component {
    state = {};
    model = {};

    render() {
        const {classes} = this.props
        return (
            <MuiThemeProvider theme={theme}>
                <CssBaseline />
                <div className={classes.root}>
                    <Button >Hello</Button>
                </div>
            </MuiThemeProvider>
        )
    }

    componentWillMount() {
    }

    static css() {
        return {
            root: {
                flexGrow: 1,
            }
        }
    }
}

const Root = withStyles(AbstractRoot.css)(AbstractRoot);

/**
 * Entry point function.
 */
function main() {
    // Obtain the root
    const rootElement = document.getElementById('app');
    // Use the ReactDOM.render to show your component on the browser
    ReactDOM.render(
        <Root />,
        rootElement
    )
}

main();

