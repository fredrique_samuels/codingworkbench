import CwbNode from "../node"

interface Employee {
    name: string;
}

interface EmployeeStore {
    employees: {[s: string]: Employee }
}

// node data accessors
const get_name= (data: Employee) => data ? data.name : null;
const get_store_entry = (data: EmployeeStore | null, name: string) => data ? data.employees[name] : null;
const has_store_entry = (data: EmployeeStore | null, name: string): boolean => (data !== null && Object.keys(data.employees).includes(name));

// function to print the name attribute
const print_name = (nameAttribute: CwbNode.IAttribute) => console.log(`Employee "${nameAttribute.name}"=${nameAttribute.value}`);
const print_employee = (iElement: CwbNode.IElement) => iElement.get("name").asAttribute(print_name);
const search_employee = (iElement: CwbNode.IElement) => iElement.get("employee1").asElement(print_employee);

// Create a test employee
const employeeObj: Employee = { name: "Jack" };

// Create a test store
const employeeStore: EmployeeStore = {
    employees:  {
        employee1: employeeObj
    }
};

// Describe the attribute and how to pull the value
const nameAttributeDef = new CwbNode.AttributeBuilder<string, Employee>(get_name).build();
const employeeElementDef = new CwbNode.ElementBuilder<Employee, EmployeeStore>("employee-tag", get_store_entry)
    .addChild("name", nameAttributeDef)
    .build();
const employeeGroupDef = new CwbNode.ElementGroupBuilder("employees-tag", employeeElementDef, has_store_entry).build();

// Build the root node from the employee object
const nodeTree = new CwbNode.NodeTreeBuilder("employee-store-tag", employeeStore)
    .addChild("employees", employeeGroupDef)
    .build();

// get the name attribute
nodeTree.get("employees").asElement(search_employee);



