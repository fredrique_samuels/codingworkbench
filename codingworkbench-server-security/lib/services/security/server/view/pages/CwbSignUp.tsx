import * as React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Copyright from "../components/Copyright";
import {IFormValidation} from "@codingworkbench/react/form/FormInputs";
import * as ReactDOM from "react-dom";
import SecurityViewController from "../SecurityViewController";
import {SecurityEmailInput, SecurityPasswordInput} from "../components";
import {ISecurityForm, SecurityForm} from "../components/SecurityForm";


const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function CwbSignUp() {
    const classes = useStyles();

    const [state, setState] = React.useState<ISecurityForm>(SecurityForm.createForm());
    const stateControl = {state, setState};
    const handleSignUp = () => {
        setState(SecurityForm.startLoading(state));
        SecurityViewController.do_signup(state.inputs)
            .then((validation: IFormValidation) => setState(SecurityForm.processValidationResponse(state, validation)))
            .catch(console.error)
    };

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline />
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon />
                </Avatar>
                <Typography component="h1" variant="h5">
                    Sign up
                </Typography>
                {
                    !state.validation.passed && !state.loading
                        ? (
                            <Typography component="h1" variant="h6" color={"error"}>
                                {SecurityForm.getMessage(state.validation)}
                            </Typography>
                        )
                        : null
                }
                <form className={classes.form} noValidate>
                    <Grid container spacing={2}>
                        <Grid item xs={12}>
                            <SecurityEmailInput {...stateControl} />
                        </Grid>
                        <Grid item xs={12}>
                            <SecurityPasswordInput {...stateControl} />
                        </Grid>
                        {/*<Grid item xs={12}>*/}
                        {/*    <SecurityReCaptcha {...stateControl} />*/}
                        {/*</Grid>*/}
                    </Grid>
                    <Button
                        type="button"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                        onClick={handleSignUp}
                        disabled={state.loading}
                    >
                        Sign Up
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <Link href="#" variant="body2" onClick={SecurityViewController.goto_signin} >
                                Already have an account? Sign in
                            </Link>
                        </Grid>
                    </Grid>
                </form>
            </div>
            <Box mt={5}>
                <Copyright />
            </Box>
        </Container>
    );
}

ReactDOM.render(<CwbSignUp />, document.getElementById("root"));
