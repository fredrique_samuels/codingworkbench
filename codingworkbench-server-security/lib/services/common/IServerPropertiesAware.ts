import {create_security_links, ISecurityLinks} from "../security/api/links";

export interface IServerProperties {
    jsPath: string;
    hostAlias: string;
    staticRoot: string;
    securityLinks: ISecurityLinks
}

export interface IServerPropertiesAware {
    serverProperties: IServerProperties;
}

export function server_properties_create(serverProperties: Partial<IServerProperties>): IServerProperties {
    return {
        hostAlias: serverProperties.hostAlias || "http://localhost:8080",
        jsPath: serverProperties.jsPath || "/static/js",
        securityLinks: serverProperties.securityLinks || create_security_links(),
        staticRoot: serverProperties.staticRoot || "./web-app"
    }
}

