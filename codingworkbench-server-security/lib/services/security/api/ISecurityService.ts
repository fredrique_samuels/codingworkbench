import CwbAction from "@codingworkbench/action";

import ICwbAccount from "./ICwbAccount";
import ICwbUser from "./ICwbUser";
import {ID} from "@codingworkbench/data/IUnique";

export interface ISecurityService {
    /**
     * Authenticate the user credentials.
     * Promise fails if the credentials are not valid.
     *
     * @param email {String}
     * @param password {String}
     * @return {CwbAccount} The default account for the user.
     * @error EMAIL_OR_PASS_INVALID
     */
    authenticate(email: string, password: string): Promise<CwbAction.IResponse<ICwbAccount>>;

    /**
     * Register a new user onto the system. This will also create a new account.
     * The password must be of medium to strong strength to be accepted
     *
     * @param email {String} User mail
     * @param password {String} The user password.
     * @return created Account.
     * @error EMAIL_IN_USE
     * @error EMAIL_INVALID
     * @error PASSWORD_WEAK
     */
    registerNewUser(email: string, password: string): Promise<CwbAction.IResponse<ICwbAccount>>;

    /**
     * Validate the account.
     *
     * @error INVALID_TOKEN
     */
    activateAccount(authenticationToken: string | null): Promise<CwbAction.IResponse>;

    /**
     * Check if the account activation token is live or not.
     * @param token
     */
    hasPendingActivationAccounts(email: string): Promise<CwbAction.IResponse<boolean>>

    /**
     * Request that an password reset email be sent to the registered user.
     * @error EMAIL_NOT_REGISTERED
     */
    sendPasswordResetLink(email: string): Promise<CwbAction.IResponse>;

    /**
     * Request that an password reset email be sent to the registered user.
     * @error EMAIL_NOT_REGISTERED
     * @error PASSWORD_WEAK
     * @error PASSWORD_VERIFICATION_FAILED
     */
    resetPassword(email: string, password: string, verifyPassword: string): Promise<CwbAction.IResponse>;

    /**
     * Send an activation link if the account has not been activated yet.
     *
     * @param email
     */
    sendActivationLink(email: string): Promise<CwbAction.IResponse>;

    getAccountUsers(account: ICwbAccount): Promise<ICwbUser[]>

    getUserFromEmail(email: string): Promise<ICwbUser | null>;

    getAccountById(id: ID): Promise<ICwbAccount | null>;
}

export default ISecurityService;
