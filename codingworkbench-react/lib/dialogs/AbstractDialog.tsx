import * as React from "react";

import PageDialog, {PageDialogProps} from "./PageDialog";
import IPage, {safe_page} from "../page/IPage";
import {PageDialogOptions} from "./PageDialog";

export class AbstractDialog
    <
        P extends AbstractDialogProps = AbstractDialogProps,
        S extends AbstractDialogState = AbstractDialogState
    >
    extends React.Component<P, S>
{

    constructor(props: P) {
        super(props);
    }

    public get page(): IPage { return safe_page(this.props.page) }

    render() {
        const dialogProps: PageDialogProps = {
            ... this.props,
            ... {
                title: this.props.title || "",
                text: this.props.text || "",
                content: <React.Fragment>{this.props.content || null} {this.props.children}</React.Fragment>,
                actions: this.props.actions,
                closeOnAwayClick: this.props.closeOnAwayClick
            }
        };

        return (
            <PageDialog { ...dialogProps } />
        )
    }

    public static createProps<S>(o: PageDialogOptions<S>): AbstractDialogProps {
        return {
            ... PageDialog.createProps(o),
            closeOnAwayClick: false,
            variant: "dialog"
        }
    }

}

export interface AbstractDialogProps extends PageDialogProps {
    page?: IPage;
}

export interface AbstractDialogState {

}

export default AbstractDialog;
