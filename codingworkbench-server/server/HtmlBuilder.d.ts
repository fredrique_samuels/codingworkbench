import { HtmlModel } from "./HtmlModel";
/**
 * Builder wrapped arounf HtmlTemplate.
 *
 * @see HtmlTemplate
 */
export declare class HtmlBuilder {
    protected model: HtmlModel;
    private readonly template;
    /**
     * @constructor
     */
    constructor();
    setModel(model: HtmlModel): this;
    /**
     * @see HtmlTemplate.head
     */
    addHead(tag: string): this;
    /**
     * @see HtmlTemplate.body
     */
    addBody(bCode: string): this;
    /**
     * @see HtmlTemplate.preBody
     */
    addPreBody(bCode: string): this;
    build(): string;
}
