
import IMapLike from "@codingworkbench/common/IMapLike";
import welcome_email from "./templates/welcome_email";
import {IEmailTemplateProvider} from "../../email/api/IEmailTemplateProvider";
import {EmailTemplateType, IEmailTemplate} from "../../email/api/IEmailTemplate";


export enum SecurityEmailKeys {
    /**
     * Email template that is passed the params
     *      link: string
     */
    PASSWORD_RESET_REQUEST='PASSWORD_RESET_REQUEST',

    /**
     * Email template that is passed the params
     *      link: string
     */
    ACCOUNT_ACTIVATION_REQUIRED="ACCOUNT_ACTIVATION_REQUIRED"
}

function password_reset_email(params: IMapLike<any>): string {
    const link: string = params.get("link") as string;
    return `<!DOCTYPE html><html><body><b>You have request a password reset on your account. Follow the <a href="${link}" >link</a> to complete the request.</b></body></html>`;
}
function account_activation_required(params: IMapLike<any>): string {
    return welcome_email(params);
}

export default class SecurityEmails implements IEmailTemplateProvider {
    private readonly _companyName: string;

    constructor(companyName: string) {
        this._companyName = companyName;
    }

    get(key: string): IEmailTemplate | null {
        if (key === SecurityEmailKeys.PASSWORD_RESET_REQUEST.toString()) {
            return  {
                type: EmailTemplateType.FUNCTION,
                title: `${this._companyName} Password Reset Request`,
                templateFunction: password_reset_email
            };
        }

        if (key === SecurityEmailKeys.ACCOUNT_ACTIVATION_REQUIRED.toString()) {
            return   {
                type: EmailTemplateType.FUNCTION,
                title: `${this._companyName} Account Verification`,
                templateFunction: account_activation_required
            };
        }

        return null;
    }
}
