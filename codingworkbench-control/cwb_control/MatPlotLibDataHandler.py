import matplotlib.pyplot as plt
from cwb_control.PlotDataHandler import PlotDataHandler


class MatPlotLibDataHandler(PlotDataHandler):
    id = "MatPlotLibDataHandler"

    @staticmethod
    def show_plot(ds):
        plt.figure()

        plot_keys = PlotDataHandler.get_plot_keys(ds)
        grid = 100 * min(9, len(plot_keys)) + 10
        index = 0
        for k in plot_keys:
            if index == 9:
                break
            index = index + 1
            d = ds.get(k)
            t = list(map(lambda x: x[0], d))
            d = list(map(lambda x: x[1], d))

            plt.subplot(grid + index)
            plt.ylabel(k.replace("." + PlotDataHandler.id, ""))
            plt.plot(t, d, 'b')

        plt.show()

