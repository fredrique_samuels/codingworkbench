import {HtmlBuilder} from "@codingworkbench/server/HtmlBuilder";


export class ReactHtmlBuilder extends HtmlBuilder {

    private pr: PathResolver;
    private readonly isProd: boolean = false;

    constructor(pathResolver: PathResolver, isProd: boolean = false) {
        super();
        this.isProd = isProd;
        this.pr = pathResolver;
        
        this.addBody(`<div id="root"></div>`);
        this.addHead(`<link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" />`);
        this.addHead(`<script src="${this.resourcePath(this.reactJs)}"></script>`);
        this.addHead(`<script src="${this.resourcePath(this.reactDomJs)}"></script>`)
    }

    get reactJs(): string {
        if (this.isProd) throw new Error("Specify react production libs!!");
        return "react.development.js";
    }

    get reactDomJs(): string {
        if (this.isProd) throw new Error("Specify react production libs!!");
        return "react-dom.development.js";
    }

    public addReactViewJs(viewFile: string): this {
        this.addBody(`<script src="${this.resourcePath(viewFile)}"></script>`);
        return this;
    }

    private resourcePath(viewFile: string) {
        return this.pr.resolve(viewFile);
    }
}

export interface PathResolver {

    /**
     * @return The path to the file.
     * @param res The resource needed.
     */
    resolve(res: string): string;
}

