
import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import {RequestContext} from "@codingworkbench/server/common";
import CwbAction from "@codingworkbench/action";

import {ISecurityHandlerContext, ISecurityHandlerFactoryContext} from "../../api/context";
import {web_respond_redirect, web_respond_with_500, web_respond_with_error, web_respond_with_ok} from "../../api/response";
import ICwbAccount from "../../api/ICwbAccount";
import CwbLog from "@codingworkbench/log";

async function handlerAsync<C extends ISecurityHandlerContext>(requestContext: RequestContext<C>) {
    const {request, response} = requestContext;
    const {securityService, securityLinks} = requestContext.context;
    const {email} = request.body;

    try {
        // authenticate
        const actionResponse: CwbAction.IResponse<ICwbAccount> = await securityService.sendActivationLink(email);
        if (actionResponse.error) {
            CwbLog.warning(new Error(`Resend activation link failed ${email} ${actionResponse.message}`));
            web_respond_redirect(requestContext, securityLinks.signInPage);
            return
        }
        web_respond_with_ok(requestContext);
    } catch (e) {
        web_respond_with_500(e, requestContext);
    }
}

export default function DoResendLink<C extends ISecurityHandlerContext>(factoryContext: ISecurityHandlerFactoryContext): IRequestHandler<C> {
    return new RequestHandlerBuilder(factoryContext.links.doResendActivationLink, () => {}, handlerAsync).setMethod("post").setAsync().build();
}

