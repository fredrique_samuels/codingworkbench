import * as body_parser from "body-parser";
import * as redisConnect from 'connect-redis';
import * as cookieParser from "cookie-parser";
import * as express from "express";
import * as fileUpload from "express-fileupload";
import * as session from "express-session";
import * as path from "path";
import * as process from "process";
import * as redis from "redis";
import * as common from "./common";

import CwbLog from "@codingworkbench/log";

import {ExpressHttpFactory, expressTransformHandler, expressTransformHandlerAsync} from "./_express";
import IRequestHandler from "./IRequestHandler";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";


/**
 * Simple next function.
 */
export type ExpressApplicationNextFunction = express.NextFunction;

/**
 * Middleware function called before handlers passed to ExpressApplication#addHandlers.
 * Useful for filtering expires sessions or access control requests or
 */
export type ExpressApplicationMiddleware = (req: common.Request, res: common.Response, next: ExpressApplicationNextFunction) => any;


export interface IExpressApplication<ContextType> {
    getApp(): express.Express;
    addMiddleware(mw: ExpressApplicationMiddleware): this;
    addHandlers(handlers: IRequestHandler<ContextType>[]): this;
    serveStatic(path: string, resolve: string): this;
}

/**
 * Options needed to run express.
 */
export interface ExpressApplicationOptions<Context=any> {
    /**
     * Path to the a connection.properties file.
     */
    connectionPropertiesFile?: string;

    /**
     * The port the server should listen on.
     */
    port?: number;

    /**
     * File upload size limit in bytes. Defaults to 50MB.
     */
    fileUploadLimit?: number;

    /**
     * A function called just before  app.listen() is called.
     * Useful for added custom setup on the underlying express.Express object.
     */
    setupFunc?: (app: express.Express) => void

    /**
     * Function to be called when the server starts up.
     */
    onStartFunc?: (applicationContext: any, expressApplication: IExpressApplication<Context>) => Promise<void>

    /**
     * Indicate if session middleware settings.
     */
    enabledSessions?: boolean;

    /**
     * Flag to switch on in memeory data access for testing
     */
    useMemoryStorage?: boolean;
}

/**
 * Constant for one megabyte.
 */
export const ONE_MEGA_BYTE = 1024 * 1024;

/**
 * Bind a all the RequestPath handlers to an  express.Express.
 *
 * @param app The target application
 * @param handlers The list of handlers to bind
 * @param context The web request context.
 */
export function bind_handlers_to_app<C={}>(app: express.Express, handlers: IRequestHandler<C>[], context: C) {
    // map handlers.
    handlers.forEach(
        routeDef => {
            const {method, url, handler, isAsync, handlerAsync} = routeDef;
            CwbLog.info(`Adding handler : ${method} ${url} `);

            let _method = method ? method : "get";

            if(_method.toLowerCase() === "get") {
                if (isAsync) {
                    app.get(url,  expressTransformHandlerAsync(handlerAsync, context));
                } else {
                    app.get(url, expressTransformHandler(handler, context));
                }
                return
            }

            if(_method.toLowerCase() === "post") {
                if (isAsync) {
                    app.post(url,  expressTransformHandlerAsync(handlerAsync, context));
                } else {
                    app.post(url,  expressTransformHandler(handler, context));
                }
                return
            }

            throw new Error(`Error binding "${url}". Invalid Method "${method}"`)
        }
    )
}

export function setup_express(options: ExpressApplicationOptions, app: express.Express) {
    app.use('/static', express.static( path.resolve("./public") ) );
    app.use(cookieParser("secretSign#143_!223"));
    app.use(body_parser.urlencoded({ extended: false }));
    app.use(body_parser.json());

    // default options upload options
    // https://www.npmjs.com/package/express-fileupload
    app.use(fileUpload(
        {
            limits: { fileSize: options.fileUploadLimit || ONE_MEGA_BYTE *  50 }
        }
    ));
}

export function configure_redis(connectionProperties: IPropertiesReader | null, options: ExpressApplicationOptions, app: express.Express) {
    if(!options.enabledSessions || !connectionProperties ) {
        return;
    }

    const env = process.env.NODE_ENV || "test";
    const host = connectionProperties.getString(`redis.${env}.host`, "127.0.0.1");
    const port = connectionProperties.getNumber(`redis.${env}.port`, 6379);
    const ttl = connectionProperties.getNumber(`redis.${env}.ttl`, 86400);
    const password = connectionProperties.getString(`redis.${env}.password`, "");

    CwbLog.info(`connecting to redis server ${host}:${port} ttl=${ttl}`);

    const redisStore = redisConnect(session);

    const redisClient = redis.createClient({host: host, port: port, password: password ? password : undefined});
    redisClient.on('error', (err: Error) => {
        if (err) {
            CwbLog.error(err);
        }
    });

    const sessionOptions = {
        secret: 'ThisIsHowYouUseRedisSessionStorage',
        resave: false,
        saveUninitialized: false,
        store: new redisStore({client: redisClient, ttl: ttl}),
    };
    app.use(session(sessionOptions));
}

export function bind_user_defined_middleware(middlewares: ExpressApplicationMiddleware[], app: express.Express) {
    const httpFactory = new ExpressHttpFactory();
    middlewares.map(mw => {
        return (req: express.Request, res: express.Response, next: express.NextFunction) => {
            mw(httpFactory.createRequest(req), httpFactory.createResponse(res), next);
        };
    })
        .forEach(mw => {
            app.use(mw);
        });
}

