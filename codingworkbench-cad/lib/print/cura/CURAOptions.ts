import IOption from "../../common/IOption";


export const ICURA_PROFILE_CREALITY_ENDER3: IOption = {
    key: "creality_ender3",
    label: "Creality Ender3",
    value: "creality_ender3.def.json"
} 

