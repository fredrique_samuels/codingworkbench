import * as React from "react"
import {IHasState, updateComponentStateProperty, updateComponentState, ReactUpdateState} from "./IHasState";


/**
 * An React Component with methods to update specific state components.
 */
export default class HasStateComponent <P, S >
    extends React.Component<P, S>
    implements IHasState<P, S> {

    /**
     * @see IHasState#updateStateProperty
     */
    updateStateProperty<K extends keyof S>(property: keyof S, updates: any = {}) {
        updateComponentStateProperty(this, property, updates);
    }

    /**
     * @see IHasState#updateState
     */
    updateState<K extends keyof S>(updates: Pick<S, K>, property?: keyof S, propertyUpdates: any = {}) {
        updateComponentState(this, null, property, updates);
    }

    /**
     * @see IHasState#setState
     */
    setState<K extends keyof S>(state: ReactUpdateState<P, S, K>, callback?: () => void): void {
        super.setState(state, callback);
    }
}
