import * as React from 'react';

import Dialog, {DialogProps} from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import IPage from "../page/IPage";
import {IOpenClose} from "../common/OpenClose";


export class PageDialog extends React.Component<PageDialogProps> {

    public render() {
        const {props} = this;

        const closeOnWayClick = props.closeOnAwayClick
            ? () => { if(props.onClose) props.onClose() }
            : undefined;


        const dialogProps: DialogProps = {
            open: props.open,
            onClose : closeOnWayClick
        };

        if(props.variant === "user") {
            return <Dialog
                {...dialogProps}
                fullWidth={ Boolean(dialogProps.maxWidth) }
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                {props.content}
            </Dialog>
        }

        return (
            <Dialog
                {...dialogProps}
                fullWidth={true}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                {
                    props.title
                        ? <DialogTitle id="alert-dialog-title">{props.title}</DialogTitle>
                        : null

                }
                <DialogContent>
                    {
                        props.text
                            ?   (
                                <DialogContentText id="alert-dialog-description">
                                    { props.text }
                                </DialogContentText>
                            )
                            : null
                    }
                    {
                        props.content
                    }
                </DialogContent>
                <DialogActions>
                    { props.actions }
                </DialogActions>
            </Dialog>
        );
    }

    public static createPropsForPage<S>(page: IPage<S>, dialogKey: keyof S, options: Partial<DialogOptions>): PageDialogProps {
        const finalCloseHandler = (results: any) => {
            try {
                if (options.onClose) {
                    options.onClose(results);
                }
            } catch(e) {
                page.logError(e)
            } finally {
                page.close(dialogKey);
            }
        };

        return {
            ... {
                open: false,
                closeOnAwayClick: true,
                variant: "dialog",
                maxWidth: false
            },
            ... options,
            ... {
                onClose : finalCloseHandler
            }
        }
    }

    public static createProps<S>(options: PageDialogOptions<S>): PageDialogProps {
        return PageDialog.createPropsForPage(options.page, options.dialogKey, options);
    }
}

export interface DialogOptions {
    title?: string,
    text?: string,
    content?: React.ReactNode,
    actions?: JSX.Element[],
    onClose?: (formData?: any) => void
}

export interface PageDialogOptions<S> extends DialogOptions {
    page: IPage<S>;
    dialogKey: keyof S;
}

export interface PageDialogProps extends IOpenClose, DialogOptions {
    closeOnAwayClick: boolean,
    variant: "dialog" | "user"
    maxWidth:  'xs' | 'sm' | 'md' | 'lg' | 'xl' | false
}

export default PageDialog;
