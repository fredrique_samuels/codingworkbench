import CwbInject from "@codingworkbench/common/inject";

import ISecurityAwareContext from "../api/context";
import IEmailAwareContext from "../../email/api/IEmailAwareContext";
import {EmailSourceCredentials} from "../../email/bizimpl/EmailService";
import {EmailSource} from "../../email/api/EmailSource";
import SecurityEmails from "../bizimpl/emails";
import ISecurityService from "../api/ISecurityService";
import SecurityService from "../bizimpl/SecurityService";
import {IEmailTemplateProvider} from "../../email/api/IEmailTemplateProvider";


async function create_security_emailSourceCredentials(deps: CwbInject.Injector<IEmailAwareContext>): Promise<EmailSourceCredentials> {
    const emailSourceCredentials: EmailSourceCredentials = {};
    const nodeEnv = process.env.NODE_ENV || "test";
    const connectionProperties = await deps.get("connectionProperties");

    emailSourceCredentials[EmailSource.NO_REPLY.toString()] = {
        user: connectionProperties.getString(`mailer.${nodeEnv}.noreply.email`),
        secret: connectionProperties.getString(`mailer.${nodeEnv}.noreply.password`)
    };

    return emailSourceCredentials;
}

async function create_security_emailTemplateProvider(deps: CwbInject.Injector<ISecurityAwareContext>): Promise<IEmailTemplateProvider> {
    return new SecurityEmails("Coding Workbench");
}

async function create_securityCallbackHost(deps: CwbInject.Injector<ISecurityAwareContext>): Promise<string> {
    return "http://localhost:8080";
}

async function create_securityService(dependencies: CwbInject.Injector<ISecurityAwareContext>): Promise<ISecurityService> {
    return new SecurityService(
        await dependencies.get('sqlDao'),
        await dependencies.get('securityCallbackHost'),
        await dependencies.get('securityLinks'),
        await dependencies.get('encryptionPolicy'),
        await dependencies.get("emailService"),
        await dependencies.get("emailTemplateProvider")
    );
}

export default class SecurityAwareInjector extends CwbInject.Module<ISecurityAwareContext> {

    constructor() {
        super();
            this.bind("emailTemplateProvider", create_security_emailTemplateProvider)
                .bind("securityCallbackHost", create_securityCallbackHost)
                .bind("emailSourceCredentials", create_security_emailSourceCredentials)
                .bind("securityService", create_securityService)
                .bind("securityLinks", async (injector) => (await (injector.get("serverProperties"))).securityLinks);
    }
}
