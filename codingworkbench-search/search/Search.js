"use strict";
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.INF_PAGE_SIZE = Number.MAX_SAFE_INTEGER;
function mapResults(searchResults, transformer) {
    return __assign({}, searchResults, { results: searchResults.results.map(transformer) });
}
exports.mapResults = mapResults;
function search_results_from_array(array) {
    return {
        results: array.slice(),
        pages: 1,
        currentPage: 1,
    };
}
exports.search_results_from_array = search_results_from_array;
// TODO build from an existing SearchParams
var SearchBuilder = /** @class */ (function () {
    function SearchBuilder(searchParams) {
        this._filters = new SearchFiltersBuilder(searchParams ? searchParams.filters : {});
        this._pagination = new PaginationParamsBuilder(searchParams ? searchParams.pagination : undefined);
    }
    Object.defineProperty(SearchBuilder.prototype, "filters", {
        get: function () {
            return this._filters;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(SearchBuilder.prototype, "pagination", {
        get: function () {
            return this._pagination;
        },
        enumerable: true,
        configurable: true
    });
    SearchBuilder.prototype.build = function () {
        return {
            filters: this._filters.build(),
            pagination: this._pagination.build(),
        };
    };
    return SearchBuilder;
}());
exports.SearchBuilder = SearchBuilder;
var SearchFiltersBuilder = /** @class */ (function () {
    function SearchFiltersBuilder(f) {
        this.filters = f ? this.copy(f) : {};
    }
    SearchFiltersBuilder.prototype.addFilter = function (name, value, operation) {
        this.filters[name] = {
            value: value,
            operation: operation || 'eq',
        };
        return this;
    };
    SearchFiltersBuilder.prototype.addAll = function (filters) {
        this.filters = __assign({}, this.filters, filters);
        return this;
    };
    SearchFiltersBuilder.prototype.build = function () {
        return this.copy(this.filters);
    };
    SearchFiltersBuilder.prototype.copy = function (d) {
        var r = {};
        Object.keys(d).forEach(function (k) { return r[k] = __assign({}, d[k]); });
        return r;
    };
    return SearchFiltersBuilder;
}());
var PaginationParamsBuilder = /** @class */ (function () {
    function PaginationParamsBuilder(p) {
        this.params = p ? __assign({}, p) : PaginationParamsBuilder.init();
    }
    PaginationParamsBuilder.prototype.setPageSize = function (o) {
        this.params.pageSize = o;
        return this;
    };
    PaginationParamsBuilder.prototype.setOffset = function (o) {
        this.params.offset = o;
        return this;
    };
    PaginationParamsBuilder.prototype.setSortField = function (o) {
        this.params.sortField = o;
        return this;
    };
    PaginationParamsBuilder.prototype.setSortOrder = function (o) {
        this.params.sortOrder = o;
        return this;
    };
    PaginationParamsBuilder.prototype.build = function () {
        return __assign({}, this.params);
    };
    PaginationParamsBuilder.init = function () {
        return {
            pageSize: 25,
            offset: 0,
            totalRecords: 0,
            sortField: null,
            sortOrder: 'ASC',
        };
    };
    return PaginationParamsBuilder;
}());
var SearchResultBuilder = /** @class */ (function () {
    function SearchResultBuilder(results) {
        this._results = {
            results: results.slice(),
            currentPage: 1,
            pages: 1
        };
    }
    SearchResultBuilder.prototype.build = function () {
        return __assign({}, this._results);
    };
    return SearchResultBuilder;
}());
exports.SearchResultBuilder = SearchResultBuilder;
function fix_search_object(userSearch) {
    // make sure there are not undefined fields
    var pageParams = new PaginationParamsBuilder(userSearch.pagination).build();
    var filters = new SearchFiltersBuilder(userSearch.filters).build();
    var pageSize = pageParams.pageSize <= 0 ? 25 : pageParams.pageSize;
    var invalidOffset = pageParams.offset > 0 && pageParams.offset % pageSize !== 0;
    // make sure offset is modulus 0 off page size
    return __assign({}, userSearch, { pagination: __assign({}, pageParams, { offset: invalidOffset ? 0 : pageParams.offset, pageSize: pageSize }), filters: __assign({}, filters) });
}
function create_single_page_results(results, search) {
    return {
        results: results.slice(),
        refresh: __assign({}, search, { pagination: __assign({}, search.pagination, { totalRecords: results.length }) }),
        currentPage: 1,
        pages: 1
    };
}
function is_single_page_results(results, pageParams) {
    return results.length < pageParams.pageSize && pageParams.offset === 0;
}
function create_paged_results(dataFunctions, search, results) {
    return __awaiter(this, void 0, void 0, function () {
        var pagination, inf_page_size, total, res, totalPages, currentPage, create_prev_search, create_next_search;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    pagination = search.pagination;
                    inf_page_size = (pagination).pageSize === exports.INF_PAGE_SIZE;
                    return [4 /*yield*/, dataFunctions.totalSupplier(search)];
                case 1:
                    total = _a.sent();
                    res = inf_page_size ? 0 : total % (pagination).pageSize;
                    totalPages = inf_page_size ? 1 : ((total - res) / (pagination).pageSize) + (res > 0 ? 1 : 0);
                    currentPage = inf_page_size ? 1 : (pagination).offset == 0 ? 1 : ((pagination).offset / (pagination).pageSize) + 1;
                    create_prev_search = function () {
                        return currentPage > 1
                            ? __assign({}, search, { pagination: __assign({}, pagination, { offset: pagination.offset - pagination.pageSize, totalRecords: total }) }) : undefined;
                    };
                    create_next_search = function () {
                        return currentPage < totalPages
                            ? __assign({}, search, { pagination: __assign({}, pagination, { offset: pagination.offset + pagination.pageSize, totalRecords: total }) }) : undefined;
                    };
                    return [2 /*return*/, {
                            results: results.slice(),
                            prev: create_prev_search(),
                            refresh: __assign({}, search, { pagination: __assign({}, pagination, { totalRecords: total }) }),
                            next: create_next_search(),
                            currentPage: currentPage,
                            pages: totalPages
                        }];
            }
        });
    });
}
function do_paged_search(userSearch, dataFunctions) {
    return __awaiter(this, void 0, void 0, function () {
        var search, results;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    search = fix_search_object(userSearch);
                    return [4 /*yield*/, dataFunctions.dataSupplier(search)];
                case 1:
                    results = _a.sent();
                    if (is_single_page_results(results, search.pagination)) {
                        return [2 /*return*/, create_single_page_results(results, search)];
                    }
                    return [4 /*yield*/, create_paged_results(dataFunctions, search, results)];
                case 2: return [2 /*return*/, _a.sent()];
            }
        });
    });
}
exports.do_paged_search = do_paged_search;
//# sourceMappingURL=Search.js.map