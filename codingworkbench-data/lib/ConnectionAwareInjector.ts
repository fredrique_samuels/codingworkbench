import CwbInject from "@codingworkbench/common/inject";
import CwbLog from "@codingworkbench/log";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
import DbConnection from "./DbConnection";
import MysqlConnection from "./MysqlConnection";
import MongoConnection from "./MongoConnection";
import CrudDataAccessor from "./CrudDataAccessor";
import IConnectionAware from "./IConnectionAware";


async function create_sqlConnection(deps: CwbInject.Injector<IConnectionAware>)
    : Promise<DbConnection>
{
    const dbConnection = new MysqlConnection(MysqlConnection.getConnectionConfigFromReader(await deps.get("connectionProperties")));
    try {
        await dbConnection.connect();
    } catch (e) {
        CwbLog.warning(e)
    }
    return dbConnection;
}

async function create_mongoConnection(deps: CwbInject.Injector<IConnectionAware>)
    : Promise<DbConnection>
{
    const reader = await deps.get("connectionProperties");
    const dbConnection = new MongoConnection(reader, "");
    try {
        await dbConnection.connect();
    } catch (e) {
        CwbLog.warning(e)
    }
    return dbConnection;
}

export default class ConnectionAwareInjector extends CwbInject.Module<IConnectionAware> {

    constructor(connectionProperties: IPropertiesReader) {
        super();
        this.bind("connectionProperties", async (): Promise<IPropertiesReader> => connectionProperties);
        this.bind("sqlConnection", create_sqlConnection);
        this.bind("sqlDao", async (deps): Promise<CrudDataAccessor> => new CrudDataAccessor(await deps.get("sqlConnection")));
        this.bind("mongoConnection", create_mongoConnection);
        this.bind("mongoDao", async (deps): Promise<CrudDataAccessor> => new CrudDataAccessor(await deps.get("mongoConnection")))
    }
}
