"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var log_1 = require("cwb-core/log");
var CURAPaths_1 = require("../cura/CURAPaths");
var CURASlicer_1 = require("../cura/CURASlicer");
log_1.setup_log4js();
log_1.log_enable_console();
var curaPaths = CURAPaths_1.CURAPaths.fromPropertiesFile("./config/printer.properties");
console.log(curaPaths);
var curaSlicer = new CURASlicer_1.default(curaPaths);
curaSlicer.slice("./resources/Marvin.stl", "./resources/MarvinOut.gcode", {
    definitionTarget: "creality_ender3.def.json",
})
    .then(console.log)
    .catch(console.error);
