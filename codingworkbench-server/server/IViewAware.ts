import {IViewFactory} from "./common";

export interface IViewAware {
    viewFactory: IViewFactory;
}

export default IViewAware;
