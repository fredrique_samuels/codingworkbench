"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var log4js = require("log4js");
function setup_log4js(level) {
    if (level === void 0) { level = "info"; }
    log4js.configure({
        appenders: { log: { type: "file", filename: "log4js.log" } },
        categories: { default: { appenders: ["log"], level: "info" } },
    });
}
exports.setup_log4js = setup_log4js;
var logger = log4js.getLogger();
var SETTINGS = {
    consoleEnabled: false,
};
function log_enable_console() {
    SETTINGS.consoleEnabled = true;
}
exports.log_enable_console = log_enable_console;
function log_error(e) {
    if (SETTINGS.consoleEnabled) {
        console.error(e);
    }
    logger.error(e.message, e.stack);
}
exports.log_error = log_error;
function log_warning(e) {
    if (SETTINGS.consoleEnabled) {
        console.warn(e);
    }
    logger.warn(e.message, e.stack);
}
exports.log_warning = log_warning;
function log_info_message(message) {
    if (SETTINGS.consoleEnabled) {
        console.info(message);
    }
    logger.info(message);
}
exports.log_info_message = log_info_message;
