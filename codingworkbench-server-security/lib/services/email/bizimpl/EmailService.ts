

import CwbLog from "@codingworkbench/log";

import IEmailService from "../api/IEmailService";
import {IEmailContent} from "../api/IEmailContent";

import {ICredentials} from "@codingworkbench/common/ICredentials";
import IEmailTransporter from "../api/IEmailTransporter";

export type EmailSourceCredentials = { [s: string]: ICredentials };

export default class EmailService implements IEmailService {
    private readonly sources: EmailSourceCredentials;
    private readonly transporter: IEmailTransporter;

    constructor(sources: EmailSourceCredentials, transporter: IEmailTransporter) {
        this.sources = sources;
        this.transporter = transporter;
    }

    public async sendRawHtmlMail(source: string, mail: IEmailContent): Promise<void> {

        const credential = this.sources[source];
        if (!credential) {
            CwbLog.error(new Error(`No emails source registered ${source}`));
            return
        }

        if (!mail.email) {
            CwbLog.error(Error(`No destination email provided`));
            return
        }

        const mailOptions = {
            from: credential.user,
            to: mail.email,
            subject: mail.title,
            html: mail.body
        };

        return this.transporter.transport(credential, mailOptions);
    }

}
