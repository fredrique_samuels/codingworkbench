
/**
 * Interface for a executable update to a table.
 */
export interface DbUpdateRunner {

    /**
     * Run the update.
     *
     * @returns The affected rows.
     */
    execute(): Promise<any[]>
}

export default DbUpdateRunner;
