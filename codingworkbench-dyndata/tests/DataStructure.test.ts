
import {deepStrictEqual, ok} from 'assert'
import IConnectionAware, {connection_aware_close} from "@codingworkbench/data/IConnectionAware";
import CwbLog from "@codingworkbench/log"
import ConnectionAwareTestInjector from "@codingworkbench/data/ConnectionAwareTestInjector";
import CwbTestCase from "@codingworkbench/testcase";
import {DataStructure, DataStructureDao} from "../lib/DataStructure";

describe('DataTypeDef', function() {

    this.timeout(20000);

    let unit: IConnectionAware;

    before(async () => {
        CwbLog.setup();
        CwbLog.enable_console();
        const connectionInjector = new ConnectionAwareTestInjector();
        unit = await connectionInjector.getAll()
    });

    after(async () => {
        await connection_aware_close(unit);
    });

    it("#persists", (done) => {

        async function _e() {
            // given
            const dataStructure = await new DataStructure(unit.mongoDao)
                .setName(await CwbTestCase.generateUUID())
                .save();

            // when
            const [obj] = await new DataStructureDao(unit.mongoDao).getByIds([dataStructure.id]);

            // then
            ok(obj);
        }

        _e().then(done).catch(done)
    });

    it("#properties.persist", (done) => {

        async function _e() {
            // given
            const dataStructure = await new DataStructure(unit.mongoDao)
                .setName(await CwbTestCase.generateUUID())
                .save();

            // when
            const [obj] = await new DataStructureDao(unit.mongoDao).getByIds([dataStructure.id]);

            // then
            ok(obj);
            deepStrictEqual(Object.keys(obj.properties).length, 1);
        }

        _e().then(done).catch(done)
    });

    // it("properties.create", (done) => {
    //
    //     async function _e() {
    //         // given
    //         const dataTypeDef = new DataStructure(unit.mongoDao)
    //             .update({
    //                 ...new DataStructure(unit.mongoDao).createParams(),
    //                 name: await CwbTestCase.generateUUID()
    //             });
    //         await dataTypeDef.save();
    //
    //         // when
    //         const [obj] = await new DataStructureDao(unit.mongoDao).getByIds([dataTypeDef.id]);
    //
    //         // then
    //         ok(obj);
    //     }
    //
    //     _e().then(done).catch(done)
    // });

});
