import CwbLog from "@codingworkbench/log";

import IUserSession from "@codingworkbench/data/IUserSession";
import IFileData from "@codingworkbench/common/IFileData";

import * as common from "./common"

export function session_expired(sess?: IUserSession): boolean {
    if (sess) {
        const invalidUserId = sess.userId === null;
        const invalidAccountId = sess.accountId === null;
        const preserveSession = sess.ttl <= 0;
        const expired = (new Date().getTime() - sess.created) > sess.ttl;
        if (preserveSession) return false;
        return invalidUserId || invalidAccountId || expired;
    }
    return true;
}

export function load_session_info_from_req(req: common.Request): IUserSession {
    return req.session.get("key") as IUserSession;
}

export function has_active_session(req: common.Request): boolean {
    return !session_expired(load_session_info_from_req(req));
}

export interface SessionAwareHandler<Params, Body> {
    session: IUserSession;
    params: Params;
    body: Body;
    files: IFileData[];
}

export function request_unpack_files(requestContext: common.RequestContext, options: RequestContextUnpackOptions = {}): IFileData[] {
    if ( requestContext.request.files ) {
        return requestContext.request.files.get( options.filesKey || "file");
    }
    return [];
}

export interface RequestContextUnpackOptions {
    filesKey?: string;
}

export function request_context_unpack<Params={}, Body={}>(requestContext: common.RequestContext, options: RequestContextUnpackOptions = {}): SessionAwareHandler<Params, Body> {
    return {
        session: load_session_info_from_req(requestContext.request),
        params: requestContext.request.params as Params,
        body: requestContext.request.body as Body,
        files: request_unpack_files(requestContext, options)
    }
}

export function request_handler_decorate_500_response<C>(handler: common.RequestHandlerFunc<C>): common.RequestHandlerFunc<C> {
    return (context: common.RequestContext<C>) => {
        try {
            handler(context);
        } catch (e) {
            CwbLog.error(e);
            new common.ResponseBuilder().send500(context.response);
        }
    }
}

export function request_handler_async_decorate_500_response<C>(handler: common.RequestHandlerAsync<C>): common.RequestHandlerAsync<C> {
    return async (context: common.RequestContext<C>) => {
        try {
            await handler(context);
        } catch (e) {
            CwbLog.error(e);
            new common.ResponseBuilder().send500(context.response);
        }
    }
}
