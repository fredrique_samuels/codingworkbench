import SettingItem from "../common/SettingItem";
import ISettingOption from "../common/SettingOption";
import ISliceOutput from "lib/ISliceJobOutput";
export default interface ISlicerService {
    getOptions(): Promise<SettingItem[]>;
    getDefinitions(): Promise<ISettingOption[]>;
    slice(stlString: string, definition: string, options: SettingItem[]): Promise<ISliceOutput>;
}
