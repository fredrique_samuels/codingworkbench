"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var action_1 = require("@codingworkbench/action");
function openUrlInCurrentTab(url) {
    open(url, "_self");
}
exports.openUrlInCurrentTab = openUrlInCurrentTab;
function preventDefault(event) {
    event.preventDefault();
}
exports.preventDefault = preventDefault;
var ResponseBuilder = /** @class */ (function (_super) {
    __extends(ResponseBuilder, _super);
    function ResponseBuilder() {
        return _super.call(this) || this;
    }
    ResponseBuilder.prototype.sendJson = function (response) {
        response.sendJson(this.json());
    };
    ResponseBuilder.prototype.send500 = function (response) {
        response.sendStatus(500, "Internal Server Error");
    };
    return ResponseBuilder;
}(action_1.default.ResponseBuilder));
exports.ResponseBuilder = ResponseBuilder;
