@codingworkbench/inject
======

Goal
-----

Lightweight dependency inject module for Typescript that has no dependencies. 
Uses powerful [generics features](https://www.typescriptlang.org/docs/handbook/generics.html)  of Typescript
to enforce correct binding of dependencies.     

Install
-----
```sh
npm install @codingworkbench/inject --save
```

Basic Usage
-----
```typescript
import CwbInject from "@codingworkbench/inject"

// Application domain interfaces and implementations
interface ITodoService {}

class TodoServiceImpl implements ITodoService {}


// Describe the module dependencies
interface ITodoModule {
    todoService: ITodoService;
}

// Create a module for the application domain
const todoModule = new CwbInject.Module<ITodoModule>();

// Bind the todoService property to the TodoService constructor
todoModule.bind("todoService", TodoServiceImpl);

// Request the todoService property and use as needed
const todoService = todoModule.getProperty("todoService");
```

Factory Functions
-----
```typescript
// Bind a function to the todoService property
todoModule.bindFactoryFunction("todoService", async () => new TodoService());
```

Injecting Dependencies
-----
TodoStore.ts  

```typescript
 
export interface ITodoStore {}

export class TodoStoreImpl implements ITodoStore {}

```

TodoService.ts  

```typescript
import {ITodoStore} from "./TodoStore"
 
export interface ITodoService {}

export interface ITodoServiceArgs {
    todoStore: ITodoStore;
}

export class TodoServiceImpl implements ITodoService {
    private store: ITodoStore;
    constructor(args: ITodoServiceArgs) {
        this.store = args.todoStore;
    }
}

```

inject.ts

```typescript
import CwbInject from "@codingworkbench/inject"

import {ITodoStore} from "./TodoStore"
import {ITodoService, ITodoServiceArgs} from "./TodoService"

export interface ITodoModule {
    todoStore: ITodoStore;
    todoService: ITodoService;
}

const todoServiceInjectionProperties: CwbInject.InjectionProperties<ITodoServiceArgs> = {
    todoStore: true,
};
export todoServiceInjectionProperties;

```

main.ts
```typescript
import CwbInject from "@codingworkbench/inject"

import {TodoStoreImpl} from "./TodoStore"
import {TodoServiceImpl} from "./TodoService"
import {todoServiceInjectionProperties, ITodoModule} from "./inject"


// Create a module for the application domain
const todoModule = new CwbInject.Module<ITodoModule>();

// bind the store object
todoModule.bind("todoStore", TodoStoreImpl);

// Bind the todoService property to the TodoService constructor
todoModule.bind("todoService", TodoServiceImpl, todoServiceInjectionProperties);
// or
// todoModule.bindFactoryFunction("todoService", async (args) => new TodoServiceImpl(args), todoServiceInjectionProperties);

// Request the todoService property and use as needed
const todoService = todoModule.getProperty("todoService");

```

API
-----
#### class Module &lt;MC&gt;

An object provider that lazy constructs object as they are requested. Modules can be combined to 
create more complex models.

* __MC__: (Template) Interface type that has all the properties that can be required by the module to construct.

##### methods
* __getProperty( propertyKey ): this__
    <br/> __propertyKey__ _(keyof __MC__)_ : Request the value for the given property.
    <br/>
     
* __bind( propertyKey, constructor [, injectionProperties ] ): this__ 
    <br/> __propertyKey__ _(keyof __MC__)_ : A key that had is part of the module type __MC__.
    <br/> __constructor__ _(Constructor)_: Reference to a constructor that should be called when populating the property.
    <br/> __injectionProperties__ _(InjectionKeys)_: The properties that should be injected into the constructor.
    <br/><br/> Bind a class constructor to a property. The constructor will be called when _getProperty(propertyKey)_ is invoked.
    <br/>
     
* __bindFactoryFunction( propertyKey, factoryFunction [, injectionProperties ] ): this__ 
    <br/> __propertyKey__ _(keyof __MC__)_: A key that had is part of the module type __MC__.
    <br/> __factoryFunction__ _(function (args) : Promise&lt;R&gt;)_ : A function that returns a new object. If _injectionProperties_ is set then the properties values will be passed to this function call.
    <br/> __injectionProperties__ _(InjectionKeys)_: The properties that should be passed into the _factoryFunction_.
    <br/><br/> Bind a factory function to a property. The function will be called when _getProperty(propertyKey)_ is invoked.
    <br/>
     

License
-------
The ISC License

Copyright (c) 2020 Fredrique Samuels (https://codingworkbench.com)

Permission to use, copy, modify, and/or distribute this software for any
purpose with or without fee is hereby granted, provided that the above
copyright notice and this permission notice appear in all copies.

THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF OR
IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.

