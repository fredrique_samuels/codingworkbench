import IRequestHandler from "@codingworkbench/server/IRequestHandler";
import {RequestHandlerBuilder} from "@codingworkbench/server/RequestHandlerBuilder";
import {RequestContext} from "@codingworkbench/server/common";

import {ISecurityHandlerContext, ISecurityHandlerFactoryContext} from "../../api/context";
import {web_respond_redirect} from "../../api/response";

async function handlerAsync<C extends ISecurityHandlerContext>(requestContext: RequestContext<C>) {
    requestContext.request.session.destroy();
    web_respond_redirect(requestContext, requestContext.context.securityLinks.signInPage);
}

export default function DoSignOut<C extends ISecurityHandlerContext>(factoryContext: ISecurityHandlerFactoryContext): IRequestHandler<C> {
    return new RequestHandlerBuilder(factoryContext.links.doSignOut, () => {}, handlerAsync).setMethod("post").setAsync().build();
}

