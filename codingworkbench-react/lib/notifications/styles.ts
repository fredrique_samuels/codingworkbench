import {makeStyles, Theme} from "@material-ui/core";
import {amber, green} from "@material-ui/core/colors";

const iconStyle = {
    icon: {
        fontSize: 20,
    }
};


export default makeStyles((theme: Theme) => ({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: theme.palette.error.dark,
    },
    info: {
        backgroundColor: theme.palette.primary.main,
    },
    warning: {
        backgroundColor: amber[700],
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
    margin: {
        margin: theme.spacing(1),
    },
    ...iconStyle,
}));
