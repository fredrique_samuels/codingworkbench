import Optional from "./Optional";

namespace CwbInject {

    export type TypedPropertyRestriction<T, I> = { [K in keyof T]: T[K] extends I ? K : never }[keyof T];

    export type FactoryFunction<R, Args> = (args: Injector<Args>) => Promise<R>;

    export type InjectionProperties<T> = { [P in keyof Required<T>]: boolean };

    export type ArgsConstructor<R, Args> = new (args: Args) => R;

    export interface ICreationStrategies<R, MC, Args= any> {
        factoryFunction?: FactoryFunction<R, MC>;
        constructorFunction?: ArgsConstructor<R, Args>;
    }

    export interface ICreationStrategiesMap { [s: string]: ICreationStrategies<any, any, any>; }

    export interface Injector<MC> {
        get<K extends keyof MC>(propertyKey: K): Promise<MC[K]>;
        getOptional<K extends keyof MC>(propertyKey: K): Promise<Optional<MC[K]>>;
    }

    export interface IModule<MC> extends Injector<MC> {
        merge<MC2>(other: Module<MC2>): Module<MC & MC2>;
        bind<E, Args= Required<MC>>( propertyKey: TypedPropertyRestriction<MC, E>, factoryFunction: FactoryFunction<E, Args>): this
        getAll(): Promise<MC>;
    }

    const assertCircularInjection = (p1: string, p2: string) =>  {
        if (p1 === p2) {
            throw new Error(`circular injection detected for property ${p1}`);
        }
    };

    function createProxyInjector<MC>(parent: Module<MC>, propertyKey: string): Injector<MC> {
        return {
            get: async function<K extends keyof MC>(p: K): Promise<MC[K]> {
                assertCircularInjection(p as string, propertyKey);
                return await parent.createOrGetValue(p as string, true);
            },
            getOptional: async function<K extends keyof MC>(p: K): Promise<Optional<MC[K]>> {
                assertCircularInjection(p as string, propertyKey);
                return Optional.forValue<MC[K]>(await parent.createOrGetValue(p as string, false));
            }
        };
    }

    /**
     * A module to contain a given collection of dependencies.
     *
     * MC defines all the properties in this module.
     */
    export class Module<MC> implements IModule<MC> {

        private static assertCreationProps(propertyKey: string, creationProps?: ICreationStrategies<any, any, any>) {
            if (!creationProps) {
                throw new Error(`No creation strategy found for property value ${propertyKey}`);
            }
        }
        private readonly target: IStrategiesAndValuesCache<MC>;

        constructor(strategiesMap?: ICreationStrategiesMap) {
            this.target = {
                creationStrategies: { ... strategiesMap || {}  },
                values:  {},
            };
        }

        /**
         * Combine two modules. Note neither module is allowed to have populated values
         * when merging.
         *
         * @param other The
         */
        public merge<MC2>(other: Module<MC2>): Module<MC & MC2> {
            if (this.hasValues()) {
                throw new Error(`Module 'this' has values already set. Merging is not allowed with modules that have values`);
            }

            if (other.hasValues()) {
                throw new Error(`Module 'other' has values already set. Merging is not allowed with modules that have values`);
            }

            return new Module<MC&MC2>({
                ...this.target.creationStrategies,
                ...other.target.creationStrategies,
            });
        }

        /**
         * Get the current module proxy that will lazy create the property
         * values as they are accessed.
         */
        public async get<K extends keyof MC>(propertyKey: K): Promise<MC[K]> {
            return await this.createOrGetValue(propertyKey.toString(), true) as MC[K];
        }

        /**
         * Get the current module proxy that will lazy create the property
         * values as they are accessed.
         */
        public async getOptional<K extends keyof MC>(propertyKey: K): Promise<Optional<MC[K]>> {
            return Optional.forValue<MC[K]>(await this.createOrGetValue(propertyKey.toString(), true) as MC[K]);
        }

        public async getAll(): Promise<MC> {
            const result: any = {};
            const me = this;
            const keys = Object.keys(this.target.creationStrategies);
            for (const p of keys) {
                result[p] = await me.createOrGetValue(p, true);
            }
            return result as MC;
        }

        /**
         * Add a factory function for a given property on type MC.
         *
         * Example:
         *
         *  interface ITodoService {}
         *  class TodoService {}
         *
         *  interface IModule {
         *      todoService: ITodoService;
         *  }
         *
         *  const module = new Module<IModule>();
         *  module.addFactoryFunction("todoService", (moduleContext: IModule) => new TodoService());
         *
         * @param propertyKey The property to map the result of the factory function to.
         * @param factoryFunction The factory function.
         * @param injectionKeys Optional keys to be injected into instance constructor call
         */
        public bind<E, Args= Required<MC>>(
                propertyKey: TypedPropertyRestriction<MC, E>,
                factoryFunction: FactoryFunction<E, Args>,
                injectionKeys: InjectionProperties<Args> | null = null,
        ): this {
            this.target.creationStrategies[propertyKey.toString()] = {
                factoryFunction,
            };
            return this;
        }

        protected hasValues(): boolean {
            return Object.keys(this.target.values).length > 0;
        }

        public async createOrGetValue(propertyKey: string, required: boolean): Promise<any> {
            if (this.target.values[propertyKey] !== undefined) {
                if (this.target.values[propertyKey] === null) {
                    if (required) {
                        throw new Error(`Unable to create injection dependent for key '${propertyKey}' `)
                    } else {
                        return null;
                    }
                }
                return this.target.values[propertyKey];
            }

            const creationProps = this.getCreationStrategies(propertyKey);
            this.target.values[propertyKey] = await this.createNewInstance(creationProps, propertyKey);
            if (!this.target.values[propertyKey]) {
                return this.createOrGetValue(propertyKey, required);
            }
            return this.target.values[propertyKey];
        }

        private getCreationStrategies(propertyKey: string) {
            const creationProps = this.target.creationStrategies[propertyKey] as ICreationStrategies<any, any, any>;
            CwbInject.Module.assertCreationProps(propertyKey, creationProps);
            return creationProps;
        }

        private async createNewInstance(creationProps: ICreationStrategies<any, any, any>, propertyKey: string) {
            let instance: any | undefined;
            if (creationProps.factoryFunction) {
                instance = await creationProps.factoryFunction(createProxyInjector(this, propertyKey));
            }
            return instance;
        }
    }

    interface IStrategiesAndValuesCache<C> {
        values: {[s: string]: any};
        creationStrategies: ICreationStrategiesMap;
    }

    function createInstance<T, Args>(c: ArgsConstructor<T, Args>, args: Args): T {
        return new c(args);
    }
}

export default CwbInject;
