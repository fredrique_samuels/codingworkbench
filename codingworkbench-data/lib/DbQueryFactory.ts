import CwbSearch from "@codingworkbench/search";

import {DbTable} from "./DbTable";
import {DbQuery} from "./DbQuery";

export interface DbQueryFactory {
    createUpdateQuery(table: DbTable, record: any): DbQuery;

    createInsertQuery(table: DbTable, record: any): DbQuery;

    createSearchQuery(table: DbTable, search: CwbSearch.Search): DbQuery;

    createCountQuery(table: DbTable, search: CwbSearch.Search): DbQuery;

    createDeleteObjectQuery(table: DbTable, record: any): DbQuery;
}

export default DbQueryFactory;
