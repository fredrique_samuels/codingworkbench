import * as HtmlTemplate from "./HtmlTemplate";
import {HTML_DATA_MODEL_ID, HtmlModel} from "./HtmlModel";



/**
 * Builder wrapped arounf HtmlTemplate.
 *
 * @see HtmlTemplate
 */
export class HtmlBuilder {
    
    protected model: HtmlModel;
    private readonly template: HtmlTemplate.HtmlTemplate;

    /**
     * @constructor
     */
    constructor() {
        this.template = {
            title: "",
            head: [],
            body: [],
            preBody: []
        }
        this.model = {};
    }

    public setModel(model: HtmlModel): this {
        this.model = {...model};
        return this;
    }

    /**
     * @see HtmlTemplate.head
     */
    addHead(tag: string)
        : this
    {
        this.template.head.push(tag);
        return this;
    }

    /**
     * @see HtmlTemplate.body
     */
    addBody(bCode: string)
        : this
    {
        this.template.body.push(bCode);
        return this;
    }

    /**
     * @see HtmlTemplate.preBody
     */
    addPreBody(bCode: string)
        : this
    {
        this.template.preBody.push(bCode);
        return this;
    }

    build() : string {
        this.addPreBody(`<script>var ${HTML_DATA_MODEL_ID} = ${JSON.stringify(this.model)}</script>`);
        return HtmlTemplate.build_html(this.template);
    }
}

