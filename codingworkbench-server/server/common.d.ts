/// <reference types="node" />
import CwbAction from "@codingworkbench/action";
import { IncomingHttpHeaders } from "http";
import { HtmlBuilder } from "./HtmlBuilder";
import { HtmlModel } from "./HtmlModel";
import IUploadedFiles from "@codingworkbench/common/IUploadedFiles";
export declare function openUrlInCurrentTab(url: string): void;
/**
 * Standard http request methods.
 */
export declare type Method = 'get' | 'post' | 'delete' | 'put';
export declare function preventDefault(event: any): any;
/**
 * Http request structure.
 */
export interface Request {
    readonly sessionID: string;
    /**
     * Rhe request url.
     */
    readonly url: string;
    /**
     * The request method.
     */
    readonly method: Method;
    /**
     * The request body if any.
     */
    readonly body: any;
    /**
     * Any request params.
     */
    readonly params: any;
    /**
     * @see IncomingHttpHeaders
     */
    readonly headers: IncomingHttpHeaders;
    /**
     * Any uploaded files.
     */
    readonly files: IUploadedFiles;
    /**
     * Optional session object.
     */
    readonly session: Session;
}
/**
 * A wrapper object for all data needed to handle an incoming http request.
 */
export interface RequestAware {
    request: Request;
    response: Response;
}
/**
 * A wrapper object for all data needed to handle an incoming http request.
 */
export interface RequestContext<C = {}> extends RequestAware {
    context: C;
    nextFunction?: (reqContext: RequestContext<C>) => void;
}
export interface Session {
    /**
     * The session unique id.
     */
    id: string;
    /**
     * Set a value on the session object.
     * @param name The value name
     * @param value The value.
     */
    set(name: string, value: any): void;
    /**
     * Get a value from the session.
     *
     * @param name The value name.
     */
    get(name: string): any;
    /**
     * Destroy the current session
     *
     * @param errFunc Optional function to capture errors.
     */
    destroy(errFunc?: (err: any) => void): void;
}
/**
 * Function signature to handler incoming web requests
 */
export declare type RequestHandlerFunc<C = {}> = (reqContext: RequestContext<C>) => void;
/**
 * Function signature to handler incoming web requests
 */
export declare type RequestHandlerAsync<C = {}> = (reqContext: RequestContext<C>) => Promise<void>;
/**
 * Http response structure.
 */
export interface Response {
    /**
     * Respond with unstructured text and terminate.
     *
     * @param text The response text.
     */
    sendText(text: string): void;
    /**
     * Respond with a json string and terminate.
     *
     * @param json The response json.
     */
    sendJson(json: string): void;
    /**
     * Respond with a json string and terminate.
     *
     * @param obj The object to serialize and send.
     * @param pretty Boolean to indicate that the json should be prettified.
     */
    sendJsonObj(obj: any, pretty?: boolean): void;
    /**
     * Respond with a specific status code and text then terminate.
     * @param statusCode The response status code.
     * @param text The response text.
     */
    sendStatus(statusCode: number, text: string): void;
    /**
     * Redirect to a different url and terminate.
     *
     * @param url The target url.
     */
    redirect(url: string): void;
    /**
     * Send a view response using the html factory provided
     *
     * @param htmlFactory {HtmlModel}
     * @param view {String} the view name
     * @param model {any} Options model object
     */
    view(view: View): void;
    /** indicate that one of the terminal methods has been invoked. */
    readonly terminated: boolean;
}
/**
 * Factory interface to convert 3rd party http objects
 * to Http.Request and Http.Response.
 */
export interface RequestFactory<RQ, RS> {
    /**
     * Create a Request from the given input.
     * @param rq RQ
     */
    createRequest(rq: RQ): Request;
    /**
     * Create a Response from the given input.
     *
     * @param rs RS
     */
    createResponse(rs: RS): Response;
}
/**
 * HTMl for rendering.
 */
export interface View {
    readonly html: string;
}
export interface IViewFactory {
    createHtmlBuilder(): HtmlBuilder;
    modelAndView(name: string, model?: HtmlModel): View;
}
export declare class ResponseBuilder extends CwbAction.ResponseBuilder {
    constructor();
    sendJson(response: Response): void;
    send500(response: Response): void;
}
