import ISliceSummary from "../ISliceSummary";
import {
    CURA_FILAMENT_USED_PARSER,
    CURA_MAX_X_PARSER,
    CURA_MAX_Y_PARSER, CURA_MAX_Z_PARSER,
    CURA_MIN_X_PARSER,
    CURA_MIN_Y_PARSER,
    CURA_MIN_Z_PARSER,
    CURA_TIME_FORMATTED_PARSER,
    CURA_TIME_PARSER
} from "./parsers";
import ISliceResults from "lib/print/ISliceResults";

export default class CURASliceSummaryExtractor {
    private readonly gcode: string;

    constructor(gcode: string) {
        this.gcode = gcode;
    }

    public extractFromOutput(output: string): ISliceResults {
        const summary: any = {};

        output.split("\n")
            .forEach(line => {
                if(line.startsWith(CURA_TIME_PARSER.prefix)) {
                    summary.timeInSeconds = CURA_TIME_PARSER.parse(line)
                } else if(line.startsWith(CURA_TIME_FORMATTED_PARSER.prefix)) {
                    summary.timeFormatted = CURA_TIME_FORMATTED_PARSER.parse(line)
                }
                else if(line.startsWith(CURA_FILAMENT_USED_PARSER.prefix)) {
                    summary.filamentUsedInMeters = CURA_FILAMENT_USED_PARSER.parse(line)
                }
                else if(line.startsWith(CURA_MIN_X_PARSER.prefix)) {
                    summary.minx = CURA_MIN_X_PARSER.parse(line)
                }
                else if(line.startsWith(CURA_MIN_Y_PARSER.prefix)) {
                    summary.miny = CURA_MIN_Y_PARSER.parse(line)
                }
                else if(line.startsWith(CURA_MIN_Z_PARSER.prefix)) {
                    summary.minz = CURA_MIN_Z_PARSER.parse(line)
                }
                else if(line.startsWith(CURA_MAX_X_PARSER.prefix)) {
                    summary.maxx = CURA_MAX_X_PARSER.parse(line)
                }
                else if(line.startsWith(CURA_MAX_Y_PARSER.prefix)) {
                    summary.maxy = CURA_MAX_Y_PARSER.parse(line)
                }
                else if(line.startsWith(CURA_MAX_Z_PARSER.prefix)) {
                    summary.maxz = CURA_MAX_Z_PARSER.parse(line)
                }
            });


        if(summary.minx !== undefined && summary.maxx !== undefined) {
            summary.dimx = summary.maxx - summary.minx;
            summary.centerx = summary.minx + 0.5 * summary.dimx;
        }

        if(summary.miny !== undefined && summary.maxy !== undefined) {
            summary.dimy = summary.maxy - summary.miny;
            summary.centery = summary.miny + 0.5 * summary.dimy;
        }

        if(summary.minz !== undefined && summary.maxz !== undefined) {
            summary.dimz = summary.maxz - summary.minz;
            summary.centerz = summary.minz + 0.5 * summary.dimz;
        }

        return {
            id: "",
            gcode: this.gcode,
            summary: summary
        }
    }
}
