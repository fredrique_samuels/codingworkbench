import ISliceSummary from "./ISliceSummary";
import IProcessStdio from "../process/IProcessStdio";

export default interface ISliceResults {
    readonly id: string;
    readonly gcode: string;
    readonly summary: ISliceSummary;
}
