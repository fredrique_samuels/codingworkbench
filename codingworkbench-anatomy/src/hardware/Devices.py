import sys
import glob
import serial


def get_serial_ports():
    """ Lists serial port names

            :raises EnvironmentError:
                On unsupported or unknown platforms
            :returns:
                A list of the serial ports available on the system
        """
    if sys.platform.startswith('win'):
        return ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        return glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        return glob.glob('/dev/tty.*')

    raise EnvironmentError('Unsupported platform')


def is_serial_port_open(port):
    try:
        s = serial.Serial(port)
        return s.is_open
    except (OSError, serial.SerialException):
        pass
    return False


def close_serial_port(port):
    try:
        s = serial.Serial(port)
        if not s.is_open:
            s.close()
    except (OSError, serial.SerialException):
        pass

