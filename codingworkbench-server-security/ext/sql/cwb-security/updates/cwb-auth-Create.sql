

CREATE TABLE IF NOT EXISTS
    cwb_auth_user (
        id INT UNSIGNED NOT NULL AUTO_INCREMENT,
        email VARCHAR(255),
        password_hash VARCHAR(1024),
        doe DATE,
        uoe INT,
        dlu DATE,
        ulu INT,
        archived BOOLEAN,
        PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS
    cwb_auth_account (
             id INT UNSIGNED NOT NULL AUTO_INCREMENT,
             username VARCHAR(255),
             activation_token TEXT,
             activated BOOLEAN,
             doe DATE,
             uoe INT,
             dlu DATE,
             ulu INT,
             archived BOOLEAN,
             PRIMARY KEY (id)
)ENGINE=InnoDB;

CREATE TABLE IF NOT EXISTS
    cwb_auth_user_account (
      cwb_auth_user_id INT UNSIGNED NOT NULL,
      cwb_auth_account_id INT UNSIGNED NOT NULL,
      KEY `cwb_auth_user_id_idx` (`cwb_auth_user_id`),
      KEY `cwb_auth_account_id_idx` (`cwb_auth_account_id`)
)ENGINE=InnoDB;
