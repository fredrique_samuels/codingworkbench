import {ICredentials} from "@codingworkbench/common/ICredentials";

export default interface ISecurityCredentials extends ICredentials {
    username: string;
}
