
import IEmailTransporter from "../api/IEmailTransporter";
import {ICredentials} from "@codingworkbench/common/ICredentials";


export default class MockedEmailTransporter implements IEmailTransporter {

    public emails: any[] = [];

    async transport(credentials: ICredentials, mail: any): Promise<void> {
        this.emails.push({
            email: mail.email,
            title: mail.title,
            body: mail.body
        });
    }

}
