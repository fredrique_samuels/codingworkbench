import CwbLog from "@codingworkbench/log";
import {ExpressApplicationOptions, IExpressApplication} from "@codingworkbench/server/ExpressApplicationUtils";
import ISecurityAwareContext from "../lib/services/security/api/context";
import {IServerProperties, server_properties_create} from "../lib/services/common/IServerPropertiesAware";
import SecurityAwareApplication, {SecureApplicationContextFactory} from "../lib/services/security/api/SecurityAwareApplication";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";

export interface ApplicationContext extends ISecurityAwareContext {}

class ApplicationContextFactory extends SecureApplicationContextFactory<ApplicationContext> {

    constructor(serverProps: IServerProperties) {
        super(serverProps);
    }

    createContext(options: ExpressApplicationOptions, connectionProperties: IPropertiesReader | null): Promise<ApplicationContext> {
        return this.createContextInternal(connectionProperties);
    }

    async configureApplication(context: ApplicationContext, app: IExpressApplication<ApplicationContext>): Promise<void> {}
}

try {

    CwbLog.setup();
    CwbLog.enable_console();

    const securityAwareApplicationProperties = server_properties_create({});
    const application = new SecurityAwareApplication<ApplicationContext>(securityAwareApplicationProperties);
    const contextFactory = new ApplicationContextFactory(securityAwareApplicationProperties);

    const options: ExpressApplicationOptions = {
        port: 8080,
        enabledSessions: true,
        connectionPropertiesFile: "demo/conf/connection.properties"
    };
    application.start(options, contextFactory).catch(CwbLog.error);
} catch (e) {
    CwbLog.error(e)
}
