import IUnique from "@codingworkbench/data/IUnique";
import IAuditAware from "@codingworkbench/data/IAuditAware";
import IArchiveAware from "@codingworkbench/data/IArchiveAware";


export interface ICwbUser extends IUnique, IAuditAware, IArchiveAware {
    /* Username */
    username: string;
    /* unique email */
    email: string;
    /* the user password encrypted */
    password_hash: string;
    /*  password requested reset token */
    password_reset_token: string | null;
}

export default ICwbUser;
