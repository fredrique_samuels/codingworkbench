import {HtmlModel, ModelBuilder} from "@codingworkbench/server/HtmlModel";


export interface SecureLinkData {
    // security settings
    securePagePattern: string;
    secureDataPattern: string;

    // session
    sessionCheck: string;
}

export interface ISecurityLinks extends SecureLinkData {
    // page urls
    signUpPage: string;
    signInPage: string;
    resetPasswordPage: string;
    forgotPasswordPage: string;
    accountActivationRequiredPage: string;
    homePage: string;
    userHomePage: string;

    // page views
    signUpPageView: string;
    signInPageView: string;
    resetPasswordView: string;
    forgotPasswordView: string;
    accountActivationRequiredView: string;
    activateAccountSuccessView: string;
    homePageView: string;
    userHomePageView: string;

    // actions
    doSignUp: string;
    doSignIn: string;
    doSignOut: string
    doActivateAccount: string;
    doResendActivationLink: string;
    doResetPassword: string;
}

export function create_security_links(): ISecurityLinks {
    return {

        // sign in
        signInPage:"/signin.html",
        signInPageView:"CwbSignIn",
        doSignIn:"/doSignIn",

        // sign out
        doSignOut:"/doSignOut",

        // registration
        signUpPage:"/signup.html",
        signUpPageView:"CwbSignUp",
        doSignUp:"/doSignUp",

        // activate account
        accountActivationRequiredPage:"/activate_acc_req/:email",
        accountActivationRequiredView:"CwbAccountActivationRequired",

        doResendActivationLink:"/doResendActivationLink",
        doActivateAccount:"/doActivateAccount/:token",
        activateAccountSuccessView:"CwbAccountActivated",

        // forgot password
        forgotPasswordPage: "/forgotpass.html",
        forgotPasswordView: "CwbForgotPassword",

        // reset password
        resetPasswordPage:"/resetpass.html",
        resetPasswordView:"CwbResetPassword",
        doResetPassword:"/doResetPassword",

        // home
        homePage:"/home.html",
        homePageView:"CwbHome",

        // user home
        userHomePage:"/dashboard.html",
        userHomePageView:"CwbUserDashboard",

        // security data
        securePagePattern:"/secure/user/",
        secureDataPattern:"/secure/data/",
        sessionCheck:"/secure/data/user/session/check",
    }
}

function url_join(...paths: string[]): string {
    let res = "";
    paths.forEach((p: string, idx: number) => {
        if (idx === 0) {
            res = p;
        } else {
            if (res.endsWith("/")) {
                if (p.startsWith("/")) {
                    res = `${res}${p.substring(1)}`;
                } else {
                    res = `${res}${p}`;
                }
            } else {
                if (p.startsWith("/")) {
                    res = `${res}${p}`;
                } else {
                    res = `${res}/${p}`;
                }
            }
        }
    });
    return res;
}

export const SECURITY_LINKS_MODEL: string = "security_links";

export function links_write<L extends SecureLinkData>(links: L, builder: ModelBuilder)
    : ModelBuilder {
    return builder.put(SECURITY_LINKS_MODEL, links);
}

export function security_links_read<L extends SecureLinkData>(model: HtmlModel) : L {
    return model[SECURITY_LINKS_MODEL]
}

export function create_secure_page_url(url: string, siteLinks: SecureLinkData): string {
    if (!url.startsWith(siteLinks.securePagePattern)) {
        return url_join(siteLinks.securePagePattern, url);
    }
    return url;
}

export function create_secure_data_url(url: string, siteLinks: SecureLinkData): string {
    if (!url.startsWith(siteLinks.secureDataPattern)) {
        return url_join(siteLinks.secureDataPattern, url);
    }
    return url;
}
