
export enum SecurityErrorCode {
    EMAIL_OR_PASS_INVALID = "EMAIL_OR_PASS_INVALID",
    EMAIL_IN_USE = "EMAIL_IN_USE",
    EMAIL_INVALID = "EMAIL_INVALID",
    EMAIL_NOT_REGISTERED = "EMAIL_NOT_REGISTERED",
    PASSWORD_WEAK = "PASSWORD_WEAK",
    INVALID_TOKEN = "INVALID_TOKEN",
    PASSWORD_VERIFICATION_FAILED = "PASSWORD_VERIFICATION_FAILED",
}
export default SecurityErrorCode;
