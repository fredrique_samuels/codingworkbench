
export interface IEmployee {
    name: string;
    age: number;
    address: IAddress;
}

export interface IAddress {
    street: string;
}

export type EmployeeStore = {[s:string]: IEmployee};

export const EMPLOYEE1 = "employee1";
export const employee1 = {
    name: "Jack",
    age: 30,
    address: {
        street: "Rover Street"
    }
};

export const employeeStore: EmployeeStore = {
    employee1
};
