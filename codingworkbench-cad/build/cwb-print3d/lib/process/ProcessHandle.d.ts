import IProcessOutput from "./IProcessOutput";
export default class ProcessHandle {
    execute(command: string): Promise<IProcessOutput>;
}
