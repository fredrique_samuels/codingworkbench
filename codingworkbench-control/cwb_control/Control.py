from cwb_control.DataPointChangeHandler import DataPointChangeHandler
from cwb_control.Plant import Plant


class IgnoreSetPointChangeErrorDelta (DataPointChangeHandler):
    id = "IgnoreSetPointChangeErrorDelta"

    def __init__(self, target):
        DataPointChangeHandler.__init__(self)
        self.target = target

    def handle(self, key, value, data_store):
        data_store.set(self.target, True)


class Control(Plant):

    def __init__(self, name, ds, current_value_prop, target_value_prop):
        Plant.__init__(self, "{}.plant".format(name), ds)

        self.target_value_prop = target_value_prop
        self.current_value_prop = current_value_prop
        self.set_point = self.create_data_point("set_point", 0)
        self.error = self.create_data_point("error", 0)
        self.error_dt = self.create_data_point("error.dt", 0)
        self.error_prev = self.create_data_point("error.prev", 0)
        self.error_sum = self.create_data_point("error.sum", 0)
        self.kp = self.create_data_point("kp", 0)
        self.ki = self.create_data_point("ki", 0)
        self.kd = self.create_data_point("kd", 0)
        self.p = self.create_data_point("p", 0)
        self.i = self.create_data_point("i", 0)
        self.d = self.create_data_point("d", 0)
        self.gain = self.create_data_point("gain", 0)
        self.ignore_error_dt = self.create_data_point("ignore_error_dt", False)

    def enable_set_point_change_error_delta_ignore(self):
        handler_id = "{}.{}".format(self.set_point.id, IgnoreSetPointChangeErrorDelta.id)
        self.data_store.add_change_handler(handler_id, IgnoreSetPointChangeErrorDelta(self.ignore_error_dt.id))
        self.set_point.enable_handler(handler_id)

    def disable_set_point_change_error_delta_ignore(self):
        handler_id = "{}.{}".format(self.set_point.id, IgnoreSetPointChangeErrorDelta.id)
        self.set_point.disable_handler(handler_id)

    def update(self, dt):
        error = self.set_point.get() - self.data_store.get(self.current_value_prop)
        error_dt = 0 if self.ignore_error_dt.get() else error - self.error_prev.get()
        error_sum = error + self.error_sum.get()

        p = self.kp.get() * error
        i = self.ki.get() + ( self.ki.get() * error * dt)
        d = self.kd.get() * error_dt * dt

        gain = p + i + d

        self.ignore_error_dt.set(False)
        self.error.set(error)
        self.error_dt.set(error_dt)
        self.error_prev.set(error)
        self.error_sum.set(error_sum)

        self.p.set(p)
        self.i.set(i)
        self.d.set(d)
        self.gain.set(gain)

        self.data_store.set(self.target_value_prop, gain + self.data_store.get(self.target_value_prop))
        return self

    def pid(self):
        return self.p.get(), self.i.get(), self.d.get()
