import CwbLog from "@codingworkbench/log";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
import ExpressApplication, {ExpressContextFactory} from "../server/ExpressApplication";
import {ExpressApplicationOptions, IExpressApplication} from "../server/ExpressApplicationUtils";
import {HtmlBuilder} from "../server/HtmlBuilder";
import {View} from "../server/common";
import {RequestHandlerBuilder} from "../server/RequestHandlerBuilder";


/**
 * Enable logging
 */
CwbLog.setup();
CwbLog.enable_console();


/**
 * Application context setup
 */
interface MyApplicationContext {

}

class MyApplicationContextFactory implements ExpressContextFactory<MyApplicationContext> {

    async createContext(options: ExpressApplicationOptions,
                  connectionProperties: IPropertiesReader | null)
        : Promise<MyApplicationContext> {
        return {};
    }

    async configureApplication(context: MyApplicationContext, app: IExpressApplication<MyApplicationContext>): Promise<void> {

    }
}

/**
 * Create an express application
 */

const application = new ExpressApplication<MyApplicationContext>();
const home_handler = new RequestHandlerBuilder("/", context => {
        const view: View = {
            html: new HtmlBuilder().addBody("<h1>Hello</h1>").build(),
        };
        context.response.view(view);
    }).build();

application.addHandlers([ home_handler ]);

/**
 * Run the application
 */
application.start({ port: 8080, }, new MyApplicationContextFactory())
    .catch(CwbLog.error);
