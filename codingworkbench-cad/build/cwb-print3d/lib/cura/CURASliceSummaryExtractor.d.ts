import ISliceSummary from "../ISliceSummary";
export default class CURASliceSummaryExtractor {
    private readonly gcode;
    constructor(gcode: string);
    extractFromOutput(output: string): ISliceSummary;
}
