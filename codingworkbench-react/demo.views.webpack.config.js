const path = require('path');
const fs = require('fs');
const webpackUtils = require("@codingworkbench/build");

// calculated paths
const OUTPUT_DIR = path.resolve(__dirname, 'web-app', 'js');

/* The folder to search for all the .tsx used as entry points */
const VIEWS_ROOT = "./demo/views/";
try {
    fs.mkdirSync(OUTPUT_DIR, {recursive: true});
} catch (e) {
    console.error(e)
}
const config = webpackUtils.create_config(OUTPUT_DIR);

/* entries*/
webpackUtils.push_entries(config, VIEWS_ROOT, ".tsx");

/* loaders */
webpackUtils.ts_loader(config);
webpackUtils.tsconfig_paths(config, "./tsconfig.json");

webpackUtils.raw_loader(config, /\.txt$/);
webpackUtils.raw_loader(config, /\.glsl/);

/* library configs */
webpackUtils.exclude_react(config);
webpackUtils.copy_react_js(config, OUTPUT_DIR);
webpackUtils.ignore_pg_native(config);

/* rebuild on source changes */
// webpackUtils.watch_source(config);

/* minimize source for prod */
//webpackUtils.minimize(CONFIG);
module.exports = config;
