import IPage from "./IPage";

export interface IPageAware<S={}> {
    page: IPage<S>
}
