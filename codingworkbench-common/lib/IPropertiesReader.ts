
/**
 * Interface to read property file values.
 */
export interface IPropertiesReader {

    /**
     * Read a property as a string.
     *
     * @param property The property name.
     * @param fallback The value to return if the property does not exists.
     */
    getString(property: string, fallback?: string) : string;

    /**
     * Read a property as a number.
     *
     * @param property The property name.
     * @param fallback The value to return if the property does not exists.
     */
    getNumber(property: string, fallback?: number) : number;
}

export default IPropertiesReader;
