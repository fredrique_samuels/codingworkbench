import CwbTestCase from "@codingworkbench/testcase";
import PropertiesFileReader from "@codingworkbench/common/PropertiesFileReader";

import {MongoConnection} from "./MongoConnection";

import {CrudDataAccessor} from "./CrudDataAccessor";
import {MysqlConnection} from "./MysqlConnection";

namespace ConnectionTestCase {

    export interface IMongoConnectionTestProps {
        mongoConnectionPropertiesFile?: string;
        mongoConnectionScope?: string;
    }

    export interface IMongoConnectionTestCase extends IMongoConnectionTestProps, CwbTestCase.IDestroyable {
        mongoConnection: MongoConnection;
        mongoDataAccessor: CrudDataAccessor;
    }

    export interface IMysqlConnectionTestProps {
        mysqlConnectionPropertiesFile?: string;
        mysqlConnectionScope?: string;
    }

    export interface IMysqlConnectionTestCase extends IMysqlConnectionTestProps, CwbTestCase.IDestroyable {
        mysqlConnection: MysqlConnection;
        mysqlDataAccessor: CrudDataAccessor;
    }


    async function createMysqlConnection(connProps: ConnectionTestCase.IMysqlConnectionTestProps) {
        const connectionString = MysqlConnection.readConnectionString(connProps.mysqlConnectionPropertiesFile || "./conf/connection.properties");
        const mysqlConnection: MysqlConnection = new MysqlConnection(connectionString);
        await mysqlConnection.connect();
        return mysqlConnection;
    }

    async function createMongoConnection(connProps: ConnectionTestCase.IMongoConnectionTestProps) {
        const propertiesReader = new PropertiesFileReader(connProps.mongoConnectionPropertiesFile || "./conf/connection.properties");
        const mongoConnection = new MongoConnection(propertiesReader, connProps.mongoConnectionScope || "test_scope");
        await mongoConnection.connect();
        return mongoConnection;
    }
    export async function createMysqlConnectionTestCase(pick: Partial<ConnectionTestCase.IMysqlConnectionTestCase>): Promise<ConnectionTestCase.IMysqlConnectionTestCase> {
        const mysqlConnection = pick.mysqlConnection || await createMysqlConnection(pick);
        const mysqlDataAccessor = pick.mysqlDataAccessor || new CrudDataAccessor(mysqlConnection);

        const destroy: CwbTestCase.DestroyCallback = async (unit: ConnectionTestCase.IMysqlConnectionTestCase) => {
            try {
                await unit.mysqlConnection.close();
            } catch (e) {
                console.error(e);
            }
        };

        return {
            mysqlConnection,
            mysqlDataAccessor,
            destroyCallbacks: CwbTestCase.createDestroyable(pick.mysqlConnection ? [] : [destroy], pick),
        };
    }

    export async function createMongoConnectionTestCase(pick: Partial<ConnectionTestCase.IMongoConnectionTestCase>): Promise<ConnectionTestCase.IMongoConnectionTestCase> {
        const mongoConnection =  pick.mongoConnection || await createMongoConnection(pick);
        const mongoDataAccessor = pick.mongoDataAccessor || new CrudDataAccessor(mongoConnection);

        const destroy: CwbTestCase.DestroyCallback = async (unit: ConnectionTestCase.IMongoConnectionTestCase) => {
            try {
                await unit.mongoConnection.close();
            } catch (e) {
                console.error(e);
            }
        };

        return {
            mongoConnection,
            mongoDataAccessor,
            destroyCallbacks: CwbTestCase.createDestroyable(pick.mongoConnection ? [] : [destroy], pick),
        };
    }

}

export default ConnectionTestCase;
