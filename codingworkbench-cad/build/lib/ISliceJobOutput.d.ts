import ProcessStdio from "./process/ProcessStdio";
import ISliceResults from "./ISliceResults";
export default interface ISliceJobOutput {
    readonly stdio: ProcessStdio;
    readonly results: ISliceResults;
}
