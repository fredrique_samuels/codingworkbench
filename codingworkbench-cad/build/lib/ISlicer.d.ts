import ISliceJobOutput from "./ISliceJobOutput";
export default interface ISlicer {
    slice(stlFile: string, gCodeFile: string, options: any): Promise<ISliceJobOutput>;
}
