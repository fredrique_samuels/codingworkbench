import IPage from "./IPage";
import {RequestAction} from "@codingworkbench/server/RequestAction";

interface NotificationSetting {
    enabled: boolean;
    error?: boolean;
    message?: string;
}

/**
 * Builder for constructing a web request that has context of a View.
 */
export class PageAction extends RequestAction {
    private readonly _notifyFailureSettings: NotificationSetting;
    private readonly _notifySuccessSettings: NotificationSetting;
    private readonly viewContext: IPage;

    /**
     *
     * if a view context is provided
     *
     * @param url The url to execute.
     * @param page Optional view context.
     *
     * @constructor
     */
    constructor(url: string, page: IPage) {
        super(url);
        this.viewContext = page;
        this._notifyFailureSettings = {
            enabled: false,
            error: false
        };
        this._notifySuccessSettings = {
            enabled: false
        };
    }

    /**
     * Enable notifications on failures.
     *
     * @return this
     */
    enabledNotifyOnFail(): this {
        this._notifyFailureSettings.enabled = true;
        return this;
    }

    /**
     * Override the standard failure message.
     *
     * @return this
     */
    setFailMessage(message?: string): this {
        this._notifyFailureSettings.message = message;
        return this;
    }

    /**
     * Show the failure message as a error instead of a warning.
     *
     * @return this.
     */
    showFailAsError(): this {
        this._notifyFailureSettings.error = true;
        return this;
    }

    /**
     * Enable notifications on successes.
     *
     * @return this
     */
    enabledNotifyOnSuccess(): this {
        this._notifySuccessSettings.enabled = true;
        return this;
    }

    /**
     * Override the standard success message.
     *
     * @return this
     */
    setSuccessMessage(message?: string): this {
        this._notifySuccessSettings.message = message;
        return this;
    }

    protected dispatchFailure(standardMessage: string, e: Error) {
        const {enabled, message, error} = this._notifyFailureSettings;
        if(!enabled) {
            return;
        }

        let msg = message || standardMessage;
        if (error) {
            this.viewContext.pushErrorNotification(msg);
        } else {
            this.viewContext.pushWarningNotification(msg);
        }
        this.viewContext.logError(e);
    }

    protected dispatchSuccess(standardMessage: string) {
        const {enabled, message} = this._notifySuccessSettings;
        if(!enabled) {
            return;
        }

        let msg = message || standardMessage;
        this.viewContext.pushSuccessNotification(msg);
    }
}
