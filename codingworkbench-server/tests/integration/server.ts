import ExpressApplication, {ExpressContextFactory} from "../../server/ExpressApplication";
import {ExpressApplicationOptions} from "../../server/ExpressApplicationUtils";
import IPropertiesReader from "@codingworkbench/common/IPropertiesReader";
import {RequestHandlerBuilder} from "../../server/RequestHandlerBuilder";
import {View} from "../../server/common";
import {HtmlBuilder} from "../../server/HtmlBuilder";


interface MyApplicationContext {}

class MyApplicationContextFactory implements ExpressContextFactory<MyApplicationContext> {

    async createContext(options: ExpressApplicationOptions, connectionProperties: IPropertiesReader | null) : Promise<MyApplicationContext> {
        return {};
    }
}

const application = new ExpressApplication<MyApplicationContext>();
const home_handler = new RequestHandlerBuilder("/", context => {
    const view: View = {
        html: new HtmlBuilder().addBody("<h1>Hello</h1>").build(),
    };
    context.response.view(view);
}).build();

application.addHandlers([ home_handler ]);
application.start({ port: 8080, }, new MyApplicationContextFactory()).catch(console.error);
export default application.getApp();
