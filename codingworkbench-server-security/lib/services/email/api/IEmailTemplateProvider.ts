import {IEmailTemplate} from "./IEmailTemplate";

export interface IEmailTemplateProvider {
    get(key: string): IEmailTemplate | null;
}
