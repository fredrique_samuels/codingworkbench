import * as React from "react";
import Typography from "@material-ui/core/Typography";

import AbstractDialog, {AbstractDialogProps, AbstractDialogState} from "./AbstractDialog";
import {SubmitButton} from "../buttons/SubmitButton";
import {PageDialogOptions} from "./PageDialog";


export class InformationDialog
    extends React.Component<InformationDialogProps, InformationDialogState>
{

    constructor(props: InformationDialogProps) {
        super(props);
    }

    render() {
        const p: InformationDialogProps = {
            ... this.props,
            ...{
                content: this.renderContent(),
                actions: this.renderActions(),
            }
        };
        return <AbstractDialog {...p} />;
    }

    protected renderContent(): React.ReactNode {
        const {props} = this;
        return <Typography variant={"body2"} >
            {props.content}
        </Typography>
            ;
    }

    protected renderActions(): JSX.Element[] {
        const {onClick, buttonLabel} = this.props;
        if (onClick) {
            return [
                <SubmitButton key="submit-button" onClick={onClick} label={ buttonLabel || "Ok"}/>
            ];
        }
        return []
    }

    public static createProps<StateType>( text: string, o: PageDialogOptions<StateType>)
        : InformationDialogProps {
        return {
            ... AbstractDialog.createProps(o),
            text: text,
        };
    }
}

export interface InformationDialogProps extends AbstractDialogProps {
    onClick?: () => {}
    buttonLabel?: string;
}

export interface InformationDialogState extends AbstractDialogState {}

export default InformationDialog;

