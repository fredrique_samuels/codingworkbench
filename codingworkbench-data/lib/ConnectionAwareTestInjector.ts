import PropertiesFileReader from "@codingworkbench/common/PropertiesFileReader";
import ConnectionAwareInjector from "./ConnectionAwareInjector";

export default class ConnectionAwareTestInjector extends ConnectionAwareInjector {
    constructor() {
        super(new PropertiesFileReader("./tests/connection.properties"));
    }
}


